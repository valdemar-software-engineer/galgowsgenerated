set javax.xml.accessExternalSchema=all
SET DIR=..\src\main\resources\wsdl
rem SET WSDL2JAVA=c:\Java\apache-cxf\last\bin\wsdl2java -verbose -client -d gen -encoding UTF-8
rem SET WSDL2JAVA=c:\Java\apache-cxf\versions\apache-cxf-3.1.6\bin\wsdl2java -verbose -client -d gen -encoding UTF-8
SET WSDL2JAVA=c:\Java\CXF\apache-cxf-3.1.8\bin\wsdl2java -verbose -client -d gen -encoding UTF-8
rem %WSDL2JAVA% %DIR%\ServiceFundoInvestimento.wsdl
rem %WSDL2JAVA% %DIR%\ServiceRecibos.wsdl
rem %WSDL2JAVA% %DIR%\ServiceANBID.wsdl
rem %WSDL2JAVA% %DIR%\ServicePosicaoAtivos.wsdl
rem %WSDL2JAVA% %DIR%\ServicePLCota.wsdl
%WSDL2JAVA% %DIR%\ServiceExtratoCotas.wsdl
pause
