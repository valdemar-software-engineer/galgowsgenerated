package com.stianbima.schematesteconectividade;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Dados de Saída do Teste de Conectividade.
 * <p>
 * <p>
 * <p>Java class for DadosSaidaComplexType complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType name="DadosSaidaComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Mensagem" type="{http://www.stianbima.com/SchemaTesteConectividade}TypeMsg"/>
 *         &lt;element name="NomeArquivo" type="{http://www.stianbid.com.br/Common}Max35Text"/>
 *         &lt;element name="BinaryArq" type="{http://www.stianbima.com/SchemaTesteConectividade}TypeArquivoBinario"/>
 *         &lt;element name="TamanhoArq" type="{http://www.stianbima.com/SchemaTesteConectividade}TypeTamArq"/>
 *         &lt;element name="DtHrMensagem" type="{http://www.stianbid.com.br/Common}ISODateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DadosSaidaComplexType",
         propOrder = {"mensagem", "nomeArquivo", "binaryArq", "tamanhoArq", "dtHrMensagem"})
public class DadosSaidaComplexType {

    @XmlElement(name = "Mensagem", required = true) protected String mensagem;
    @XmlElement(name = "NomeArquivo", required = true) protected String nomeArquivo;
    @XmlElement(name = "BinaryArq", required = true) @XmlMimeType("application/octet-stream") protected DataHandler
        binaryArq;
    @XmlElement(name = "TamanhoArq") protected long tamanhoArq;
    @XmlElement(name = "DtHrMensagem", required = true) protected XMLGregorianCalendar dtHrMensagem;

    /**
     * Gets the value of the binaryArq property.
     *
     * @return possible object is
     * {@link DataHandler }
     */
    public DataHandler getBinaryArq() {
        return binaryArq;
    }

    /**
     * Sets the value of the binaryArq property.
     *
     * @param value allowed object is
     *              {@link DataHandler }
     */
    public void setBinaryArq(DataHandler value) {
        this.binaryArq = value;
    }

    /**
     * Gets the value of the dtHrMensagem property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtHrMensagem() {
        return dtHrMensagem;
    }

    /**
     * Sets the value of the dtHrMensagem property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtHrMensagem(XMLGregorianCalendar value) {
        this.dtHrMensagem = value;
    }

    /**
     * Gets the value of the mensagem property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Sets the value of the mensagem property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMensagem(String value) {
        this.mensagem = value;
    }

    /**
     * Gets the value of the nomeArquivo property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNomeArquivo() {
        return nomeArquivo;
    }

    /**
     * Sets the value of the nomeArquivo property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNomeArquivo(String value) {
        this.nomeArquivo = value;
    }

    /**
     * Gets the value of the tamanhoArq property.
     */
    public long getTamanhoArq() {
        return tamanhoArq;
    }

    /**
     * Sets the value of the tamanhoArq property.
     */
    public void setTamanhoArq(long value) {
        this.tamanhoArq = value;
    }

}
