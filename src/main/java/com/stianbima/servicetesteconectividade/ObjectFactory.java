package com.stianbima.servicetesteconectividade;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.stianbima.servicetesteconectividade package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.stianbima.servicetesteconectividade
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TestarConectividade }
     */
    public TestarConectividade createTestarConectividade() {
        return new TestarConectividade();
    }

    /**
     * Create an instance of {@link TestarConectividadeFault }
     */
    public TestarConectividadeFault createTestarConectividadeFault() {
        return new TestarConectividadeFault();
    }

    /**
     * Create an instance of {@link TestarConectividadeResponse }
     */
    public TestarConectividadeResponse createTestarConectividadeResponse() {
        return new TestarConectividadeResponse();
    }

}
