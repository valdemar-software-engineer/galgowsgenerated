package com.stianbima.servicetesteconectividade;

import br.com.stianbid.common.MessageExceptionComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TestarConectividade_fault" type="{http://www.stianbid.com.br/Common}MessageExceptionComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"testarConectividadeFault"})
@XmlRootElement(name = "TestarConectividade_fault")
public class TestarConectividadeFault {

    @XmlElement(name = "TestarConectividade_fault", required = true) protected MessageExceptionComplexType
        testarConectividadeFault;

    /**
     * Gets the value of the testarConectividadeFault property.
     *
     * @return possible object is
     * {@link MessageExceptionComplexType }
     */
    public MessageExceptionComplexType getTestarConectividadeFault() {
        return testarConectividadeFault;
    }

    /**
     * Sets the value of the testarConectividadeFault property.
     *
     * @param value allowed object is
     *              {@link MessageExceptionComplexType }
     */
    public void setTestarConectividadeFault(MessageExceptionComplexType value) {
        this.testarConectividadeFault = value;
    }

}
