package com.stianbima.servicetesteconectividade;

import com.stianbima.schematesteconectividade.DadosSaidaComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Resposta" type="{http://www.stianbima.com/SchemaTesteConectividade}DadosSaidaComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"resposta"})
@XmlRootElement(name = "TestarConectividadeResponse")
public class TestarConectividadeResponse {

    @XmlElement(name = "Resposta", required = true) protected DadosSaidaComplexType resposta;

    /**
     * Gets the value of the resposta property.
     *
     * @return possible object is
     * {@link DadosSaidaComplexType }
     */
    public DadosSaidaComplexType getResposta() {
        return resposta;
    }

    /**
     * Sets the value of the resposta property.
     *
     * @param value allowed object is
     *              {@link DadosSaidaComplexType }
     */
    public void setResposta(DadosSaidaComplexType value) {
        this.resposta = value;
    }

}
