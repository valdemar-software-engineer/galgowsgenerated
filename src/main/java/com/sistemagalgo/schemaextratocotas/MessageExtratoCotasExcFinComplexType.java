package com.sistemagalgo.schemaextratocotas;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de MessageExtratoCotasExcFinComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageExtratoCotasExcFinComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType">
 *       &lt;sequence>
 *         &lt;element name="Extrato" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}ExtratoCotasExcFinComplexType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageExtratoCotasExcFinComplexType", propOrder = {"extrato"})
public class MessageExtratoCotasExcFinComplexType extends MessageRequestComplexType {

    @XmlElement(name = "Extrato", required = true) protected List<ExtratoCotasExcFinComplexType> extrato;

    /**
     * Gets the value of the extrato property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extrato property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtrato().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExtratoCotasExcFinComplexType }
     */
    public List<ExtratoCotasExcFinComplexType> getExtrato() {
        if (extrato == null) {
            extrato = new ArrayList<ExtratoCotasExcFinComplexType>();
        }
        return this.extrato;
    }

}
