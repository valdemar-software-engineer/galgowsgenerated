package com.sistemagalgo.schemaextratocotas;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;


/**
 * <p>Classe Java de MessageExtratoCotasConsumirComplexType complex type.
 *
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 *
 * <pre>
 * &lt;complexType name="MessageExtratoCotasConsumirComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CdSTICotista" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}STIIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="CdSTI" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}STIIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="TpExtrato" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}TpExtratoType" minOccurs="0"/&gt;
 *         &lt;element name="StatusInfo" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}StatusInfoType" minOccurs="0"/&gt;
 *         &lt;element name="Atuacao" type="{http://www.stianbid.com.br/Common}AtuacaoType" minOccurs="0"/&gt;
 *         &lt;element name="DtInic" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="DtFin" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="DtInicEnvio" type="{http://www.stianbid.com.br/Common}ISODateTime" minOccurs="0"/&gt;
 *         &lt;element name="DtFimEnvio" type="{http://www.stianbid.com.br/Common}ISODateTime" minOccurs="0"/&gt;
 *         &lt;element name="Moeda" type="{http://www.stianbid.com.br/Common}CurrencyCode" minOccurs="0"/&gt;
 *         &lt;element name="Marcadores" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}MarcadoresComplexType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageExtratoCotasConsumirComplexType", propOrder = {
    "cdSTICotista",
    "cdSTI",
    "tpExtrato",
    "statusInfo",
    "atuacao",
    "dtInic",
    "dtFin",
    "dtInicEnvio",
    "dtFimEnvio",
    "moeda",
    "marcadores"
})
public class MessageExtratoCotasConsumirComplexType
    extends MessageRequestComplexType {

    @XmlElement(name = "CdSTICotista")
    protected Integer cdSTICotista;
    @XmlElement(name = "CdSTI")
    protected Integer cdSTI;
    @XmlElement(name = "TpExtrato")
    @XmlSchemaType(name = "string")
    protected TpExtratoType tpExtrato;
    @XmlElement(name = "StatusInfo")
    protected Integer statusInfo;
    @XmlElement(name = "Atuacao")
    protected BigInteger atuacao;
    @XmlElement(name = "DtInic")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dtInic;
    @XmlElement(name = "DtFin")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dtFin;
    @XmlElement(name = "DtInicEnvio")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dtInicEnvio;
    @XmlElement(name = "DtFimEnvio")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dtFimEnvio;
    @XmlElement(name = "Moeda")
    protected String moeda;
    @XmlElement(name = "Marcadores")
    protected MarcadoresComplexType marcadores;

    /**
     * Obtém o valor da propriedade atuacao.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getAtuacao() {
        return atuacao;
    }

    /**
     * Define o valor da propriedade atuacao.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setAtuacao(BigInteger value) {
        this.atuacao = value;
    }

    /**
     * Obtém o valor da propriedade cdSTI.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getCdSTI() {
        return cdSTI;
    }

    /**
     * Define o valor da propriedade cdSTI.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setCdSTI(Integer value) {
        this.cdSTI = value;
    }

    /**
     * Obtém o valor da propriedade cdSTICotista.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getCdSTICotista() {
        return cdSTICotista;
    }

    /**
     * Define o valor da propriedade cdSTICotista.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setCdSTICotista(Integer value) {
        this.cdSTICotista = value;
    }

    /**
     * Obtém o valor da propriedade dtFimEnvio.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDtFimEnvio() {
        return dtFimEnvio;
    }

    /**
     * Define o valor da propriedade dtFimEnvio.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDtFimEnvio(XMLGregorianCalendar value) {
        this.dtFimEnvio = value;
    }

    /**
     * Obtém o valor da propriedade dtFin.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDtFin() {
        return dtFin;
    }

    /**
     * Define o valor da propriedade dtFin.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDtFin(XMLGregorianCalendar value) {
        this.dtFin = value;
    }

    /**
     * Obtém o valor da propriedade dtInic.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDtInic() {
        return dtInic;
    }

    /**
     * Define o valor da propriedade dtInic.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDtInic(XMLGregorianCalendar value) {
        this.dtInic = value;
    }

    /**
     * Obtém o valor da propriedade dtInicEnvio.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDtInicEnvio() {
        return dtInicEnvio;
    }

    /**
     * Define o valor da propriedade dtInicEnvio.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDtInicEnvio(XMLGregorianCalendar value) {
        this.dtInicEnvio = value;
    }

    /**
     * Obtém o valor da propriedade marcadores.
     *
     * @return
     *     possible object is
     *     {@link MarcadoresComplexType }
     *
     */
    public MarcadoresComplexType getMarcadores() {
        return marcadores;
    }

    /**
     * Define o valor da propriedade marcadores.
     *
     * @param value
     *     allowed object is
     *     {@link MarcadoresComplexType }
     *
     */
    public void setMarcadores(MarcadoresComplexType value) {
        this.marcadores = value;
    }

    /**
     * Obtém o valor da propriedade moeda.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMoeda() {
        return moeda;
    }

    /**
     * Define o valor da propriedade moeda.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMoeda(String value) {
        this.moeda = value;
    }

    /**
     * Obtém o valor da propriedade statusInfo.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getStatusInfo() {
        return statusInfo;
    }

    /**
     * Define o valor da propriedade statusInfo.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setStatusInfo(Integer value) {
        this.statusInfo = value;
    }

    /**
     * Obtém o valor da propriedade tpExtrato.
     *
     * @return
     *     possible object is
     *     {@link TpExtratoType }
     *
     */
    public TpExtratoType getTpExtrato() {
        return tpExtrato;
    }

    /**
     * Define o valor da propriedade tpExtrato.
     *
     * @param value
     *     allowed object is
     *     {@link TpExtratoType }
     *
     */
    public void setTpExtrato(TpExtratoType value) {
        this.tpExtrato = value;
    }

}
