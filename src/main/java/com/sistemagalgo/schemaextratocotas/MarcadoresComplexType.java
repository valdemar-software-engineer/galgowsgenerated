package com.sistemagalgo.schemaextratocotas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Marcadores
 * <p>
 * <p>Classe Java de MarcadoresComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MarcadoresComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MarcadorExtrato" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}MarcadorStringType"/>
 *         &lt;element name="MarcadorListaInfo" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}MarcadorLongType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarcadoresComplexType", propOrder = {"marcadorExtrato", "marcadorListaInfo"})
public class MarcadoresComplexType {

    @XmlElement(name = "MarcadorExtrato", required = true) protected String marcadorExtrato;
    @XmlElement(name = "MarcadorListaInfo") protected long marcadorListaInfo;

    /**
     * Obtém o valor da propriedade marcadorExtrato.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMarcadorExtrato() {
        return marcadorExtrato;
    }

    /**
     * Define o valor da propriedade marcadorExtrato.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMarcadorExtrato(String value) {
        this.marcadorExtrato = value;
    }

    /**
     * Obtém o valor da propriedade marcadorListaInfo.
     */
    public long getMarcadorListaInfo() {
        return marcadorListaInfo;
    }

    /**
     * Define o valor da propriedade marcadorListaInfo.
     */
    public void setMarcadorListaInfo(long value) {
        this.marcadorListaInfo = value;
    }

}
