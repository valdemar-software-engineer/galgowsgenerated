package com.sistemagalgo.schemaextratocotas;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sistemagalgo.schemaextratocotas package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Extratos_QNAME = new QName("http://www.sistemagalgo.com/SchemaExtratoCotas", "Extratos");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sistemagalgo.schemaextratocotas
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExtratoCotasExcFinComplexType }
     *
     */
    public ExtratoCotasExcFinComplexType createExtratoCotasExcFinComplexType() {
        return new ExtratoCotasExcFinComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageExtratoCotasExcFinComplexType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://www.sistemagalgo.com/SchemaExtratoCotas", name = "Extratos")
    public JAXBElement<MessageExtratoCotasExcFinComplexType> createExtratos(MessageExtratoCotasExcFinComplexType value) {
        return new JAXBElement<MessageExtratoCotasExcFinComplexType>(_Extratos_QNAME, MessageExtratoCotasExcFinComplexType.class,
            null, value);
    }

    /**
     * Create an instance of {@link MarcadoresComplexType }
     *
     */
    public MarcadoresComplexType createMarcadoresComplexType() {
        return new MarcadoresComplexType();
    }

    /**
     * Create an instance of {@link MessageExtratoCotasCancelarComplexType }
     *
     */
    public MessageExtratoCotasCancelarComplexType createMessageExtratoCotasCancelarComplexType() {
        return new MessageExtratoCotasCancelarComplexType();
    }

    /**
     * Create an instance of {@link MessageExtratoCotasComplexType }
     *
     */
    public MessageExtratoCotasComplexType createMessageExtratoCotasComplexType() {
        return new MessageExtratoCotasComplexType();
    }

    /**
     * Create an instance of {@link MessageExtratoCotasConsumirComplexType }
     *
     */
    public MessageExtratoCotasConsumirComplexType createMessageExtratoCotasConsumirComplexType() {
        return new MessageExtratoCotasConsumirComplexType();
    }

    /**
     * Create an instance of {@link MessageExtratoCotasConsumirPendComplexType }
     *
     */
    public MessageExtratoCotasConsumirPendComplexType createMessageExtratoCotasConsumirPendComplexType() {
        return new MessageExtratoCotasConsumirPendComplexType();
    }

    /**
     * Create an instance of {@link MessageExtratoCotasExcFinComplexType }
     *
     */
    public MessageExtratoCotasExcFinComplexType createMessageExtratoCotasExcFinComplexType() {
        return new MessageExtratoCotasExcFinComplexType();
    }

    /**
     * Create an instance of {@link MessageExtratoCotasRespPendComplexType }
     *
     */
    public MessageExtratoCotasRespPendComplexType createMessageExtratoCotasRespPendComplexType() {
        return new MessageExtratoCotasRespPendComplexType();
    }

}
