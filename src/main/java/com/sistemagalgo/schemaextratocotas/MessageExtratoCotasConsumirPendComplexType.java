package com.sistemagalgo.schemaextratocotas;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Consumir Extrato de Cotas pendentes
 * <p>
 * <p>
 * <p>Classe Java de MessageExtratoCotasConsumirPendComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageExtratoCotasConsumirPendComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType">
 *       &lt;sequence>
 *         &lt;element name="Marcadores" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}MarcadoresComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageExtratoCotasConsumirPendComplexType", propOrder = {"marcadores"})
public class MessageExtratoCotasConsumirPendComplexType extends MessageRequestComplexType {

    @XmlElement(name = "Marcadores") protected MarcadoresComplexType marcadores;

    /**
     * Obtém o valor da propriedade marcadores.
     *
     * @return possible object is
     * {@link MarcadoresComplexType }
     */
    public MarcadoresComplexType getMarcadores() {
        return marcadores;
    }

    /**
     * Define o valor da propriedade marcadores.
     *
     * @param value allowed object is
     *              {@link MarcadoresComplexType }
     */
    public void setMarcadores(MarcadoresComplexType value) {
        this.marcadores = value;
    }

}
