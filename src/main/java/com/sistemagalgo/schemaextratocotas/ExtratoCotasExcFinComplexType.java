package com.sistemagalgo.schemaextratocotas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de ExtratoCotasExcFinComplexType complex type.
 *
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 *
 * <pre>
 * &lt;complexType name="ExtratoCotasExcFinComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="CdSTICotista" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}STIIdentifier"/&gt;
 *           &lt;element name="ISINCotista" type="{http://www.stianbid.com.br/Common}ISINIdentifier"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice&gt;
 *           &lt;element name="CdSTI" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}STIIdentifier"/&gt;
 *           &lt;element name="ISIN" type="{http://www.stianbid.com.br/Common}ISINIdentifier"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="DtBase" type="{http://www.stianbid.com.br/Common}ISODate"/&gt;
 *         &lt;element name="Moeda" type="{http://www.stianbid.com.br/Common}CurrencyCode"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtratoCotasExcFinComplexType", propOrder = {
    "cdSTICotista",
    "isinCotista",
    "cdSTI",
    "isin",
    "dtBase",
    "moeda"
})
public class ExtratoCotasExcFinComplexType {

    @XmlElement(name = "CdSTICotista")
    protected Integer cdSTICotista;
    @XmlElement(name = "ISINCotista")
    protected String isinCotista;
    @XmlElement(name = "CdSTI")
    protected Integer cdSTI;
    @XmlElement(name = "ISIN")
    protected String isin;
    @XmlElement(name = "DtBase", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dtBase;
    @XmlElement(name = "Moeda", required = true)
    protected String moeda;

    /**
     * Obtém o valor da propriedade cdSTI.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getCdSTI() {
        return cdSTI;
    }

    /**
     * Define o valor da propriedade cdSTI.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setCdSTI(Integer value) {
        this.cdSTI = value;
    }

    /**
     * Obtém o valor da propriedade cdSTICotista.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getCdSTICotista() {
        return cdSTICotista;
    }

    /**
     * Define o valor da propriedade cdSTICotista.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setCdSTICotista(Integer value) {
        this.cdSTICotista = value;
    }

    /**
     * Obtém o valor da propriedade dtBase.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDtBase() {
        return dtBase;
    }

    /**
     * Define o valor da propriedade dtBase.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDtBase(XMLGregorianCalendar value) {
        this.dtBase = value;
    }

    /**
     * Obtém o valor da propriedade isin.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getISIN() {
        return isin;
    }

    /**
     * Define o valor da propriedade isin.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setISIN(String value) {
        this.isin = value;
    }

    /**
     * Obtém o valor da propriedade isinCotista.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getISINCotista() {
        return isinCotista;
    }

    /**
     * Define o valor da propriedade isinCotista.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setISINCotista(String value) {
        this.isinCotista = value;
    }

    /**
     * Obtém o valor da propriedade moeda.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMoeda() {
        return moeda;
    }

    /**
     * Define o valor da propriedade moeda.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMoeda(String value) {
        this.moeda = value;
    }

}
