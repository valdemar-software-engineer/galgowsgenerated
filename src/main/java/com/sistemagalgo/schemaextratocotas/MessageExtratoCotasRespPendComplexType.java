package com.sistemagalgo.schemaextratocotas;

import iso.std.iso._20022.tech.xsd.camt_043_001.FundDetailedConfirmedCashForecastReportV03;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Consumir Extrato de Cotas Pendentes
 * <p>
 * <p>
 * <p>Classe Java de MessageExtratoCotasRespPendComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageExtratoCotasRespPendComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Marcador" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}MarcadoresComplexType" minOccurs="0"/>
 *         &lt;element name="FndDtldConfdCshFcstRptV03" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}FundDetailedConfirmedCashForecastReportV03"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageExtratoCotasRespPendComplexType", propOrder = {"marcador", "fndDtldConfdCshFcstRptV03"})
public class MessageExtratoCotasRespPendComplexType {

    @XmlElement(name = "Marcador") protected MarcadoresComplexType marcador;
    @XmlElement(name = "FndDtldConfdCshFcstRptV03",
                required = true) protected FundDetailedConfirmedCashForecastReportV03 fndDtldConfdCshFcstRptV03;

    /**
     * Obtém o valor da propriedade fndDtldConfdCshFcstRptV03.
     *
     * @return possible object is
     * {@link FundDetailedConfirmedCashForecastReportV03 }
     */
    public FundDetailedConfirmedCashForecastReportV03 getFndDtldConfdCshFcstRptV03() {
        return fndDtldConfdCshFcstRptV03;
    }

    /**
     * Define o valor da propriedade fndDtldConfdCshFcstRptV03.
     *
     * @param value allowed object is
     *              {@link FundDetailedConfirmedCashForecastReportV03 }
     */
    public void setFndDtldConfdCshFcstRptV03(FundDetailedConfirmedCashForecastReportV03 value) {
        this.fndDtldConfdCshFcstRptV03 = value;
    }

    /**
     * Obtém o valor da propriedade marcador.
     *
     * @return possible object is
     * {@link MarcadoresComplexType }
     */
    public MarcadoresComplexType getMarcador() {
        return marcador;
    }

    /**
     * Define o valor da propriedade marcador.
     *
     * @param value allowed object is
     *              {@link MarcadoresComplexType }
     */
    public void setMarcador(MarcadoresComplexType value) {
        this.marcador = value;
    }

}
