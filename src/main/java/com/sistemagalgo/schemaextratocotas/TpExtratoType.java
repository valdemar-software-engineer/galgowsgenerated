package com.sistemagalgo.schemaextratocotas;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de TpExtratoType.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="TpExtratoType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SETT"/>
 *     &lt;enumeration value="SCRI"/>
 *     &lt;enumeration value="MOSE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "TpExtratoType")
@XmlEnum
public enum TpExtratoType {

    SETT,
    SCRI,
    MOSE;

    public static TpExtratoType fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
