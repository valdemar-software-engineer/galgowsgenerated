package com.sistemagalgo.schemaextratocotas;

import iso.std.iso._20022.tech.xsd.camt_043_001.FundDetailedConfirmedCashForecastReportV03;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Extrato de Cotas
 * <p>
 * <p>Classe Java de MessageExtratoCotasComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageExtratoCotasComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FndDtldConfdCshFcstRptV03" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}FundDetailedConfirmedCashForecastReportV03"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageExtratoCotasComplexType", propOrder = {"fndDtldConfdCshFcstRptV03"})
public class MessageExtratoCotasComplexType {

    @XmlElement(name = "FndDtldConfdCshFcstRptV03",
                required = true) protected FundDetailedConfirmedCashForecastReportV03 fndDtldConfdCshFcstRptV03;

    /**
     * Obtém o valor da propriedade fndDtldConfdCshFcstRptV03.
     *
     * @return possible object is
     * {@link FundDetailedConfirmedCashForecastReportV03 }
     */
    public FundDetailedConfirmedCashForecastReportV03 getFndDtldConfdCshFcstRptV03() {
        return fndDtldConfdCshFcstRptV03;
    }

    /**
     * Define o valor da propriedade fndDtldConfdCshFcstRptV03.
     *
     * @param value allowed object is
     *              {@link FundDetailedConfirmedCashForecastReportV03 }
     */
    public void setFndDtldConfdCshFcstRptV03(FundDetailedConfirmedCashForecastReportV03 value) {
        this.fndDtldConfdCshFcstRptV03 = value;
    }

}
