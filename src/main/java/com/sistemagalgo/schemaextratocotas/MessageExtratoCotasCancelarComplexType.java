package com.sistemagalgo.schemaextratocotas;

import iso.std.iso._20022.tech.xsd.camt_045_001.FundDetailedConfirmedCashForecastReportCancellationV02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de MessageExtratoCotasCancelarComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageExtratoCotasCancelarComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FndDtldConfdCshFcstRptCxlV02" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}FundDetailedConfirmedCashForecastReportCancellationV02"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageExtratoCotasCancelarComplexType", propOrder = {"fndDtldConfdCshFcstRptCxlV02"})
public class MessageExtratoCotasCancelarComplexType {

    @XmlElement(name = "FndDtldConfdCshFcstRptCxlV02",
                required = true) protected FundDetailedConfirmedCashForecastReportCancellationV02
        fndDtldConfdCshFcstRptCxlV02;

    /**
     * Obtém o valor da propriedade fndDtldConfdCshFcstRptCxlV02.
     *
     * @return possible object is
     * {@link FundDetailedConfirmedCashForecastReportCancellationV02 }
     */
    public FundDetailedConfirmedCashForecastReportCancellationV02 getFndDtldConfdCshFcstRptCxlV02() {
        return fndDtldConfdCshFcstRptCxlV02;
    }

    /**
     * Define o valor da propriedade fndDtldConfdCshFcstRptCxlV02.
     *
     * @param value allowed object is
     *              {@link FundDetailedConfirmedCashForecastReportCancellationV02 }
     */
    public void setFndDtldConfdCshFcstRptCxlV02(FundDetailedConfirmedCashForecastReportCancellationV02 value) {
        this.fndDtldConfdCshFcstRptCxlV02 = value;
    }

}
