package com.sistemagalgo.serviceposicaoativos;

import com.sistemagalgo.schemaposicaoativos.MessagePosicaoAtivosConsPendComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RequisicaoConsulta" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}MessagePosicaoAtivosConsPendComplexType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"requisicaoConsulta"})
@XmlRootElement(name = "ConsumirPendentes")
public class ConsumirPendentes {

    @XmlElement(name = "RequisicaoConsulta", required = true) protected MessagePosicaoAtivosConsPendComplexType
        requisicaoConsulta;

    /**
     * Obtém o valor da propriedade requisicaoConsulta.
     *
     * @return possible object is
     * {@link MessagePosicaoAtivosConsPendComplexType }
     */
    public MessagePosicaoAtivosConsPendComplexType getRequisicaoConsulta() {
        return requisicaoConsulta;
    }

    /**
     * Define o valor da propriedade requisicaoConsulta.
     *
     * @param value allowed object is
     *              {@link MessagePosicaoAtivosConsPendComplexType }
     */
    public void setRequisicaoConsulta(MessagePosicaoAtivosConsPendComplexType value) {
        this.requisicaoConsulta = value;
    }

}
