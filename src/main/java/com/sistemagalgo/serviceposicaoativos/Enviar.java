package com.sistemagalgo.serviceposicaoativos;

import com.sistemagalgo.schemaposicaoativos.MessagePosicaoAtivosComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DadosEnviar" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}MessagePosicaoAtivosComplexType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"dadosEnviar"})
@XmlRootElement(name = "Enviar")
public class Enviar {

    @XmlElement(name = "DadosEnviar", required = true) protected MessagePosicaoAtivosComplexType dadosEnviar;

    /**
     * Obtém o valor da propriedade dadosEnviar.
     *
     * @return possible object is
     * {@link MessagePosicaoAtivosComplexType }
     */
    public MessagePosicaoAtivosComplexType getDadosEnviar() {
        return dadosEnviar;
    }

    /**
     * Define o valor da propriedade dadosEnviar.
     *
     * @param value allowed object is
     *              {@link MessagePosicaoAtivosComplexType }
     */
    public void setDadosEnviar(MessagePosicaoAtivosComplexType value) {
        this.dadosEnviar = value;
    }

}
