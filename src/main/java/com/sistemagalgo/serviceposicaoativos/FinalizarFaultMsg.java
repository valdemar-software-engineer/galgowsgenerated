package com.sistemagalgo.serviceposicaoativos;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.1.1
 * 2015-10-03T16:48:16.736-03:00
 * Generated source version: 3.1.1
 */

@WebFault(name = "Finalizar_fault", targetNamespace = "http://www.sistemagalgo.com/ServicePosicaoAtivos/")
public class FinalizarFaultMsg extends Exception {

    private com.sistemagalgo.serviceposicaoativos.FinalizarFault finalizarFault;

    public FinalizarFaultMsg() {
        super();
    }

    public FinalizarFaultMsg(String message) {
        super(message);
    }

    public FinalizarFaultMsg(String message, Throwable cause) {
        super(message, cause);
    }

    public FinalizarFaultMsg(String message, com.sistemagalgo.serviceposicaoativos.FinalizarFault finalizarFault) {
        super(message);
        this.finalizarFault = finalizarFault;
    }

    public FinalizarFaultMsg(String message, com.sistemagalgo.serviceposicaoativos.FinalizarFault finalizarFault,
        Throwable cause) {
        super(message, cause);
        this.finalizarFault = finalizarFault;
    }

    public com.sistemagalgo.serviceposicaoativos.FinalizarFault getFaultInfo() {
        return this.finalizarFault;
    }
}
