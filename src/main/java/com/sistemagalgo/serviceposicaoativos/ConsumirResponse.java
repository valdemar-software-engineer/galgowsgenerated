package com.sistemagalgo.serviceposicaoativos;

import com.sistemagalgo.schemaposicaoativos.MessagePosicaoAtivosComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RetornoConsulta" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}MessagePosicaoAtivosComplexType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"retornoConsulta"})
@XmlRootElement(name = "ConsumirResponse")
public class ConsumirResponse {

    @XmlElement(name = "RetornoConsulta", required = true) protected MessagePosicaoAtivosComplexType retornoConsulta;

    /**
     * Obtém o valor da propriedade retornoConsulta.
     *
     * @return possible object is
     * {@link MessagePosicaoAtivosComplexType }
     */
    public MessagePosicaoAtivosComplexType getRetornoConsulta() {
        return retornoConsulta;
    }

    /**
     * Define o valor da propriedade retornoConsulta.
     *
     * @param value allowed object is
     *              {@link MessagePosicaoAtivosComplexType }
     */
    public void setRetornoConsulta(MessagePosicaoAtivosComplexType value) {
        this.retornoConsulta = value;
    }

}
