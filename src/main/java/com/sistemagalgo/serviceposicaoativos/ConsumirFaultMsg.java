package com.sistemagalgo.serviceposicaoativos;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.1.1
 * 2015-10-03T16:48:16.718-03:00
 * Generated source version: 3.1.1
 */

@WebFault(name = "Consumir_fault", targetNamespace = "http://www.sistemagalgo.com/ServicePosicaoAtivos/")
public class ConsumirFaultMsg extends Exception {

    private com.sistemagalgo.serviceposicaoativos.ConsumirFault consumirFault;

    public ConsumirFaultMsg() {
        super();
    }

    public ConsumirFaultMsg(String message) {
        super(message);
    }

    public ConsumirFaultMsg(String message, Throwable cause) {
        super(message, cause);
    }

    public ConsumirFaultMsg(String message, com.sistemagalgo.serviceposicaoativos.ConsumirFault consumirFault) {
        super(message);
        this.consumirFault = consumirFault;
    }

    public ConsumirFaultMsg(String message, com.sistemagalgo.serviceposicaoativos.ConsumirFault consumirFault,
        Throwable cause) {
        super(message, cause);
        this.consumirFault = consumirFault;
    }

    public com.sistemagalgo.serviceposicaoativos.ConsumirFault getFaultInfo() {
        return this.consumirFault;
    }
}
