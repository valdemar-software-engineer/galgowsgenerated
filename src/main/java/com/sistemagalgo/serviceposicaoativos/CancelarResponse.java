package com.sistemagalgo.serviceposicaoativos;

import br.com.stianbid.common.MessageReceiptComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReciboCancelar" type="{http://www.stianbid.com.br/Common}MessageReceiptComplexType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"reciboCancelar"})
@XmlRootElement(name = "CancelarResponse")
public class CancelarResponse {

    @XmlElement(name = "ReciboCancelar", required = true) protected MessageReceiptComplexType reciboCancelar;

    /**
     * Obtém o valor da propriedade reciboCancelar.
     *
     * @return possible object is
     * {@link MessageReceiptComplexType }
     */
    public MessageReceiptComplexType getReciboCancelar() {
        return reciboCancelar;
    }

    /**
     * Define o valor da propriedade reciboCancelar.
     *
     * @param value allowed object is
     *              {@link MessageReceiptComplexType }
     */
    public void setReciboCancelar(MessageReceiptComplexType value) {
        this.reciboCancelar = value;
    }

}
