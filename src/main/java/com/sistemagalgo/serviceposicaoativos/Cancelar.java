package com.sistemagalgo.serviceposicaoativos;

import com.sistemagalgo.schemaposicaoativos.MessagePosicaoAtivosComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DadosCancelar" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}MessagePosicaoAtivosComplexType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"dadosCancelar"})
@XmlRootElement(name = "Cancelar")
public class Cancelar {

    @XmlElement(name = "DadosCancelar", required = true) protected MessagePosicaoAtivosComplexType dadosCancelar;

    /**
     * Obtém o valor da propriedade dadosCancelar.
     *
     * @return possible object is
     * {@link MessagePosicaoAtivosComplexType }
     */
    public MessagePosicaoAtivosComplexType getDadosCancelar() {
        return dadosCancelar;
    }

    /**
     * Define o valor da propriedade dadosCancelar.
     *
     * @param value allowed object is
     *              {@link MessagePosicaoAtivosComplexType }
     */
    public void setDadosCancelar(MessagePosicaoAtivosComplexType value) {
        this.dadosCancelar = value;
    }

}
