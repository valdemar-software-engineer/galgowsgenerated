package com.sistemagalgo.serviceposicaoativos;

import com.sistemagalgo.schemaposicaoativos.MessagePosicaoAtivosExcFinComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DadosFinalizar" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}MessagePosicaoAtivosExcFinComplexType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"dadosFinalizar"})
@XmlRootElement(name = "Finalizar")
public class Finalizar {

    @XmlElement(name = "DadosFinalizar", required = true) protected MessagePosicaoAtivosExcFinComplexType
        dadosFinalizar;

    /**
     * Obtém o valor da propriedade dadosFinalizar.
     *
     * @return possible object is
     * {@link MessagePosicaoAtivosExcFinComplexType }
     */
    public MessagePosicaoAtivosExcFinComplexType getDadosFinalizar() {
        return dadosFinalizar;
    }

    /**
     * Define o valor da propriedade dadosFinalizar.
     *
     * @param value allowed object is
     *              {@link MessagePosicaoAtivosExcFinComplexType }
     */
    public void setDadosFinalizar(MessagePosicaoAtivosExcFinComplexType value) {
        this.dadosFinalizar = value;
    }

}
