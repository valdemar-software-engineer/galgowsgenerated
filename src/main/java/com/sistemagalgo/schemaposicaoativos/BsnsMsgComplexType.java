package com.sistemagalgo.schemaposicaoativos;

import iso.std.iso._20022.tech.xsd.head_001_001.BusinessApplicationHeaderV01;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Business Message
 * <p>
 * <p>Classe Java de BsnsMsgComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="BsnsMsgComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}AppHdr"/&gt;
 *         &lt;element ref="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Document"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BsnsMsgComplexType", propOrder = {"appHdr", "document"})
public class BsnsMsgComplexType {

    @XmlElement(name = "AppHdr",
                namespace = "urn:iso:std:iso:20022:tech:xsd:head.001.001.01",
                required = true) protected BusinessApplicationHeaderV01 appHdr;
    @XmlElement(name = "Document",
                namespace = "urn:iso:std:iso:20022:tech:xsd:semt.003.001.04",
                required = true) protected Document document;

    /**
     * Cabeçalho BAH
     *
     * @return possible object is
     * {@link BusinessApplicationHeaderV01 }
     */
    public BusinessApplicationHeaderV01 getAppHdr() {
        return appHdr;
    }

    /**
     * Define o valor da propriedade appHdr.
     *
     * @param value allowed object is
     *              {@link BusinessApplicationHeaderV01 }
     */
    public void setAppHdr(BusinessApplicationHeaderV01 value) {
        this.appHdr = value;
    }

    /**
     * Posição de Ativos
     *
     * @return possible object is
     * {@link Document }
     */
    public Document getDocument() {
        return document;
    }

    /**
     * Define o valor da propriedade document.
     *
     * @param value allowed object is
     *              {@link Document }
     */
    public void setDocument(Document value) {
        this.document = value;
    }

}
