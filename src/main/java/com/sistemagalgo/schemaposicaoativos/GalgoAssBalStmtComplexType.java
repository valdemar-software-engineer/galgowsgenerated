package com.sistemagalgo.schemaposicaoativos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Galgo Assets Balance Statement
 * <p>
 * <p>
 * <p>Classe Java de GalgoAssBalStmtComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="GalgoAssBalStmtComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GalgoHdr" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}GalgoHdrComplexType"/&gt;
 *         &lt;element name="BsnsMsg" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}BsnsMsgComplexType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GalgoAssBalStmtComplexType", propOrder = {"galgoHdr", "bsnsMsg"})
public class GalgoAssBalStmtComplexType {

    @XmlElement(name = "GalgoHdr", required = true) protected GalgoHdrComplexType galgoHdr;
    @XmlElement(name = "BsnsMsg", required = true) protected List<BsnsMsgComplexType> bsnsMsg;

    /**
     * Gets the value of the bsnsMsg property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bsnsMsg property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBsnsMsg().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BsnsMsgComplexType }
     */
    public List<BsnsMsgComplexType> getBsnsMsg() {
        if (bsnsMsg == null) {
            bsnsMsg = new ArrayList<BsnsMsgComplexType>();
        }
        return this.bsnsMsg;
    }

    /**
     * Obtém o valor da propriedade galgoHdr.
     *
     * @return possible object is
     * {@link GalgoHdrComplexType }
     */
    public GalgoHdrComplexType getGalgoHdr() {
        return galgoHdr;
    }

    /**
     * Define o valor da propriedade galgoHdr.
     *
     * @param value allowed object is
     *              {@link GalgoHdrComplexType }
     */
    public void setGalgoHdr(GalgoHdrComplexType value) {
        this.galgoHdr = value;
    }

}
