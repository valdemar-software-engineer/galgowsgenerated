package com.sistemagalgo.schemaposicaoativos;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.sistemagalgo.schemaposicaoativos package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PosicaoAtivosCarteira_QNAME = new QName(
        "http://www.sistemagalgo.com/SchemaPosicaoAtivos", "PosicaoAtivosCarteira");
    private final static QName _GalgoAssBalStmt_QNAME = new QName("http://www.sistemagalgo.com/SchemaPosicaoAtivos",
        "GalgoAssBalStmt");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sistemagalgo.schemaposicaoativos
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BsnsMsgComplexType }
     */
    public BsnsMsgComplexType createBsnsMsgComplexType() {
        return new BsnsMsgComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GalgoAssBalStmtComplexType }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.sistemagalgo.com/SchemaPosicaoAtivos", name = "GalgoAssBalStmt")
    public JAXBElement<GalgoAssBalStmtComplexType> createGalgoAssBalStmt(GalgoAssBalStmtComplexType value) {
        return new JAXBElement<GalgoAssBalStmtComplexType>(_GalgoAssBalStmt_QNAME, GalgoAssBalStmtComplexType.class,
            null, value);
    }

    /**
     * Create an instance of {@link GalgoAssBalStmtComplexType }
     */
    public GalgoAssBalStmtComplexType createGalgoAssBalStmtComplexType() {
        return new GalgoAssBalStmtComplexType();
    }

    /**
     * Create an instance of {@link GalgoHdrComplexType }
     */
    public GalgoHdrComplexType createGalgoHdrComplexType() {
        return new GalgoHdrComplexType();
    }

    /**
     * Create an instance of {@link MarkersComplexType }
     */
    public MarkersComplexType createMarkersComplexType() {
        return new MarkersComplexType();
    }

    /**
     * Create an instance of {@link MessagePosicaoAtivosComplexType }
     */
    public MessagePosicaoAtivosComplexType createMessagePosicaoAtivosComplexType() {
        return new MessagePosicaoAtivosComplexType();
    }

    /**
     * Create an instance of {@link MessagePosicaoAtivosConsPendComplexType }
     */
    public MessagePosicaoAtivosConsPendComplexType createMessagePosicaoAtivosConsPendComplexType() {
        return new MessagePosicaoAtivosConsPendComplexType();
    }

    /**
     * Create an instance of {@link MessagePosicaoAtivosConsumirComplexType }
     */
    public MessagePosicaoAtivosConsumirComplexType createMessagePosicaoAtivosConsumirComplexType() {
        return new MessagePosicaoAtivosConsumirComplexType();
    }

    /**
     * Create an instance of {@link MessagePosicaoAtivosExcFinComplexType }
     */
    public MessagePosicaoAtivosExcFinComplexType createMessagePosicaoAtivosExcFinComplexType() {
        return new MessagePosicaoAtivosExcFinComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessagePosicaoAtivosExcFinComplexType }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.sistemagalgo.com/SchemaPosicaoAtivos", name = "PosicaoAtivosCarteira")
    public JAXBElement<MessagePosicaoAtivosExcFinComplexType> createPosicaoAtivosCarteira(
        MessagePosicaoAtivosExcFinComplexType value) {
        return new JAXBElement<MessagePosicaoAtivosExcFinComplexType>(_PosicaoAtivosCarteira_QNAME,
            MessagePosicaoAtivosExcFinComplexType.class, null, value);
    }

    /**
     * Create an instance of {@link PosicaoAtivosExcFinComplexType }
     */
    public PosicaoAtivosExcFinComplexType createPosicaoAtivosExcFinComplexType() {
        return new PosicaoAtivosExcFinComplexType();
    }

    /**
     * Create an instance of {@link STIComplexType }
     */
    public STIComplexType createSTIComplexType() {
        return new STIComplexType();
    }

}
