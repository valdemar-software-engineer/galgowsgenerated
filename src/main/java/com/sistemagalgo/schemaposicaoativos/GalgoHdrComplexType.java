package com.sistemagalgo.schemaposicaoativos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Cabeçalho Sistema Galgo
 * <p>
 * <p>Classe Java de GalgoHdrComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="GalgoHdrComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idMsgSender"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="256"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="qtElementCount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="LastPg" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Markers" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}MarkersComplexType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GalgoHdrComplexType", propOrder = {"idMsgSender", "qtElementCount", "lastPg", "markers"})
public class GalgoHdrComplexType {

    @XmlElement(required = true) protected String idMsgSender;
    protected Long qtElementCount;
    @XmlElement(name = "LastPg") protected Boolean lastPg;
    @XmlElement(name = "Markers") protected MarkersComplexType markers;

    /**
     * Obtém o valor da propriedade idMsgSender.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdMsgSender() {
        return idMsgSender;
    }

    /**
     * Define o valor da propriedade idMsgSender.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdMsgSender(String value) {
        this.idMsgSender = value;
    }

    /**
     * Obtém o valor da propriedade markers.
     *
     * @return possible object is
     * {@link MarkersComplexType }
     */
    public MarkersComplexType getMarkers() {
        return markers;
    }

    /**
     * Define o valor da propriedade markers.
     *
     * @param value allowed object is
     *              {@link MarkersComplexType }
     */
    public void setMarkers(MarkersComplexType value) {
        this.markers = value;
    }

    /**
     * Obtém o valor da propriedade qtElementCount.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getQtElementCount() {
        return qtElementCount;
    }

    /**
     * Define o valor da propriedade qtElementCount.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setQtElementCount(Long value) {
        this.qtElementCount = value;
    }

    /**
     * Obtém o valor da propriedade lastPg.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isLastPg() {
        return lastPg;
    }

    /**
     * Define o valor da propriedade lastPg.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setLastPg(Boolean value) {
        this.lastPg = value;
    }

}
