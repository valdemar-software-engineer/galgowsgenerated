package com.sistemagalgo.schemaposicaoativos;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Marcadores
 * <p>
 * <p>Classe Java de MessagePosicaoAtivosConsPendComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessagePosicaoAtivosConsPendComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Marcadores" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}MarkersComplexType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessagePosicaoAtivosConsPendComplexType", propOrder = {"marcadores"})
public class MessagePosicaoAtivosConsPendComplexType extends MessageRequestComplexType {

    @XmlElement(name = "Marcadores") protected MarkersComplexType marcadores;

    /**
     * Obtém o valor da propriedade marcadores.
     *
     * @return possible object is
     * {@link MarkersComplexType }
     */
    public MarkersComplexType getMarcadores() {
        return marcadores;
    }

    /**
     * Define o valor da propriedade marcadores.
     *
     * @param value allowed object is
     *              {@link MarkersComplexType }
     */
    public void setMarcadores(MarkersComplexType value) {
        this.marcadores = value;
    }

}
