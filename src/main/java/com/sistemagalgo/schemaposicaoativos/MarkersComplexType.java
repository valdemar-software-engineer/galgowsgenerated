package com.sistemagalgo.schemaposicaoativos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Marcadores
 * <p>
 * <p>Classe Java de MarkersComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MarkersComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RptMarker" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}MarkerStringType"/&gt;
 *         &lt;element name="AssMarker" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}MarkerLongType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarkersComplexType", propOrder = {"rptMarker", "assMarker"})
public class MarkersComplexType {

    @XmlElement(name = "RptMarker", required = true) protected String rptMarker;
    @XmlElement(name = "AssMarker") protected long assMarker;

    /**
     * Obtém o valor da propriedade assMarker.
     */
    public long getAssMarker() {
        return assMarker;
    }

    /**
     * Define o valor da propriedade assMarker.
     */
    public void setAssMarker(long value) {
        this.assMarker = value;
    }

    /**
     * Obtém o valor da propriedade rptMarker.
     *
     * @return possible object is
     * {@link String }
     */
    public String getRptMarker() {
        return rptMarker;
    }

    /**
     * Define o valor da propriedade rptMarker.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setRptMarker(String value) {
        this.rptMarker = value;
    }

}
