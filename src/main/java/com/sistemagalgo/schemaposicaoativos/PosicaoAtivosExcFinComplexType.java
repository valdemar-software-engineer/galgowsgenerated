package com.sistemagalgo.schemaposicaoativos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Posição de Ativos
 * <p>
 * <p>Classe Java de PosicaoAtivosExcFinComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PosicaoAtivosExcFinComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="CdSTI" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}STIIdentifier"/&gt;
 *           &lt;element name="CNPJ" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}CNPJIdentifier"/&gt;
 *           &lt;element name="Sigla" type="{http://www.stianbid.com.br/Common}Max20Text"/&gt;
 *           &lt;element name="ISIN" type="{http://www.stianbid.com.br/Common}ISINIdentifier"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="DtBase" type="{http://www.stianbid.com.br/Common}ISODate"/&gt;
 *         &lt;element name="Moeda" type="{http://www.stianbid.com.br/Common}CurrencyCode"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PosicaoAtivosExcFinComplexType", propOrder = {"cdSTI", "cnpj", "sigla", "isin", "dtBase", "moeda"})
public class PosicaoAtivosExcFinComplexType {

    @XmlElement(name = "CdSTI") protected Integer cdSTI;
    @XmlElement(name = "CNPJ") protected Long cnpj;
    @XmlElement(name = "Sigla") protected String sigla;
    @XmlElement(name = "ISIN") protected String isin;
    @XmlElement(name = "DtBase", required = true) @XmlSchemaType(name = "date") protected XMLGregorianCalendar dtBase;
    @XmlElement(name = "Moeda", required = true) protected String moeda;

    /**
     * Obtém o valor da propriedade cnpj.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getCNPJ() {
        return cnpj;
    }

    /**
     * Define o valor da propriedade cnpj.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setCNPJ(Long value) {
        this.cnpj = value;
    }

    /**
     * Obtém o valor da propriedade cdSTI.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCdSTI() {
        return cdSTI;
    }

    /**
     * Define o valor da propriedade cdSTI.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCdSTI(Integer value) {
        this.cdSTI = value;
    }

    /**
     * Obtém o valor da propriedade dtBase.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtBase() {
        return dtBase;
    }

    /**
     * Define o valor da propriedade dtBase.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtBase(XMLGregorianCalendar value) {
        this.dtBase = value;
    }

    /**
     * Obtém o valor da propriedade isin.
     *
     * @return possible object is
     * {@link String }
     */
    public String getISIN() {
        return isin;
    }

    /**
     * Define o valor da propriedade isin.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setISIN(String value) {
        this.isin = value;
    }

    /**
     * Obtém o valor da propriedade moeda.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMoeda() {
        return moeda;
    }

    /**
     * Define o valor da propriedade moeda.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMoeda(String value) {
        this.moeda = value;
    }

    /**
     * Obtém o valor da propriedade sigla.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSigla() {
        return sigla;
    }

    /**
     * Define o valor da propriedade sigla.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSigla(String value) {
        this.sigla = value;
    }

}
