package com.sistemagalgo.schemaposicaoativos;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * Consumir Posição de Ativos da Carteira
 * <p>
 * <p>
 * <p>Classe Java de MessagePosicaoAtivosConsumirComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessagePosicaoAtivosConsumirComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CdObjetos" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}STIComplexType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="DtPosicaoAtivos" type="{http://www.stianbid.com.br/Common}ISODate"/&gt;
 *         &lt;element name="Moeda" type="{http://www.stianbid.com.br/Common}CurrencyCode"/&gt;
 *         &lt;element name="Marcadores" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}MarkersComplexType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessagePosicaoAtivosConsumirComplexType",
         propOrder = {"cdObjetos", "dtPosicaoAtivos", "moeda", "marcadores"})
public class MessagePosicaoAtivosConsumirComplexType extends MessageRequestComplexType {

    @XmlElement(name = "CdObjetos", required = true) protected List<STIComplexType> cdObjetos;
    @XmlElement(name = "DtPosicaoAtivos", required = true) @XmlSchemaType(name = "date") protected XMLGregorianCalendar
        dtPosicaoAtivos;
    @XmlElement(name = "Moeda", required = true) protected String moeda;
    @XmlElement(name = "Marcadores") protected MarkersComplexType marcadores;

    /**
     * Gets the value of the cdObjetos property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cdObjetos property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCdObjetos().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link STIComplexType }
     */
    public List<STIComplexType> getCdObjetos() {
        if (cdObjetos == null) {
            cdObjetos = new ArrayList<STIComplexType>();
        }
        return this.cdObjetos;
    }

    /**
     * Obtém o valor da propriedade dtPosicaoAtivos.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtPosicaoAtivos() {
        return dtPosicaoAtivos;
    }

    /**
     * Define o valor da propriedade dtPosicaoAtivos.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtPosicaoAtivos(XMLGregorianCalendar value) {
        this.dtPosicaoAtivos = value;
    }

    /**
     * Obtém o valor da propriedade marcadores.
     *
     * @return possible object is
     * {@link MarkersComplexType }
     */
    public MarkersComplexType getMarcadores() {
        return marcadores;
    }

    /**
     * Define o valor da propriedade marcadores.
     *
     * @param value allowed object is
     *              {@link MarkersComplexType }
     */
    public void setMarcadores(MarkersComplexType value) {
        this.marcadores = value;
    }

    /**
     * Obtém o valor da propriedade moeda.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMoeda() {
        return moeda;
    }

    /**
     * Define o valor da propriedade moeda.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMoeda(String value) {
        this.moeda = value;
    }

}
