package com.sistemagalgo.schemaposicaoativos;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Posição de Ativos
 * <p>
 * <p>Classe Java de MessagePosicaoAtivosExcFinComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessagePosicaoAtivosExcFinComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PosicaoAtivos" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}PosicaoAtivosExcFinComplexType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessagePosicaoAtivosExcFinComplexType", propOrder = {"posicaoAtivos"})
public class MessagePosicaoAtivosExcFinComplexType extends MessageRequestComplexType {

    @XmlElement(name = "PosicaoAtivos", required = true) protected List<PosicaoAtivosExcFinComplexType> posicaoAtivos;

    /**
     * Gets the value of the posicaoAtivos property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the posicaoAtivos property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPosicaoAtivos().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PosicaoAtivosExcFinComplexType }
     */
    public List<PosicaoAtivosExcFinComplexType> getPosicaoAtivos() {
        if (posicaoAtivos == null) {
            posicaoAtivos = new ArrayList<PosicaoAtivosExcFinComplexType>();
        }
        return this.posicaoAtivos;
    }

}
