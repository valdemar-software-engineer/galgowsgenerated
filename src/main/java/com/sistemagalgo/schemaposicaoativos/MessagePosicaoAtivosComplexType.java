package com.sistemagalgo.schemaposicaoativos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Posição de Ativos da Carteira
 * <p>
 * <p>Classe Java de MessagePosicaoAtivosComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessagePosicaoAtivosComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GalgoAssBalStmt" type="{http://www.sistemagalgo.com/SchemaPosicaoAtivos}GalgoAssBalStmtComplexType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessagePosicaoAtivosComplexType", propOrder = {"galgoAssBalStmt"})
public class MessagePosicaoAtivosComplexType {

    @XmlElement(name = "GalgoAssBalStmt", required = true) protected GalgoAssBalStmtComplexType galgoAssBalStmt;

    /**
     * Obtém o valor da propriedade galgoAssBalStmt.
     *
     * @return possible object is
     * {@link GalgoAssBalStmtComplexType }
     */
    public GalgoAssBalStmtComplexType getGalgoAssBalStmt() {
        return galgoAssBalStmt;
    }

    /**
     * Define o valor da propriedade galgoAssBalStmt.
     *
     * @param value allowed object is
     *              {@link GalgoAssBalStmtComplexType }
     */
    public void setGalgoAssBalStmt(GalgoAssBalStmtComplexType value) {
        this.galgoAssBalStmt = value;
    }

}
