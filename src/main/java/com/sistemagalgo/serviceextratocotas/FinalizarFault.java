package com.sistemagalgo.serviceextratocotas;

import br.com.stianbid.common.MessageExceptionComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Finalizar_fault" type="{http://www.stianbid.com.br/Common}MessageExceptionComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"finalizarFault"})
@XmlRootElement(name = "Finalizar_fault")
public class FinalizarFault {

    @XmlElement(name = "Finalizar_fault", required = true) protected MessageExceptionComplexType finalizarFault;

    /**
     * Obtém o valor da propriedade finalizarFault.
     *
     * @return possible object is
     * {@link MessageExceptionComplexType }
     */
    public MessageExceptionComplexType getFinalizarFault() {
        return finalizarFault;
    }

    /**
     * Define o valor da propriedade finalizarFault.
     *
     * @param value allowed object is
     *              {@link MessageExceptionComplexType }
     */
    public void setFinalizarFault(MessageExceptionComplexType value) {
        this.finalizarFault = value;
    }

}
