package com.sistemagalgo.serviceextratocotas;

import com.sistemagalgo.schemaextratocotas.MessageExtratoCotasCancelarComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DadosCancelamento" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}MessageExtratoCotasCancelarComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"dadosCancelamento"})
@XmlRootElement(name = "Cancelar")
public class Cancelar {

    @XmlElement(name = "DadosCancelamento", required = true) protected MessageExtratoCotasCancelarComplexType
        dadosCancelamento;

    /**
     * Obtém o valor da propriedade dadosCancelamento.
     *
     * @return possible object is
     * {@link MessageExtratoCotasCancelarComplexType }
     */
    public MessageExtratoCotasCancelarComplexType getDadosCancelamento() {
        return dadosCancelamento;
    }

    /**
     * Define o valor da propriedade dadosCancelamento.
     *
     * @param value allowed object is
     *              {@link MessageExtratoCotasCancelarComplexType }
     */
    public void setDadosCancelamento(MessageExtratoCotasCancelarComplexType value) {
        this.dadosCancelamento = value;
    }

}
