package com.sistemagalgo.serviceextratocotas;

import com.sistemagalgo.schemaextratocotas.MessageExtratoCotasRespPendComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Retorno" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}MessageExtratoCotasRespPendComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"retorno"})
@XmlRootElement(name = "ConsumirPendentesResponse")
public class ConsumirPendentesResponse {

    @XmlElement(name = "Retorno", required = true) protected MessageExtratoCotasRespPendComplexType retorno;

    /**
     * Obtém o valor da propriedade retorno.
     *
     * @return possible object is
     * {@link MessageExtratoCotasRespPendComplexType }
     */
    public MessageExtratoCotasRespPendComplexType getRetorno() {
        return retorno;
    }

    /**
     * Define o valor da propriedade retorno.
     *
     * @param value allowed object is
     *              {@link MessageExtratoCotasRespPendComplexType }
     */
    public void setRetorno(MessageExtratoCotasRespPendComplexType value) {
        this.retorno = value;
    }

}
