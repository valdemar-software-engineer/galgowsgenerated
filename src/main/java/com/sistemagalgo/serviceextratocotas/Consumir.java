package com.sistemagalgo.serviceextratocotas;

import com.sistemagalgo.schemaextratocotas.MessageExtratoCotasConsumirComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Requisicao" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}MessageExtratoCotasConsumirComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"requisicao"})
@XmlRootElement(name = "Consumir")
public class Consumir {

    @XmlElement(name = "Requisicao", required = true) protected MessageExtratoCotasConsumirComplexType requisicao;

    /**
     * Obtém o valor da propriedade requisicao.
     *
     * @return possible object is
     * {@link MessageExtratoCotasConsumirComplexType }
     */
    public MessageExtratoCotasConsumirComplexType getRequisicao() {
        return requisicao;
    }

    /**
     * Define o valor da propriedade requisicao.
     *
     * @param value allowed object is
     *              {@link MessageExtratoCotasConsumirComplexType }
     */
    public void setRequisicao(MessageExtratoCotasConsumirComplexType value) {
        this.requisicao = value;
    }

}
