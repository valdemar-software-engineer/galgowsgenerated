package com.sistemagalgo.serviceextratocotas;

import br.com.stianbid.common.MessageReceiptComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReciboEnvio" type="{http://www.stianbid.com.br/Common}MessageReceiptComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"reciboEnvio"})
@XmlRootElement(name = "EnviarResponse")
public class EnviarResponse {

    @XmlElement(name = "ReciboEnvio", required = true) protected MessageReceiptComplexType reciboEnvio;

    /**
     * Obtém o valor da propriedade reciboEnvio.
     *
     * @return possible object is
     * {@link MessageReceiptComplexType }
     */
    public MessageReceiptComplexType getReciboEnvio() {
        return reciboEnvio;
    }

    /**
     * Define o valor da propriedade reciboEnvio.
     *
     * @param value allowed object is
     *              {@link MessageReceiptComplexType }
     */
    public void setReciboEnvio(MessageReceiptComplexType value) {
        this.reciboEnvio = value;
    }

}
