package com.sistemagalgo.serviceextratocotas;

import com.sistemagalgo.schemaextratocotas.MessageExtratoCotasComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DadosEnvio" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}MessageExtratoCotasComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"dadosEnvio"})
@XmlRootElement(name = "Enviar")
public class Enviar {

    @XmlElement(name = "DadosEnvio", required = true) protected MessageExtratoCotasComplexType dadosEnvio;

    /**
     * Obtém o valor da propriedade dadosEnvio.
     *
     * @return possible object is
     * {@link MessageExtratoCotasComplexType }
     */
    public MessageExtratoCotasComplexType getDadosEnvio() {
        return dadosEnvio;
    }

    /**
     * Define o valor da propriedade dadosEnvio.
     *
     * @param value allowed object is
     *              {@link MessageExtratoCotasComplexType }
     */
    public void setDadosEnvio(MessageExtratoCotasComplexType value) {
        this.dadosEnvio = value;
    }

}
