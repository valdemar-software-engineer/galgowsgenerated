package com.sistemagalgo.serviceextratocotas;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.sistemagalgo.serviceextratocotas package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sistemagalgo.serviceextratocotas
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Cancelar }
     */
    public Cancelar createCancelar() {
        return new Cancelar();
    }

    /**
     * Create an instance of {@link CancelarFault }
     */
    public CancelarFault createCancelarFault() {
        return new CancelarFault();
    }

    /**
     * Create an instance of {@link CancelarResponse }
     */
    public CancelarResponse createCancelarResponse() {
        return new CancelarResponse();
    }

    /**
     * Create an instance of {@link Consumir }
     */
    public Consumir createConsumir() {
        return new Consumir();
    }

    /**
     * Create an instance of {@link ConsumirFault }
     */
    public ConsumirFault createConsumirFault() {
        return new ConsumirFault();
    }

    /**
     * Create an instance of {@link ConsumirPendentes }
     */
    public ConsumirPendentes createConsumirPendentes() {
        return new ConsumirPendentes();
    }

    /**
     * Create an instance of {@link ConsumirPendentesFault }
     */
    public ConsumirPendentesFault createConsumirPendentesFault() {
        return new ConsumirPendentesFault();
    }

    /**
     * Create an instance of {@link ConsumirPendentesResponse }
     */
    public ConsumirPendentesResponse createConsumirPendentesResponse() {
        return new ConsumirPendentesResponse();
    }

    /**
     * Create an instance of {@link ConsumirResponse }
     */
    public ConsumirResponse createConsumirResponse() {
        return new ConsumirResponse();
    }

    /**
     * Create an instance of {@link Enviar }
     */
    public Enviar createEnviar() {
        return new Enviar();
    }

    /**
     * Create an instance of {@link EnviarFault }
     */
    public EnviarFault createEnviarFault() {
        return new EnviarFault();
    }

    /**
     * Create an instance of {@link EnviarResponse }
     */
    public EnviarResponse createEnviarResponse() {
        return new EnviarResponse();
    }

    /**
     * Create an instance of {@link Excluir }
     */
    public Excluir createExcluir() {
        return new Excluir();
    }

    /**
     * Create an instance of {@link ExcluirFault }
     */
    public ExcluirFault createExcluirFault() {
        return new ExcluirFault();
    }

    /**
     * Create an instance of {@link ExcluirResponse }
     */
    public ExcluirResponse createExcluirResponse() {
        return new ExcluirResponse();
    }

    /**
     * Create an instance of {@link Finalizar }
     */
    public Finalizar createFinalizar() {
        return new Finalizar();
    }

    /**
     * Create an instance of {@link FinalizarFault }
     */
    public FinalizarFault createFinalizarFault() {
        return new FinalizarFault();
    }

    /**
     * Create an instance of {@link FinalizarResponse }
     */
    public FinalizarResponse createFinalizarResponse() {
        return new FinalizarResponse();
    }

}
