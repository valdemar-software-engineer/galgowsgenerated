package com.sistemagalgo.serviceextratocotas;

import com.sistemagalgo.schemaextratocotas.MessageExtratoCotasExcFinComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DadosFinalizar" type="{http://www.sistemagalgo.com/SchemaExtratoCotas}MessageExtratoCotasExcFinComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"dadosFinalizar"})
@XmlRootElement(name = "Finalizar")
public class Finalizar {

    @XmlElement(name = "DadosFinalizar", required = true) protected MessageExtratoCotasExcFinComplexType dadosFinalizar;

    /**
     * Obtém o valor da propriedade dadosFinalizar.
     *
     * @return possible object is
     * {@link MessageExtratoCotasExcFinComplexType }
     */
    public MessageExtratoCotasExcFinComplexType getDadosFinalizar() {
        return dadosFinalizar;
    }

    /**
     * Define o valor da propriedade dadosFinalizar.
     *
     * @param value allowed object is
     *              {@link MessageExtratoCotasExcFinComplexType }
     */
    public void setDadosFinalizar(MessageExtratoCotasExcFinComplexType value) {
        this.dadosFinalizar = value;
    }

}
