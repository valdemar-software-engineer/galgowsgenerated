package br.anbimacom.balanceforsubaccountbrazil;

import iso.std.iso._20022.tech.xsd.semt_003_001.ActiveOrHistoricCurrencyAnd13DecimalAmount;
import iso.std.iso._20022.tech.xsd.semt_003_001.GenericIdentification20;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ShHldgIdType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ShHldgIdType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text"/&gt;
 *         &lt;element name="Prtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification20" minOccurs="0"/&gt;
 *         &lt;element name="TtlBookValChng"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyAnd13DecimalAmount"/&gt;
 *                   &lt;element name="Sgn" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PlusOrMinusIndicator"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShHldgIdType", propOrder = {"id", "prtry", "ttlBookValChng"})
public class ShHldgIdType {

    @XmlElement(name = "Id", required = true) protected String id;
    @XmlElement(name = "Prtry") protected GenericIdentification20 prtry;
    @XmlElement(name = "TtlBookValChng", required = true) protected ShHldgIdType.TtlBookValChng ttlBookValChng;

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link String }
     */
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade prtry.
     *
     * @return possible object is
     * {@link GenericIdentification20 }
     */
    public GenericIdentification20 getPrtry() {
        return prtry;
    }

    /**
     * Define o valor da propriedade prtry.
     *
     * @param value allowed object is
     *              {@link GenericIdentification20 }
     */
    public void setPrtry(GenericIdentification20 value) {
        this.prtry = value;
    }

    /**
     * Obtém o valor da propriedade ttlBookValChng.
     *
     * @return possible object is
     * {@link ShHldgIdType.TtlBookValChng }
     */
    public ShHldgIdType.TtlBookValChng getTtlBookValChng() {
        return ttlBookValChng;
    }

    /**
     * Define o valor da propriedade ttlBookValChng.
     *
     * @param value allowed object is
     *              {@link ShHldgIdType.TtlBookValChng }
     */
    public void setTtlBookValChng(ShHldgIdType.TtlBookValChng value) {
        this.ttlBookValChng = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * <p>
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * <p>
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyAnd13DecimalAmount"/&gt;
     *         &lt;element name="Sgn" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PlusOrMinusIndicator"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"amt", "sgn"})
    public static class TtlBookValChng {

        @XmlElement(name = "Amt", required = true) protected ActiveOrHistoricCurrencyAnd13DecimalAmount amt;
        @XmlElement(name = "Sgn") protected boolean sgn;

        /**
         * Obtém o valor da propriedade amt.
         *
         * @return possible object is
         * {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
         */
        public ActiveOrHistoricCurrencyAnd13DecimalAmount getAmt() {
            return amt;
        }

        /**
         * Define o valor da propriedade amt.
         *
         * @param value allowed object is
         *              {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
         */
        public void setAmt(ActiveOrHistoricCurrencyAnd13DecimalAmount value) {
            this.amt = value;
        }

        /**
         * Obtém o valor da propriedade sgn.
         */
        public boolean isSgn() {
            return sgn;
        }

        /**
         * Define o valor da propriedade sgn.
         */
        public void setSgn(boolean value) {
            this.sgn = value;
        }

    }

}
