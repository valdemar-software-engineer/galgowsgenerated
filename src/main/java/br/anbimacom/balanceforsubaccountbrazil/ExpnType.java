package br.anbimacom.balanceforsubaccountbrazil;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * Expenses - Despesas (Taxas)
 * <p>
 * <p>Classe Java de ExpnType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ExpnType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MgmtFeeRate" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PercentageRate"/&gt;
 *         &lt;element name="EqulFee" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}EqulFeeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExpnType", propOrder = {"mgmtFeeRate", "equlFee"})
public class ExpnType {

    @XmlElement(name = "MgmtFeeRate", required = true) protected BigDecimal mgmtFeeRate;
    @XmlElement(name = "EqulFee") protected EqulFeeType equlFee;

    /**
     * Obtém o valor da propriedade equlFee.
     *
     * @return possible object is
     * {@link EqulFeeType }
     */
    public EqulFeeType getEqulFee() {
        return equlFee;
    }

    /**
     * Define o valor da propriedade equlFee.
     *
     * @param value allowed object is
     *              {@link EqulFeeType }
     */
    public void setEqulFee(EqulFeeType value) {
        this.equlFee = value;
    }

    /**
     * Obtém o valor da propriedade mgmtFeeRate.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getMgmtFeeRate() {
        return mgmtFeeRate;
    }

    /**
     * Define o valor da propriedade mgmtFeeRate.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setMgmtFeeRate(BigDecimal value) {
        this.mgmtFeeRate = value;
    }

}
