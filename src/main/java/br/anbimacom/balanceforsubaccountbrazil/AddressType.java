package br.anbimacom.balanceforsubaccountbrazil;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de AddressType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="AddressType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="StrtNm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max70Text"/&gt;
 *         &lt;element name="BldgNb" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max16Text" minOccurs="0"/&gt;
 *         &lt;element name="Flr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max16Text" minOccurs="0"/&gt;
 *         &lt;element name="PstCd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max16Text" minOccurs="0"/&gt;
 *         &lt;element name="TwnNm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text"/&gt;
 *         &lt;element name="CtrySubDvsn" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text"/&gt;
 *         &lt;element name="Ctry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}CountryCode"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressType", propOrder = {"strtNm", "bldgNb", "flr", "pstCd", "twnNm", "ctrySubDvsn", "ctry"})
public class AddressType {

    @XmlElement(name = "StrtNm", required = true) protected String strtNm;
    @XmlElement(name = "BldgNb") protected String bldgNb;
    @XmlElement(name = "Flr") protected String flr;
    @XmlElement(name = "PstCd") protected String pstCd;
    @XmlElement(name = "TwnNm", required = true) protected String twnNm;
    @XmlElement(name = "CtrySubDvsn", required = true) protected String ctrySubDvsn;
    @XmlElement(name = "Ctry", required = true) protected String ctry;

    /**
     * Obtém o valor da propriedade bldgNb.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBldgNb() {
        return bldgNb;
    }

    /**
     * Define o valor da propriedade bldgNb.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBldgNb(String value) {
        this.bldgNb = value;
    }

    /**
     * Obtém o valor da propriedade ctry.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCtry() {
        return ctry;
    }

    /**
     * Define o valor da propriedade ctry.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCtry(String value) {
        this.ctry = value;
    }

    /**
     * Obtém o valor da propriedade ctrySubDvsn.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCtrySubDvsn() {
        return ctrySubDvsn;
    }

    /**
     * Define o valor da propriedade ctrySubDvsn.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCtrySubDvsn(String value) {
        this.ctrySubDvsn = value;
    }

    /**
     * Obtém o valor da propriedade flr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFlr() {
        return flr;
    }

    /**
     * Define o valor da propriedade flr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFlr(String value) {
        this.flr = value;
    }

    /**
     * Obtém o valor da propriedade pstCd.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPstCd() {
        return pstCd;
    }

    /**
     * Define o valor da propriedade pstCd.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPstCd(String value) {
        this.pstCd = value;
    }

    /**
     * Obtém o valor da propriedade strtNm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getStrtNm() {
        return strtNm;
    }

    /**
     * Define o valor da propriedade strtNm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setStrtNm(String value) {
        this.strtNm = value;
    }

    /**
     * Obtém o valor da propriedade twnNm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTwnNm() {
        return twnNm;
    }

    /**
     * Define o valor da propriedade twnNm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTwnNm(String value) {
        this.twnNm = value;
    }

}
