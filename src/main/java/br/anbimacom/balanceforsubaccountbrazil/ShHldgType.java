package br.anbimacom.balanceforsubaccountbrazil;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de ShHldgType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ShHldgType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShHldgId" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}ShHldgIdType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShHldgType", propOrder = {"shHldgId"})
public class ShHldgType {

    @XmlElement(name = "ShHldgId", required = true) protected List<ShHldgIdType> shHldgId;

    /**
     * Gets the value of the shHldgId property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shHldgId property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShHldgId().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShHldgIdType }
     */
    public List<ShHldgIdType> getShHldgId() {
        if (shHldgId == null) {
            shHldgId = new ArrayList<ShHldgIdType>();
        }
        return this.shHldgId;
    }

}
