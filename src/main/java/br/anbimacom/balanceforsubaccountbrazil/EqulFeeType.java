package br.anbimacom.balanceforsubaccountbrazil;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de EqulFeeType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="EqulFeeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Rate" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PercentageRate"/&gt;
 *         &lt;element name="EqulIndex" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}EqulIndexType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EqulFeeType", propOrder = {"rate", "equlIndex"})
public class EqulFeeType {

    @XmlElement(name = "Rate", required = true) protected BigDecimal rate;
    @XmlElement(name = "EqulIndex", required = true) protected List<EqulIndexType> equlIndex;

    /**
     * Gets the value of the equlIndex property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the equlIndex property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEqulIndex().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EqulIndexType }
     */
    public List<EqulIndexType> getEqulIndex() {
        if (equlIndex == null) {
            equlIndex = new ArrayList<EqulIndexType>();
        }
        return this.equlIndex;
    }

    /**
     * Obtém o valor da propriedade rate.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Define o valor da propriedade rate.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setRate(BigDecimal value) {
        this.rate = value;
    }

}
