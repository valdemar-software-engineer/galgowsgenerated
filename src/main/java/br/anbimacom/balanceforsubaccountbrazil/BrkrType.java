package br.anbimacom.balanceforsubaccountbrazil;

import iso.std.iso._20022.tech.xsd.semt_003_001.ActiveOrHistoricCurrencyAnd13DecimalAmount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * BrokerType - Identificar o ambiente de negociação
 * e as taxas pagas.
 * <p>
 * <p>
 * <p>Classe Java de BrkrType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="BrkrType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BrkrPdVl"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyAnd13DecimalAmount"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="RebaVl" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyAnd13DecimalAmount"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="MktIdrCd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}MICIdentifier"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BrkrType", propOrder = {"brkrPdVl", "rebaVl", "mktIdrCd"})
public class BrkrType {

    @XmlElement(name = "BrkrPdVl", required = true) protected BrkrType.BrkrPdVl brkrPdVl;
    @XmlElement(name = "RebaVl") protected BrkrType.RebaVl rebaVl;
    @XmlElement(name = "MktIdrCd", required = true) protected String mktIdrCd;

    /**
     * Obtém o valor da propriedade brkrPdVl.
     *
     * @return possible object is
     * {@link BrkrType.BrkrPdVl }
     */
    public BrkrType.BrkrPdVl getBrkrPdVl() {
        return brkrPdVl;
    }

    /**
     * Define o valor da propriedade brkrPdVl.
     *
     * @param value allowed object is
     *              {@link BrkrType.BrkrPdVl }
     */
    public void setBrkrPdVl(BrkrType.BrkrPdVl value) {
        this.brkrPdVl = value;
    }

    /**
     * Obtém o valor da propriedade mktIdrCd.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMktIdrCd() {
        return mktIdrCd;
    }

    /**
     * Define o valor da propriedade mktIdrCd.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMktIdrCd(String value) {
        this.mktIdrCd = value;
    }

    /**
     * Obtém o valor da propriedade rebaVl.
     *
     * @return possible object is
     * {@link BrkrType.RebaVl }
     */
    public BrkrType.RebaVl getRebaVl() {
        return rebaVl;
    }

    /**
     * Define o valor da propriedade rebaVl.
     *
     * @param value allowed object is
     *              {@link BrkrType.RebaVl }
     */
    public void setRebaVl(BrkrType.RebaVl value) {
        this.rebaVl = value;
    }

    /**
     * <p>Classe Java de anonymous complex type.
     * <p>
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * <p>
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyAnd13DecimalAmount"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"amt"})
    public static class BrkrPdVl {

        @XmlElement(name = "Amt", required = true) protected ActiveOrHistoricCurrencyAnd13DecimalAmount amt;

        /**
         * Obtém o valor da propriedade amt.
         *
         * @return possible object is
         * {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
         */
        public ActiveOrHistoricCurrencyAnd13DecimalAmount getAmt() {
            return amt;
        }

        /**
         * Define o valor da propriedade amt.
         *
         * @param value allowed object is
         *              {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
         */
        public void setAmt(ActiveOrHistoricCurrencyAnd13DecimalAmount value) {
            this.amt = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * <p>
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * <p>
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyAnd13DecimalAmount"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"amt"})
    public static class RebaVl {

        @XmlElement(name = "Amt", required = true) protected ActiveOrHistoricCurrencyAnd13DecimalAmount amt;

        /**
         * Obtém o valor da propriedade amt.
         *
         * @return possible object is
         * {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
         */
        public ActiveOrHistoricCurrencyAnd13DecimalAmount getAmt() {
            return amt;
        }

        /**
         * Define o valor da propriedade amt.
         *
         * @param value allowed object is
         *              {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
         */
        public void setAmt(ActiveOrHistoricCurrencyAnd13DecimalAmount value) {
            this.amt = value;
        }

    }

}
