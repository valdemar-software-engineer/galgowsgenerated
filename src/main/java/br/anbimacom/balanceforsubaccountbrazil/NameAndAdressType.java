package br.anbimacom.balanceforsubaccountbrazil;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de NameAndAdressType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="NameAndAdressType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Nm"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="350"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Adr" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}AddressType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NameAndAdressType", propOrder = {"nm", "adr"})
public class NameAndAdressType {

    @XmlElement(name = "Nm", required = true) protected String nm;
    @XmlElement(name = "Adr", required = true) protected AddressType adr;

    /**
     * Obtém o valor da propriedade adr.
     *
     * @return possible object is
     * {@link AddressType }
     */
    public AddressType getAdr() {
        return adr;
    }

    /**
     * Define o valor da propriedade adr.
     *
     * @param value allowed object is
     *              {@link AddressType }
     */
    public void setAdr(AddressType value) {
        this.adr = value;
    }

    /**
     * Obtém o valor da propriedade nm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNm() {
        return nm;
    }

    /**
     * Define o valor da propriedade nm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNm(String value) {
        this.nm = value;
    }

}
