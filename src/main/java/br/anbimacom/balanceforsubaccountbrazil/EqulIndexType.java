package br.anbimacom.balanceforsubaccountbrazil;

import iso.std.iso._20022.tech.xsd.semt_003_001.GenericIdentification1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Classe Java de EqulIndexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="EqulIndexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Rate" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PercentageRate"/&gt;
 *         &lt;element name="Code"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Prtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification1" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EqulIndexType", propOrder = {"rate", "code"})
public class EqulIndexType {

    @XmlElement(name = "Rate", required = true) protected BigDecimal rate;
    @XmlElement(name = "Code", required = true) protected EqulIndexType.Code code;

    /**
     * Obtém o valor da propriedade code.
     *
     * @return possible object is
     * {@link EqulIndexType.Code }
     */
    public EqulIndexType.Code getCode() {
        return code;
    }

    /**
     * Define o valor da propriedade code.
     *
     * @param value allowed object is
     *              {@link EqulIndexType.Code }
     */
    public void setCode(EqulIndexType.Code value) {
        this.code = value;
    }

    /**
     * Obtém o valor da propriedade rate.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Define o valor da propriedade rate.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setRate(BigDecimal value) {
        this.rate = value;
    }

    /**
     * <p>Classe Java de anonymous complex type.
     * <p>
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * <p>
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Prtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification1" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"prtry"})
    public static class Code {

        @XmlElement(name = "Prtry") protected GenericIdentification1 prtry;

        /**
         * Obtém o valor da propriedade prtry.
         *
         * @return possible object is
         * {@link GenericIdentification1 }
         */
        public GenericIdentification1 getPrtry() {
            return prtry;
        }

        /**
         * Define o valor da propriedade prtry.
         *
         * @param value allowed object is
         *              {@link GenericIdentification1 }
         */
        public void setPrtry(GenericIdentification1 value) {
            this.prtry = value;
        }

    }

}
