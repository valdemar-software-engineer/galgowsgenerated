package br.anbimacom.balanceforsubaccountbrazil;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the br.anbimacom.balanceforsubaccountbrazil package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.anbimacom.balanceforsubaccountbrazil
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddressType }
     */
    public AddressType createAddressType() {
        return new AddressType();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData }
     */
    public BalForSubAcctBrData createBalForSubAcctBrData() {
        return new BalForSubAcctBrData();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.CcyFwdOptn }
     */
    public BalForSubAcctBrData.CcyFwdOptn createBalForSubAcctBrDataCcyFwdOptn() {
        return new BalForSubAcctBrData.CcyFwdOptn();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.CcyFwdOptn.Tp }
     */
    public BalForSubAcctBrData.CcyFwdOptn.Tp createBalForSubAcctBrDataCcyFwdOptnTp() {
        return new BalForSubAcctBrData.CcyFwdOptn.Tp();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.CcyOptn }
     */
    public BalForSubAcctBrData.CcyOptn createBalForSubAcctBrDataCcyOptn() {
        return new BalForSubAcctBrData.CcyOptn();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.Corp }
     */
    public BalForSubAcctBrData.Corp createBalForSubAcctBrDataCorp() {
        return new BalForSubAcctBrData.Corp();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.Corp.RiskLv }
     */
    public BalForSubAcctBrData.Corp.RiskLv createBalForSubAcctBrDataCorpRiskLv() {
        return new BalForSubAcctBrData.Corp.RiskLv();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.Debe }
     */
    public BalForSubAcctBrData.Debe createBalForSubAcctBrDataDebe() {
        return new BalForSubAcctBrData.Debe();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.Debe.RiskLv }
     */
    public BalForSubAcctBrData.Debe.RiskLv createBalForSubAcctBrDataDebeRiskLv() {
        return new BalForSubAcctBrData.Debe.RiskLv();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.Gove }
     */
    public BalForSubAcctBrData.Gove createBalForSubAcctBrDataGove() {
        return new BalForSubAcctBrData.Gove();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.Gove.RiskLv }
     */
    public BalForSubAcctBrData.Gove.RiskLv createBalForSubAcctBrDataGoveRiskLv() {
        return new BalForSubAcctBrData.Gove.RiskLv();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.Guar }
     */
    public BalForSubAcctBrData.Guar createBalForSubAcctBrDataGuar() {
        return new BalForSubAcctBrData.Guar();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.HedgTp }
     */
    public BalForSubAcctBrData.HedgTp createBalForSubAcctBrDataHedgTp() {
        return new BalForSubAcctBrData.HedgTp();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.PensPlanPart }
     */
    public BalForSubAcctBrData.PensPlanPart createBalForSubAcctBrDataPensPlanPart() {
        return new BalForSubAcctBrData.PensPlanPart();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.PensPlanPart.Pens }
     */
    public BalForSubAcctBrData.PensPlanPart.Pens createBalForSubAcctBrDataPensPlanPartPens() {
        return new BalForSubAcctBrData.PensPlanPart.Pens();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.PensPlanPart.Pens.Prtry }
     */
    public BalForSubAcctBrData.PensPlanPart.Pens.Prtry createBalForSubAcctBrDataPensPlanPartPensPrtry() {
        return new BalForSubAcctBrData.PensPlanPart.Pens.Prtry();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.RealStatePrtfl }
     */
    public BalForSubAcctBrData.RealStatePrtfl createBalForSubAcctBrDataRealStatePrtfl() {
        return new BalForSubAcctBrData.RealStatePrtfl();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.RealStatePrtfl.Enterprise }
     */
    public BalForSubAcctBrData.RealStatePrtfl.Enterprise createBalForSubAcctBrDataRealStatePrtflEnterprise() {
        return new BalForSubAcctBrData.RealStatePrtfl.Enterprise();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.RealStatePrtfl.Enterprise.Prtry }
     */
    public BalForSubAcctBrData.RealStatePrtfl.Enterprise.Prtry createBalForSubAcctBrDataRealStatePrtflEnterprisePrtry() {
        return new BalForSubAcctBrData.RealStatePrtfl.Enterprise.Prtry();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.RealStatePrtfl.Lease }
     */
    public BalForSubAcctBrData.RealStatePrtfl.Lease createBalForSubAcctBrDataRealStatePrtflLease() {
        return new BalForSubAcctBrData.RealStatePrtfl.Lease();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.RealStatePrtfl.Lease.Tp }
     */
    public BalForSubAcctBrData.RealStatePrtfl.Lease.Tp createBalForSubAcctBrDataRealStatePrtflLeaseTp() {
        return new BalForSubAcctBrData.RealStatePrtfl.Lease.Tp();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.RealStatePrtfl.LgObjtRsn }
     */
    public BalForSubAcctBrData.RealStatePrtfl.LgObjtRsn createBalForSubAcctBrDataRealStatePrtflLgObjtRsn() {
        return new BalForSubAcctBrData.RealStatePrtfl.LgObjtRsn();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.RealStatePrtfl.PropTp }
     */
    public BalForSubAcctBrData.RealStatePrtfl.PropTp createBalForSubAcctBrDataRealStatePrtflPropTp() {
        return new BalForSubAcctBrData.RealStatePrtfl.PropTp();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.RealStatePrtfl.RatBookVal }
     */
    public BalForSubAcctBrData.RealStatePrtfl.RatBookVal createBalForSubAcctBrDataRealStatePrtflRatBookVal() {
        return new BalForSubAcctBrData.RealStatePrtfl.RatBookVal();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.RealStatePrtfl.UseTp }
     */
    public BalForSubAcctBrData.RealStatePrtfl.UseTp createBalForSubAcctBrDataRealStatePrtflUseTp() {
        return new BalForSubAcctBrData.RealStatePrtfl.UseTp();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.RealStatePrtfl.ValtInf }
     */
    public BalForSubAcctBrData.RealStatePrtfl.ValtInf createBalForSubAcctBrDataRealStatePrtflValtInf() {
        return new BalForSubAcctBrData.RealStatePrtfl.ValtInf();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.RealStatePrtfl.ValtInf.EvaTp }
     */
    public BalForSubAcctBrData.RealStatePrtfl.ValtInf.EvaTp createBalForSubAcctBrDataRealStatePrtflValtInfEvaTp() {
        return new BalForSubAcctBrData.RealStatePrtfl.ValtInf.EvaTp();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.RealStatePrtfl.ValtInf.Prtry }
     */
    public BalForSubAcctBrData.RealStatePrtfl.ValtInf.Prtry createBalForSubAcctBrDataRealStatePrtflValtInfPrtry() {
        return new BalForSubAcctBrData.RealStatePrtfl.ValtInf.Prtry();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.RiskLv }
     */
    public BalForSubAcctBrData.RiskLv createBalForSubAcctBrDataRiskLv() {
        return new BalForSubAcctBrData.RiskLv();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.Swap }
     */
    public BalForSubAcctBrData.Swap createBalForSubAcctBrDataSwap() {
        return new BalForSubAcctBrData.Swap();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.Swap.Guar }
     */
    public BalForSubAcctBrData.Swap.Guar createBalForSubAcctBrDataSwapGuar() {
        return new BalForSubAcctBrData.Swap.Guar();
    }

    /**
     * Create an instance of {@link BalForSubAcctBrData.Swap.HedgTp }
     */
    public BalForSubAcctBrData.Swap.HedgTp createBalForSubAcctBrDataSwapHedgTp() {
        return new BalForSubAcctBrData.Swap.HedgTp();
    }

    /**
     * Create an instance of {@link BrkType }
     */
    public BrkType createBrkType() {
        return new BrkType();
    }

    /**
     * Create an instance of {@link BrkType.BrkId }
     */
    public BrkType.BrkId createBrkTypeBrkId() {
        return new BrkType.BrkId();
    }

    /**
     * Create an instance of {@link BrkType.BrkId.BrkfSvcrTp }
     */
    public BrkType.BrkId.BrkfSvcrTp createBrkTypeBrkIdBrkfSvcrTp() {
        return new BrkType.BrkId.BrkfSvcrTp();
    }

    /**
     * Create an instance of {@link BrkrType }
     */
    public BrkrType createBrkrType() {
        return new BrkrType();
    }

    /**
     * Create an instance of {@link BrkrType.BrkrPdVl }
     */
    public BrkrType.BrkrPdVl createBrkrTypeBrkrPdVl() {
        return new BrkrType.BrkrPdVl();
    }

    /**
     * Create an instance of {@link BrkrType.RebaVl }
     */
    public BrkrType.RebaVl createBrkrTypeRebaVl() {
        return new BrkrType.RebaVl();
    }

    /**
     * Create an instance of {@link EqulFeeType }
     */
    public EqulFeeType createEqulFeeType() {
        return new EqulFeeType();
    }

    /**
     * Create an instance of {@link EqulIndexType }
     */
    public EqulIndexType createEqulIndexType() {
        return new EqulIndexType();
    }

    /**
     * Create an instance of {@link EqulIndexType.Code }
     */
    public EqulIndexType.Code createEqulIndexTypeCode() {
        return new EqulIndexType.Code();
    }

    /**
     * Create an instance of {@link ExpnType }
     */
    public ExpnType createExpnType() {
        return new ExpnType();
    }

    /**
     * Create an instance of {@link NameAndAdressType }
     */
    public NameAndAdressType createNameAndAdressType() {
        return new NameAndAdressType();
    }

    /**
     * Create an instance of {@link PositionType }
     */
    public PositionType createPositionType() {
        return new PositionType();
    }

    /**
     * Create an instance of {@link PositionType.Indx }
     */
    public PositionType.Indx createPositionTypeIndx() {
        return new PositionType.Indx();
    }

    /**
     * Create an instance of {@link ReceInvstmtFndType }
     */
    public ReceInvstmtFndType createReceInvstmtFndType() {
        return new ReceInvstmtFndType();
    }

    /**
     * Create an instance of {@link ReceInvstmtFndType.TtlBookValChng }
     */
    public ReceInvstmtFndType.TtlBookValChng createReceInvstmtFndTypeTtlBookValChng() {
        return new ReceInvstmtFndType.TtlBookValChng();
    }

    /**
     * Create an instance of {@link ShHldgIdType }
     */
    public ShHldgIdType createShHldgIdType() {
        return new ShHldgIdType();
    }

    /**
     * Create an instance of {@link ShHldgIdType.TtlBookValChng }
     */
    public ShHldgIdType.TtlBookValChng createShHldgIdTypeTtlBookValChng() {
        return new ShHldgIdType.TtlBookValChng();
    }

    /**
     * Create an instance of {@link ShHldgType }
     */
    public ShHldgType createShHldgType() {
        return new ShHldgType();
    }

}
