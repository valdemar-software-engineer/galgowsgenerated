package br.anbimacom.balanceforsubaccountbrazil;

import iso.std.iso._20022.tech.xsd.semt_003_001.ActiveOrHistoricCurrencyAnd13DecimalAmount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ReceInvstmtFndType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ReceInvstmtFndType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TtlBookValChng"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyAnd13DecimalAmount"/&gt;
 *                   &lt;element name="Sgn" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PlusOrMinusIndicator"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceInvstmtFndType", propOrder = {"ttlBookValChng"})
public class ReceInvstmtFndType {

    @XmlElement(name = "TtlBookValChng", required = true) protected ReceInvstmtFndType.TtlBookValChng ttlBookValChng;

    /**
     * Obtém o valor da propriedade ttlBookValChng.
     *
     * @return possible object is
     * {@link ReceInvstmtFndType.TtlBookValChng }
     */
    public ReceInvstmtFndType.TtlBookValChng getTtlBookValChng() {
        return ttlBookValChng;
    }

    /**
     * Define o valor da propriedade ttlBookValChng.
     *
     * @param value allowed object is
     *              {@link ReceInvstmtFndType.TtlBookValChng }
     */
    public void setTtlBookValChng(ReceInvstmtFndType.TtlBookValChng value) {
        this.ttlBookValChng = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * <p>
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * <p>
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyAnd13DecimalAmount"/&gt;
     *         &lt;element name="Sgn" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PlusOrMinusIndicator"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"amt", "sgn"})
    public static class TtlBookValChng {

        @XmlElement(name = "Amt", required = true) protected ActiveOrHistoricCurrencyAnd13DecimalAmount amt;
        @XmlElement(name = "Sgn") protected boolean sgn;

        /**
         * Obtém o valor da propriedade amt.
         *
         * @return possible object is
         * {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
         */
        public ActiveOrHistoricCurrencyAnd13DecimalAmount getAmt() {
            return amt;
        }

        /**
         * Define o valor da propriedade amt.
         *
         * @param value allowed object is
         *              {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
         */
        public void setAmt(ActiveOrHistoricCurrencyAnd13DecimalAmount value) {
            this.amt = value;
        }

        /**
         * Obtém o valor da propriedade sgn.
         */
        public boolean isSgn() {
            return sgn;
        }

        /**
         * Define o valor da propriedade sgn.
         */
        public void setSgn(boolean value) {
            this.sgn = value;
        }

    }

}
