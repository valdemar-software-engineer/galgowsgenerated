package br.anbimacom.balanceforsubaccountbrazil;

import iso.std.iso._20022.tech.xsd.semt_003_001.GenericIdentification19;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Classe Java de PositionType complex type.
 *
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 *
 * <pre>
 * &lt;complexType name="PositionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Rt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PercentageRate"/&gt;
 *         &lt;element name="Indx" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Prtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification19"/&gt;
 *                   &lt;element name="Rt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PercentageRate"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Bal" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}DecimalNumber"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PositionType", propOrder = {"rt", "indx", "bal"})
public class PositionType {

    @XmlElement(name = "Rt", required = true) protected BigDecimal rt;
    @XmlElement(name = "Indx") protected Indx indx;
    @XmlElement(name = "Bal", required = true) protected BigDecimal bal;

    /**
     * Obtém o valor da propriedade bal.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    public BigDecimal getBal() {
        return bal;
    }

    /**
     * Define o valor da propriedade bal.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *
     */
    public void setBal(BigDecimal value) {
        this.bal = value;
    }

    /**
     * Obtém o valor da propriedade indx.
     *
     * @return
     *     possible object is
     *     {@link Indx }
     *
     */
    public Indx getIndx() {
        return indx;
    }

    /**
     * Define o valor da propriedade indx.
     *
     * @param value
     *     allowed object is
     *     {@link Indx }
     *
     */
    public void setIndx(Indx value) {
        this.indx = value;
    }

    /**
     * Obtém o valor da propriedade rt.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    public BigDecimal getRt() {
        return rt;
    }

    /**
     * Define o valor da propriedade rt.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *
     */
    public void setRt(BigDecimal value) {
        this.rt = value;
    }

    /**
     * <p>Classe Java de anonymous complex type.
     *
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Prtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification19"/&gt;
     *         &lt;element name="Rt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PercentageRate"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"prtry", "rt"}) public static class Indx {

        @XmlElement(name = "Prtry", required = true) protected GenericIdentification19 prtry;
        @XmlElement(name = "Rt", required = true) protected BigDecimal rt;

        /**
         * Obtém o valor da propriedade prtry.
         *
         * @return
         *     possible object is
         *     {@link GenericIdentification19 }
         *
         */
        public GenericIdentification19 getPrtry() {
            return prtry;
        }

        /**
         * Define o valor da propriedade prtry.
         *
         * @param value
         *     allowed object is
         *     {@link GenericIdentification19 }
         *
         */
        public void setPrtry(GenericIdentification19 value) {
            this.prtry = value;
        }

        /**
         * Obtém o valor da propriedade rt.
         *
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *
         */
        public BigDecimal getRt() {
            return rt;
        }

        /**
         * Define o valor da propriedade rt.
         *
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *
         */
        public void setRt(BigDecimal value) {
            this.rt = value;
        }

    }

}
