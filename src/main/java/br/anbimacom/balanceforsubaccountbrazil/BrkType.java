package br.anbimacom.balanceforsubaccountbrazil;

import iso.std.iso._20022.tech.xsd.semt_003_001.GenericIdentification20;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Brokerage - Corretagem
 * <p>
 * <p>Classe Java de BrkType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="BrkType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BrkId" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
 *                   &lt;element name="Prtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification20" minOccurs="0"/&gt;
 *                   &lt;element name="BrkfSvcrTp" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Cd"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                   &lt;enumeration value="REEX"/&gt;
 *                                   &lt;enumeration value="EXEC"/&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="TransactionNb" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}DecimalNumber" minOccurs="0"/&gt;
 *                   &lt;element name="Brkr" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}BrkrType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BrkType", propOrder = {"brkId"})
public class BrkType {

    @XmlElement(name = "BrkId", required = true) protected List<BrkType.BrkId> brkId;

    /**
     * Gets the value of the brkId property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the brkId property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBrkId().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BrkType.BrkId }
     */
    public List<BrkType.BrkId> getBrkId() {
        if (brkId == null) {
            brkId = new ArrayList<BrkType.BrkId>();
        }
        return this.brkId;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * <p>
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * <p>
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
     *         &lt;element name="Prtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification20" minOccurs="0"/&gt;
     *         &lt;element name="BrkfSvcrTp" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Cd"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                         &lt;enumeration value="REEX"/&gt;
     *                         &lt;enumeration value="EXEC"/&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="TransactionNb" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}DecimalNumber" minOccurs="0"/&gt;
     *         &lt;element name="Brkr" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}BrkrType" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"id", "prtry", "brkfSvcrTp", "transactionNb", "brkr"})
    public static class BrkId {

        @XmlElement(name = "Id") protected String id;
        @XmlElement(name = "Prtry") protected GenericIdentification20 prtry;
        @XmlElement(name = "BrkfSvcrTp") protected BrkType.BrkId.BrkfSvcrTp brkfSvcrTp;
        @XmlElement(name = "TransactionNb") protected BigDecimal transactionNb;
        @XmlElement(name = "Brkr", required = true) protected List<BrkrType> brkr;

        /**
         * Obtém o valor da propriedade brkfSvcrTp.
         *
         * @return possible object is
         * {@link BrkType.BrkId.BrkfSvcrTp }
         */
        public BrkType.BrkId.BrkfSvcrTp getBrkfSvcrTp() {
            return brkfSvcrTp;
        }

        /**
         * Define o valor da propriedade brkfSvcrTp.
         *
         * @param value allowed object is
         *              {@link BrkType.BrkId.BrkfSvcrTp }
         */
        public void setBrkfSvcrTp(BrkType.BrkId.BrkfSvcrTp value) {
            this.brkfSvcrTp = value;
        }

        /**
         * Gets the value of the brkr property.
         * <p>
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the brkr property.
         * <p>
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBrkr().add(newItem);
         * </pre>
         * <p>
         * <p>
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BrkrType }
         */
        public List<BrkrType> getBrkr() {
            if (brkr == null) {
                brkr = new ArrayList<BrkrType>();
            }
            return this.brkr;
        }

        /**
         * Obtém o valor da propriedade id.
         *
         * @return possible object is
         * {@link String }
         */
        public String getId() {
            return id;
        }

        /**
         * Define o valor da propriedade id.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setId(String value) {
            this.id = value;
        }

        /**
         * Obtém o valor da propriedade prtry.
         *
         * @return possible object is
         * {@link GenericIdentification20 }
         */
        public GenericIdentification20 getPrtry() {
            return prtry;
        }

        /**
         * Define o valor da propriedade prtry.
         *
         * @param value allowed object is
         *              {@link GenericIdentification20 }
         */
        public void setPrtry(GenericIdentification20 value) {
            this.prtry = value;
        }

        /**
         * Obtém o valor da propriedade transactionNb.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getTransactionNb() {
            return transactionNb;
        }

        /**
         * Define o valor da propriedade transactionNb.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setTransactionNb(BigDecimal value) {
            this.transactionNb = value;
        }

        /**
         * <p>Classe Java de anonymous complex type.
         * <p>
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * <p>
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Cd"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *               &lt;enumeration value="REEX"/&gt;
         *               &lt;enumeration value="EXEC"/&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {"cd"})
        public static class BrkfSvcrTp {

            @XmlElement(name = "Cd", required = true) protected String cd;

            /**
             * Obtém o valor da propriedade cd.
             *
             * @return possible object is
             * {@link String }
             */
            public String getCd() {
                return cd;
            }

            /**
             * Define o valor da propriedade cd.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setCd(String value) {
                this.cd = value;
            }

        }

    }

}
