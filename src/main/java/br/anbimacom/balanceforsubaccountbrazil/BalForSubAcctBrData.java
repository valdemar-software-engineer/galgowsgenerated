package br.anbimacom.balanceforsubaccountbrazil;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de anonymous complex type.
 *
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RiskLv" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Cd"&gt;
 *                     &lt;simpleType&gt;
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                         &lt;enumeration value="LOWW"/&gt;
 *                         &lt;enumeration value="MEDI"/&gt;
 *                         &lt;enumeration value="HIGH"/&gt;
 *                         &lt;enumeration value="MEHI"/&gt;
 *                       &lt;/restriction&gt;
 *                     &lt;/simpleType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PensPlanPart" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Pens" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
 *                             &lt;element name="Prtry" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Exact4AlphaNumericText"/&gt;
 *                                       &lt;element name="Issr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text"/&gt;
 *                                       &lt;element name="SchmeNm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="Percpart" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PercentageRate"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Expn" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}ExpnType" minOccurs="0"/&gt;
 *         &lt;element name="Brk" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}BrkType" minOccurs="0"/&gt;
 *         &lt;element name="ShHldg" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}ShHldgType" minOccurs="0"/&gt;
 *         &lt;element name="ReceInvstmtFnd" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}ReceInvstmtFndType" minOccurs="0"/&gt;
 *         &lt;element name="Guar" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Cd"&gt;
 *                     &lt;simpleType&gt;
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                         &lt;enumeration value="BOGU"/&gt;
 *                         &lt;enumeration value="NOGU"/&gt;
 *                         &lt;enumeration value="LOGU"/&gt;
 *                         &lt;enumeration value="SHGU"/&gt;
 *                       &lt;/restriction&gt;
 *                     &lt;/simpleType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="LongPos" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}PositionType" minOccurs="0"/&gt;
 *         &lt;element name="ShortPos" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}PositionType" minOccurs="0"/&gt;
 *         &lt;element name="StrtDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="HedgTp" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Cd"&gt;
 *                     &lt;simpleType&gt;
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                         &lt;enumeration value="NOHE"/&gt;
 *                         &lt;enumeration value="FIHE"/&gt;
 *                         &lt;enumeration value="EQHE"/&gt;
 *                       &lt;/restriction&gt;
 *                     &lt;/simpleType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Debe" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ConvInd" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *                   &lt;element name="ProfSharInd" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *                   &lt;element name="SpeInd" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *                   &lt;element name="RiskLv" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Cd"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                   &lt;enumeration value="LOWW"/&gt;
 *                                   &lt;enumeration value="MEDI"/&gt;
 *                                   &lt;enumeration value="HIGH"/&gt;
 *                                   &lt;enumeration value="MEHI"/&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Gove" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="RiskLv" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Cd"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                   &lt;enumeration value="LOWW"/&gt;
 *                                   &lt;enumeration value="MEDI"/&gt;
 *                                   &lt;enumeration value="HIGH"/&gt;
 *                                   &lt;enumeration value="MEHI"/&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Corp" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="RiskLv" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Cd"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                   &lt;enumeration value="LOWW"/&gt;
 *                                   &lt;enumeration value="MEDI"/&gt;
 *                                   &lt;enumeration value="HIGH"/&gt;
 *                                   &lt;enumeration value="MEHI"/&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="UndrlygSctiesMtrtyDt" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="Swap" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Guar"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Cd"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                   &lt;enumeration value="BOGU"/&gt;
 *                                   &lt;enumeration value="NOGU"/&gt;
 *                                   &lt;enumeration value="LOGU"/&gt;
 *                                   &lt;enumeration value="SHGU"/&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="LongPos" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}PositionType" minOccurs="0"/&gt;
 *                   &lt;element name="ShortPos" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}PositionType" minOccurs="0"/&gt;
 *                   &lt;element name="StrtDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODate" minOccurs="0"/&gt;
 *                   &lt;element name="HedgTp" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Cd"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                   &lt;enumeration value="NOHE"/&gt;
 *                                   &lt;enumeration value="FIHE"/&gt;
 *                                   &lt;enumeration value="EQHE"/&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="RealStatePrtfl" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="NmAndAdr" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}NameAndAdressType" minOccurs="0"/&gt;
 *                   &lt;element name="FundPartProperty" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *                   &lt;element name="BookVal" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *                   &lt;element name="RatBookVal" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Code"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                   &lt;enumeration value="NONE"/&gt;
 *                                   &lt;enumeration value="DEPR"/&gt;
 *                                   &lt;enumeration value="REPA"/&gt;
 *                                   &lt;enumeration value="REVA"/&gt;
 *                                   &lt;enumeration value="FVPP"/&gt;
 *                                   &lt;enumeration value="OTHR"/&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="ValtInf" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="TtlValAmt" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *                             &lt;element name="ValDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                             &lt;element name="EvaTp" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Cd"&gt;
 *                                         &lt;simpleType&gt;
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                             &lt;enumeration value="INDV"/&gt;
 *                                             &lt;enumeration value="CORP"/&gt;
 *                                           &lt;/restriction&gt;
 *                                         &lt;/simpleType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="Id" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}Max35Text" minOccurs="0"/&gt;
 *                             &lt;element name="Prtry" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Id"&gt;
 *                                         &lt;simpleType&gt;
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                             &lt;enumeration value="CNPJ"/&gt;
 *                                             &lt;enumeration value="CPF"/&gt;
 *                                           &lt;/restriction&gt;
 *                                         &lt;/simpleType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="Issr" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}Max35Text"/&gt;
 *                                       &lt;element name="SchmeNm" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}Max35Text" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Lease" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Tp"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Cd"&gt;
 *                                         &lt;simpleType&gt;
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                             &lt;enumeration value="AGRD"/&gt;
 *                                             &lt;enumeration value="DELA"/&gt;
 *                                           &lt;/restriction&gt;
 *                                         &lt;/simpleType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyAnd13DecimalAmount_SimpleType"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="RpOptnInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator"/&gt;
 *                   &lt;element name="RpOptnDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODate" minOccurs="0"/&gt;
 *                   &lt;element name="PropTp"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Cd"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                   &lt;enumeration value="INDU"/&gt;
 *                                   &lt;enumeration value="COMM"/&gt;
 *                                   &lt;enumeration value="STOR"/&gt;
 *                                   &lt;enumeration value="MALL"/&gt;
 *                                   &lt;enumeration value="HOSP"/&gt;
 *                                   &lt;enumeration value="HOTE"/&gt;
 *                                   &lt;enumeration value="THPA"/&gt;
 *                                   &lt;enumeration value="RURA"/&gt;
 *                                   &lt;enumeration value="RESI"/&gt;
 *                                   &lt;enumeration value="LOTT"/&gt;
 *                                   &lt;enumeration value="OTHR"/&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="LgObjtInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator"/&gt;
 *                   &lt;element name="LgObjtRsn" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Desc" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max140Text" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="UseTp" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Cd"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                   &lt;enumeration value="ENTE"/&gt;
 *                                   &lt;enumeration value="REIN"/&gt;
 *                                   &lt;enumeration value="REFD"/&gt;
 *                                   &lt;enumeration value="OWUS"/&gt;
 *                                   &lt;enumeration value="OTHR"/&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="RegnNb" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Enterprise" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
 *                             &lt;element name="Prtry" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Exact4AlphaNumericText"/&gt;
 *                                       &lt;element name="Issr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text"/&gt;
 *                                       &lt;element name="SchmeNm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CcyOptn" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Guar" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CcyFwdOptn" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Tp"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Cd"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                   &lt;enumeration value="FORW"/&gt;
 *                                   &lt;enumeration value="NDFW"/&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",
         propOrder = {"riskLv", "pensPlanPart", "expn", "brk", "shHldg", "receInvstmtFnd", "guar", "longPos", "shortPos", "strtDt", "hedgTp", "debe", "gove", "corp", "undrlygSctiesMtrtyDt", "swap", "realStatePrtfl", "ccyOptn", "ccyFwdOptn"})
@XmlRootElement(name = "BalForSubAcctBrData")
public class BalForSubAcctBrData {

    @XmlElement(name = "RiskLv") protected RiskLv riskLv;
    @XmlElement(name = "PensPlanPart") protected PensPlanPart pensPlanPart;
    @XmlElement(name = "Expn") protected ExpnType expn;
    @XmlElement(name = "Brk") protected BrkType brk;
    @XmlElement(name = "ShHldg") protected ShHldgType shHldg;
    @XmlElement(name = "ReceInvstmtFnd") protected ReceInvstmtFndType receInvstmtFnd;
    @XmlElement(name = "Guar") protected Guar guar;
    @XmlElement(name = "LongPos") protected PositionType longPos;
    @XmlElement(name = "ShortPos") protected PositionType shortPos;
    @XmlElement(name = "StrtDt") @XmlSchemaType(name = "date") protected XMLGregorianCalendar strtDt;
    @XmlElement(name = "HedgTp") protected HedgTp hedgTp;
    @XmlElement(name = "Debe") protected Debe debe;
    @XmlElement(name = "Gove") protected Gove gove;
    @XmlElement(name = "Corp") protected Corp corp;
    @XmlElement(name = "UndrlygSctiesMtrtyDt") @XmlSchemaType(name = "date") protected XMLGregorianCalendar
        undrlygSctiesMtrtyDt;
    @XmlElement(name = "Swap") protected Swap swap;
    @XmlElement(name = "RealStatePrtfl") protected RealStatePrtfl realStatePrtfl;
    @XmlElement(name = "CcyOptn") protected CcyOptn ccyOptn;
    @XmlElement(name = "CcyFwdOptn") protected CcyFwdOptn ccyFwdOptn;

    /**
     * Obtém o valor da propriedade brk.
     *
     * @return
     *     possible object is
     *     {@link BrkType }
     *
     */
    public BrkType getBrk() {
        return brk;
    }

    /**
     * Define o valor da propriedade brk.
     *
     * @param value
     *     allowed object is
     *     {@link BrkType }
     *
     */
    public void setBrk(BrkType value) {
        this.brk = value;
    }

    /**
     * Obtém o valor da propriedade ccyFwdOptn.
     *
     * @return
     *     possible object is
     *     {@link CcyFwdOptn }
     *
     */
    public CcyFwdOptn getCcyFwdOptn() {
        return ccyFwdOptn;
    }

    /**
     * Define o valor da propriedade ccyFwdOptn.
     *
     * @param value
     *     allowed object is
     *     {@link CcyFwdOptn }
     *
     */
    public void setCcyFwdOptn(CcyFwdOptn value) {
        this.ccyFwdOptn = value;
    }

    /**
     * Obtém o valor da propriedade ccyOptn.
     *
     * @return
     *     possible object is
     *     {@link CcyOptn }
     *
     */
    public CcyOptn getCcyOptn() {
        return ccyOptn;
    }

    /**
     * Define o valor da propriedade ccyOptn.
     *
     * @param value
     *     allowed object is
     *     {@link CcyOptn }
     *
     */
    public void setCcyOptn(CcyOptn value) {
        this.ccyOptn = value;
    }

    /**
     * Obtém o valor da propriedade corp.
     *
     * @return
     *     possible object is
     *     {@link Corp }
     *
     */
    public Corp getCorp() {
        return corp;
    }

    /**
     * Define o valor da propriedade corp.
     *
     * @param value
     *     allowed object is
     *     {@link Corp }
     *
     */
    public void setCorp(Corp value) {
        this.corp = value;
    }

    /**
     * Obtém o valor da propriedade debe.
     *
     * @return
     *     possible object is
     *     {@link Debe }
     *
     */
    public Debe getDebe() {
        return debe;
    }

    /**
     * Define o valor da propriedade debe.
     *
     * @param value
     *     allowed object is
     *     {@link Debe }
     *
     */
    public void setDebe(Debe value) {
        this.debe = value;
    }

    /**
     * Obtém o valor da propriedade expn.
     *
     * @return
     *     possible object is
     *     {@link ExpnType }
     *
     */
    public ExpnType getExpn() {
        return expn;
    }

    /**
     * Define o valor da propriedade expn.
     *
     * @param value
     *     allowed object is
     *     {@link ExpnType }
     *
     */
    public void setExpn(ExpnType value) {
        this.expn = value;
    }

    /**
     * Obtém o valor da propriedade gove.
     *
     * @return
     *     possible object is
     *     {@link Gove }
     *
     */
    public Gove getGove() {
        return gove;
    }

    /**
     * Define o valor da propriedade gove.
     *
     * @param value
     *     allowed object is
     *     {@link Gove }
     *
     */
    public void setGove(Gove value) {
        this.gove = value;
    }

    /**
     * Obtém o valor da propriedade guar.
     *
     * @return
     *     possible object is
     *     {@link Guar }
     *
     */
    public Guar getGuar() {
        return guar;
    }

    /**
     * Define o valor da propriedade guar.
     *
     * @param value
     *     allowed object is
     *     {@link Guar }
     *
     */
    public void setGuar(Guar value) {
        this.guar = value;
    }

    /**
     * Obtém o valor da propriedade hedgTp.
     *
     * @return
     *     possible object is
     *     {@link HedgTp }
     *
     */
    public HedgTp getHedgTp() {
        return hedgTp;
    }

    /**
     * Define o valor da propriedade hedgTp.
     *
     * @param value
     *     allowed object is
     *     {@link HedgTp }
     *
     */
    public void setHedgTp(HedgTp value) {
        this.hedgTp = value;
    }

    /**
     * Obtém o valor da propriedade longPos.
     *
     * @return
     *     possible object is
     *     {@link PositionType }
     *
     */
    public PositionType getLongPos() {
        return longPos;
    }

    /**
     * Define o valor da propriedade longPos.
     *
     * @param value
     *     allowed object is
     *     {@link PositionType }
     *
     */
    public void setLongPos(PositionType value) {
        this.longPos = value;
    }

    /**
     * Obtém o valor da propriedade pensPlanPart.
     *
     * @return
     *     possible object is
     *     {@link PensPlanPart }
     *
     */
    public PensPlanPart getPensPlanPart() {
        return pensPlanPart;
    }

    /**
     * Define o valor da propriedade pensPlanPart.
     *
     * @param value
     *     allowed object is
     *     {@link PensPlanPart }
     *
     */
    public void setPensPlanPart(PensPlanPart value) {
        this.pensPlanPart = value;
    }

    /**
     * Obtém o valor da propriedade realStatePrtfl.
     *
     * @return
     *     possible object is
     *     {@link RealStatePrtfl }
     *
     */
    public RealStatePrtfl getRealStatePrtfl() {
        return realStatePrtfl;
    }

    /**
     * Define o valor da propriedade realStatePrtfl.
     *
     * @param value
     *     allowed object is
     *     {@link RealStatePrtfl }
     *
     */
    public void setRealStatePrtfl(RealStatePrtfl value) {
        this.realStatePrtfl = value;
    }

    /**
     * Obtém o valor da propriedade receInvstmtFnd.
     *
     * @return
     *     possible object is
     *     {@link ReceInvstmtFndType }
     *
     */
    public ReceInvstmtFndType getReceInvstmtFnd() {
        return receInvstmtFnd;
    }

    /**
     * Define o valor da propriedade receInvstmtFnd.
     *
     * @param value
     *     allowed object is
     *     {@link ReceInvstmtFndType }
     *
     */
    public void setReceInvstmtFnd(ReceInvstmtFndType value) {
        this.receInvstmtFnd = value;
    }

    /**
     * Obtém o valor da propriedade riskLv.
     *
     * @return
     *     possible object is
     *     {@link RiskLv }
     *
     */
    public RiskLv getRiskLv() {
        return riskLv;
    }

    /**
     * Define o valor da propriedade riskLv.
     *
     * @param value
     *     allowed object is
     *     {@link RiskLv }
     *
     */
    public void setRiskLv(RiskLv value) {
        this.riskLv = value;
    }

    /**
     * Obtém o valor da propriedade shHldg.
     *
     * @return
     *     possible object is
     *     {@link ShHldgType }
     *
     */
    public ShHldgType getShHldg() {
        return shHldg;
    }

    /**
     * Define o valor da propriedade shHldg.
     *
     * @param value
     *     allowed object is
     *     {@link ShHldgType }
     *
     */
    public void setShHldg(ShHldgType value) {
        this.shHldg = value;
    }

    /**
     * Obtém o valor da propriedade shortPos.
     *
     * @return
     *     possible object is
     *     {@link PositionType }
     *
     */
    public PositionType getShortPos() {
        return shortPos;
    }

    /**
     * Define o valor da propriedade shortPos.
     *
     * @param value
     *     allowed object is
     *     {@link PositionType }
     *
     */
    public void setShortPos(PositionType value) {
        this.shortPos = value;
    }

    /**
     * Obtém o valor da propriedade strtDt.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getStrtDt() {
        return strtDt;
    }

    /**
     * Define o valor da propriedade strtDt.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setStrtDt(XMLGregorianCalendar value) {
        this.strtDt = value;
    }

    /**
     * Obtém o valor da propriedade swap.
     *
     * @return
     *     possible object is
     *     {@link Swap }
     *
     */
    public Swap getSwap() {
        return swap;
    }

    /**
     * Define o valor da propriedade swap.
     *
     * @param value
     *     allowed object is
     *     {@link Swap }
     *
     */
    public void setSwap(Swap value) {
        this.swap = value;
    }

    /**
     * Obtém o valor da propriedade undrlygSctiesMtrtyDt.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getUndrlygSctiesMtrtyDt() {
        return undrlygSctiesMtrtyDt;
    }

    /**
     * Define o valor da propriedade undrlygSctiesMtrtyDt.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setUndrlygSctiesMtrtyDt(XMLGregorianCalendar value) {
        this.undrlygSctiesMtrtyDt = value;
    }

    /**
     * <p>Classe Java de anonymous complex type.
     *
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Tp"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Cd"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                         &lt;enumeration value="FORW"/&gt;
     *                         &lt;enumeration value="NDFW"/&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"tp"}) public static class CcyFwdOptn {

        @XmlElement(name = "Tp", required = true) protected Tp tp;

        /**
         * Obtém o valor da propriedade tp.
         *
         * @return
         *     possible object is
         *     {@link Tp }
         *
         */
        public Tp getTp() {
            return tp;
        }

        /**
         * Define o valor da propriedade tp.
         *
         * @param value
         *     allowed object is
         *     {@link Tp }
         *
         */
        public void setTp(Tp value) {
            this.tp = value;
        }


        /**
         * <p>Classe Java de anonymous complex type.
         *
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Cd"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *               &lt;enumeration value="FORW"/&gt;
         *               &lt;enumeration value="NDFW"/&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"cd"}) public static class Tp {

            @XmlElement(name = "Cd", required = true) protected String cd;

            /**
             * Obtém o valor da propriedade cd.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCd() {
                return cd;
            }

            /**
             * Define o valor da propriedade cd.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCd(String value) {
                this.cd = value;
            }

        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     *
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Guar" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"guar"}) public static class CcyOptn {

        @XmlElement(name = "Guar") protected boolean guar;

        /**
         * Obtém o valor da propriedade guar.
         *
         */
        public boolean isGuar() {
            return guar;
        }

        /**
         * Define o valor da propriedade guar.
         *
         */
        public void setGuar(boolean value) {
            this.guar = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     *
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="RiskLv" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Cd"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                         &lt;enumeration value="LOWW"/&gt;
     *                         &lt;enumeration value="MEDI"/&gt;
     *                         &lt;enumeration value="HIGH"/&gt;
     *                         &lt;enumeration value="MEHI"/&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"riskLv"}) public static class Corp {

        @XmlElement(name = "RiskLv") protected RiskLv riskLv;

        /**
         * Obtém o valor da propriedade riskLv.
         *
         * @return
         *     possible object is
         *     {@link RiskLv }
         *
         */
        public RiskLv getRiskLv() {
            return riskLv;
        }

        /**
         * Define o valor da propriedade riskLv.
         *
         * @param value
         *     allowed object is
         *     {@link RiskLv }
         *
         */
        public void setRiskLv(RiskLv value) {
            this.riskLv = value;
        }


        /**
         * <p>Classe Java de anonymous complex type.
         *
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Cd"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *               &lt;enumeration value="LOWW"/&gt;
         *               &lt;enumeration value="MEDI"/&gt;
         *               &lt;enumeration value="HIGH"/&gt;
         *               &lt;enumeration value="MEHI"/&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"cd"}) public static class RiskLv {

            @XmlElement(name = "Cd", required = true) protected String cd;

            /**
             * Obtém o valor da propriedade cd.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCd() {
                return cd;
            }

            /**
             * Define o valor da propriedade cd.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCd(String value) {
                this.cd = value;
            }

        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     *
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ConvInd" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
     *         &lt;element name="ProfSharInd" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
     *         &lt;element name="SpeInd" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
     *         &lt;element name="RiskLv" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Cd"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                         &lt;enumeration value="LOWW"/&gt;
     *                         &lt;enumeration value="MEDI"/&gt;
     *                         &lt;enumeration value="HIGH"/&gt;
     *                         &lt;enumeration value="MEHI"/&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "",
                                                   propOrder = {"convInd", "profSharInd", "speInd", "riskLv"}) public static class Debe {

        @XmlElement(name = "ConvInd") protected boolean convInd;
        @XmlElement(name = "ProfSharInd") protected boolean profSharInd;
        @XmlElement(name = "SpeInd") protected boolean speInd;
        @XmlElement(name = "RiskLv") protected RiskLv riskLv;

        /**
         * Obtém o valor da propriedade riskLv.
         *
         * @return
         *     possible object is
         *     {@link RiskLv }
         *
         */
        public RiskLv getRiskLv() {
            return riskLv;
        }

        /**
         * Define o valor da propriedade riskLv.
         *
         * @param value
         *     allowed object is
         *     {@link RiskLv }
         *
         */
        public void setRiskLv(RiskLv value) {
            this.riskLv = value;
        }

        /**
         * Obtém o valor da propriedade convInd.
         *
         */
        public boolean isConvInd() {
            return convInd;
        }

        /**
         * Define o valor da propriedade convInd.
         *
         */
        public void setConvInd(boolean value) {
            this.convInd = value;
        }

        /**
         * Obtém o valor da propriedade profSharInd.
         *
         */
        public boolean isProfSharInd() {
            return profSharInd;
        }

        /**
         * Define o valor da propriedade profSharInd.
         *
         */
        public void setProfSharInd(boolean value) {
            this.profSharInd = value;
        }

        /**
         * Obtém o valor da propriedade speInd.
         *
         */
        public boolean isSpeInd() {
            return speInd;
        }

        /**
         * Define o valor da propriedade speInd.
         *
         */
        public void setSpeInd(boolean value) {
            this.speInd = value;
        }

        /**
         * <p>Classe Java de anonymous complex type.
         *
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Cd"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *               &lt;enumeration value="LOWW"/&gt;
         *               &lt;enumeration value="MEDI"/&gt;
         *               &lt;enumeration value="HIGH"/&gt;
         *               &lt;enumeration value="MEHI"/&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"cd"}) public static class RiskLv {

            @XmlElement(name = "Cd", required = true) protected String cd;

            /**
             * Obtém o valor da propriedade cd.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCd() {
                return cd;
            }

            /**
             * Define o valor da propriedade cd.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCd(String value) {
                this.cd = value;
            }

        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     *
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="RiskLv" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Cd"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                         &lt;enumeration value="LOWW"/&gt;
     *                         &lt;enumeration value="MEDI"/&gt;
     *                         &lt;enumeration value="HIGH"/&gt;
     *                         &lt;enumeration value="MEHI"/&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"riskLv"}) public static class Gove {

        @XmlElement(name = "RiskLv") protected RiskLv riskLv;

        /**
         * Obtém o valor da propriedade riskLv.
         *
         * @return
         *     possible object is
         *     {@link RiskLv }
         *
         */
        public RiskLv getRiskLv() {
            return riskLv;
        }

        /**
         * Define o valor da propriedade riskLv.
         *
         * @param value
         *     allowed object is
         *     {@link RiskLv }
         *
         */
        public void setRiskLv(RiskLv value) {
            this.riskLv = value;
        }


        /**
         * <p>Classe Java de anonymous complex type.
         *
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Cd"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *               &lt;enumeration value="LOWW"/&gt;
         *               &lt;enumeration value="MEDI"/&gt;
         *               &lt;enumeration value="HIGH"/&gt;
         *               &lt;enumeration value="MEHI"/&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"cd"}) public static class RiskLv {

            @XmlElement(name = "Cd", required = true) protected String cd;

            /**
             * Obtém o valor da propriedade cd.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCd() {
                return cd;
            }

            /**
             * Define o valor da propriedade cd.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCd(String value) {
                this.cd = value;
            }

        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     *
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Cd"&gt;
     *           &lt;simpleType&gt;
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *               &lt;enumeration value="BOGU"/&gt;
     *               &lt;enumeration value="NOGU"/&gt;
     *               &lt;enumeration value="LOGU"/&gt;
     *               &lt;enumeration value="SHGU"/&gt;
     *             &lt;/restriction&gt;
     *           &lt;/simpleType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"cd"}) public static class Guar {

        @XmlElement(name = "Cd", required = true) protected String cd;

        /**
         * Obtém o valor da propriedade cd.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCd() {
            return cd;
        }

        /**
         * Define o valor da propriedade cd.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCd(String value) {
            this.cd = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     *
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Cd"&gt;
     *           &lt;simpleType&gt;
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *               &lt;enumeration value="NOHE"/&gt;
     *               &lt;enumeration value="FIHE"/&gt;
     *               &lt;enumeration value="EQHE"/&gt;
     *             &lt;/restriction&gt;
     *           &lt;/simpleType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"cd"}) public static class HedgTp {

        @XmlElement(name = "Cd", required = true) protected String cd;

        /**
         * Obtém o valor da propriedade cd.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCd() {
            return cd;
        }

        /**
         * Define o valor da propriedade cd.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCd(String value) {
            this.cd = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     *
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Pens" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
     *                   &lt;element name="Prtry" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Exact4AlphaNumericText"/&gt;
     *                             &lt;element name="Issr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text"/&gt;
     *                             &lt;element name="SchmeNm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="Percpart" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PercentageRate"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"pens"}) public static class PensPlanPart {

        @XmlElement(name = "Pens", required = true) protected List<Pens> pens;

        /**
         * Gets the value of the pens property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the pens property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPens().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Pens }
         *
         *
         */
        public List<Pens> getPens() {
            if (pens == null) {
                pens = new ArrayList<Pens>();
            }
            return this.pens;
        }


        /**
         * <p>Classe Java de anonymous complex type.
         *
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
         *         &lt;element name="Prtry" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Exact4AlphaNumericText"/&gt;
         *                   &lt;element name="Issr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text"/&gt;
         *                   &lt;element name="SchmeNm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="Percpart" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PercentageRate"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "",
                                                       propOrder = {"id", "prtry", "percpart"}) public static class Pens {

            @XmlElement(name = "Id") protected String id;
            @XmlElement(name = "Prtry") protected Prtry prtry;
            @XmlElement(name = "Percpart", required = true) protected BigDecimal percpart;

            /**
             * Obtém o valor da propriedade id.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getId() {
                return id;
            }

            /**
             * Define o valor da propriedade id.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setId(String value) {
                this.id = value;
            }

            /**
             * Obtém o valor da propriedade percpart.
             *
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *
             */
            public BigDecimal getPercpart() {
                return percpart;
            }

            /**
             * Define o valor da propriedade percpart.
             *
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *
             */
            public void setPercpart(BigDecimal value) {
                this.percpart = value;
            }

            /**
             * Obtém o valor da propriedade prtry.
             *
             * @return
             *     possible object is
             *     {@link Prtry }
             *
             */
            public Prtry getPrtry() {
                return prtry;
            }

            /**
             * Define o valor da propriedade prtry.
             *
             * @param value
             *     allowed object is
             *     {@link Prtry }
             *
             */
            public void setPrtry(Prtry value) {
                this.prtry = value;
            }

            /**
             * <p>Classe Java de anonymous complex type.
             *
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Exact4AlphaNumericText"/&gt;
             *         &lt;element name="Issr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text"/&gt;
             *         &lt;element name="SchmeNm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "",
                                                           propOrder = {"id", "issr", "schmeNm"}) public static class Prtry {

                @XmlElement(name = "Id", required = true) protected String id;
                @XmlElement(name = "Issr", required = true) protected String issr;
                @XmlElement(name = "SchmeNm") protected String schmeNm;

                /**
                 * Obtém o valor da propriedade id.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getId() {
                    return id;
                }

                /**
                 * Define o valor da propriedade id.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setId(String value) {
                    this.id = value;
                }

                /**
                 * Obtém o valor da propriedade issr.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getIssr() {
                    return issr;
                }

                /**
                 * Define o valor da propriedade issr.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setIssr(String value) {
                    this.issr = value;
                }

                /**
                 * Obtém o valor da propriedade schmeNm.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getSchmeNm() {
                    return schmeNm;
                }

                /**
                 * Define o valor da propriedade schmeNm.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setSchmeNm(String value) {
                    this.schmeNm = value;
                }

            }

        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     *
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="NmAndAdr" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}NameAndAdressType" minOccurs="0"/&gt;
     *         &lt;element name="FundPartProperty" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
     *         &lt;element name="BookVal" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
     *         &lt;element name="RatBookVal" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Code"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                         &lt;enumeration value="NONE"/&gt;
     *                         &lt;enumeration value="DEPR"/&gt;
     *                         &lt;enumeration value="REPA"/&gt;
     *                         &lt;enumeration value="REVA"/&gt;
     *                         &lt;enumeration value="FVPP"/&gt;
     *                         &lt;enumeration value="OTHR"/&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="ValtInf" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="TtlValAmt" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
     *                   &lt;element name="ValDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *                   &lt;element name="EvaTp" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Cd"&gt;
     *                               &lt;simpleType&gt;
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                                   &lt;enumeration value="INDV"/&gt;
     *                                   &lt;enumeration value="CORP"/&gt;
     *                                 &lt;/restriction&gt;
     *                               &lt;/simpleType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="Id" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}Max35Text" minOccurs="0"/&gt;
     *                   &lt;element name="Prtry" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Id"&gt;
     *                               &lt;simpleType&gt;
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                                   &lt;enumeration value="CNPJ"/&gt;
     *                                   &lt;enumeration value="CPF"/&gt;
     *                                 &lt;/restriction&gt;
     *                               &lt;/simpleType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="Issr" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}Max35Text"/&gt;
     *                             &lt;element name="SchmeNm" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}Max35Text" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Lease" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Tp"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Cd"&gt;
     *                               &lt;simpleType&gt;
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                                   &lt;enumeration value="AGRD"/&gt;
     *                                   &lt;enumeration value="DELA"/&gt;
     *                                 &lt;/restriction&gt;
     *                               &lt;/simpleType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyAnd13DecimalAmount_SimpleType"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="RpOptnInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator"/&gt;
     *         &lt;element name="RpOptnDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODate" minOccurs="0"/&gt;
     *         &lt;element name="PropTp"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Cd"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                         &lt;enumeration value="INDU"/&gt;
     *                         &lt;enumeration value="COMM"/&gt;
     *                         &lt;enumeration value="STOR"/&gt;
     *                         &lt;enumeration value="MALL"/&gt;
     *                         &lt;enumeration value="HOSP"/&gt;
     *                         &lt;enumeration value="HOTE"/&gt;
     *                         &lt;enumeration value="THPA"/&gt;
     *                         &lt;enumeration value="RURA"/&gt;
     *                         &lt;enumeration value="RESI"/&gt;
     *                         &lt;enumeration value="LOTT"/&gt;
     *                         &lt;enumeration value="OTHR"/&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="LgObjtInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator"/&gt;
     *         &lt;element name="LgObjtRsn" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Desc" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max140Text" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="UseTp" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Cd"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                         &lt;enumeration value="ENTE"/&gt;
     *                         &lt;enumeration value="REIN"/&gt;
     *                         &lt;enumeration value="REFD"/&gt;
     *                         &lt;enumeration value="OWUS"/&gt;
     *                         &lt;enumeration value="OTHR"/&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="RegnNb" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Enterprise" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
     *                   &lt;element name="Prtry" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Exact4AlphaNumericText"/&gt;
     *                             &lt;element name="Issr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text"/&gt;
     *                             &lt;element name="SchmeNm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "",
                                                   propOrder = {"nmAndAdr", "fundPartProperty", "bookVal", "ratBookVal", "valtInf", "lease", "rpOptnInd", "rpOptnDt", "propTp", "lgObjtInd", "lgObjtRsn", "useTp", "enterprise"}) public static class RealStatePrtfl {

        @XmlElement(name = "NmAndAdr") protected NameAndAdressType nmAndAdr;
        @XmlElement(name = "FundPartProperty") protected double fundPartProperty;
        @XmlElement(name = "BookVal") protected double bookVal;
        @XmlElement(name = "RatBookVal") protected RatBookVal ratBookVal;
        @XmlElement(name = "ValtInf") protected ValtInf valtInf;
        @XmlElement(name = "Lease") protected List<Lease> lease;
        @XmlElement(name = "RpOptnInd") protected boolean rpOptnInd;
        @XmlElement(name = "RpOptnDt") @XmlSchemaType(name = "date") protected XMLGregorianCalendar rpOptnDt;
        @XmlElement(name = "PropTp", required = true) protected PropTp propTp;
        @XmlElement(name = "LgObjtInd") protected boolean lgObjtInd;
        @XmlElement(name = "LgObjtRsn") protected LgObjtRsn lgObjtRsn;
        @XmlElement(name = "UseTp") protected UseTp useTp;
        @XmlElement(name = "Enterprise") protected List<Enterprise> enterprise;

        /**
         * Obtém o valor da propriedade bookVal.
         *
         */
        public double getBookVal() {
            return bookVal;
        }

        /**
         * Define o valor da propriedade bookVal.
         *
         */
        public void setBookVal(double value) {
            this.bookVal = value;
        }

        /**
         * Gets the value of the enterprise property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the enterprise property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEnterprise().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Enterprise }
         *
         *
         */
        public List<Enterprise> getEnterprise() {
            if (enterprise == null) {
                enterprise = new ArrayList<Enterprise>();
            }
            return this.enterprise;
        }

        /**
         * Obtém o valor da propriedade fundPartProperty.
         *
         */
        public double getFundPartProperty() {
            return fundPartProperty;
        }

        /**
         * Define o valor da propriedade fundPartProperty.
         *
         */
        public void setFundPartProperty(double value) {
            this.fundPartProperty = value;
        }

        /**
         * Gets the value of the lease property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the lease property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLease().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Lease }
         *
         *
         */
        public List<Lease> getLease() {
            if (lease == null) {
                lease = new ArrayList<Lease>();
            }
            return this.lease;
        }

        /**
         * Obtém o valor da propriedade lgObjtRsn.
         *
         * @return
         *     possible object is
         *     {@link LgObjtRsn }
         *
         */
        public LgObjtRsn getLgObjtRsn() {
            return lgObjtRsn;
        }

        /**
         * Define o valor da propriedade lgObjtRsn.
         *
         * @param value
         *     allowed object is
         *     {@link LgObjtRsn }
         *
         */
        public void setLgObjtRsn(LgObjtRsn value) {
            this.lgObjtRsn = value;
        }

        /**
         * Obtém o valor da propriedade nmAndAdr.
         *
         * @return
         *     possible object is
         *     {@link NameAndAdressType }
         *
         */
        public NameAndAdressType getNmAndAdr() {
            return nmAndAdr;
        }

        /**
         * Define o valor da propriedade nmAndAdr.
         *
         * @param value
         *     allowed object is
         *     {@link NameAndAdressType }
         *
         */
        public void setNmAndAdr(NameAndAdressType value) {
            this.nmAndAdr = value;
        }

        /**
         * Obtém o valor da propriedade propTp.
         *
         * @return
         *     possible object is
         *     {@link PropTp }
         *
         */
        public PropTp getPropTp() {
            return propTp;
        }

        /**
         * Define o valor da propriedade propTp.
         *
         * @param value
         *     allowed object is
         *     {@link PropTp }
         *
         */
        public void setPropTp(PropTp value) {
            this.propTp = value;
        }

        /**
         * Obtém o valor da propriedade ratBookVal.
         *
         * @return
         *     possible object is
         *     {@link RatBookVal }
         *
         */
        public RatBookVal getRatBookVal() {
            return ratBookVal;
        }

        /**
         * Define o valor da propriedade ratBookVal.
         *
         * @param value
         *     allowed object is
         *     {@link RatBookVal }
         *
         */
        public void setRatBookVal(RatBookVal value) {
            this.ratBookVal = value;
        }

        /**
         * Obtém o valor da propriedade rpOptnDt.
         *
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *
         */
        public XMLGregorianCalendar getRpOptnDt() {
            return rpOptnDt;
        }

        /**
         * Define o valor da propriedade rpOptnDt.
         *
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *
         */
        public void setRpOptnDt(XMLGregorianCalendar value) {
            this.rpOptnDt = value;
        }

        /**
         * Obtém o valor da propriedade useTp.
         *
         * @return
         *     possible object is
         *     {@link UseTp }
         *
         */
        public UseTp getUseTp() {
            return useTp;
        }

        /**
         * Define o valor da propriedade useTp.
         *
         * @param value
         *     allowed object is
         *     {@link UseTp }
         *
         */
        public void setUseTp(UseTp value) {
            this.useTp = value;
        }

        /**
         * Obtém o valor da propriedade valtInf.
         *
         * @return
         *     possible object is
         *     {@link ValtInf }
         *
         */
        public ValtInf getValtInf() {
            return valtInf;
        }

        /**
         * Define o valor da propriedade valtInf.
         *
         * @param value
         *     allowed object is
         *     {@link ValtInf }
         *
         */
        public void setValtInf(ValtInf value) {
            this.valtInf = value;
        }

        /**
         * Obtém o valor da propriedade lgObjtInd.
         *
         */
        public boolean isLgObjtInd() {
            return lgObjtInd;
        }

        /**
         * Define o valor da propriedade lgObjtInd.
         *
         */
        public void setLgObjtInd(boolean value) {
            this.lgObjtInd = value;
        }

        /**
         * Obtém o valor da propriedade rpOptnInd.
         *
         */
        public boolean isRpOptnInd() {
            return rpOptnInd;
        }

        /**
         * Define o valor da propriedade rpOptnInd.
         *
         */
        public void setRpOptnInd(boolean value) {
            this.rpOptnInd = value;
        }

        /**
         * <p>Classe Java de anonymous complex type.
         *
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
         *         &lt;element name="Prtry" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Exact4AlphaNumericText"/&gt;
         *                   &lt;element name="Issr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text"/&gt;
         *                   &lt;element name="SchmeNm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "",
                                                       propOrder = {"id", "prtry"}) public static class Enterprise {

            @XmlElement(name = "Id") protected String id;
            @XmlElement(name = "Prtry") protected Prtry prtry;

            /**
             * Obtém o valor da propriedade id.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getId() {
                return id;
            }

            /**
             * Define o valor da propriedade id.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setId(String value) {
                this.id = value;
            }

            /**
             * Obtém o valor da propriedade prtry.
             *
             * @return
             *     possible object is
             *     {@link Prtry }
             *
             */
            public Prtry getPrtry() {
                return prtry;
            }

            /**
             * Define o valor da propriedade prtry.
             *
             * @param value
             *     allowed object is
             *     {@link Prtry }
             *
             */
            public void setPrtry(Prtry value) {
                this.prtry = value;
            }


            /**
             * <p>Classe Java de anonymous complex type.
             *
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Exact4AlphaNumericText"/&gt;
             *         &lt;element name="Issr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text"/&gt;
             *         &lt;element name="SchmeNm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "",
                                                           propOrder = {"id", "issr", "schmeNm"}) public static class Prtry {

                @XmlElement(name = "Id", required = true) protected String id;
                @XmlElement(name = "Issr", required = true) protected String issr;
                @XmlElement(name = "SchmeNm") protected String schmeNm;

                /**
                 * Obtém o valor da propriedade id.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getId() {
                    return id;
                }

                /**
                 * Define o valor da propriedade id.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setId(String value) {
                    this.id = value;
                }

                /**
                 * Obtém o valor da propriedade issr.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getIssr() {
                    return issr;
                }

                /**
                 * Define o valor da propriedade issr.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setIssr(String value) {
                    this.issr = value;
                }

                /**
                 * Obtém o valor da propriedade schmeNm.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getSchmeNm() {
                    return schmeNm;
                }

                /**
                 * Define o valor da propriedade schmeNm.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setSchmeNm(String value) {
                    this.schmeNm = value;
                }

            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         *
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Tp"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Cd"&gt;
         *                     &lt;simpleType&gt;
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *                         &lt;enumeration value="AGRD"/&gt;
         *                         &lt;enumeration value="DELA"/&gt;
         *                       &lt;/restriction&gt;
         *                     &lt;/simpleType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyAnd13DecimalAmount_SimpleType"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"tp", "amt"}) public static class Lease {

            @XmlElement(name = "Tp", required = true) protected Tp tp;
            @XmlElement(name = "Amt", required = true) protected BigDecimal amt;

            /**
             * Obtém o valor da propriedade amt.
             *
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *
             */
            public BigDecimal getAmt() {
                return amt;
            }

            /**
             * Define o valor da propriedade amt.
             *
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *
             */
            public void setAmt(BigDecimal value) {
                this.amt = value;
            }

            /**
             * Obtém o valor da propriedade tp.
             *
             * @return
             *     possible object is
             *     {@link Tp }
             *
             */
            public Tp getTp() {
                return tp;
            }

            /**
             * Define o valor da propriedade tp.
             *
             * @param value
             *     allowed object is
             *     {@link Tp }
             *
             */
            public void setTp(Tp value) {
                this.tp = value;
            }

            /**
             * <p>Classe Java de anonymous complex type.
             *
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Cd"&gt;
             *           &lt;simpleType&gt;
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
             *               &lt;enumeration value="AGRD"/&gt;
             *               &lt;enumeration value="DELA"/&gt;
             *             &lt;/restriction&gt;
             *           &lt;/simpleType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"cd"}) public static class Tp {

                @XmlElement(name = "Cd", required = true) protected String cd;

                /**
                 * Obtém o valor da propriedade cd.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getCd() {
                    return cd;
                }

                /**
                 * Define o valor da propriedade cd.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setCd(String value) {
                    this.cd = value;
                }

            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         *
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Desc" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max140Text" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"desc"}) public static class LgObjtRsn {

            @XmlElement(name = "Desc") protected String desc;

            /**
             * Obtém o valor da propriedade desc.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getDesc() {
                return desc;
            }

            /**
             * Define o valor da propriedade desc.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setDesc(String value) {
                this.desc = value;
            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         *
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Cd"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *               &lt;enumeration value="INDU"/&gt;
         *               &lt;enumeration value="COMM"/&gt;
         *               &lt;enumeration value="STOR"/&gt;
         *               &lt;enumeration value="MALL"/&gt;
         *               &lt;enumeration value="HOSP"/&gt;
         *               &lt;enumeration value="HOTE"/&gt;
         *               &lt;enumeration value="THPA"/&gt;
         *               &lt;enumeration value="RURA"/&gt;
         *               &lt;enumeration value="RESI"/&gt;
         *               &lt;enumeration value="LOTT"/&gt;
         *               &lt;enumeration value="OTHR"/&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"cd"}) public static class PropTp {

            @XmlElement(name = "Cd", required = true) protected String cd;

            /**
             * Obtém o valor da propriedade cd.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCd() {
                return cd;
            }

            /**
             * Define o valor da propriedade cd.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCd(String value) {
                this.cd = value;
            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         *
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Code"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *               &lt;enumeration value="NONE"/&gt;
         *               &lt;enumeration value="DEPR"/&gt;
         *               &lt;enumeration value="REPA"/&gt;
         *               &lt;enumeration value="REVA"/&gt;
         *               &lt;enumeration value="FVPP"/&gt;
         *               &lt;enumeration value="OTHR"/&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"code"}) public static class RatBookVal {

            @XmlElement(name = "Code", required = true) protected String code;

            /**
             * Obtém o valor da propriedade code.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCode() {
                return code;
            }

            /**
             * Define o valor da propriedade code.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCode(String value) {
                this.code = value;
            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         *
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Cd"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *               &lt;enumeration value="ENTE"/&gt;
         *               &lt;enumeration value="REIN"/&gt;
         *               &lt;enumeration value="REFD"/&gt;
         *               &lt;enumeration value="OWUS"/&gt;
         *               &lt;enumeration value="OTHR"/&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="RegnNb" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "",
                                                       propOrder = {"cd", "regnNb"}) public static class UseTp {

            @XmlElement(name = "Cd", required = true) protected String cd;
            @XmlElement(name = "RegnNb") protected String regnNb;

            /**
             * Obtém o valor da propriedade cd.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCd() {
                return cd;
            }

            /**
             * Define o valor da propriedade cd.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCd(String value) {
                this.cd = value;
            }

            /**
             * Obtém o valor da propriedade regnNb.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getRegnNb() {
                return regnNb;
            }

            /**
             * Define o valor da propriedade regnNb.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setRegnNb(String value) {
                this.regnNb = value;
            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         *
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="TtlValAmt" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
         *         &lt;element name="ValDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
         *         &lt;element name="EvaTp" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Cd"&gt;
         *                     &lt;simpleType&gt;
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *                         &lt;enumeration value="INDV"/&gt;
         *                         &lt;enumeration value="CORP"/&gt;
         *                       &lt;/restriction&gt;
         *                     &lt;/simpleType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="Id" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}Max35Text" minOccurs="0"/&gt;
         *         &lt;element name="Prtry" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Id"&gt;
         *                     &lt;simpleType&gt;
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *                         &lt;enumeration value="CNPJ"/&gt;
         *                         &lt;enumeration value="CPF"/&gt;
         *                       &lt;/restriction&gt;
         *                     &lt;/simpleType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="Issr" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}Max35Text"/&gt;
         *                   &lt;element name="SchmeNm" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}Max35Text" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "",
                                                       propOrder = {"ttlValAmt", "valDate", "evaTp", "id", "prtry"}) public static class ValtInf {

            @XmlElement(name = "TtlValAmt") protected Double ttlValAmt;
            @XmlElement(name = "ValDate") @XmlSchemaType(name = "date") protected XMLGregorianCalendar valDate;
            @XmlElement(name = "EvaTp") protected EvaTp evaTp;
            @XmlElement(name = "Id") protected String id;
            @XmlElement(name = "Prtry") protected Prtry prtry;

            /**
             * Obtém o valor da propriedade evaTp.
             *
             * @return
             *     possible object is
             *     {@link EvaTp }
             *
             */
            public EvaTp getEvaTp() {
                return evaTp;
            }

            /**
             * Define o valor da propriedade evaTp.
             *
             * @param value
             *     allowed object is
             *     {@link EvaTp }
             *
             */
            public void setEvaTp(EvaTp value) {
                this.evaTp = value;
            }

            /**
             * Obtém o valor da propriedade id.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getId() {
                return id;
            }

            /**
             * Define o valor da propriedade id.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setId(String value) {
                this.id = value;
            }

            /**
             * Obtém o valor da propriedade prtry.
             *
             * @return
             *     possible object is
             *     {@link Prtry }
             *
             */
            public Prtry getPrtry() {
                return prtry;
            }

            /**
             * Define o valor da propriedade prtry.
             *
             * @param value
             *     allowed object is
             *     {@link Prtry }
             *
             */
            public void setPrtry(Prtry value) {
                this.prtry = value;
            }

            /**
             * Obtém o valor da propriedade ttlValAmt.
             *
             * @return
             *     possible object is
             *     {@link Double }
             *
             */
            public Double getTtlValAmt() {
                return ttlValAmt;
            }

            /**
             * Define o valor da propriedade ttlValAmt.
             *
             * @param value
             *     allowed object is
             *     {@link Double }
             *
             */
            public void setTtlValAmt(Double value) {
                this.ttlValAmt = value;
            }

            /**
             * Obtém o valor da propriedade valDate.
             *
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *
             */
            public XMLGregorianCalendar getValDate() {
                return valDate;
            }

            /**
             * Define o valor da propriedade valDate.
             *
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *
             */
            public void setValDate(XMLGregorianCalendar value) {
                this.valDate = value;
            }

            /**
             * <p>Classe Java de anonymous complex type.
             *
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Cd"&gt;
             *           &lt;simpleType&gt;
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
             *               &lt;enumeration value="INDV"/&gt;
             *               &lt;enumeration value="CORP"/&gt;
             *             &lt;/restriction&gt;
             *           &lt;/simpleType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"cd"}) public static class EvaTp {

                @XmlElement(name = "Cd", required = true) protected String cd;

                /**
                 * Obtém o valor da propriedade cd.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getCd() {
                    return cd;
                }

                /**
                 * Define o valor da propriedade cd.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setCd(String value) {
                    this.cd = value;
                }

            }


            /**
             * <p>Classe Java de anonymous complex type.
             *
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Id"&gt;
             *           &lt;simpleType&gt;
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
             *               &lt;enumeration value="CNPJ"/&gt;
             *               &lt;enumeration value="CPF"/&gt;
             *             &lt;/restriction&gt;
             *           &lt;/simpleType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="Issr" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}Max35Text"/&gt;
             *         &lt;element name="SchmeNm" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}Max35Text" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "",
                                                           propOrder = {"id", "issr", "schmeNm"}) public static class Prtry {

                @XmlElement(name = "Id", required = true) protected String id;
                @XmlElement(name = "Issr", required = true) protected String issr;
                @XmlElement(name = "SchmeNm") protected String schmeNm;

                /**
                 * Obtém o valor da propriedade id.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getId() {
                    return id;
                }

                /**
                 * Define o valor da propriedade id.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setId(String value) {
                    this.id = value;
                }

                /**
                 * Obtém o valor da propriedade issr.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getIssr() {
                    return issr;
                }

                /**
                 * Define o valor da propriedade issr.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setIssr(String value) {
                    this.issr = value;
                }

                /**
                 * Obtém o valor da propriedade schmeNm.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getSchmeNm() {
                    return schmeNm;
                }

                /**
                 * Define o valor da propriedade schmeNm.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setSchmeNm(String value) {
                    this.schmeNm = value;
                }

            }

        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     *
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Cd"&gt;
     *           &lt;simpleType&gt;
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *               &lt;enumeration value="LOWW"/&gt;
     *               &lt;enumeration value="MEDI"/&gt;
     *               &lt;enumeration value="HIGH"/&gt;
     *               &lt;enumeration value="MEHI"/&gt;
     *             &lt;/restriction&gt;
     *           &lt;/simpleType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"cd"}) public static class RiskLv {

        @XmlElement(name = "Cd", required = true) protected String cd;

        /**
         * Obtém o valor da propriedade cd.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCd() {
            return cd;
        }

        /**
         * Define o valor da propriedade cd.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCd(String value) {
            this.cd = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     *
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Guar"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Cd"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                         &lt;enumeration value="BOGU"/&gt;
     *                         &lt;enumeration value="NOGU"/&gt;
     *                         &lt;enumeration value="LOGU"/&gt;
     *                         &lt;enumeration value="SHGU"/&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="LongPos" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}PositionType" minOccurs="0"/&gt;
     *         &lt;element name="ShortPos" type="{http://www.anbimacom.br/BalanceForSubAccountBrazil}PositionType" minOccurs="0"/&gt;
     *         &lt;element name="StrtDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODate" minOccurs="0"/&gt;
     *         &lt;element name="HedgTp" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Cd"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                         &lt;enumeration value="NOHE"/&gt;
     *                         &lt;enumeration value="FIHE"/&gt;
     *                         &lt;enumeration value="EQHE"/&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "",
                                                   propOrder = {"guar", "longPos", "shortPos", "strtDt", "hedgTp"}) public static class Swap {

        @XmlElement(name = "Guar", required = true) protected Guar guar;
        @XmlElement(name = "LongPos") protected PositionType longPos;
        @XmlElement(name = "ShortPos") protected PositionType shortPos;
        @XmlElement(name = "StrtDt") @XmlSchemaType(name = "date") protected XMLGregorianCalendar strtDt;
        @XmlElement(name = "HedgTp") protected HedgTp hedgTp;

        /**
         * Obtém o valor da propriedade guar.
         *
         * @return
         *     possible object is
         *     {@link Guar }
         *
         */
        public Guar getGuar() {
            return guar;
        }

        /**
         * Define o valor da propriedade guar.
         *
         * @param value
         *     allowed object is
         *     {@link Guar }
         *
         */
        public void setGuar(Guar value) {
            this.guar = value;
        }

        /**
         * Obtém o valor da propriedade hedgTp.
         *
         * @return
         *     possible object is
         *     {@link HedgTp }
         *
         */
        public HedgTp getHedgTp() {
            return hedgTp;
        }

        /**
         * Define o valor da propriedade hedgTp.
         *
         * @param value
         *     allowed object is
         *     {@link HedgTp }
         *
         */
        public void setHedgTp(HedgTp value) {
            this.hedgTp = value;
        }

        /**
         * Obtém o valor da propriedade longPos.
         *
         * @return
         *     possible object is
         *     {@link PositionType }
         *
         */
        public PositionType getLongPos() {
            return longPos;
        }

        /**
         * Define o valor da propriedade longPos.
         *
         * @param value
         *     allowed object is
         *     {@link PositionType }
         *
         */
        public void setLongPos(PositionType value) {
            this.longPos = value;
        }

        /**
         * Obtém o valor da propriedade shortPos.
         *
         * @return
         *     possible object is
         *     {@link PositionType }
         *
         */
        public PositionType getShortPos() {
            return shortPos;
        }

        /**
         * Define o valor da propriedade shortPos.
         *
         * @param value
         *     allowed object is
         *     {@link PositionType }
         *
         */
        public void setShortPos(PositionType value) {
            this.shortPos = value;
        }

        /**
         * Obtém o valor da propriedade strtDt.
         *
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *
         */
        public XMLGregorianCalendar getStrtDt() {
            return strtDt;
        }

        /**
         * Define o valor da propriedade strtDt.
         *
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *
         */
        public void setStrtDt(XMLGregorianCalendar value) {
            this.strtDt = value;
        }

        /**
         * <p>Classe Java de anonymous complex type.
         *
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Cd"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *               &lt;enumeration value="BOGU"/&gt;
         *               &lt;enumeration value="NOGU"/&gt;
         *               &lt;enumeration value="LOGU"/&gt;
         *               &lt;enumeration value="SHGU"/&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"cd"}) public static class Guar {

            @XmlElement(name = "Cd", required = true) protected String cd;

            /**
             * Obtém o valor da propriedade cd.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCd() {
                return cd;
            }

            /**
             * Define o valor da propriedade cd.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCd(String value) {
                this.cd = value;
            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         *
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Cd"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *               &lt;enumeration value="NOHE"/&gt;
         *               &lt;enumeration value="FIHE"/&gt;
         *               &lt;enumeration value="EQHE"/&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD) @XmlType(name = "", propOrder = {"cd"}) public static class HedgTp {

            @XmlElement(name = "Cd", required = true) protected String cd;

            /**
             * Obtém o valor da propriedade cd.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCd() {
                return cd;
            }

            /**
             * Define o valor da propriedade cd.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCd(String value) {
                this.cd = value;
            }

        }

    }

}
