/**
 *
 */
package br.com.galgo.galgowsgenerated;

import br.com.stianbid.serviceplcota.ServicePLCota;
import br.com.stianbid.serviceplcota.ServicePLCota_Service;
import com.sistemagalgo.serviceextratocotas.ServiceExtratoCotas;
import com.sistemagalgo.serviceextratocotas.ServiceExtratoCotas_Service;
import com.sistemagalgo.serviceposicaoativos.ServicePosicaoAtivos;
import com.sistemagalgo.serviceposicaoativos.ServicePosicaoAtivos_Service;

import java.net.URL;

/**
 * Classe responsável por instanciar os WS's do Galgo
 *
 * @author valdemar.arantes
 */
public class GalgoServicesFactory {

    private static final String PATH_WSDL_SERVICE_PL_COTA = "wsdl/ServicePLCota.wsdl";
    private static final URL WSDL_LOCATION_PLCOTA;

    private static final String PATH_WSDL_SERVICE_POS_ATIVOS = "wsdl/ServicePosicaoAtivos.wsdl";
    private static final URL WSDL_LOCATION_POS_ATIVOS;

    private static final String PATH_WSDL_SERVICE_EXTRATO = "wsdl/ServiceExtratoCotas.wsdl";
    private static final URL WSDL_LOCATION_EXTRATO;

    static {
        WSDL_LOCATION_PLCOTA = GalgoServicesFactory.class.getClassLoader().getResource(PATH_WSDL_SERVICE_PL_COTA);
        WSDL_LOCATION_POS_ATIVOS = GalgoServicesFactory.class.getClassLoader().getResource(
            PATH_WSDL_SERVICE_POS_ATIVOS);
        WSDL_LOCATION_EXTRATO = GalgoServicesFactory.class.getClassLoader().getResource(PATH_WSDL_SERVICE_EXTRATO);
    }

    /**
     * Este método está implementado para impedir que esta classe seja instanciada
     */
    private GalgoServicesFactory() {
    }

    /**
     * @return Uma instância do Web Service de PL Cota
     */
    public static ServicePLCota newPLCotaServiceInstance() {
        if (WSDL_LOCATION_PLCOTA == null) {
            throw new RuntimeException("Arquivo " + PATH_WSDL_SERVICE_PL_COTA + " nao encontrado no classpath");
        }
        ServicePLCota_Service service = new ServicePLCota_Service(WSDL_LOCATION_PLCOTA);
        return service.getServicePLCotaSOAP();
    }

    /**
     * @return Uma instância do Web Service de Posicao de Ativos
     */
    public static ServicePosicaoAtivos newPosAtivosServiceInstance() {
        if (WSDL_LOCATION_POS_ATIVOS == null) {
            throw new RuntimeException("Arquivo " + PATH_WSDL_SERVICE_POS_ATIVOS + " nao encontrado no classpath");
        }
        ServicePosicaoAtivos_Service service = new ServicePosicaoAtivos_Service(WSDL_LOCATION_POS_ATIVOS);
        return service.getServicePosicaoAtivosSOAP();
    }

    /**
     * @return Uma instância do Web Service de Extrato
     */
    public static ServiceExtratoCotas newServiceExtratoCotasInstance() {
        if (WSDL_LOCATION_POS_ATIVOS == null) {
            throw new RuntimeException("Arquivo " + PATH_WSDL_SERVICE_EXTRATO + " nao encontrado no classpath");
        }
        ServiceExtratoCotas_Service service = new ServiceExtratoCotas_Service(WSDL_LOCATION_EXTRATO);
        return service.getServiceExtratoCotasSOAP();
    }
}
