package br.com.stianbid.common;

import br.com.stianbid.schemaplcota.MessageBatchResponsePendLongComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Lote de mensagens que podem ser enviadas ao sistema para
 * tratamento.
 * <p>
 * <p>
 * <p>Classe Java de MessageBatchComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageBatchComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idMsgSender" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tsMsgCreated" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="qtElementCount"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int"&gt;
 *               &lt;maxExclusive value="10000"/&gt;
 *               &lt;minExclusive value="0"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="icLimitExceed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageBatchComplexType",
         propOrder = {"idMsgSender", "tsMsgCreated", "qtElementCount", "icLimitExceed"})
@XmlSeeAlso({MessageBatchPendComplexType.class, MessageBatchResponsePendLongComplexType.class, MessageReceiptBatchComplexType.class})
public class MessageBatchComplexType {

    @XmlElement(required = true) protected String idMsgSender;
    @XmlElement(required = true) @XmlSchemaType(name = "dateTime") protected XMLGregorianCalendar tsMsgCreated;
    protected int qtElementCount;
    protected Boolean icLimitExceed;

    /**
     * Obtém o valor da propriedade idMsgSender.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdMsgSender() {
        return idMsgSender;
    }

    /**
     * Define o valor da propriedade idMsgSender.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdMsgSender(String value) {
        this.idMsgSender = value;
    }

    /**
     * Obtém o valor da propriedade qtElementCount.
     */
    public int getQtElementCount() {
        return qtElementCount;
    }

    /**
     * Define o valor da propriedade qtElementCount.
     */
    public void setQtElementCount(int value) {
        this.qtElementCount = value;
    }

    /**
     * Obtém o valor da propriedade tsMsgCreated.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getTsMsgCreated() {
        return tsMsgCreated;
    }

    /**
     * Define o valor da propriedade tsMsgCreated.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setTsMsgCreated(XMLGregorianCalendar value) {
        this.tsMsgCreated = value;
    }

    /**
     * Obtém o valor da propriedade icLimitExceed.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isIcLimitExceed() {
        return icLimitExceed;
    }

    /**
     * Define o valor da propriedade icLimitExceed.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setIcLimitExceed(Boolean value) {
        this.icLimitExceed = value;
    }

}
