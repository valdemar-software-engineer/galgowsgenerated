package br.com.stianbid.common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the br.com.stianbid.common package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Receipt_QNAME = new QName("http://www.stianbid.com.br/Common", "Receipt");
    private final static QName _MessageReceiptBatch_QNAME = new QName("http://www.stianbid.com.br/Common",
        "MessageReceiptBatch");
    private final static QName _MessageReceiptRequest_QNAME = new QName("http://www.stianbid.com.br/Common",
        "MessageReceiptRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.stianbid.common
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActiveCurrencyAndAmount }
     */
    public ActiveCurrencyAndAmount createActiveCurrencyAndAmount() {
        return new ActiveCurrencyAndAmount();
    }

    /**
     * Create an instance of {@link AddInfItemComplexType }
     */
    public AddInfItemComplexType createAddInfItemComplexType() {
        return new AddInfItemComplexType();
    }

    /**
     * Create an instance of {@link AddtlInfComplexType }
     */
    public AddtlInfComplexType createAddtlInfComplexType() {
        return new AddtlInfComplexType();
    }

    /**
     * Create an instance of {@link ErrorElementComplexType }
     */
    public ErrorElementComplexType createErrorElementComplexType() {
        return new ErrorElementComplexType();
    }

    /**
     * Create an instance of {@link IdReceiptComplexType }
     */
    public IdReceiptComplexType createIdReceiptComplexType() {
        return new IdReceiptComplexType();
    }

    /**
     * Create an instance of {@link IdSenderReceiptComplexType }
     */
    public IdSenderReceiptComplexType createIdSenderReceiptComplexType() {
        return new IdSenderReceiptComplexType();
    }

    /**
     * Create an instance of {@link IdentifRegistroComplexType }
     */
    public IdentifRegistroComplexType createIdentifRegistroComplexType() {
        return new IdentifRegistroComplexType();
    }

    /**
     * Create an instance of {@link InvestmentRecordIdComplexType }
     */
    public InvestmentRecordIdComplexType createInvestmentRecordIdComplexType() {
        return new InvestmentRecordIdComplexType();
    }

    /**
     * Create an instance of {@link MessageBatchComplexType }
     */
    public MessageBatchComplexType createMessageBatchComplexType() {
        return new MessageBatchComplexType();
    }

    /**
     * Create an instance of {@link MessageBatchPendComplexType }
     */
    public MessageBatchPendComplexType createMessageBatchPendComplexType() {
        return new MessageBatchPendComplexType();
    }

    /**
     * Create an instance of {@link MessageExceptionComplexType }
     */
    public MessageExceptionComplexType createMessageExceptionComplexType() {
        return new MessageExceptionComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageReceiptBatchComplexType }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.stianbid.com.br/Common", name = "MessageReceiptBatch")
    public JAXBElement<MessageReceiptBatchComplexType> createMessageReceiptBatch(MessageReceiptBatchComplexType value) {
        return new JAXBElement<MessageReceiptBatchComplexType>(_MessageReceiptBatch_QNAME,
            MessageReceiptBatchComplexType.class, null, value);
    }

    /**
     * Create an instance of {@link MessageReceiptBatchComplexType }
     */
    public MessageReceiptBatchComplexType createMessageReceiptBatchComplexType() {
        return new MessageReceiptBatchComplexType();
    }

    /**
     * Create an instance of {@link MessageReceiptComplexType }
     */
    public MessageReceiptComplexType createMessageReceiptComplexType() {
        return new MessageReceiptComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageReceiptRequestComplexType }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.stianbid.com.br/Common", name = "MessageReceiptRequest")
    public JAXBElement<MessageReceiptRequestComplexType> createMessageReceiptRequest(
        MessageReceiptRequestComplexType value) {
        return new JAXBElement<MessageReceiptRequestComplexType>(_MessageReceiptRequest_QNAME,
            MessageReceiptRequestComplexType.class, null, value);
    }

    /**
     * Create an instance of {@link MessageReceiptRequestComplexType }
     */
    public MessageReceiptRequestComplexType createMessageReceiptRequestComplexType() {
        return new MessageReceiptRequestComplexType();
    }

    /**
     * Create an instance of {@link MessageRequestComplexType }
     */
    public MessageRequestComplexType createMessageRequestComplexType() {
        return new MessageRequestComplexType();
    }

    /**
     * Create an instance of {@link MessageRequestPendComplexType }
     */
    public MessageRequestPendComplexType createMessageRequestPendComplexType() {
        return new MessageRequestPendComplexType();
    }

    /**
     * Create an instance of {@link MessageSenderReceiptRequestComplexType }
     */
    public MessageSenderReceiptRequestComplexType createMessageSenderReceiptRequestComplexType() {
        return new MessageSenderReceiptRequestComplexType();
    }

    /**
     * Create an instance of {@link ObjectReceiptComplexType }
     */
    public ObjectReceiptComplexType createObjectReceiptComplexType() {
        return new ObjectReceiptComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageReceiptComplexType }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.stianbid.com.br/Common", name = "Receipt")
    public JAXBElement<MessageReceiptComplexType> createReceipt(MessageReceiptComplexType value) {
        return new JAXBElement<MessageReceiptComplexType>(_Receipt_QNAME, MessageReceiptComplexType.class, null, value);
    }

    /**
     * Create an instance of {@link ServicoComplexType }
     */
    public ServicoComplexType createServicoComplexType() {
        return new ServicoComplexType();
    }

}
