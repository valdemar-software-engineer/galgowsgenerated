package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de PriceMethod1Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="PriceMethod1Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;length value="4"/&gt;
 *     &lt;enumeration value="FORW"/&gt;
 *     &lt;enumeration value="HIST"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "PriceMethod1Code")
@XmlEnum
public enum PriceMethod1Code {

    FORW,
    HIST;

    public static PriceMethod1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
