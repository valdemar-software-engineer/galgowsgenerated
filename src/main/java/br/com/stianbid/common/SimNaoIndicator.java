package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de SimNaoIndicator.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="SimNaoIndicator"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;minLength value="3"/&gt;
 *     &lt;maxLength value="3"/&gt;
 *     &lt;enumeration value="Sim"/&gt;
 *     &lt;enumeration value="Nao"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "SimNaoIndicator")
@XmlEnum
public enum SimNaoIndicator {

    @XmlEnumValue("Sim")
    SIM("Sim"),
    @XmlEnumValue("Nao")
    NAO("Nao");
    private final String value;

    SimNaoIndicator(String v) {
        value = v;
    }

    public static SimNaoIndicator fromValue(String v) {
        for (SimNaoIndicator c : SimNaoIndicator.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public String value() {
        return value;
    }

}
