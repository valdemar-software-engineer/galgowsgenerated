package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Recibo da solicitação enviada.
 * <p>
 * <p>
 * <p>Classe Java de IdSenderReceiptComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="IdSenderReceiptComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idMsgSender" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdSenderReceiptComplexType", propOrder = {"idMsgSender"})
public class IdSenderReceiptComplexType {

    @XmlElement(required = true) protected String idMsgSender;

    /**
     * Obtém o valor da propriedade idMsgSender.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdMsgSender() {
        return idMsgSender;
    }

    /**
     * Define o valor da propriedade idMsgSender.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdMsgSender(String value) {
        this.idMsgSender = value;
    }

}
