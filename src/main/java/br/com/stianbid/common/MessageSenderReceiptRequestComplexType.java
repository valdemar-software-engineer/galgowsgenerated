package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Solicita um lote de mensagens de recibos enviadas ao
 * sistema anteriormente.
 * <p>
 * <p>
 * <p>Classe Java de MessageSenderReceiptRequestComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageSenderReceiptRequestComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="receipt" type="{http://www.stianbid.com.br/Common}IdSenderReceiptComplexType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageSenderReceiptRequestComplexType", propOrder = {"receipt"})
public class MessageSenderReceiptRequestComplexType extends MessageRequestComplexType {

    @XmlElement(required = true) protected List<IdSenderReceiptComplexType> receipt;

    /**
     * Gets the value of the receipt property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receipt property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceipt().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdSenderReceiptComplexType }
     */
    public List<IdSenderReceiptComplexType> getReceipt() {
        if (receipt == null) {
            receipt = new ArrayList<IdSenderReceiptComplexType>();
        }
        return this.receipt;
    }

}
