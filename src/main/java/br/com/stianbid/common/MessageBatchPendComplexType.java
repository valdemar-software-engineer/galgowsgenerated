package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Lote de mensagens que podem ser enviadas ao sistema para
 * tratamento. Utilizado somente para consulta de fundos
 * pendentes.
 * <p>
 * <p>
 * <p>Classe Java de MessageBatchPendComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageBatchPendComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageBatchComplexType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Marcador" type="{http://www.stianbid.com.br/Common}MarcadorType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageBatchPendComplexType", propOrder = {"marcador"})
public class MessageBatchPendComplexType extends MessageBatchComplexType {

    @XmlElement(name = "Marcador") protected String marcador;

    /**
     * Obtém o valor da propriedade marcador.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMarcador() {
        return marcador;
    }

    /**
     * Define o valor da propriedade marcador.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMarcador(String value) {
        this.marcador = value;
    }

}
