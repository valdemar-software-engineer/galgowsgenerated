package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Identificador de Registro.
 * <p>
 * <p>Classe Java de IdentifRegistroComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="IdentifRegistroComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/&gt;
 *         &lt;element name="DescricaoId" type="{http://www.stianbid.com.br/Common}TypeDescIdentif" minOccurs="0"/&gt;
 *         &lt;element name="IdAux" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/&gt;
 *         &lt;element name="DescricaoIdAux" type="{http://www.stianbid.com.br/Common}TypeDescIdentif" minOccurs="0"/&gt;
 *         &lt;element name="Dt" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="Moeda" type="{http://www.stianbid.com.br/Common}ActiveCurrencyCode" minOccurs="0"/&gt;
 *         &lt;element name="Servico" type="{http://www.stianbid.com.br/Common}Max150Text" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentifRegistroComplexType",
         propOrder = {"id", "descricaoId", "idAux", "descricaoIdAux", "dt", "moeda", "servico"})
public class IdentifRegistroComplexType {

    @XmlElement(name = "Id") protected String id;
    @XmlElement(name = "DescricaoId") protected Integer descricaoId;
    @XmlElement(name = "IdAux") protected String idAux;
    @XmlElement(name = "DescricaoIdAux") protected Integer descricaoIdAux;
    @XmlElement(name = "Dt") @XmlSchemaType(name = "date") protected XMLGregorianCalendar dt;
    @XmlElement(name = "Moeda") protected String moeda;
    @XmlElement(name = "Servico") protected String servico;

    /**
     * Obtém o valor da propriedade descricaoId.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getDescricaoId() {
        return descricaoId;
    }

    /**
     * Define o valor da propriedade descricaoId.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setDescricaoId(Integer value) {
        this.descricaoId = value;
    }

    /**
     * Obtém o valor da propriedade descricaoIdAux.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getDescricaoIdAux() {
        return descricaoIdAux;
    }

    /**
     * Define o valor da propriedade descricaoIdAux.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setDescricaoIdAux(Integer value) {
        this.descricaoIdAux = value;
    }

    /**
     * Obtém o valor da propriedade dt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDt() {
        return dt;
    }

    /**
     * Define o valor da propriedade dt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDt(XMLGregorianCalendar value) {
        this.dt = value;
    }

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link String }
     */
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade idAux.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdAux() {
        return idAux;
    }

    /**
     * Define o valor da propriedade idAux.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdAux(String value) {
        this.idAux = value;
    }

    /**
     * Obtém o valor da propriedade moeda.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMoeda() {
        return moeda;
    }

    /**
     * Define o valor da propriedade moeda.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMoeda(String value) {
        this.moeda = value;
    }

    /**
     * Obtém o valor da propriedade servico.
     *
     * @return possible object is
     * {@link String }
     */
    public String getServico() {
        return servico;
    }

    /**
     * Define o valor da propriedade servico.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setServico(String value) {
        this.servico = value;
    }

}
