package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Informações dos elementos da solicitação inicial.
 * <p>
 * <p>
 * <p>Classe Java de ObjectReceiptComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ObjectReceiptComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="order" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="status" type="{http://www.stianbid.com.br/Common}StatusErro"/&gt;
 *         &lt;element name="errorTarget" type="{http://www.stianbid.com.br/Common}ErrorElementComplexType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="NrProtocolo" type="{http://www.stianbid.com.br/Common}NumeroProtocolo" minOccurs="0"/&gt;
 *         &lt;element name="Identif" type="{http://www.stianbid.com.br/Common}IdentifRegistroComplexType" minOccurs="0"/&gt;
 *         &lt;element name="AddInfo" type="{http://www.stianbid.com.br/Common}AddtlInfComplexType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectReceiptComplexType",
         propOrder = {"order", "status", "errorTarget", "nrProtocolo", "identif", "addInfo"})
public class ObjectReceiptComplexType {

    protected long order;
    protected int status;
    protected List<ErrorElementComplexType> errorTarget;
    @XmlElement(name = "NrProtocolo") protected Long nrProtocolo;
    @XmlElement(name = "Identif") protected IdentifRegistroComplexType identif;
    @XmlElement(name = "AddInfo") protected AddtlInfComplexType addInfo;

    /**
     * Obtém o valor da propriedade addInfo.
     *
     * @return possible object is
     * {@link AddtlInfComplexType }
     */
    public AddtlInfComplexType getAddInfo() {
        return addInfo;
    }

    /**
     * Define o valor da propriedade addInfo.
     *
     * @param value allowed object is
     *              {@link AddtlInfComplexType }
     */
    public void setAddInfo(AddtlInfComplexType value) {
        this.addInfo = value;
    }

    /**
     * Gets the value of the errorTarget property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorTarget property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorTarget().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorElementComplexType }
     */
    public List<ErrorElementComplexType> getErrorTarget() {
        if (errorTarget == null) {
            errorTarget = new ArrayList<ErrorElementComplexType>();
        }
        return this.errorTarget;
    }

    /**
     * Obtém o valor da propriedade identif.
     *
     * @return possible object is
     * {@link IdentifRegistroComplexType }
     */
    public IdentifRegistroComplexType getIdentif() {
        return identif;
    }

    /**
     * Define o valor da propriedade identif.
     *
     * @param value allowed object is
     *              {@link IdentifRegistroComplexType }
     */
    public void setIdentif(IdentifRegistroComplexType value) {
        this.identif = value;
    }

    /**
     * Obtém o valor da propriedade nrProtocolo.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getNrProtocolo() {
        return nrProtocolo;
    }

    /**
     * Define o valor da propriedade nrProtocolo.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setNrProtocolo(Long value) {
        this.nrProtocolo = value;
    }

    /**
     * Obtém o valor da propriedade order.
     */
    public long getOrder() {
        return order;
    }

    /**
     * Define o valor da propriedade order.
     */
    public void setOrder(long value) {
        this.order = value;
    }

    /**
     * Obtém o valor da propriedade status.
     */
    public int getStatus() {
        return status;
    }

    /**
     * Define o valor da propriedade status.
     */
    public void setStatus(int value) {
        this.status = value;
    }

}
