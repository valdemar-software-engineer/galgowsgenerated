package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de EventFrequency2Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="EventFrequency2Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;length value="4"/&gt;
 *     &lt;enumeration value="CLOS"/&gt;
 *     &lt;enumeration value="DAIL"/&gt;
 *     &lt;enumeration value="MNTH"/&gt;
 *     &lt;enumeration value="QUTR"/&gt;
 *     &lt;enumeration value="SEMI"/&gt;
 *     &lt;enumeration value="WEEK"/&gt;
 *     &lt;enumeration value="YEAR"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "EventFrequency2Code")
@XmlEnum
public enum EventFrequency2Code {

    CLOS,
    DAIL,
    MNTH,
    QUTR,
    SEMI,
    WEEK,
    YEAR;

    public static EventFrequency2Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
