package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de MessageExceptionComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageExceptionComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idException" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="dsException" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageExceptionComplexType", propOrder = {"idException", "dsException"})
public class MessageExceptionComplexType {

    @XmlElement(required = true) protected String idException;
    @XmlElement(required = true) protected String dsException;

    /**
     * Obtém o valor da propriedade dsException.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDsException() {
        return dsException;
    }

    /**
     * Define o valor da propriedade dsException.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDsException(String value) {
        this.dsException = value;
    }

    /**
     * Obtém o valor da propriedade idException.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdException() {
        return idException;
    }

    /**
     * Define o valor da propriedade idException.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdException(String value) {
        this.idException = value;
    }

}
