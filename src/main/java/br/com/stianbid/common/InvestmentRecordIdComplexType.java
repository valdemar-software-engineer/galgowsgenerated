package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Identificação do registro de investimento comumemente
 * utilizado pelas estruturas de negócio.
 * <p>
 * <p>
 * <p>Classe Java de InvestmentRecordIdComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="InvestmentRecordIdComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="CdSti" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *           &lt;element name="ISIN" type="{http://www.stianbid.com.br/Common}ISINIdentifier" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="NmFantasia" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/&gt;
 *         &lt;element name="Serie" type="{http://www.stianbid.com.br/Common}NumeroSerie" minOccurs="0"/&gt;
 *         &lt;element name="Subordinada" type="{http://www.stianbid.com.br/Common}Max70Text" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvestmentRecordIdComplexType", propOrder = {"cdSti", "isin", "nmFantasia", "serie", "subordinada"})
public class InvestmentRecordIdComplexType {

    @XmlElement(name = "CdSti") protected Integer cdSti;
    @XmlElement(name = "ISIN") protected String isin;
    @XmlElement(name = "NmFantasia") protected String nmFantasia;
    @XmlElement(name = "Serie") protected Integer serie;
    @XmlElement(name = "Subordinada") protected String subordinada;

    /**
     * Obtém o valor da propriedade cdSti.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCdSti() {
        return cdSti;
    }

    /**
     * Define o valor da propriedade cdSti.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCdSti(Integer value) {
        this.cdSti = value;
    }

    /**
     * Obtém o valor da propriedade isin.
     *
     * @return possible object is
     * {@link String }
     */
    public String getISIN() {
        return isin;
    }

    /**
     * Define o valor da propriedade isin.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setISIN(String value) {
        this.isin = value;
    }

    /**
     * Obtém o valor da propriedade nmFantasia.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNmFantasia() {
        return nmFantasia;
    }

    /**
     * Define o valor da propriedade nmFantasia.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNmFantasia(String value) {
        this.nmFantasia = value;
    }

    /**
     * Obtém o valor da propriedade serie.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getSerie() {
        return serie;
    }

    /**
     * Define o valor da propriedade serie.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setSerie(Integer value) {
        this.serie = value;
    }

    /**
     * Obtém o valor da propriedade subordinada.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSubordinada() {
        return subordinada;
    }

    /**
     * Define o valor da propriedade subordinada.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSubordinada(String value) {
        this.subordinada = value;
    }

}
