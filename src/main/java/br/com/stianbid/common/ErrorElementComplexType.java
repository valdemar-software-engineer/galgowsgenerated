package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Ponto que se refere ao erro informado e o erro
 * <p>
 * <p>
 * <p>Classe Java de ErrorElementComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ErrorElementComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="errorPath" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="errorKey" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="errorMessage" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="originValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorElementComplexType", propOrder = {"errorPath", "errorKey", "errorMessage", "originValue"})
public class ErrorElementComplexType {

    @XmlElement(required = true) protected String errorPath;
    @XmlElement(required = true) protected String errorKey;
    @XmlElement(required = true) protected String errorMessage;
    protected String originValue;

    /**
     * Obtém o valor da propriedade errorKey.
     *
     * @return possible object is
     * {@link String }
     */
    public String getErrorKey() {
        return errorKey;
    }

    /**
     * Define o valor da propriedade errorKey.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setErrorKey(String value) {
        this.errorKey = value;
    }

    /**
     * Obtém o valor da propriedade errorMessage.
     *
     * @return possible object is
     * {@link String }
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Define o valor da propriedade errorMessage.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setErrorMessage(String value) {
        this.errorMessage = value;
    }

    /**
     * Obtém o valor da propriedade errorPath.
     *
     * @return possible object is
     * {@link String }
     */
    public String getErrorPath() {
        return errorPath;
    }

    /**
     * Define o valor da propriedade errorPath.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setErrorPath(String value) {
        this.errorPath = value;
    }

    /**
     * Obtém o valor da propriedade originValue.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOriginValue() {
        return originValue;
    }

    /**
     * Define o valor da propriedade originValue.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setOriginValue(String value) {
        this.originValue = value;
    }

}
