package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ServicoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ServicoComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="PLCota" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *           &lt;element name="FundoInvest" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *           &lt;element name="CarteiraAdm" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *           &lt;element name="ExtratoCota" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *           &lt;element name="PosicaoAtivo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServicoComplexType",
         propOrder = {"plCota", "fundoInvest", "carteiraAdm", "extratoCota", "posicaoAtivo"})
public class ServicoComplexType {

    @XmlElement(name = "PLCota") protected String plCota;
    @XmlElement(name = "FundoInvest") protected String fundoInvest;
    @XmlElement(name = "CarteiraAdm") protected String carteiraAdm;
    @XmlElement(name = "ExtratoCota") protected String extratoCota;
    @XmlElement(name = "PosicaoAtivo") protected String posicaoAtivo;

    /**
     * Obtém o valor da propriedade carteiraAdm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCarteiraAdm() {
        return carteiraAdm;
    }

    /**
     * Define o valor da propriedade carteiraAdm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCarteiraAdm(String value) {
        this.carteiraAdm = value;
    }

    /**
     * Obtém o valor da propriedade extratoCota.
     *
     * @return possible object is
     * {@link String }
     */
    public String getExtratoCota() {
        return extratoCota;
    }

    /**
     * Define o valor da propriedade extratoCota.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setExtratoCota(String value) {
        this.extratoCota = value;
    }

    /**
     * Obtém o valor da propriedade fundoInvest.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFundoInvest() {
        return fundoInvest;
    }

    /**
     * Define o valor da propriedade fundoInvest.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFundoInvest(String value) {
        this.fundoInvest = value;
    }

    /**
     * Obtém o valor da propriedade plCota.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPLCota() {
        return plCota;
    }

    /**
     * Define o valor da propriedade plCota.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPLCota(String value) {
        this.plCota = value;
    }

    /**
     * Obtém o valor da propriedade posicaoAtivo.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPosicaoAtivo() {
        return posicaoAtivo;
    }

    /**
     * Define o valor da propriedade posicaoAtivo.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPosicaoAtivo(String value) {
        this.posicaoAtivo = value;
    }

}
