package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Recibo da solicitação enviada.
 * <p>
 * <p>
 * <p>Classe Java de IdReceiptComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="IdReceiptComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idReceipt" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdReceiptComplexType", propOrder = {"idReceipt"})
public class IdReceiptComplexType {

    @XmlElement(required = true) protected String idReceipt;

    /**
     * Obtém o valor da propriedade idReceipt.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdReceipt() {
        return idReceipt;
    }

    /**
     * Define o valor da propriedade idReceipt.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdReceipt(String value) {
        this.idReceipt = value;
    }

}
