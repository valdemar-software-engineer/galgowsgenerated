package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de EUSavingsDirective1Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="EUSavingsDirective1Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;length value="4"/&gt;
 *     &lt;enumeration value="EUSI"/&gt;
 *     &lt;enumeration value="EUSO"/&gt;
 *     &lt;enumeration value="VARI"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "EUSavingsDirective1Code")
@XmlEnum
public enum EUSavingsDirective1Code {

    EUSI,
    EUSO,
    VARI;

    public static EUSavingsDirective1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
