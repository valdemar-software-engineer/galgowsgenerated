package br.com.stianbid.common;

import br.com.stianbid.schemafundo.DadosConsComplexType;
import br.com.stianbid.schemafundo.RequestAlterarFundoComplexType;
import br.com.stianbid.schemafundo.RequestAtualizarFundosComplexType;
import br.com.stianbid.schemafundo.RequestConsultaDocumentoComplexType;
import br.com.stianbid.schemafundo.RequestConsumirFundosComplexType;
import br.com.stianbid.schemafundo.RequestConsumirVersoesFundoComplexType;
import br.com.stianbid.schemafundo.RequestEnviaDocComplexType;
import br.com.stianbid.schemafundo.RequestFinalizarFundosComplexType;
import br.com.stianbid.schemafundo.RequestIncluirFundoComplexType;
import br.com.stianbid.schemaplcota.MessageBatchPendLongComplexType;
import br.com.stianbid.schemaplcota.MessageConsultaPLCotaComplexType;
import com.sistemagalgo.schemaextratocotas.MessageExtratoCotasConsumirComplexType;
import com.sistemagalgo.schemaextratocotas.MessageExtratoCotasConsumirPendComplexType;
import com.sistemagalgo.schemaextratocotas.MessageExtratoCotasExcFinComplexType;
import com.sistemagalgo.schemaposicaoativos.MessagePosicaoAtivosConsPendComplexType;
import com.sistemagalgo.schemaposicaoativos.MessagePosicaoAtivosConsumirComplexType;
import com.sistemagalgo.schemaposicaoativos.MessagePosicaoAtivosExcFinComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de MessageRequestComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageRequestComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idMsgSender" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="qtMaxElement" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageRequestComplexType", propOrder = {"idMsgSender", "qtMaxElement"})
@XmlSeeAlso({DadosConsComplexType.class,
                MessageConsultaPLCotaComplexType.class,
                MessageBatchPendLongComplexType.class,
                MessagePosicaoAtivosConsPendComplexType.class,
                MessagePosicaoAtivosConsumirComplexType.class,
                MessagePosicaoAtivosExcFinComplexType.class,
                MessagePosicaoAtivosExcFinComplexType.class,
                MessageReceiptRequestComplexType.class,
                MessageRequestPendComplexType.class,
                MessageSenderReceiptRequestComplexType.class,
                RequestAlterarFundoComplexType.class,
                RequestAtualizarFundosComplexType.class,
                RequestConsultaDocumentoComplexType.class,
                RequestConsumirFundosComplexType.class,
                RequestConsumirVersoesFundoComplexType.class,
                RequestEnviaDocComplexType.class,
                RequestFinalizarFundosComplexType.class,
                RequestIncluirFundoComplexType.class,
                MessageRequestPendComplexType.class,
                MessageExtratoCotasExcFinComplexType.class,
                MessageExtratoCotasConsumirComplexType.class,
                MessageExtratoCotasConsumirPendComplexType.class
            })
public class MessageRequestComplexType {

    @XmlElement(required = true) protected String idMsgSender;
    protected Integer qtMaxElement;

    /**
     * Obtém o valor da propriedade idMsgSender.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdMsgSender() {
        return idMsgSender;
    }

    /**
     * Define o valor da propriedade idMsgSender.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdMsgSender(String value) {
        this.idMsgSender = value;
    }

    /**
     * Obtém o valor da propriedade qtMaxElement.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getQtMaxElement() {
        return qtMaxElement;
    }

    /**
     * Define o valor da propriedade qtMaxElement.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setQtMaxElement(Integer value) {
        this.qtMaxElement = value;
    }

}
