package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de AddressType2Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="AddressType2Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;length value="4"/&gt;
 *     &lt;enumeration value="ADDR"/&gt;
 *     &lt;enumeration value="BIZZ"/&gt;
 *     &lt;enumeration value="DLVY"/&gt;
 *     &lt;enumeration value="HOME"/&gt;
 *     &lt;enumeration value="MLTO"/&gt;
 *     &lt;enumeration value="PBOX"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "AddressType2Code")
@XmlEnum
public enum AddressType2Code {

    ADDR,
    BIZZ,
    DLVY,
    HOME,
    MLTO,
    PBOX;

    public static AddressType2Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
