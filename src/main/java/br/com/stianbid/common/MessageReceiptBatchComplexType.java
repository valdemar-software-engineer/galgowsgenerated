package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Lote de mensagens de recibos enviadas ao sistema
 * anteriormente.
 * <p>
 * <p>
 * <p>Classe Java de MessageReceiptBatchComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageReceiptBatchComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageBatchComplexType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="receipt" type="{http://www.stianbid.com.br/Common}MessageReceiptComplexType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageReceiptBatchComplexType", propOrder = {"receipt"})
public class MessageReceiptBatchComplexType extends MessageBatchComplexType {

    @XmlElement(required = true) protected List<MessageReceiptComplexType> receipt;

    /**
     * Gets the value of the receipt property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receipt property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceipt().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MessageReceiptComplexType }
     */
    public List<MessageReceiptComplexType> getReceipt() {
        if (receipt == null) {
            receipt = new ArrayList<MessageReceiptComplexType>();
        }
        return this.receipt;
    }

}
