package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * Recibo da solicitação enviada.
 * <p>
 * <p>
 * <p>Classe Java de MessageReceiptComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageReceiptComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idReceipt" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="idMsgSender" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tsCreated" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="idProcessStatus" type="{http://www.stianbid.com.br/Common}StatusRecibo"/&gt;
 *         &lt;element name="tsProcessStatus" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="qtEstimatedProcessingTime" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="objectReceipt" type="{http://www.stianbid.com.br/Common}ObjectReceiptComplexType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageReceiptComplexType",
         propOrder = {"idReceipt", "idMsgSender", "tsCreated", "idProcessStatus", "tsProcessStatus", "qtEstimatedProcessingTime", "objectReceipt"})
public class MessageReceiptComplexType {

    @XmlElement(required = true) protected String idReceipt;
    @XmlElement(required = true) protected String idMsgSender;
    @XmlElement(required = true) @XmlSchemaType(name = "dateTime") protected XMLGregorianCalendar tsCreated;
    protected long idProcessStatus;
    @XmlElement(required = true) @XmlSchemaType(name = "dateTime") protected XMLGregorianCalendar tsProcessStatus;
    protected Long qtEstimatedProcessingTime;
    protected List<ObjectReceiptComplexType> objectReceipt;

    /**
     * Obtém o valor da propriedade idMsgSender.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdMsgSender() {
        return idMsgSender;
    }

    /**
     * Define o valor da propriedade idMsgSender.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdMsgSender(String value) {
        this.idMsgSender = value;
    }

    /**
     * Obtém o valor da propriedade idProcessStatus.
     */
    public long getIdProcessStatus() {
        return idProcessStatus;
    }

    /**
     * Define o valor da propriedade idProcessStatus.
     */
    public void setIdProcessStatus(long value) {
        this.idProcessStatus = value;
    }

    /**
     * Obtém o valor da propriedade idReceipt.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdReceipt() {
        return idReceipt;
    }

    /**
     * Define o valor da propriedade idReceipt.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdReceipt(String value) {
        this.idReceipt = value;
    }

    /**
     * Gets the value of the objectReceipt property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objectReceipt property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjectReceipt().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReceiptComplexType }
     */
    public List<ObjectReceiptComplexType> getObjectReceipt() {
        if (objectReceipt == null) {
            objectReceipt = new ArrayList<ObjectReceiptComplexType>();
        }
        return this.objectReceipt;
    }

    /**
     * Obtém o valor da propriedade qtEstimatedProcessingTime.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getQtEstimatedProcessingTime() {
        return qtEstimatedProcessingTime;
    }

    /**
     * Define o valor da propriedade qtEstimatedProcessingTime.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setQtEstimatedProcessingTime(Long value) {
        this.qtEstimatedProcessingTime = value;
    }

    /**
     * Obtém o valor da propriedade tsCreated.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getTsCreated() {
        return tsCreated;
    }

    /**
     * Define o valor da propriedade tsCreated.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setTsCreated(XMLGregorianCalendar value) {
        this.tsCreated = value;
    }

    /**
     * Obtém o valor da propriedade tsProcessStatus.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getTsProcessStatus() {
        return tsProcessStatus;
    }

    /**
     * Define o valor da propriedade tsProcessStatus.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setTsProcessStatus(XMLGregorianCalendar value) {
        this.tsProcessStatus = value;
    }

}
