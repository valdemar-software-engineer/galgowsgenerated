package br.com.stianbid.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de MessageRequestPendComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageRequestPendComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Marcador" type="{http://www.stianbid.com.br/Common}MarcadorType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageRequestPendComplexType", propOrder = {"marcador"})
public class MessageRequestPendComplexType extends MessageRequestComplexType {

    @XmlElement(name = "Marcador") protected String marcador;

    /**
     * Obtém o valor da propriedade marcador.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMarcador() {
        return marcador;
    }

    /**
     * Define o valor da propriedade marcador.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMarcador(String value) {
        this.marcador = value;
    }

}
