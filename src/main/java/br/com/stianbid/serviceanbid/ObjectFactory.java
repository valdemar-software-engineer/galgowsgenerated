package br.com.stianbid.serviceanbid;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the br.com.stianbid.serviceanbid package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.stianbid.serviceanbid
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AtualizarCodigoANBID }
     */
    public AtualizarCodigoANBID createAtualizarCodigoANBID() {
        return new AtualizarCodigoANBID();
    }

    /**
     * Create an instance of {@link AtualizarCodigoANBIDFault }
     */
    public AtualizarCodigoANBIDFault createAtualizarCodigoANBIDFault() {
        return new AtualizarCodigoANBIDFault();
    }

    /**
     * Create an instance of {@link AtualizarCodigoANBIDResponse }
     */
    public AtualizarCodigoANBIDResponse createAtualizarCodigoANBIDResponse() {
        return new AtualizarCodigoANBIDResponse();
    }

    /**
     * Create an instance of {@link AvaliarPLCota }
     */
    public AvaliarPLCota createAvaliarPLCota() {
        return new AvaliarPLCota();
    }

    /**
     * Create an instance of {@link AvaliarPLCotaFault }
     */
    public AvaliarPLCotaFault createAvaliarPLCotaFault() {
        return new AvaliarPLCotaFault();
    }

    /**
     * Create an instance of {@link AvaliarPLCotaResponse }
     */
    public AvaliarPLCotaResponse createAvaliarPLCotaResponse() {
        return new AvaliarPLCotaResponse();
    }

    /**
     * Create an instance of {@link ConsumirCartAdmPendentes }
     */
    public ConsumirCartAdmPendentes createConsumirCartAdmPendentes() {
        return new ConsumirCartAdmPendentes();
    }

    /**
     * Create an instance of {@link ConsumirCartAdmPendentesFault }
     */
    public ConsumirCartAdmPendentesFault createConsumirCartAdmPendentesFault() {
        return new ConsumirCartAdmPendentesFault();
    }

    /**
     * Create an instance of {@link ConsumirCartAdmPendentesResponse }
     */
    public ConsumirCartAdmPendentesResponse createConsumirCartAdmPendentesResponse() {
        return new ConsumirCartAdmPendentesResponse();
    }

    /**
     * Create an instance of {@link ConsumirFundosPendentes }
     */
    public ConsumirFundosPendentes createConsumirFundosPendentes() {
        return new ConsumirFundosPendentes();
    }

    /**
     * Create an instance of {@link ConsumirFundosPendentesResponse }
     */
    public ConsumirFundosPendentesResponse createConsumirFundosPendentesResponse() {
        return new ConsumirFundosPendentesResponse();
    }

    /**
     * Create an instance of {@link ConsumirPendentesFault }
     */
    public ConsumirPendentesFault createConsumirPendentesFault() {
        return new ConsumirPendentesFault();
    }

}
