package br.com.stianbid.serviceanbid;

import br.com.stianbid.common.MessageExceptionComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AvaliarPLCota_fault" type="{http://www.stianbid.com.br/Common}MessageExceptionComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"avaliarPLCotaFault"})
@XmlRootElement(name = "AvaliarPLCota_fault")
public class AvaliarPLCotaFault {

    @XmlElement(name = "AvaliarPLCota_fault", required = true) protected MessageExceptionComplexType avaliarPLCotaFault;

    /**
     * Obtém o valor da propriedade avaliarPLCotaFault.
     *
     * @return possible object is
     * {@link MessageExceptionComplexType }
     */
    public MessageExceptionComplexType getAvaliarPLCotaFault() {
        return avaliarPLCotaFault;
    }

    /**
     * Define o valor da propriedade avaliarPLCotaFault.
     *
     * @param value allowed object is
     *              {@link MessageExceptionComplexType }
     */
    public void setAvaliarPLCotaFault(MessageExceptionComplexType value) {
        this.avaliarPLCotaFault = value;
    }

}
