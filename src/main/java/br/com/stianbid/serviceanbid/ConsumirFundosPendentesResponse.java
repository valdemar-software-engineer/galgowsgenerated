package br.com.stianbid.serviceanbid;

import br.com.stianbid.schemafundo.FPPRprtPendComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetornoConsulta" type="{http://www.stianbid.com.br/SchemaFundo}FPPRprtPendComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"retornoConsulta"})
@XmlRootElement(name = "ConsumirFundosPendentesResponse")
public class ConsumirFundosPendentesResponse {

    @XmlElement(name = "RetornoConsulta", required = true) protected FPPRprtPendComplexType retornoConsulta;

    /**
     * Obtém o valor da propriedade retornoConsulta.
     *
     * @return possible object is
     * {@link FPPRprtPendComplexType }
     */
    public FPPRprtPendComplexType getRetornoConsulta() {
        return retornoConsulta;
    }

    /**
     * Define o valor da propriedade retornoConsulta.
     *
     * @param value allowed object is
     *              {@link FPPRprtPendComplexType }
     */
    public void setRetornoConsulta(FPPRprtPendComplexType value) {
        this.retornoConsulta = value;
    }

}
