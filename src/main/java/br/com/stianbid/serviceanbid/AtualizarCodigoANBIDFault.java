package br.com.stianbid.serviceanbid;

import br.com.stianbid.common.MessageExceptionComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AtualizarCodANBID_fault" type="{http://www.stianbid.com.br/Common}MessageExceptionComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"atualizarCodANBIDFault"})
@XmlRootElement(name = "AtualizarCodigoANBID_fault")
public class AtualizarCodigoANBIDFault {

    @XmlElement(name = "AtualizarCodANBID_fault", required = true) protected MessageExceptionComplexType
        atualizarCodANBIDFault;

    /**
     * Obtém o valor da propriedade atualizarCodANBIDFault.
     *
     * @return possible object is
     * {@link MessageExceptionComplexType }
     */
    public MessageExceptionComplexType getAtualizarCodANBIDFault() {
        return atualizarCodANBIDFault;
    }

    /**
     * Define o valor da propriedade atualizarCodANBIDFault.
     *
     * @param value allowed object is
     *              {@link MessageExceptionComplexType }
     */
    public void setAtualizarCodANBIDFault(MessageExceptionComplexType value) {
        this.atualizarCodANBIDFault = value;
    }

}
