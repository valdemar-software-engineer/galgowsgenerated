package br.com.stianbid.serviceanbid;

import br.com.stianbid.schemaanbid.MessageUpdateComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequisicaoCodigoANBID" type="{http://www.stianbid.com.br/SchemaANBID}MessageUpdateComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"requisicaoCodigoANBID"})
@XmlRootElement(name = "AtualizarCodigoANBID")
public class AtualizarCodigoANBID {

    @XmlElement(name = "RequisicaoCodigoANBID", required = true) protected MessageUpdateComplexType
        requisicaoCodigoANBID;

    /**
     * Obtém o valor da propriedade requisicaoCodigoANBID.
     *
     * @return possible object is
     * {@link MessageUpdateComplexType }
     */
    public MessageUpdateComplexType getRequisicaoCodigoANBID() {
        return requisicaoCodigoANBID;
    }

    /**
     * Define o valor da propriedade requisicaoCodigoANBID.
     *
     * @param value allowed object is
     *              {@link MessageUpdateComplexType }
     */
    public void setRequisicaoCodigoANBID(MessageUpdateComplexType value) {
        this.requisicaoCodigoANBID = value;
    }

}
