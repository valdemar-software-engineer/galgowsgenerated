package br.com.stianbid.serviceanbid;

import br.com.stianbid.schemaplcota.MessageAvaliarPLCotaComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequisicaoValidacao" type="{http://www.stianbid.com.br/SchemaPLCota}MessageAvaliarPLCotaComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"requisicaoValidacao"})
@XmlRootElement(name = "AvaliarPLCota")
public class AvaliarPLCota {

    @XmlElement(name = "RequisicaoValidacao", required = true) protected MessageAvaliarPLCotaComplexType
        requisicaoValidacao;

    /**
     * Obtém o valor da propriedade requisicaoValidacao.
     *
     * @return possible object is
     * {@link MessageAvaliarPLCotaComplexType }
     */
    public MessageAvaliarPLCotaComplexType getRequisicaoValidacao() {
        return requisicaoValidacao;
    }

    /**
     * Define o valor da propriedade requisicaoValidacao.
     *
     * @param value allowed object is
     *              {@link MessageAvaliarPLCotaComplexType }
     */
    public void setRequisicaoValidacao(MessageAvaliarPLCotaComplexType value) {
        this.requisicaoValidacao = value;
    }

}
