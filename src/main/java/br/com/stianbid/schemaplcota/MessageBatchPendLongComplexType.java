package br.com.stianbid.schemaplcota;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de MessageBatchPendLongComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageBatchPendLongComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Marcador" type="{http://www.stianbid.com.br/SchemaPLCota}MarcadorLongType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageBatchPendLongComplexType", propOrder = {"marcador"})
public class MessageBatchPendLongComplexType extends MessageRequestComplexType {

    @XmlElement(name = "Marcador") protected Long marcador;

    /**
     * Obtém o valor da propriedade marcador.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getMarcador() {
        return marcador;
    }

    /**
     * Define o valor da propriedade marcador.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setMarcador(Long value) {
        this.marcador = value;
    }

}
