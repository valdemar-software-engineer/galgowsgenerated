package br.com.stianbid.schemaplcota;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Busca Entidade
 * <p>
 * <p>Classe Java de BuscaEntidadeComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="BuscaEntidadeComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Entidade" type="{http://www.stianbid.com.br/SchemaPLCota}EntidadeComplexType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BuscaEntidadeComplexType", propOrder = {"entidade"})
public class BuscaEntidadeComplexType {

    @XmlElement(name = "Entidade", required = true) protected List<EntidadeComplexType> entidade;

    /**
     * Gets the value of the entidade property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the entidade property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEntidade().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntidadeComplexType }
     */
    public List<EntidadeComplexType> getEntidade() {
        if (entidade == null) {
            entidade = new ArrayList<EntidadeComplexType>();
        }
        return this.entidade;
    }

}
