package br.com.stianbid.schemaplcota;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;


/**
 * <p>Classe Java de ParametrosPLCotaComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ParametrosPLCotaComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="BuscaEntidade" type="{http://www.stianbid.com.br/SchemaPLCota}BuscaEntidadeComplexType" minOccurs="0"/&gt;
 *           &lt;element name="BuscaObjeto" type="{http://www.stianbid.com.br/SchemaPLCota}BuscaObjetoComplexType" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="DtInicEnvioCota" type="{http://www.stianbid.com.br/Common}ISODateTime" minOccurs="0"/&gt;
 *         &lt;element name="DtFinEnvioCota" type="{http://www.stianbid.com.br/Common}ISODateTime" minOccurs="0"/&gt;
 *         &lt;element name="DtInicInfo" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="DtFinInfo" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="StatusInfo" type="{http://www.stianbid.com.br/SchemaPLCota}StatusInfoType" minOccurs="0"/&gt;
 *         &lt;element name="Atuacao" type="{http://www.stianbid.com.br/Common}AtuacaoType" minOccurs="0"/&gt;
 *         &lt;element name="PgNb" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParametrosPLCotaComplexType",
         propOrder = {"buscaEntidade", "buscaObjeto", "dtInicEnvioCota", "dtFinEnvioCota", "dtInicInfo", "dtFinInfo", "statusInfo", "atuacao", "pgNb"})
public class ParametrosPLCotaComplexType {

    @XmlElement(name = "BuscaEntidade") protected BuscaEntidadeComplexType buscaEntidade;
    @XmlElement(name = "BuscaObjeto") protected BuscaObjetoComplexType buscaObjeto;
    @XmlElement(name = "DtInicEnvioCota") @XmlSchemaType(name = "dateTime") protected XMLGregorianCalendar
        dtInicEnvioCota;
    @XmlElement(name = "DtFinEnvioCota") @XmlSchemaType(name = "dateTime") protected XMLGregorianCalendar
        dtFinEnvioCota;
    @XmlElement(name = "DtInicInfo") @XmlSchemaType(name = "date") protected XMLGregorianCalendar dtInicInfo;
    @XmlElement(name = "DtFinInfo") @XmlSchemaType(name = "date") protected XMLGregorianCalendar dtFinInfo;
    @XmlElement(name = "StatusInfo") protected Integer statusInfo;
    @XmlElement(name = "Atuacao") protected BigInteger atuacao;
    @XmlElement(name = "PgNb") protected int pgNb;

    /**
     * Obtém o valor da propriedade atuacao.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getAtuacao() {
        return atuacao;
    }

    /**
     * Define o valor da propriedade atuacao.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setAtuacao(BigInteger value) {
        this.atuacao = value;
    }

    /**
     * Obtém o valor da propriedade buscaEntidade.
     *
     * @return possible object is
     * {@link BuscaEntidadeComplexType }
     */
    public BuscaEntidadeComplexType getBuscaEntidade() {
        return buscaEntidade;
    }

    /**
     * Define o valor da propriedade buscaEntidade.
     *
     * @param value allowed object is
     *              {@link BuscaEntidadeComplexType }
     */
    public void setBuscaEntidade(BuscaEntidadeComplexType value) {
        this.buscaEntidade = value;
    }

    /**
     * Obtém o valor da propriedade buscaObjeto.
     *
     * @return possible object is
     * {@link BuscaObjetoComplexType }
     */
    public BuscaObjetoComplexType getBuscaObjeto() {
        return buscaObjeto;
    }

    /**
     * Define o valor da propriedade buscaObjeto.
     *
     * @param value allowed object is
     *              {@link BuscaObjetoComplexType }
     */
    public void setBuscaObjeto(BuscaObjetoComplexType value) {
        this.buscaObjeto = value;
    }

    /**
     * Obtém o valor da propriedade dtFinEnvioCota.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtFinEnvioCota() {
        return dtFinEnvioCota;
    }

    /**
     * Define o valor da propriedade dtFinEnvioCota.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtFinEnvioCota(XMLGregorianCalendar value) {
        this.dtFinEnvioCota = value;
    }

    /**
     * Obtém o valor da propriedade dtFinInfo.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtFinInfo() {
        return dtFinInfo;
    }

    /**
     * Define o valor da propriedade dtFinInfo.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtFinInfo(XMLGregorianCalendar value) {
        this.dtFinInfo = value;
    }

    /**
     * Obtém o valor da propriedade dtInicEnvioCota.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInicEnvioCota() {
        return dtInicEnvioCota;
    }

    /**
     * Define o valor da propriedade dtInicEnvioCota.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInicEnvioCota(XMLGregorianCalendar value) {
        this.dtInicEnvioCota = value;
    }

    /**
     * Obtém o valor da propriedade dtInicInfo.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInicInfo() {
        return dtInicInfo;
    }

    /**
     * Define o valor da propriedade dtInicInfo.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInicInfo(XMLGregorianCalendar value) {
        this.dtInicInfo = value;
    }

    /**
     * Obtém o valor da propriedade pgNb.
     */
    public int getPgNb() {
        return pgNb;
    }

    /**
     * Define o valor da propriedade pgNb.
     */
    public void setPgNb(int value) {
        this.pgNb = value;
    }

    /**
     * Obtém o valor da propriedade statusInfo.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getStatusInfo() {
        return statusInfo;
    }

    /**
     * Define o valor da propriedade statusInfo.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setStatusInfo(Integer value) {
        this.statusInfo = value;
    }

}
