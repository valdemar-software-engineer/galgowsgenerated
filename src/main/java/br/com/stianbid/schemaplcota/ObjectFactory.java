package br.com.stianbid.schemaplcota;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the br.com.stianbid.schemaplcota package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PlCota_QNAME = new QName("http://www.stianbid.com.br/SchemaPLCota", "PlCota");
    private final static QName _ConsultaPLCotaRequest_QNAME = new QName("http://www.stianbid.com.br/SchemaPLCota",
        "ConsultaPLCotaRequest");
    private final static QName _ValidarPLCotas_QNAME = new QName("http://www.stianbid.com.br/SchemaPLCota",
        "ValidarPLCotas");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.stianbid.schemaplcota
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BuscaEntidadeComplexType }
     */
    public BuscaEntidadeComplexType createBuscaEntidadeComplexType() {
        return new BuscaEntidadeComplexType();
    }

    /**
     * Create an instance of {@link BuscaObjetoComplexType }
     */
    public BuscaObjetoComplexType createBuscaObjetoComplexType() {
        return new BuscaObjetoComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageConsultaPLCotaComplexType }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.stianbid.com.br/SchemaPLCota", name = "ConsultaPLCotaRequest")
    public JAXBElement<MessageConsultaPLCotaComplexType> createConsultaPLCotaRequest(
        MessageConsultaPLCotaComplexType value) {
        return new JAXBElement<MessageConsultaPLCotaComplexType>(_ConsultaPLCotaRequest_QNAME,
            MessageConsultaPLCotaComplexType.class, null, value);
    }

    /**
     * Create an instance of {@link EntidadeComplexType }
     */
    public EntidadeComplexType createEntidadeComplexType() {
        return new EntidadeComplexType();
    }

    /**
     * Create an instance of {@link MessageAvaliarPLCotaComplexType }
     */
    public MessageAvaliarPLCotaComplexType createMessageAvaliarPLCotaComplexType() {
        return new MessageAvaliarPLCotaComplexType();
    }

    /**
     * Create an instance of {@link MessageBatchPendLongComplexType }
     */
    public MessageBatchPendLongComplexType createMessageBatchPendLongComplexType() {
        return new MessageBatchPendLongComplexType();
    }

    /**
     * Create an instance of {@link MessageBatchResponsePendLongComplexType }
     */
    public MessageBatchResponsePendLongComplexType createMessageBatchResponsePendLongComplexType() {
        return new MessageBatchResponsePendLongComplexType();
    }

    /**
     * Create an instance of {@link MessageCancelarPLCotaComplexType }
     */
    public MessageCancelarPLCotaComplexType createMessageCancelarPLCotaComplexType() {
        return new MessageCancelarPLCotaComplexType();
    }

    /**
     * Create an instance of {@link MessageConsultaPLCotaComplexType }
     */
    public MessageConsultaPLCotaComplexType createMessageConsultaPLCotaComplexType() {
        return new MessageConsultaPLCotaComplexType();
    }

    /**
     * Create an instance of {@link MessageEnviarPLCotaComplexType }
     */
    public MessageEnviarPLCotaComplexType createMessageEnviarPLCotaComplexType() {
        return new MessageEnviarPLCotaComplexType();
    }

    /**
     * Create an instance of {@link MessagePLCotaComplexType }
     */
    public MessagePLCotaComplexType createMessagePLCotaComplexType() {
        return new MessagePLCotaComplexType();
    }

    /**
     * Create an instance of {@link MessageRetornoPLCotaComplexType }
     */
    public MessageRetornoPLCotaComplexType createMessageRetornoPLCotaComplexType() {
        return new MessageRetornoPLCotaComplexType();
    }

    /**
     * Create an instance of {@link ObjetoComplexType }
     */
    public ObjetoComplexType createObjetoComplexType() {
        return new ObjetoComplexType();
    }

    /**
     * Create an instance of {@link ParametrosPLCotaComplexType }
     */
    public ParametrosPLCotaComplexType createParametrosPLCotaComplexType() {
        return new ParametrosPLCotaComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessagePLCotaComplexType }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.stianbid.com.br/SchemaPLCota", name = "PlCota")
    public JAXBElement<MessagePLCotaComplexType> createPlCota(MessagePLCotaComplexType value) {
        return new JAXBElement<MessagePLCotaComplexType>(_PlCota_QNAME, MessagePLCotaComplexType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageAvaliarPLCotaComplexType }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.stianbid.com.br/SchemaPLCota", name = "ValidarPLCotas")
    public JAXBElement<MessageAvaliarPLCotaComplexType> createValidarPLCotas(MessageAvaliarPLCotaComplexType value) {
        return new JAXBElement<MessageAvaliarPLCotaComplexType>(_ValidarPLCotas_QNAME,
            MessageAvaliarPLCotaComplexType.class, null, value);
    }

}
