package br.com.stianbid.schemaplcota;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Entidade
 * <p>
 * <p>Classe Java de EntidadeComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="EntidadeComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="CPF" type="{http://www.stianbid.com.br/SchemaPLCota}CPFIdentifier"/&gt;
 *           &lt;element name="CNPJ" type="{http://www.stianbid.com.br/SchemaPLCota}CNPJIdentifier"/&gt;
 *           &lt;element name="IdentifEstrangPF" type="{http://www.stianbid.com.br/Common}Max350Text"/&gt;
 *           &lt;element name="IdentifEstrangPJ" type="{http://www.stianbid.com.br/Common}Max350Text"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntidadeComplexType", propOrder = {"cpf", "cnpj", "identifEstrangPF", "identifEstrangPJ"})
public class EntidadeComplexType {

    @XmlElement(name = "CPF") protected Long cpf;
    @XmlElement(name = "CNPJ") protected Long cnpj;
    @XmlElement(name = "IdentifEstrangPF") protected String identifEstrangPF;
    @XmlElement(name = "IdentifEstrangPJ") protected String identifEstrangPJ;

    /**
     * Obtém o valor da propriedade cnpj.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getCNPJ() {
        return cnpj;
    }

    /**
     * Define o valor da propriedade cnpj.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setCNPJ(Long value) {
        this.cnpj = value;
    }

    /**
     * Obtém o valor da propriedade cpf.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getCPF() {
        return cpf;
    }

    /**
     * Define o valor da propriedade cpf.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setCPF(Long value) {
        this.cpf = value;
    }

    /**
     * Obtém o valor da propriedade identifEstrangPF.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdentifEstrangPF() {
        return identifEstrangPF;
    }

    /**
     * Define o valor da propriedade identifEstrangPF.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdentifEstrangPF(String value) {
        this.identifEstrangPF = value;
    }

    /**
     * Obtém o valor da propriedade identifEstrangPJ.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdentifEstrangPJ() {
        return identifEstrangPJ;
    }

    /**
     * Define o valor da propriedade identifEstrangPJ.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdentifEstrangPJ(String value) {
        this.identifEstrangPJ = value;
    }

}
