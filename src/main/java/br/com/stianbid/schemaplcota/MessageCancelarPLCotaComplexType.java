package br.com.stianbid.schemaplcota;

import iso.std.iso._20022.tech.xsd.reda_002_001.PriceReportCancellationV04;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de MessageCancelarPLCotaComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageCancelarPLCotaComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PricRptCxlV04" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}PriceReportCancellationV04"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageCancelarPLCotaComplexType", propOrder = {"pricRptCxlV04"})
public class MessageCancelarPLCotaComplexType {

    @XmlElement(name = "PricRptCxlV04", required = true) protected PriceReportCancellationV04 pricRptCxlV04;

    /**
     * Obtém o valor da propriedade pricRptCxlV04.
     *
     * @return possible object is
     * {@link PriceReportCancellationV04 }
     */
    public PriceReportCancellationV04 getPricRptCxlV04() {
        return pricRptCxlV04;
    }

    /**
     * Define o valor da propriedade pricRptCxlV04.
     *
     * @param value allowed object is
     *              {@link PriceReportCancellationV04 }
     */
    public void setPricRptCxlV04(PriceReportCancellationV04 value) {
        this.pricRptCxlV04 = value;
    }

}
