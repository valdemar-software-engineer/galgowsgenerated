package br.com.stianbid.schemaplcota;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Busca Objeto
 * <p>
 * <p>Classe Java de BuscaObjetoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="BuscaObjetoComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Objeto" type="{http://www.stianbid.com.br/SchemaPLCota}ObjetoComplexType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BuscaObjetoComplexType", propOrder = {"objeto"})
public class BuscaObjetoComplexType {

    @XmlElement(name = "Objeto", required = true) protected List<ObjetoComplexType> objeto;

    /**
     * Gets the value of the objeto property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objeto property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjeto().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjetoComplexType }
     */
    public List<ObjetoComplexType> getObjeto() {
        if (objeto == null) {
            objeto = new ArrayList<ObjetoComplexType>();
        }
        return this.objeto;
    }

}
