package br.com.stianbid.schemaplcota;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Consulta PLCOTA
 * <p>
 * <p>Classe Java de MessageConsultaPLCotaComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageConsultaPLCotaComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Parametros" type="{http://www.stianbid.com.br/SchemaPLCota}ParametrosPLCotaComplexType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageConsultaPLCotaComplexType", propOrder = {"parametros"})
public class MessageConsultaPLCotaComplexType extends MessageRequestComplexType {

    @XmlElement(name = "Parametros", required = true) protected ParametrosPLCotaComplexType parametros;

    /**
     * Obtém o valor da propriedade parametros.
     *
     * @return possible object is
     * {@link ParametrosPLCotaComplexType }
     */
    public ParametrosPLCotaComplexType getParametros() {
        return parametros;
    }

    /**
     * Define o valor da propriedade parametros.
     *
     * @param value allowed object is
     *              {@link ParametrosPLCotaComplexType }
     */
    public void setParametros(ParametrosPLCotaComplexType value) {
        this.parametros = value;
    }

}
