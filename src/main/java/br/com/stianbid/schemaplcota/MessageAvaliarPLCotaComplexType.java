package br.com.stianbid.schemaplcota;

import iso.std.iso._20022.tech.xsd.reda_001_001.PriceReportV04;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de MessageAvaliarPLCotaComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageAvaliarPLCotaComplexType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PricRptV04" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PriceReportV04"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageAvaliarPLCotaComplexType", propOrder = {"pricRptV04"})
public class MessageAvaliarPLCotaComplexType {

    @XmlElement(name = "PricRptV04", required = true) protected PriceReportV04 pricRptV04;

    /**
     * Obtém o valor da propriedade pricRptV04.
     *
     * @return possible object is
     * {@link PriceReportV04 }
     */
    public PriceReportV04 getPricRptV04() {
        return pricRptV04;
    }

    /**
     * Define o valor da propriedade pricRptV04.
     *
     * @param value allowed object is
     *              {@link PriceReportV04 }
     */
    public void setPricRptV04(PriceReportV04 value) {
        this.pricRptV04 = value;
    }

}
