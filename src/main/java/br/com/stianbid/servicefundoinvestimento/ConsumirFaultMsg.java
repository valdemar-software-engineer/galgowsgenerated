package br.com.stianbid.servicefundoinvestimento;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.7.3
 * 2013-03-01T10:31:52.404-03:00
 * Generated source version: 2.7.3
 */

@WebFault(name = "Consumir_fault", targetNamespace = "http://www.stianbid.com.br/ServiceFundoInvestimento/")
public class ConsumirFaultMsg extends Exception {

    private br.com.stianbid.servicefundoinvestimento.ConsumirFault consumirFault;

    public ConsumirFaultMsg() {
        super();
    }

    public ConsumirFaultMsg(String message) {
        super(message);
    }

    public ConsumirFaultMsg(String message, Throwable cause) {
        super(message, cause);
    }

    public ConsumirFaultMsg(String message, br.com.stianbid.servicefundoinvestimento.ConsumirFault consumirFault) {
        super(message);
        this.consumirFault = consumirFault;
    }

    public ConsumirFaultMsg(String message, br.com.stianbid.servicefundoinvestimento.ConsumirFault consumirFault,
        Throwable cause) {
        super(message, cause);
        this.consumirFault = consumirFault;
    }

    public br.com.stianbid.servicefundoinvestimento.ConsumirFault getFaultInfo() {
        return this.consumirFault;
    }
}
