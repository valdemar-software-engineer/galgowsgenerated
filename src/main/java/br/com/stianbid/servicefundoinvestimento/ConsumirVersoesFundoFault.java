package br.com.stianbid.servicefundoinvestimento;

import br.com.stianbid.common.MessageExceptionComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsumirVersoesFundo_fault" type="{http://www.stianbid.com.br/Common}MessageExceptionComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"consumirVersoesFundoFault"})
@XmlRootElement(name = "ConsumirVersoesFundo_fault")
public class ConsumirVersoesFundoFault {

    @XmlElement(name = "ConsumirVersoesFundo_fault", required = true) protected MessageExceptionComplexType
        consumirVersoesFundoFault;

    /**
     * Obtém o valor da propriedade consumirVersoesFundoFault.
     *
     * @return possible object is
     * {@link MessageExceptionComplexType }
     */
    public MessageExceptionComplexType getConsumirVersoesFundoFault() {
        return consumirVersoesFundoFault;
    }

    /**
     * Define o valor da propriedade consumirVersoesFundoFault.
     *
     * @param value allowed object is
     *              {@link MessageExceptionComplexType }
     */
    public void setConsumirVersoesFundoFault(MessageExceptionComplexType value) {
        this.consumirVersoesFundoFault = value;
    }

}
