package br.com.stianbid.servicefundoinvestimento;

import br.com.stianbid.schemafundo.RequestEnviaDocComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequisicaoConsulta" type="{http://www.stianbid.com.br/SchemaFundo}RequestEnviaDocComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"requisicaoConsulta"})
@XmlRootElement(name = "EnviarDocumento")
public class EnviarDocumento {

    @XmlElement(name = "RequisicaoConsulta", required = true) protected RequestEnviaDocComplexType requisicaoConsulta;

    /**
     * Obtém o valor da propriedade requisicaoConsulta.
     *
     * @return possible object is
     * {@link RequestEnviaDocComplexType }
     */
    public RequestEnviaDocComplexType getRequisicaoConsulta() {
        return requisicaoConsulta;
    }

    /**
     * Define o valor da propriedade requisicaoConsulta.
     *
     * @param value allowed object is
     *              {@link RequestEnviaDocComplexType }
     */
    public void setRequisicaoConsulta(RequestEnviaDocComplexType value) {
        this.requisicaoConsulta = value;
    }

}
