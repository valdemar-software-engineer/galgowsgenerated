package br.com.stianbid.servicefundoinvestimento;

import br.com.stianbid.schemafundo.ResponseConsultaDocumentoComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RespostaConsulta" type="{http://www.stianbid.com.br/SchemaFundo}ResponseConsultaDocumentoComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"respostaConsulta"})
@XmlRootElement(name = "ConsultarDocumentoResponse1")
public class ConsultarDocumentoResponse1 {

    @XmlElement(name = "RespostaConsulta", required = true) protected ResponseConsultaDocumentoComplexType
        respostaConsulta;

    /**
     * Obtém o valor da propriedade respostaConsulta.
     *
     * @return possible object is
     * {@link ResponseConsultaDocumentoComplexType }
     */
    public ResponseConsultaDocumentoComplexType getRespostaConsulta() {
        return respostaConsulta;
    }

    /**
     * Define o valor da propriedade respostaConsulta.
     *
     * @param value allowed object is
     *              {@link ResponseConsultaDocumentoComplexType }
     */
    public void setRespostaConsulta(ResponseConsultaDocumentoComplexType value) {
        this.respostaConsulta = value;
    }

}
