package br.com.stianbid.servicefundoinvestimento;

import br.com.stianbid.schemafundo.RequestConsumirVersoesFundoComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequisicaoConsulta" type="{http://www.stianbid.com.br/SchemaFundo}RequestConsumirVersoesFundoComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"requisicaoConsulta"})
@XmlRootElement(name = "ConsumirVersoesFundo")
public class ConsumirVersoesFundo {

    @XmlElement(name = "RequisicaoConsulta", required = true) protected RequestConsumirVersoesFundoComplexType
        requisicaoConsulta;

    /**
     * Obtém o valor da propriedade requisicaoConsulta.
     *
     * @return possible object is
     * {@link RequestConsumirVersoesFundoComplexType }
     */
    public RequestConsumirVersoesFundoComplexType getRequisicaoConsulta() {
        return requisicaoConsulta;
    }

    /**
     * Define o valor da propriedade requisicaoConsulta.
     *
     * @param value allowed object is
     *              {@link RequestConsumirVersoesFundoComplexType }
     */
    public void setRequisicaoConsulta(RequestConsumirVersoesFundoComplexType value) {
        this.requisicaoConsulta = value;
    }

}
