package br.com.stianbid.servicefundoinvestimento;

import br.com.stianbid.common.MessageExceptionComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AtualizarFundosCorrecao_fault" type="{http://www.stianbid.com.br/Common}MessageExceptionComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"atualizarFundosCorrecaoFault"})
@XmlRootElement(name = "AtualizarFundosCorrecao_fault")
public class AtualizarFundosCorrecaoFault {

    @XmlElement(name = "AtualizarFundosCorrecao_fault", required = true) protected MessageExceptionComplexType
        atualizarFundosCorrecaoFault;

    /**
     * Obtém o valor da propriedade atualizarFundosCorrecaoFault.
     *
     * @return possible object is
     * {@link MessageExceptionComplexType }
     */
    public MessageExceptionComplexType getAtualizarFundosCorrecaoFault() {
        return atualizarFundosCorrecaoFault;
    }

    /**
     * Define o valor da propriedade atualizarFundosCorrecaoFault.
     *
     * @param value allowed object is
     *              {@link MessageExceptionComplexType }
     */
    public void setAtualizarFundosCorrecaoFault(MessageExceptionComplexType value) {
        this.atualizarFundosCorrecaoFault = value;
    }

}
