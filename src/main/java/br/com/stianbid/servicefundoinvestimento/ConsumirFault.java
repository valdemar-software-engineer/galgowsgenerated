package br.com.stianbid.servicefundoinvestimento;

import br.com.stianbid.common.MessageExceptionComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Consumir_fault" type="{http://www.stianbid.com.br/Common}MessageExceptionComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"consumirFault"})
@XmlRootElement(name = "Consumir_fault")
public class ConsumirFault {

    @XmlElement(name = "Consumir_fault", required = true) protected MessageExceptionComplexType consumirFault;

    /**
     * Obtém o valor da propriedade consumirFault.
     *
     * @return possible object is
     * {@link MessageExceptionComplexType }
     */
    public MessageExceptionComplexType getConsumirFault() {
        return consumirFault;
    }

    /**
     * Define o valor da propriedade consumirFault.
     *
     * @param value allowed object is
     *              {@link MessageExceptionComplexType }
     */
    public void setConsumirFault(MessageExceptionComplexType value) {
        this.consumirFault = value;
    }

}
