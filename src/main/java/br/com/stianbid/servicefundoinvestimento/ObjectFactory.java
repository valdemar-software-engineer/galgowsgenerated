package br.com.stianbid.servicefundoinvestimento;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the br.com.stianbid.servicefundoinvestimento package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.stianbid.servicefundoinvestimento
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Alterar }
     */
    public Alterar createAlterar() {
        return new Alterar();
    }

    /**
     * Create an instance of {@link AlterarFault }
     */
    public AlterarFault createAlterarFault() {
        return new AlterarFault();
    }

    /**
     * Create an instance of {@link AlterarResponse1 }
     */
    public AlterarResponse1 createAlterarResponse1() {
        return new AlterarResponse1();
    }

    /**
     * Create an instance of {@link AtualizarFundosCorrecao }
     */
    public AtualizarFundosCorrecao createAtualizarFundosCorrecao() {
        return new AtualizarFundosCorrecao();
    }

    /**
     * Create an instance of {@link AtualizarFundosCorrecaoFault }
     */
    public AtualizarFundosCorrecaoFault createAtualizarFundosCorrecaoFault() {
        return new AtualizarFundosCorrecaoFault();
    }

    /**
     * Create an instance of {@link AtualizarFundosCorrecaoResponse }
     */
    public AtualizarFundosCorrecaoResponse createAtualizarFundosCorrecaoResponse() {
        return new AtualizarFundosCorrecaoResponse();
    }

    /**
     * Create an instance of {@link ConsultarDocumento }
     */
    public ConsultarDocumento createConsultarDocumento() {
        return new ConsultarDocumento();
    }

    /**
     * Create an instance of {@link ConsultarDocumentoFault }
     */
    public ConsultarDocumentoFault createConsultarDocumentoFault() {
        return new ConsultarDocumentoFault();
    }

    /**
     * Create an instance of {@link ConsultarDocumentoResponse1 }
     */
    public ConsultarDocumentoResponse1 createConsultarDocumentoResponse1() {
        return new ConsultarDocumentoResponse1();
    }

    /**
     * Create an instance of {@link Consumir }
     */
    public Consumir createConsumir() {
        return new Consumir();
    }

    /**
     * Create an instance of {@link ConsumirFault }
     */
    public ConsumirFault createConsumirFault() {
        return new ConsumirFault();
    }

    /**
     * Create an instance of {@link ConsumirFundosCorrecao }
     */
    public ConsumirFundosCorrecao createConsumirFundosCorrecao() {
        return new ConsumirFundosCorrecao();
    }

    /**
     * Create an instance of {@link ConsumirFundosCorrecaoFault }
     */
    public ConsumirFundosCorrecaoFault createConsumirFundosCorrecaoFault() {
        return new ConsumirFundosCorrecaoFault();
    }

    /**
     * Create an instance of {@link ConsumirFundosCorrecaoResponse }
     */
    public ConsumirFundosCorrecaoResponse createConsumirFundosCorrecaoResponse() {
        return new ConsumirFundosCorrecaoResponse();
    }

    /**
     * Create an instance of {@link ConsumirPendentes }
     */
    public ConsumirPendentes createConsumirPendentes() {
        return new ConsumirPendentes();
    }

    /**
     * Create an instance of {@link ConsumirPendentesFault }
     */
    public ConsumirPendentesFault createConsumirPendentesFault() {
        return new ConsumirPendentesFault();
    }

    /**
     * Create an instance of {@link ConsumirPendentesResponse }
     */
    public ConsumirPendentesResponse createConsumirPendentesResponse() {
        return new ConsumirPendentesResponse();
    }

    /**
     * Create an instance of {@link ConsumirResponse }
     */
    public ConsumirResponse createConsumirResponse() {
        return new ConsumirResponse();
    }

    /**
     * Create an instance of {@link ConsumirVersoesFundo }
     */
    public ConsumirVersoesFundo createConsumirVersoesFundo() {
        return new ConsumirVersoesFundo();
    }

    /**
     * Create an instance of {@link ConsumirVersoesFundoFault }
     */
    public ConsumirVersoesFundoFault createConsumirVersoesFundoFault() {
        return new ConsumirVersoesFundoFault();
    }

    /**
     * Create an instance of {@link ConsumirVersoesFundoResponse1 }
     */
    public ConsumirVersoesFundoResponse1 createConsumirVersoesFundoResponse1() {
        return new ConsumirVersoesFundoResponse1();
    }

    /**
     * Create an instance of {@link EnviarDocumento }
     */
    public EnviarDocumento createEnviarDocumento() {
        return new EnviarDocumento();
    }

    /**
     * Create an instance of {@link EnviarDocumentoFault }
     */
    public EnviarDocumentoFault createEnviarDocumentoFault() {
        return new EnviarDocumentoFault();
    }

    /**
     * Create an instance of {@link EnviarDocumentoResponse }
     */
    public EnviarDocumentoResponse createEnviarDocumentoResponse() {
        return new EnviarDocumentoResponse();
    }

    /**
     * Create an instance of {@link FinalizarFundos }
     */
    public FinalizarFundos createFinalizarFundos() {
        return new FinalizarFundos();
    }

    /**
     * Create an instance of {@link FinalizarFundosFault }
     */
    public FinalizarFundosFault createFinalizarFundosFault() {
        return new FinalizarFundosFault();
    }

    /**
     * Create an instance of {@link FinalizarFundosResponse1 }
     */
    public FinalizarFundosResponse1 createFinalizarFundosResponse1() {
        return new FinalizarFundosResponse1();
    }

    /**
     * Create an instance of {@link Incluir }
     */
    public Incluir createIncluir() {
        return new Incluir();
    }

    /**
     * Create an instance of {@link IncluirFault }
     */
    public IncluirFault createIncluirFault() {
        return new IncluirFault();
    }

    /**
     * Create an instance of {@link IncluirResponse }
     */
    public IncluirResponse createIncluirResponse() {
        return new IncluirResponse();
    }

}
