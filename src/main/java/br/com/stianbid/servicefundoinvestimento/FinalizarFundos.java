package br.com.stianbid.servicefundoinvestimento;

import br.com.stianbid.schemafundo.RequestFinalizarFundosComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequisicaoFinalizar" type="{http://www.stianbid.com.br/SchemaFundo}RequestFinalizarFundosComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"requisicaoFinalizar"})
@XmlRootElement(name = "FinalizarFundos")
public class FinalizarFundos {

    @XmlElement(name = "RequisicaoFinalizar", required = true) protected RequestFinalizarFundosComplexType
        requisicaoFinalizar;

    /**
     * Obtém o valor da propriedade requisicaoFinalizar.
     *
     * @return possible object is
     * {@link RequestFinalizarFundosComplexType }
     */
    public RequestFinalizarFundosComplexType getRequisicaoFinalizar() {
        return requisicaoFinalizar;
    }

    /**
     * Define o valor da propriedade requisicaoFinalizar.
     *
     * @param value allowed object is
     *              {@link RequestFinalizarFundosComplexType }
     */
    public void setRequisicaoFinalizar(RequestFinalizarFundosComplexType value) {
        this.requisicaoFinalizar = value;
    }

}
