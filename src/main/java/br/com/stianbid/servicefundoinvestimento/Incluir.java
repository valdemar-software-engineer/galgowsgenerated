package br.com.stianbid.servicefundoinvestimento;

import br.com.stianbid.schemafundo.RequestIncluirFundoComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequisicaoConsulta" type="{http://www.stianbid.com.br/SchemaFundo}RequestIncluirFundoComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"requisicaoConsulta"})
@XmlRootElement(name = "Incluir")
public class Incluir {

    @XmlElement(name = "RequisicaoConsulta", required = true) protected RequestIncluirFundoComplexType
        requisicaoConsulta;

    /**
     * Obtém o valor da propriedade requisicaoConsulta.
     *
     * @return possible object is
     * {@link RequestIncluirFundoComplexType }
     */
    public RequestIncluirFundoComplexType getRequisicaoConsulta() {
        return requisicaoConsulta;
    }

    /**
     * Define o valor da propriedade requisicaoConsulta.
     *
     * @param value allowed object is
     *              {@link RequestIncluirFundoComplexType }
     */
    public void setRequisicaoConsulta(RequestIncluirFundoComplexType value) {
        this.requisicaoConsulta = value;
    }

}
