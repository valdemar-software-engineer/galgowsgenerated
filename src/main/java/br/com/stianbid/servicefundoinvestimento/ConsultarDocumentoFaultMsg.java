package br.com.stianbid.servicefundoinvestimento;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.7.3
 * 2013-03-01T10:31:52.407-03:00
 * Generated source version: 2.7.3
 */

@WebFault(name = "ConsultarDocumento_fault", targetNamespace = "http://www.stianbid.com.br/ServiceFundoInvestimento/")
public class ConsultarDocumentoFaultMsg extends Exception {

    private br.com.stianbid.servicefundoinvestimento.ConsultarDocumentoFault consultarDocumentoFault;

    public ConsultarDocumentoFaultMsg() {
        super();
    }

    public ConsultarDocumentoFaultMsg(String message) {
        super(message);
    }

    public ConsultarDocumentoFaultMsg(String message, Throwable cause) {
        super(message, cause);
    }

    public ConsultarDocumentoFaultMsg(String message,
        br.com.stianbid.servicefundoinvestimento.ConsultarDocumentoFault consultarDocumentoFault) {
        super(message);
        this.consultarDocumentoFault = consultarDocumentoFault;
    }

    public ConsultarDocumentoFaultMsg(String message,
        br.com.stianbid.servicefundoinvestimento.ConsultarDocumentoFault consultarDocumentoFault, Throwable cause) {
        super(message, cause);
        this.consultarDocumentoFault = consultarDocumentoFault;
    }

    public br.com.stianbid.servicefundoinvestimento.ConsultarDocumentoFault getFaultInfo() {
        return this.consultarDocumentoFault;
    }
}
