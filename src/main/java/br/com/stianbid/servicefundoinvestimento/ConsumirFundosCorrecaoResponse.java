package br.com.stianbid.servicefundoinvestimento;

import br.com.stianbid.schemafundo.ResponseConsumirFundosComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RespostaConsulta" type="{http://www.stianbid.com.br/SchemaFundo}ResponseConsumirFundosComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"respostaConsulta"})
@XmlRootElement(name = "ConsumirFundosCorrecaoResponse")
public class ConsumirFundosCorrecaoResponse {

    @XmlElement(name = "RespostaConsulta", required = true) protected ResponseConsumirFundosComplexType
        respostaConsulta;

    /**
     * Obtém o valor da propriedade respostaConsulta.
     *
     * @return possible object is
     * {@link ResponseConsumirFundosComplexType }
     */
    public ResponseConsumirFundosComplexType getRespostaConsulta() {
        return respostaConsulta;
    }

    /**
     * Define o valor da propriedade respostaConsulta.
     *
     * @param value allowed object is
     *              {@link ResponseConsumirFundosComplexType }
     */
    public void setRespostaConsulta(ResponseConsumirFundosComplexType value) {
        this.respostaConsulta = value;
    }

}
