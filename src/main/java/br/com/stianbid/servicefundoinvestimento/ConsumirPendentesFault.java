package br.com.stianbid.servicefundoinvestimento;

import br.com.stianbid.common.MessageExceptionComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsumirPendentes_fault" type="{http://www.stianbid.com.br/Common}MessageExceptionComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"consumirPendentesFault"})
@XmlRootElement(name = "ConsumirPendentes_fault")
public class ConsumirPendentesFault {

    @XmlElement(name = "ConsumirPendentes_fault", required = true) protected MessageExceptionComplexType
        consumirPendentesFault;

    /**
     * Obtém o valor da propriedade consumirPendentesFault.
     *
     * @return possible object is
     * {@link MessageExceptionComplexType }
     */
    public MessageExceptionComplexType getConsumirPendentesFault() {
        return consumirPendentesFault;
    }

    /**
     * Define o valor da propriedade consumirPendentesFault.
     *
     * @param value allowed object is
     *              {@link MessageExceptionComplexType }
     */
    public void setConsumirPendentesFault(MessageExceptionComplexType value) {
        this.consumirPendentesFault = value;
    }

}
