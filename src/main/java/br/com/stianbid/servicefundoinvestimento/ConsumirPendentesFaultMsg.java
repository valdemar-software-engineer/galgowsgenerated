package br.com.stianbid.servicefundoinvestimento;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.7.3
 * 2013-03-01T10:31:52.421-03:00
 * Generated source version: 2.7.3
 */

@WebFault(name = "ConsumirPendentes_fault", targetNamespace = "http://www.stianbid.com.br/ServiceFundoInvestimento/")
public class ConsumirPendentesFaultMsg extends Exception {

    private br.com.stianbid.servicefundoinvestimento.ConsumirPendentesFault consumirPendentesFault;

    public ConsumirPendentesFaultMsg() {
        super();
    }

    public ConsumirPendentesFaultMsg(String message) {
        super(message);
    }

    public ConsumirPendentesFaultMsg(String message, Throwable cause) {
        super(message, cause);
    }

    public ConsumirPendentesFaultMsg(String message,
        br.com.stianbid.servicefundoinvestimento.ConsumirPendentesFault consumirPendentesFault) {
        super(message);
        this.consumirPendentesFault = consumirPendentesFault;
    }

    public ConsumirPendentesFaultMsg(String message,
        br.com.stianbid.servicefundoinvestimento.ConsumirPendentesFault consumirPendentesFault, Throwable cause) {
        super(message, cause);
        this.consumirPendentesFault = consumirPendentesFault;
    }

    public br.com.stianbid.servicefundoinvestimento.ConsumirPendentesFault getFaultInfo() {
        return this.consumirPendentesFault;
    }
}
