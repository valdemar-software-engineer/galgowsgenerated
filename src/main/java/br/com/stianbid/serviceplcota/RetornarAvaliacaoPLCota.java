package br.com.stianbid.serviceplcota;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DadosAvaliacao" type="{http://www.stianbid.com.br/ServicePLCota/}MessageAvaliacaoPLCotaComplexType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"dadosAvaliacao"})
@XmlRootElement(name = "RetornarAvaliacaoPLCota")
public class RetornarAvaliacaoPLCota {

    @XmlElement(name = "DadosAvaliacao", required = true) protected MessageAvaliacaoPLCotaComplexType dadosAvaliacao;

    /**
     * Obtém o valor da propriedade dadosAvaliacao.
     *
     * @return possible object is
     * {@link MessageAvaliacaoPLCotaComplexType }
     */
    public MessageAvaliacaoPLCotaComplexType getDadosAvaliacao() {
        return dadosAvaliacao;
    }

    /**
     * Define o valor da propriedade dadosAvaliacao.
     *
     * @param value allowed object is
     *              {@link MessageAvaliacaoPLCotaComplexType }
     */
    public void setDadosAvaliacao(MessageAvaliacaoPLCotaComplexType value) {
        this.dadosAvaliacao = value;
    }

}
