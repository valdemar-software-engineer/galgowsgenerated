package br.com.stianbid.serviceplcota;

import br.com.stianbid.schemaplcota.MessageEnviarPLCotaComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DadosEnvio" type="{http://www.stianbid.com.br/SchemaPLCota}MessageEnviarPLCotaComplexType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"dadosEnvio"})
@XmlRootElement(name = "Enviar")
public class Enviar {

    @XmlElement(name = "DadosEnvio", required = true) protected MessageEnviarPLCotaComplexType dadosEnvio;

    /**
     * Obtém o valor da propriedade dadosEnvio.
     *
     * @return possible object is
     * {@link MessageEnviarPLCotaComplexType }
     */
    public MessageEnviarPLCotaComplexType getDadosEnvio() {
        return dadosEnvio;
    }

    /**
     * Define o valor da propriedade dadosEnvio.
     *
     * @param value allowed object is
     *              {@link MessageEnviarPLCotaComplexType }
     */
    public void setDadosEnvio(MessageEnviarPLCotaComplexType value) {
        this.dadosEnvio = value;
    }

}
