package br.com.stianbid.serviceplcota;

import br.com.stianbid.schemaplcota.MessageCancelarPLCotaComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DadosCancelamento" type="{http://www.stianbid.com.br/SchemaPLCota}MessageCancelarPLCotaComplexType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"dadosCancelamento"})
@XmlRootElement(name = "Cancelar")
public class Cancelar {

    @XmlElement(name = "DadosCancelamento", required = true) protected MessageCancelarPLCotaComplexType
        dadosCancelamento;

    /**
     * Obtém o valor da propriedade dadosCancelamento.
     *
     * @return possible object is
     * {@link MessageCancelarPLCotaComplexType }
     */
    public MessageCancelarPLCotaComplexType getDadosCancelamento() {
        return dadosCancelamento;
    }

    /**
     * Define o valor da propriedade dadosCancelamento.
     *
     * @param value allowed object is
     *              {@link MessageCancelarPLCotaComplexType }
     */
    public void setDadosCancelamento(MessageCancelarPLCotaComplexType value) {
        this.dadosCancelamento = value;
    }

}
