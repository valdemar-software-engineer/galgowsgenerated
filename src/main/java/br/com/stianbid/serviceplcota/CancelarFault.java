package br.com.stianbid.serviceplcota;

import br.com.stianbid.common.MessageExceptionComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Cancelar_fault" type="{http://www.stianbid.com.br/Common}MessageExceptionComplexType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"cancelarFault"})
@XmlRootElement(name = "Cancelar_fault")
public class CancelarFault {

    @XmlElement(name = "Cancelar_fault", required = true) protected MessageExceptionComplexType cancelarFault;

    /**
     * Obtém o valor da propriedade cancelarFault.
     *
     * @return possible object is
     * {@link MessageExceptionComplexType }
     */
    public MessageExceptionComplexType getCancelarFault() {
        return cancelarFault;
    }

    /**
     * Define o valor da propriedade cancelarFault.
     *
     * @param value allowed object is
     *              {@link MessageExceptionComplexType }
     */
    public void setCancelarFault(MessageExceptionComplexType value) {
        this.cancelarFault = value;
    }

}
