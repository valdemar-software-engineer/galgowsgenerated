package br.com.stianbid.serviceplcota;

import br.com.stianbid.schemaplcota.MessageBatchPendLongComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RequisicaoConsulta" type="{http://www.stianbid.com.br/SchemaPLCota}MessageBatchPendLongComplexType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"requisicaoConsulta"})
@XmlRootElement(name = "ConsumirPendentes")
public class ConsumirPendentes {

    @XmlElement(name = "RequisicaoConsulta", required = true) protected MessageBatchPendLongComplexType
        requisicaoConsulta;

    /**
     * Obtém o valor da propriedade requisicaoConsulta.
     *
     * @return possible object is
     * {@link MessageBatchPendLongComplexType }
     */
    public MessageBatchPendLongComplexType getRequisicaoConsulta() {
        return requisicaoConsulta;
    }

    /**
     * Define o valor da propriedade requisicaoConsulta.
     *
     * @param value allowed object is
     *              {@link MessageBatchPendLongComplexType }
     */
    public void setRequisicaoConsulta(MessageBatchPendLongComplexType value) {
        this.requisicaoConsulta = value;
    }

}
