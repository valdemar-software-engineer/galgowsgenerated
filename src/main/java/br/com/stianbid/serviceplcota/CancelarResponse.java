package br.com.stianbid.serviceplcota;

import br.com.stianbid.common.MessageReceiptComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReciboCancelamento" type="{http://www.stianbid.com.br/Common}MessageReceiptComplexType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"reciboCancelamento"})
@XmlRootElement(name = "CancelarResponse")
public class CancelarResponse {

    @XmlElement(name = "ReciboCancelamento", required = true) protected MessageReceiptComplexType reciboCancelamento;

    /**
     * Obtém o valor da propriedade reciboCancelamento.
     *
     * @return possible object is
     * {@link MessageReceiptComplexType }
     */
    public MessageReceiptComplexType getReciboCancelamento() {
        return reciboCancelamento;
    }

    /**
     * Define o valor da propriedade reciboCancelamento.
     *
     * @param value allowed object is
     *              {@link MessageReceiptComplexType }
     */
    public void setReciboCancelamento(MessageReceiptComplexType value) {
        this.reciboCancelamento = value;
    }

}
