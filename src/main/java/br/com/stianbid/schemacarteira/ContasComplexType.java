package br.com.stianbid.schemacarteira;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * Contas
 * <p>
 * <p>Classe Java de ContasComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ContasComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodSusep" type="{http://www.stianbid.com.br/SchemaCarteira}Number_5" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContasComplexType", propOrder = {"codSusep"})
public class ContasComplexType {

    @XmlElement(name = "CodSusep") protected BigDecimal codSusep;

    /**
     * Obtém o valor da propriedade codSusep.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getCodSusep() {
        return codSusep;
    }

    /**
     * Define o valor da propriedade codSusep.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setCodSusep(BigDecimal value) {
        this.codSusep = value;
    }

}
