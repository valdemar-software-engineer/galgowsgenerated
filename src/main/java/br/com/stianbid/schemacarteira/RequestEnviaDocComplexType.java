package br.com.stianbid.schemacarteira;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Enviar Documentos.
 * <p>
 * <p>Classe Java de RequestEnviaDocComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="RequestEnviaDocComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType">
 *       &lt;sequence>
 *         &lt;element name="Documentos" type="{http://www.stianbid.com.br/SchemaCarteira}EnvDocBinaryComplexType" maxOccurs="5"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestEnviaDocComplexType", propOrder = {"documentos"})
public class RequestEnviaDocComplexType extends MessageRequestComplexType {

    @XmlElement(name = "Documentos", required = true) protected List<EnvDocBinaryComplexType> documentos;

    /**
     * Gets the value of the documentos property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentos property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentos().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnvDocBinaryComplexType }
     */
    public List<EnvDocBinaryComplexType> getDocumentos() {
        if (documentos == null) {
            documentos = new ArrayList<EnvDocBinaryComplexType>();
        }
        return this.documentos;
    }

}
