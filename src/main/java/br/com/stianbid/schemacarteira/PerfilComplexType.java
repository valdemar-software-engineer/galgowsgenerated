package br.com.stianbid.schemacarteira;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de PerfilComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PerfilComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CalcCota" type="{http://www.stianbid.com.br/SchemaCarteira}CalcCotaCode"/>
 *         &lt;element name="HrComprCota" type="{http://www.stianbid.com.br/Common}ISOTime"/>
 *         &lt;element name="HrComprAtivos" type="{http://www.stianbid.com.br/Common}ISOTime"/>
 *         &lt;element name="CtgAnbid" type="{http://www.stianbid.com.br/SchemaCarteira}CtgAnbid" minOccurs="0"/>
 *         &lt;element name="TpInv" type="{http://www.stianbid.com.br/SchemaCarteira}TipoInvestidor" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PerfilComplexType", propOrder = {"calcCota", "hrComprCota", "hrComprAtivos", "ctgAnbid", "tpInv"})
public class PerfilComplexType {

    @XmlElement(name = "CalcCota") protected int calcCota;
    @XmlElement(name = "HrComprCota", required = true) protected XMLGregorianCalendar hrComprCota;
    @XmlElement(name = "HrComprAtivos", required = true) protected XMLGregorianCalendar hrComprAtivos;
    @XmlElement(name = "CtgAnbid") protected Integer ctgAnbid;
    @XmlElement(name = "TpInv") protected Integer tpInv;

    /**
     * Obtém o valor da propriedade calcCota.
     */
    public int getCalcCota() {
        return calcCota;
    }

    /**
     * Define o valor da propriedade calcCota.
     */
    public void setCalcCota(int value) {
        this.calcCota = value;
    }

    /**
     * Obtém o valor da propriedade ctgAnbid.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCtgAnbid() {
        return ctgAnbid;
    }

    /**
     * Define o valor da propriedade ctgAnbid.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCtgAnbid(Integer value) {
        this.ctgAnbid = value;
    }

    /**
     * Obtém o valor da propriedade hrComprAtivos.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getHrComprAtivos() {
        return hrComprAtivos;
    }

    /**
     * Define o valor da propriedade hrComprAtivos.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setHrComprAtivos(XMLGregorianCalendar value) {
        this.hrComprAtivos = value;
    }

    /**
     * Obtém o valor da propriedade hrComprCota.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getHrComprCota() {
        return hrComprCota;
    }

    /**
     * Define o valor da propriedade hrComprCota.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setHrComprCota(XMLGregorianCalendar value) {
        this.hrComprCota = value;
    }

    /**
     * Obtém o valor da propriedade tpInv.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getTpInv() {
        return tpInv;
    }

    /**
     * Define o valor da propriedade tpInv.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setTpInv(Integer value) {
        this.tpInv = value;
    }

}
