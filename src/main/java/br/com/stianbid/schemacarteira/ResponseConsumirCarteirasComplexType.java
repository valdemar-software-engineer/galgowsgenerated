package br.com.stianbid.schemacarteira;

import br.com.stianbid.common.MessageBatchComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Consumir Carteiras.
 * <p>
 * <p>Classe Java de ResponseConsumirCarteirasComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ResponseConsumirCarteirasComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageBatchComplexType">
 *       &lt;sequence>
 *         &lt;element name="Carteiras" type="{http://www.stianbid.com.br/SchemaCarteira}STIConsumirCarteirasCorrecaoComplexType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseConsumirCarteirasComplexType", propOrder = {"carteiras"})
public class ResponseConsumirCarteirasComplexType extends MessageBatchComplexType {

    @XmlElement(name = "Carteiras", required = true) protected STIConsumirCarteirasCorrecaoComplexType carteiras;

    /**
     * Obtém o valor da propriedade carteiras.
     *
     * @return possible object is
     * {@link STIConsumirCarteirasCorrecaoComplexType }
     */
    public STIConsumirCarteirasCorrecaoComplexType getCarteiras() {
        return carteiras;
    }

    /**
     * Define o valor da propriedade carteiras.
     *
     * @param value allowed object is
     *              {@link STIConsumirCarteirasCorrecaoComplexType }
     */
    public void setCarteiras(STIConsumirCarteirasCorrecaoComplexType value) {
        this.carteiras = value;
    }

}
