package br.com.stianbid.schemacarteira;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de CPPRprtComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="CPPRprtComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType">
 *       &lt;sequence>
 *         &lt;element name="CPPRprt" type="{http://www.stianbid.com.br/SchemaCarteira}MessageCarteiraComplexType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CPPRprtComplexType", propOrder = {"cppRprt"})
public class CPPRprtComplexType extends MessageRequestComplexType {

    @XmlElement(name = "CPPRprt") protected List<MessageCarteiraComplexType> cppRprt;

    /**
     * Gets the value of the cppRprt property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cppRprt property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCPPRprt().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MessageCarteiraComplexType }
     */
    public List<MessageCarteiraComplexType> getCPPRprt() {
        if (cppRprt == null) {
            cppRprt = new ArrayList<MessageCarteiraComplexType>();
        }
        return this.cppRprt;
    }

}
