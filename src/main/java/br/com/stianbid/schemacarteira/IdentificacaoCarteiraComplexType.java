package br.com.stianbid.schemacarteira;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de IdentificacaoCarteiraComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="IdentificacaoCarteiraComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CdAnbid" type="{http://www.stianbid.com.br/SchemaCarteira}CodigoANBID" minOccurs="0"/>
 *         &lt;element name="CdSti" type="{http://www.stianbid.com.br/SchemaCarteira}STIIdentifier" minOccurs="0"/>
 *         &lt;element name="Tp" type="{http://www.stianbid.com.br/SchemaCarteira}TipoCarteira" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="CNPJ" type="{http://www.stianbid.com.br/SchemaCarteira}CNPJIdentifier" minOccurs="0"/>
 *           &lt;element name="CPF" type="{http://www.stianbid.com.br/SchemaCarteira}CPFIdentifier" minOccurs="0"/>
 *           &lt;element name="IdentEstrang" type="{http://www.stianbid.com.br/Common}Max35Text" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="Nm" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/>
 *         &lt;element name="NmFantasia" type="{http://www.stianbid.com.br/Common}Max350Text"/>
 *         &lt;element name="Sigla" type="{http://www.stianbid.com.br/Common}Max20Text"/>
 *         &lt;element name="StatusMerc" type="{http://www.stianbid.com.br/SchemaCarteira}StatusMercadoCode" minOccurs="0"/>
 *         &lt;element name="StatusAnbid" type="{http://www.stianbid.com.br/SchemaCarteira}StatusANBIDCode" minOccurs="0"/>
 *         &lt;element name="DivAnbid" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificacaoCarteiraComplexType",
         propOrder = {"cdAnbid", "cdSti", "tp", "cnpj", "cpf", "identEstrang", "nm", "nmFantasia", "sigla", "statusMerc", "statusAnbid", "divAnbid"})
public class IdentificacaoCarteiraComplexType {

    @XmlElement(name = "CdAnbid") protected Integer cdAnbid;
    @XmlElement(name = "CdSti") protected Integer cdSti;
    @XmlElement(name = "Tp") protected Integer tp;
    @XmlElement(name = "CNPJ") protected Long cnpj;
    @XmlElement(name = "CPF") protected Long cpf;
    @XmlElement(name = "IdentEstrang") protected String identEstrang;
    @XmlElement(name = "Nm") protected String nm;
    @XmlElement(name = "NmFantasia", required = true) protected String nmFantasia;
    @XmlElement(name = "Sigla", required = true) protected String sigla;
    @XmlElement(name = "StatusMerc") protected Integer statusMerc;
    @XmlElement(name = "StatusAnbid") protected Integer statusAnbid;
    @XmlElement(name = "DivAnbid") protected YesNoIndicator divAnbid;

    /**
     * Obtém o valor da propriedade cnpj.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getCNPJ() {
        return cnpj;
    }

    /**
     * Define o valor da propriedade cnpj.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setCNPJ(Long value) {
        this.cnpj = value;
    }

    /**
     * Obtém o valor da propriedade cpf.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getCPF() {
        return cpf;
    }

    /**
     * Define o valor da propriedade cpf.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setCPF(Long value) {
        this.cpf = value;
    }

    /**
     * Obtém o valor da propriedade cdAnbid.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCdAnbid() {
        return cdAnbid;
    }

    /**
     * Define o valor da propriedade cdAnbid.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCdAnbid(Integer value) {
        this.cdAnbid = value;
    }

    /**
     * Obtém o valor da propriedade cdSti.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCdSti() {
        return cdSti;
    }

    /**
     * Define o valor da propriedade cdSti.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCdSti(Integer value) {
        this.cdSti = value;
    }

    /**
     * Obtém o valor da propriedade divAnbid.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getDivAnbid() {
        return divAnbid;
    }

    /**
     * Define o valor da propriedade divAnbid.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setDivAnbid(YesNoIndicator value) {
        this.divAnbid = value;
    }

    /**
     * Obtém o valor da propriedade identEstrang.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdentEstrang() {
        return identEstrang;
    }

    /**
     * Define o valor da propriedade identEstrang.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdentEstrang(String value) {
        this.identEstrang = value;
    }

    /**
     * Obtém o valor da propriedade nm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNm() {
        return nm;
    }

    /**
     * Define o valor da propriedade nm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNm(String value) {
        this.nm = value;
    }

    /**
     * Obtém o valor da propriedade nmFantasia.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNmFantasia() {
        return nmFantasia;
    }

    /**
     * Define o valor da propriedade nmFantasia.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNmFantasia(String value) {
        this.nmFantasia = value;
    }

    /**
     * Obtém o valor da propriedade sigla.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSigla() {
        return sigla;
    }

    /**
     * Define o valor da propriedade sigla.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSigla(String value) {
        this.sigla = value;
    }

    /**
     * Obtém o valor da propriedade statusAnbid.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getStatusAnbid() {
        return statusAnbid;
    }

    /**
     * Define o valor da propriedade statusAnbid.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setStatusAnbid(Integer value) {
        this.statusAnbid = value;
    }

    /**
     * Obtém o valor da propriedade statusMerc.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getStatusMerc() {
        return statusMerc;
    }

    /**
     * Define o valor da propriedade statusMerc.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setStatusMerc(Integer value) {
        this.statusMerc = value;
    }

    /**
     * Obtém o valor da propriedade tp.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getTp() {
        return tp;
    }

    /**
     * Define o valor da propriedade tp.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setTp(Integer value) {
        this.tp = value;
    }

}
