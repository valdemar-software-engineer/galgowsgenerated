package br.com.stianbid.schemacarteira;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Consumir Carteiras Correção.
 * <p>
 * <p>Classe Java de STIConsumirCarteirasCorrecaoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="STIConsumirCarteirasCorrecaoComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CdSti" type="{http://www.stianbid.com.br/SchemaCarteira}STIIdentifier"/>
 *         &lt;element name="CarteirasCorrecao" type="{http://www.stianbid.com.br/SchemaCarteira}MessageResponseConsumirCarteirasCorrecao" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dsObservacao" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "STIConsumirCarteirasCorrecaoComplexType", propOrder = {"cdSti", "carteirasCorrecao", "dsObservacao"})
public class STIConsumirCarteirasCorrecaoComplexType {

    @XmlElement(name = "CdSti") protected int cdSti;
    @XmlElement(name = "CarteirasCorrecao") protected List<MessageResponseConsumirCarteirasCorrecao> carteirasCorrecao;
    protected String dsObservacao;

    /**
     * Gets the value of the carteirasCorrecao property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the carteirasCorrecao property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCarteirasCorrecao().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MessageResponseConsumirCarteirasCorrecao }
     */
    public List<MessageResponseConsumirCarteirasCorrecao> getCarteirasCorrecao() {
        if (carteirasCorrecao == null) {
            carteirasCorrecao = new ArrayList<MessageResponseConsumirCarteirasCorrecao>();
        }
        return this.carteirasCorrecao;
    }

    /**
     * Obtém o valor da propriedade cdSti.
     */
    public int getCdSti() {
        return cdSti;
    }

    /**
     * Define o valor da propriedade cdSti.
     */
    public void setCdSti(int value) {
        this.cdSti = value;
    }

    /**
     * Obtém o valor da propriedade dsObservacao.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDsObservacao() {
        return dsObservacao;
    }

    /**
     * Define o valor da propriedade dsObservacao.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDsObservacao(String value) {
        this.dsObservacao = value;
    }

}
