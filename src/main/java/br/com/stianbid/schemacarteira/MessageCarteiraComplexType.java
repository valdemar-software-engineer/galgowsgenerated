package br.com.stianbid.schemacarteira;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de MessageCarteiraComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageCarteiraComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DadosSTI" type="{http://www.stianbid.com.br/SchemaCarteira}STIComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageCarteiraComplexType", propOrder = {"dadosSTI"})
public class MessageCarteiraComplexType {

    @XmlElement(name = "DadosSTI") protected STIComplexType dadosSTI;

    /**
     * Obtém o valor da propriedade dadosSTI.
     *
     * @return possible object is
     * {@link STIComplexType }
     */
    public STIComplexType getDadosSTI() {
        return dadosSTI;
    }

    /**
     * Define o valor da propriedade dadosSTI.
     *
     * @param value allowed object is
     *              {@link STIComplexType }
     */
    public void setDadosSTI(STIComplexType value) {
        this.dadosSTI = value;
    }

}
