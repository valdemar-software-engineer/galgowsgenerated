package br.com.stianbid.schemacarteira;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Datas
 * <p>
 * <p>Classe Java de DatasComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="DatasComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DtEncerr" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="JustEncerr" type="{http://www.stianbid.com.br/Common}Max500Text" minOccurs="0"/>
 *         &lt;element name="CartAtiva" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DtInativ" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="JustInativ" type="{http://www.stianbid.com.br/Common}Max500Text" minOccurs="0"/>
 *         &lt;element name="DtVincAnbid" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtDesvAnbid" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtVigAlt" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtInicVersao" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtFimVersao" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtInicAtiv" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatasComplexType",
         propOrder = {"dtEncerr", "justEncerr", "cartAtiva", "dtInativ", "justInativ", "dtVincAnbid", "dtDesvAnbid", "dtVigAlt", "dtInicVersao", "dtFimVersao", "dtInicAtiv"})
public class DatasComplexType {

    @XmlElement(name = "DtEncerr") protected XMLGregorianCalendar dtEncerr;
    @XmlElement(name = "JustEncerr") protected String justEncerr;
    @XmlElement(name = "CartAtiva") protected YesNoIndicator cartAtiva;
    @XmlElement(name = "DtInativ") protected XMLGregorianCalendar dtInativ;
    @XmlElement(name = "JustInativ") protected String justInativ;
    @XmlElement(name = "DtVincAnbid") protected XMLGregorianCalendar dtVincAnbid;
    @XmlElement(name = "DtDesvAnbid") protected XMLGregorianCalendar dtDesvAnbid;
    @XmlElement(name = "DtVigAlt") protected XMLGregorianCalendar dtVigAlt;
    @XmlElement(name = "DtInicVersao") protected XMLGregorianCalendar dtInicVersao;
    @XmlElement(name = "DtFimVersao") protected XMLGregorianCalendar dtFimVersao;
    @XmlElement(name = "DtInicAtiv") protected XMLGregorianCalendar dtInicAtiv;

    /**
     * Obtém o valor da propriedade cartAtiva.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getCartAtiva() {
        return cartAtiva;
    }

    /**
     * Define o valor da propriedade cartAtiva.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setCartAtiva(YesNoIndicator value) {
        this.cartAtiva = value;
    }

    /**
     * Obtém o valor da propriedade dtDesvAnbid.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtDesvAnbid() {
        return dtDesvAnbid;
    }

    /**
     * Define o valor da propriedade dtDesvAnbid.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtDesvAnbid(XMLGregorianCalendar value) {
        this.dtDesvAnbid = value;
    }

    /**
     * Obtém o valor da propriedade dtEncerr.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtEncerr() {
        return dtEncerr;
    }

    /**
     * Define o valor da propriedade dtEncerr.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtEncerr(XMLGregorianCalendar value) {
        this.dtEncerr = value;
    }

    /**
     * Obtém o valor da propriedade dtFimVersao.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtFimVersao() {
        return dtFimVersao;
    }

    /**
     * Define o valor da propriedade dtFimVersao.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtFimVersao(XMLGregorianCalendar value) {
        this.dtFimVersao = value;
    }

    /**
     * Obtém o valor da propriedade dtInativ.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInativ() {
        return dtInativ;
    }

    /**
     * Define o valor da propriedade dtInativ.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInativ(XMLGregorianCalendar value) {
        this.dtInativ = value;
    }

    /**
     * Obtém o valor da propriedade dtInicAtiv.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInicAtiv() {
        return dtInicAtiv;
    }

    /**
     * Define o valor da propriedade dtInicAtiv.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInicAtiv(XMLGregorianCalendar value) {
        this.dtInicAtiv = value;
    }

    /**
     * Obtém o valor da propriedade dtInicVersao.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInicVersao() {
        return dtInicVersao;
    }

    /**
     * Define o valor da propriedade dtInicVersao.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInicVersao(XMLGregorianCalendar value) {
        this.dtInicVersao = value;
    }

    /**
     * Obtém o valor da propriedade dtVigAlt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtVigAlt() {
        return dtVigAlt;
    }

    /**
     * Define o valor da propriedade dtVigAlt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtVigAlt(XMLGregorianCalendar value) {
        this.dtVigAlt = value;
    }

    /**
     * Obtém o valor da propriedade dtVincAnbid.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtVincAnbid() {
        return dtVincAnbid;
    }

    /**
     * Define o valor da propriedade dtVincAnbid.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtVincAnbid(XMLGregorianCalendar value) {
        this.dtVincAnbid = value;
    }

    /**
     * Obtém o valor da propriedade justEncerr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getJustEncerr() {
        return justEncerr;
    }

    /**
     * Define o valor da propriedade justEncerr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setJustEncerr(String value) {
        this.justEncerr = value;
    }

    /**
     * Obtém o valor da propriedade justInativ.
     *
     * @return possible object is
     * {@link String }
     */
    public String getJustInativ() {
        return justInativ;
    }

    /**
     * Define o valor da propriedade justInativ.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setJustInativ(String value) {
        this.justInativ = value;
    }

}
