package br.com.stianbid.schemacarteira;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Consultar Documentos.
 * <p>
 * <p>
 * <p>
 * <p>
 * <p>
 * <p>Classe Java de RequestConsultaDocumentoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="RequestConsultaDocumentoComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="CdSti" type="{http://www.stianbid.com.br/SchemaCarteira}STIIdentifier"/>
 *           &lt;element name="NrProtocolo" type="{http://www.stianbid.com.br/SchemaCarteira}NumeroProtocolo"/>
 *         &lt;/choice>
 *         &lt;element name="idTipoDocumento" type="{http://www.stianbid.com.br/SchemaCarteira}TipoDocumento"/>
 *         &lt;element name="idIdioma" type="{http://www.stianbid.com.br/SchemaCarteira}IdiomaIdentifier"/>
 *         &lt;element name="dsDescDocto" type="{http://www.stianbid.com.br/SchemaCarteira}DescricaoDocumento" minOccurs="0"/>
 *         &lt;element name="VersaoCarteira" type="{http://www.stianbid.com.br/SchemaCarteira}VersaoCarteira" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestConsultaDocumentoComplexType",
         propOrder = {"cdSti", "nrProtocolo", "idTipoDocumento", "idIdioma", "dsDescDocto", "versaoCarteira"})
public class RequestConsultaDocumentoComplexType extends MessageRequestComplexType {

    @XmlElement(name = "CdSti") protected Integer cdSti;
    @XmlElement(name = "NrProtocolo") protected Long nrProtocolo;
    protected int idTipoDocumento;
    protected int idIdioma;
    protected String dsDescDocto;
    @XmlElement(name = "VersaoCarteira") protected Integer versaoCarteira;

    /**
     * Obtém o valor da propriedade cdSti.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCdSti() {
        return cdSti;
    }

    /**
     * Define o valor da propriedade cdSti.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCdSti(Integer value) {
        this.cdSti = value;
    }

    /**
     * Obtém o valor da propriedade dsDescDocto.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDsDescDocto() {
        return dsDescDocto;
    }

    /**
     * Define o valor da propriedade dsDescDocto.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDsDescDocto(String value) {
        this.dsDescDocto = value;
    }

    /**
     * Obtém o valor da propriedade idIdioma.
     */
    public int getIdIdioma() {
        return idIdioma;
    }

    /**
     * Define o valor da propriedade idIdioma.
     */
    public void setIdIdioma(int value) {
        this.idIdioma = value;
    }

    /**
     * Obtém o valor da propriedade idTipoDocumento.
     */
    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     * Define o valor da propriedade idTipoDocumento.
     */
    public void setIdTipoDocumento(int value) {
        this.idTipoDocumento = value;
    }

    /**
     * Obtém o valor da propriedade nrProtocolo.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getNrProtocolo() {
        return nrProtocolo;
    }

    /**
     * Define o valor da propriedade nrProtocolo.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setNrProtocolo(Long value) {
        this.nrProtocolo = value;
    }

    /**
     * Obtém o valor da propriedade versaoCarteira.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getVersaoCarteira() {
        return versaoCarteira;
    }

    /**
     * Define o valor da propriedade versaoCarteira.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setVersaoCarteira(Integer value) {
        this.versaoCarteira = value;
    }

}
