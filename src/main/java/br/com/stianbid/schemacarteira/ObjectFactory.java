package br.com.stianbid.schemacarteira;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the br.com.stianbid.schemacarteira package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CPPRprts_QNAME = new QName("http://www.stianbid.com.br/SchemaCarteira", "CPPRprts");
    private final static QName _FiltroConsulta_QNAME = new QName("http://www.stianbid.com.br/SchemaCarteira",
        "FiltroConsulta");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.stianbid.schemacarteira
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CPPRprtComplexType }
     */
    public CPPRprtComplexType createCPPRprtComplexType() {
        return new CPPRprtComplexType();
    }

    /**
     * Create an instance of {@link CPPRprtPendComplexType }
     */
    public CPPRprtPendComplexType createCPPRprtPendComplexType() {
        return new CPPRprtPendComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CPPRprtComplexType }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.stianbid.com.br/SchemaCarteira", name = "CPPRprts")
    public JAXBElement<CPPRprtComplexType> createCPPRprts(CPPRprtComplexType value) {
        return new JAXBElement<CPPRprtComplexType>(_CPPRprts_QNAME, CPPRprtComplexType.class, null, value);
    }

    /**
     * Create an instance of {@link ContasComplexType }
     */
    public ContasComplexType createContasComplexType() {
        return new ContasComplexType();
    }

    /**
     * Create an instance of {@link DadosConsComplexType }
     */
    public DadosConsComplexType createDadosConsComplexType() {
        return new DadosConsComplexType();
    }

    /**
     * Create an instance of {@link DatasComplexType }
     */
    public DatasComplexType createDatasComplexType() {
        return new DatasComplexType();
    }

    /**
     * Create an instance of {@link DocBinaryComplexType }
     */
    public DocBinaryComplexType createDocBinaryComplexType() {
        return new DocBinaryComplexType();
    }

    /**
     * Create an instance of {@link EnvDocBinaryComplexType }
     */
    public EnvDocBinaryComplexType createEnvDocBinaryComplexType() {
        return new EnvDocBinaryComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DadosConsComplexType }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.stianbid.com.br/SchemaCarteira", name = "FiltroConsulta")
    public JAXBElement<DadosConsComplexType> createFiltroConsulta(DadosConsComplexType value) {
        return new JAXBElement<DadosConsComplexType>(_FiltroConsulta_QNAME, DadosConsComplexType.class, null, value);
    }

    /**
     * Create an instance of {@link FinalizarComplexType }
     */
    public FinalizarComplexType createFinalizarComplexType() {
        return new FinalizarComplexType();
    }

    /**
     * Create an instance of {@link IdentificacaoCarteiraComplexType }
     */
    public IdentificacaoCarteiraComplexType createIdentificacaoCarteiraComplexType() {
        return new IdentificacaoCarteiraComplexType();
    }

    /**
     * Create an instance of {@link ItensRevCodComplexType }
     */
    public ItensRevCodComplexType createItensRevCodComplexType() {
        return new ItensRevCodComplexType();
    }

    /**
     * Create an instance of {@link ListaNrProtocoloComplexType }
     */
    public ListaNrProtocoloComplexType createListaNrProtocoloComplexType() {
        return new ListaNrProtocoloComplexType();
    }

    /**
     * Create an instance of {@link MessageCarteiraComplexType }
     */
    public MessageCarteiraComplexType createMessageCarteiraComplexType() {
        return new MessageCarteiraComplexType();
    }

    /**
     * Create an instance of {@link MessageResponseConsumirCarteirasCorrecao }
     */
    public MessageResponseConsumirCarteirasCorrecao createMessageResponseConsumirCarteirasCorrecao() {
        return new MessageResponseConsumirCarteirasCorrecao();
    }

    /**
     * Create an instance of {@link PerfilComplexType }
     */
    public PerfilComplexType createPerfilComplexType() {
        return new PerfilComplexType();
    }

    /**
     * Create an instance of {@link PrestServicoComplexType }
     */
    public PrestServicoComplexType createPrestServicoComplexType() {
        return new PrestServicoComplexType();
    }

    /**
     * Create an instance of {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType createPrestServicoIdentificationComplexType() {
        return new PrestServicoIdentificationComplexType();
    }

    /**
     * Create an instance of {@link RequestConsultaDocumentoComplexType }
     */
    public RequestConsultaDocumentoComplexType createRequestConsultaDocumentoComplexType() {
        return new RequestConsultaDocumentoComplexType();
    }

    /**
     * Create an instance of {@link RequestConsumirCarteirasComplexType }
     */
    public RequestConsumirCarteirasComplexType createRequestConsumirCarteirasComplexType() {
        return new RequestConsumirCarteirasComplexType();
    }

    /**
     * Create an instance of {@link RequestEnviaDocComplexType }
     */
    public RequestEnviaDocComplexType createRequestEnviaDocComplexType() {
        return new RequestEnviaDocComplexType();
    }

    /**
     * Create an instance of {@link RequestFinalizarCarteirasComplexType }
     */
    public RequestFinalizarCarteirasComplexType createRequestFinalizarCarteirasComplexType() {
        return new RequestFinalizarCarteirasComplexType();
    }

    /**
     * Create an instance of {@link ResponseConsultaDocumentoComplexType }
     */
    public ResponseConsultaDocumentoComplexType createResponseConsultaDocumentoComplexType() {
        return new ResponseConsultaDocumentoComplexType();
    }

    /**
     * Create an instance of {@link ResponseConsumirCarteirasComplexType }
     */
    public ResponseConsumirCarteirasComplexType createResponseConsumirCarteirasComplexType() {
        return new ResponseConsumirCarteirasComplexType();
    }

    /**
     * Create an instance of {@link RevCodAutoRegComplexType }
     */
    public RevCodAutoRegComplexType createRevCodAutoRegComplexType() {
        return new RevCodAutoRegComplexType();
    }

    /**
     * Create an instance of {@link STIComplexType }
     */
    public STIComplexType createSTIComplexType() {
        return new STIComplexType();
    }

    /**
     * Create an instance of {@link STIConsumirCarteirasCorrecaoComplexType }
     */
    public STIConsumirCarteirasCorrecaoComplexType createSTIConsumirCarteirasCorrecaoComplexType() {
        return new STIConsumirCarteirasCorrecaoComplexType();
    }

}
