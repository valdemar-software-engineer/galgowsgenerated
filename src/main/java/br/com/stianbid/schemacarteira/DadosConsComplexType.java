package br.com.stianbid.schemacarteira;

import br.com.stianbid.common.MessageRequestComplexType;
import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Estrutura de Filtro para Carteiras Administrada
 * <p>
 * Utilizado somente para consulta.
 * <p>
 * <p>
 * <p>Classe Java de DadosConsComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="DadosConsComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType">
 *       &lt;sequence>
 *         &lt;element name="DtInicInclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DtFimInclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="InclusaoAut" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="BuscaHistorico" type="{http://www.stianbid.com.br/Common}YesNoIndicator"/>
 *         &lt;element name="DtInicVig" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DtFimVig" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="listaNrProtocolo" type="{http://www.stianbid.com.br/SchemaCarteira}ListaNrProtocoloComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DadosConsComplexType",
         propOrder = {"dtInicInclusao", "dtFimInclusao", "inclusaoAut", "buscaHistorico", "dtInicVig", "dtFimVig", "listaNrProtocolo"})
public class DadosConsComplexType extends MessageRequestComplexType {

    @XmlElement(name = "DtInicInclusao") @XmlSchemaType(name = "dateTime") protected XMLGregorianCalendar
        dtInicInclusao;
    @XmlElement(name = "DtFimInclusao") @XmlSchemaType(name = "dateTime") protected XMLGregorianCalendar dtFimInclusao;
    @XmlElement(name = "InclusaoAut") protected YesNoIndicator inclusaoAut;
    @XmlElement(name = "BuscaHistorico", required = true) protected YesNoIndicator buscaHistorico;
    @XmlElement(name = "DtInicVig") @XmlSchemaType(name = "dateTime") protected XMLGregorianCalendar dtInicVig;
    @XmlElement(name = "DtFimVig") @XmlSchemaType(name = "dateTime") protected XMLGregorianCalendar dtFimVig;
    protected ListaNrProtocoloComplexType listaNrProtocolo;

    /**
     * Obtém o valor da propriedade buscaHistorico.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getBuscaHistorico() {
        return buscaHistorico;
    }

    /**
     * Define o valor da propriedade buscaHistorico.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setBuscaHistorico(YesNoIndicator value) {
        this.buscaHistorico = value;
    }

    /**
     * Obtém o valor da propriedade dtFimInclusao.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtFimInclusao() {
        return dtFimInclusao;
    }

    /**
     * Define o valor da propriedade dtFimInclusao.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtFimInclusao(XMLGregorianCalendar value) {
        this.dtFimInclusao = value;
    }

    /**
     * Obtém o valor da propriedade dtFimVig.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtFimVig() {
        return dtFimVig;
    }

    /**
     * Define o valor da propriedade dtFimVig.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtFimVig(XMLGregorianCalendar value) {
        this.dtFimVig = value;
    }

    /**
     * Obtém o valor da propriedade dtInicInclusao.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInicInclusao() {
        return dtInicInclusao;
    }

    /**
     * Define o valor da propriedade dtInicInclusao.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInicInclusao(XMLGregorianCalendar value) {
        this.dtInicInclusao = value;
    }

    /**
     * Obtém o valor da propriedade dtInicVig.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInicVig() {
        return dtInicVig;
    }

    /**
     * Define o valor da propriedade dtInicVig.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInicVig(XMLGregorianCalendar value) {
        this.dtInicVig = value;
    }

    /**
     * Obtém o valor da propriedade inclusaoAut.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getInclusaoAut() {
        return inclusaoAut;
    }

    /**
     * Define o valor da propriedade inclusaoAut.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setInclusaoAut(YesNoIndicator value) {
        this.inclusaoAut = value;
    }

    /**
     * Obtém o valor da propriedade listaNrProtocolo.
     *
     * @return possible object is
     * {@link ListaNrProtocoloComplexType }
     */
    public ListaNrProtocoloComplexType getListaNrProtocolo() {
        return listaNrProtocolo;
    }

    /**
     * Define o valor da propriedade listaNrProtocolo.
     *
     * @param value allowed object is
     *              {@link ListaNrProtocoloComplexType }
     */
    public void setListaNrProtocolo(ListaNrProtocoloComplexType value) {
        this.listaNrProtocolo = value;
    }

}
