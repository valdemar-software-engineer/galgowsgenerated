package br.com.stianbid.schemacarteira;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlType;


/**
 * Documentos de Carteiras.
 * <p>
 * <p>Classe Java de EnvDocBinaryComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="EnvDocBinaryComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="CdSti" type="{http://www.stianbid.com.br/SchemaCarteira}STIIdentifier"/>
 *           &lt;element name="NrProtocolo" type="{http://www.stianbid.com.br/SchemaCarteira}NumeroProtocolo"/>
 *         &lt;/choice>
 *         &lt;element name="idTipoDocumento" type="{http://www.stianbid.com.br/SchemaCarteira}TipoDocumento"/>
 *         &lt;element name="dsDescDocto" type="{http://www.stianbid.com.br/SchemaCarteira}DescricaoDocumento" minOccurs="0"/>
 *         &lt;element name="dsNomeDocumento" type="{http://www.stianbid.com.br/SchemaCarteira}NomeDocumento"/>
 *         &lt;element name="idIdioma" type="{http://www.stianbid.com.br/SchemaCarteira}IdiomaIdentifier"/>
 *         &lt;element name="binaryArquivo" type="{http://www.stianbid.com.br/SchemaCarteira}ArquivoBinario"/>
 *         &lt;element name="VersaoCarteira" type="{http://www.stianbid.com.br/SchemaCarteira}VersaoCarteira"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnvDocBinaryComplexType",
         propOrder = {"cdSti", "nrProtocolo", "idTipoDocumento", "dsDescDocto", "dsNomeDocumento", "idIdioma", "binaryArquivo", "versaoCarteira"})
public class EnvDocBinaryComplexType {

    @XmlElement(name = "CdSti") protected Integer cdSti;
    @XmlElement(name = "NrProtocolo") protected Long nrProtocolo;
    protected int idTipoDocumento;
    protected String dsDescDocto;
    @XmlElement(required = true) protected String dsNomeDocumento;
    protected int idIdioma;
    @XmlElement(required = true) @XmlMimeType("application/octet-stream") protected DataHandler binaryArquivo;
    @XmlElement(name = "VersaoCarteira") protected int versaoCarteira;

    /**
     * Obtém o valor da propriedade binaryArquivo.
     *
     * @return possible object is
     * {@link DataHandler }
     */
    public DataHandler getBinaryArquivo() {
        return binaryArquivo;
    }

    /**
     * Define o valor da propriedade binaryArquivo.
     *
     * @param value allowed object is
     *              {@link DataHandler }
     */
    public void setBinaryArquivo(DataHandler value) {
        this.binaryArquivo = value;
    }

    /**
     * Obtém o valor da propriedade cdSti.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCdSti() {
        return cdSti;
    }

    /**
     * Define o valor da propriedade cdSti.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCdSti(Integer value) {
        this.cdSti = value;
    }

    /**
     * Obtém o valor da propriedade dsDescDocto.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDsDescDocto() {
        return dsDescDocto;
    }

    /**
     * Define o valor da propriedade dsDescDocto.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDsDescDocto(String value) {
        this.dsDescDocto = value;
    }

    /**
     * Obtém o valor da propriedade dsNomeDocumento.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDsNomeDocumento() {
        return dsNomeDocumento;
    }

    /**
     * Define o valor da propriedade dsNomeDocumento.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDsNomeDocumento(String value) {
        this.dsNomeDocumento = value;
    }

    /**
     * Obtém o valor da propriedade idIdioma.
     */
    public int getIdIdioma() {
        return idIdioma;
    }

    /**
     * Define o valor da propriedade idIdioma.
     */
    public void setIdIdioma(int value) {
        this.idIdioma = value;
    }

    /**
     * Obtém o valor da propriedade idTipoDocumento.
     */
    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     * Define o valor da propriedade idTipoDocumento.
     */
    public void setIdTipoDocumento(int value) {
        this.idTipoDocumento = value;
    }

    /**
     * Obtém o valor da propriedade nrProtocolo.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getNrProtocolo() {
        return nrProtocolo;
    }

    /**
     * Define o valor da propriedade nrProtocolo.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setNrProtocolo(Long value) {
        this.nrProtocolo = value;
    }

    /**
     * Obtém o valor da propriedade versaoCarteira.
     */
    public int getVersaoCarteira() {
        return versaoCarteira;
    }

    /**
     * Define o valor da propriedade versaoCarteira.
     */
    public void setVersaoCarteira(int value) {
        this.versaoCarteira = value;
    }

}
