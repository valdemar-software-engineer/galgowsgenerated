package br.com.stianbid.schemacarteira;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de STIComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="STIComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Operacao" type="{http://www.stianbid.com.br/SchemaCarteira}OperationCode"/>
 *         &lt;element name="IdCtra" type="{http://www.stianbid.com.br/SchemaCarteira}IdentificacaoCarteiraComplexType"/>
 *         &lt;element name="Perfil" type="{http://www.stianbid.com.br/SchemaCarteira}PerfilComplexType"/>
 *         &lt;element name="Datas" type="{http://www.stianbid.com.br/SchemaCarteira}DatasComplexType" minOccurs="0"/>
 *         &lt;element name="PrestServico" type="{http://www.stianbid.com.br/SchemaCarteira}PrestServicoComplexType"/>
 *         &lt;element name="Contas" type="{http://www.stianbid.com.br/SchemaCarteira}ContasComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "STIComplexType", propOrder = {"operacao", "idCtra", "perfil", "datas", "prestServico", "contas"})
public class STIComplexType {

    @XmlElement(name = "Operacao") protected int operacao;
    @XmlElement(name = "IdCtra", required = true) protected IdentificacaoCarteiraComplexType idCtra;
    @XmlElement(name = "Perfil", required = true) protected PerfilComplexType perfil;
    @XmlElement(name = "Datas") protected DatasComplexType datas;
    @XmlElement(name = "PrestServico", required = true) protected PrestServicoComplexType prestServico;
    @XmlElement(name = "Contas") protected ContasComplexType contas;

    /**
     * Obtém o valor da propriedade contas.
     *
     * @return possible object is
     * {@link ContasComplexType }
     */
    public ContasComplexType getContas() {
        return contas;
    }

    /**
     * Define o valor da propriedade contas.
     *
     * @param value allowed object is
     *              {@link ContasComplexType }
     */
    public void setContas(ContasComplexType value) {
        this.contas = value;
    }

    /**
     * Obtém o valor da propriedade datas.
     *
     * @return possible object is
     * {@link DatasComplexType }
     */
    public DatasComplexType getDatas() {
        return datas;
    }

    /**
     * Define o valor da propriedade datas.
     *
     * @param value allowed object is
     *              {@link DatasComplexType }
     */
    public void setDatas(DatasComplexType value) {
        this.datas = value;
    }

    /**
     * Obtém o valor da propriedade idCtra.
     *
     * @return possible object is
     * {@link IdentificacaoCarteiraComplexType }
     */
    public IdentificacaoCarteiraComplexType getIdCtra() {
        return idCtra;
    }

    /**
     * Define o valor da propriedade idCtra.
     *
     * @param value allowed object is
     *              {@link IdentificacaoCarteiraComplexType }
     */
    public void setIdCtra(IdentificacaoCarteiraComplexType value) {
        this.idCtra = value;
    }

    /**
     * Obtém o valor da propriedade operacao.
     */
    public int getOperacao() {
        return operacao;
    }

    /**
     * Define o valor da propriedade operacao.
     */
    public void setOperacao(int value) {
        this.operacao = value;
    }

    /**
     * Obtém o valor da propriedade perfil.
     *
     * @return possible object is
     * {@link PerfilComplexType }
     */
    public PerfilComplexType getPerfil() {
        return perfil;
    }

    /**
     * Define o valor da propriedade perfil.
     *
     * @param value allowed object is
     *              {@link PerfilComplexType }
     */
    public void setPerfil(PerfilComplexType value) {
        this.perfil = value;
    }

    /**
     * Obtém o valor da propriedade prestServico.
     *
     * @return possible object is
     * {@link PrestServicoComplexType }
     */
    public PrestServicoComplexType getPrestServico() {
        return prestServico;
    }

    /**
     * Define o valor da propriedade prestServico.
     *
     * @param value allowed object is
     *              {@link PrestServicoComplexType }
     */
    public void setPrestServico(PrestServicoComplexType value) {
        this.prestServico = value;
    }

}
