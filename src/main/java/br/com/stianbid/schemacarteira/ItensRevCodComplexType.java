package br.com.stianbid.schemacarteira;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de itensRevCodComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="itensRevCodComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DescItemCod" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "itensRevCodComplexType", propOrder = {"descItemCod"})
public class ItensRevCodComplexType {

    @XmlElement(name = "DescItemCod") protected String descItemCod;

    /**
     * Obtém o valor da propriedade descItemCod.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDescItemCod() {
        return descItemCod;
    }

    /**
     * Define o valor da propriedade descItemCod.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDescItemCod(String value) {
        this.descItemCod = value;
    }

}
