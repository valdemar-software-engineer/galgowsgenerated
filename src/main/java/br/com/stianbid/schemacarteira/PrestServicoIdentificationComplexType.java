package br.com.stianbid.schemacarteira;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de PrestServicoIdentificationComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PrestServicoIdentificationComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="CNPJ" type="{http://www.stianbid.com.br/SchemaCarteira}CNPJIdentifier" minOccurs="0"/>
 *           &lt;element name="IdentifEstrang" type="{http://www.stianbid.com.br/Common}Max35Text" minOccurs="0"/>
 *           &lt;element name="CdSti" type="{http://www.stianbid.com.br/SchemaCarteira}STIIdentifier" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="NmFantasia" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrestServicoIdentificationComplexType", propOrder = {"cnpj", "identifEstrang", "cdSti", "nmFantasia"})
public class PrestServicoIdentificationComplexType {

    @XmlElement(name = "CNPJ") protected Long cnpj;
    @XmlElement(name = "IdentifEstrang") protected String identifEstrang;
    @XmlElement(name = "CdSti") protected Integer cdSti;
    @XmlElement(name = "NmFantasia") protected String nmFantasia;

    /**
     * Obtém o valor da propriedade cnpj.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getCNPJ() {
        return cnpj;
    }

    /**
     * Define o valor da propriedade cnpj.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setCNPJ(Long value) {
        this.cnpj = value;
    }

    /**
     * Obtém o valor da propriedade cdSti.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCdSti() {
        return cdSti;
    }

    /**
     * Define o valor da propriedade cdSti.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCdSti(Integer value) {
        this.cdSti = value;
    }

    /**
     * Obtém o valor da propriedade identifEstrang.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdentifEstrang() {
        return identifEstrang;
    }

    /**
     * Define o valor da propriedade identifEstrang.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdentifEstrang(String value) {
        this.identifEstrang = value;
    }

    /**
     * Obtém o valor da propriedade nmFantasia.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNmFantasia() {
        return nmFantasia;
    }

    /**
     * Define o valor da propriedade nmFantasia.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNmFantasia(String value) {
        this.nmFantasia = value;
    }

}
