package br.com.stianbid.schemacarteira;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de PrestServicoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PrestServicoComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Administrador" type="{http://www.stianbid.com.br/SchemaCarteira}PrestServicoIdentificationComplexType"/>
 *         &lt;element name="EmpAvRisco" type="{http://www.stianbid.com.br/SchemaCarteira}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="ControlAtivo" type="{http://www.stianbid.com.br/SchemaCarteira}PrestServicoIdentificationComplexType"/>
 *         &lt;element name="CustCota" type="{http://www.stianbid.com.br/SchemaCarteira}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="CustDeriv" type="{http://www.stianbid.com.br/SchemaCarteira}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="CustRF" type="{http://www.stianbid.com.br/SchemaCarteira}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="CustRV" type="{http://www.stianbid.com.br/SchemaCarteira}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="CustUnico" type="{http://www.stianbid.com.br/SchemaCarteira}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="GestCota" type="{http://www.stianbid.com.br/SchemaCarteira}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="GestDeriv" type="{http://www.stianbid.com.br/SchemaCarteira}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="GestRF" type="{http://www.stianbid.com.br/SchemaCarteira}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="GestRV" type="{http://www.stianbid.com.br/SchemaCarteira}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="GestUnico" type="{http://www.stianbid.com.br/SchemaCarteira}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrestServicoComplexType",
         propOrder = {"administrador", "empAvRisco", "controlAtivo", "custCota", "custDeriv", "custRF", "custRV", "custUnico", "gestCota", "gestDeriv", "gestRF", "gestRV", "gestUnico"})
public class PrestServicoComplexType {

    @XmlElement(name = "Administrador", required = true) protected PrestServicoIdentificationComplexType administrador;
    @XmlElement(name = "EmpAvRisco") protected PrestServicoIdentificationComplexType empAvRisco;
    @XmlElement(name = "ControlAtivo", required = true) protected PrestServicoIdentificationComplexType controlAtivo;
    @XmlElement(name = "CustCota") protected PrestServicoIdentificationComplexType custCota;
    @XmlElement(name = "CustDeriv") protected PrestServicoIdentificationComplexType custDeriv;
    @XmlElement(name = "CustRF") protected PrestServicoIdentificationComplexType custRF;
    @XmlElement(name = "CustRV") protected PrestServicoIdentificationComplexType custRV;
    @XmlElement(name = "CustUnico") protected PrestServicoIdentificationComplexType custUnico;
    @XmlElement(name = "GestCota") protected PrestServicoIdentificationComplexType gestCota;
    @XmlElement(name = "GestDeriv") protected PrestServicoIdentificationComplexType gestDeriv;
    @XmlElement(name = "GestRF") protected PrestServicoIdentificationComplexType gestRF;
    @XmlElement(name = "GestRV") protected PrestServicoIdentificationComplexType gestRV;
    @XmlElement(name = "GestUnico") protected PrestServicoIdentificationComplexType gestUnico;

    /**
     * Obtém o valor da propriedade administrador.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getAdministrador() {
        return administrador;
    }

    /**
     * Define o valor da propriedade administrador.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setAdministrador(PrestServicoIdentificationComplexType value) {
        this.administrador = value;
    }

    /**
     * Obtém o valor da propriedade controlAtivo.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getControlAtivo() {
        return controlAtivo;
    }

    /**
     * Define o valor da propriedade controlAtivo.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setControlAtivo(PrestServicoIdentificationComplexType value) {
        this.controlAtivo = value;
    }

    /**
     * Obtém o valor da propriedade custCota.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getCustCota() {
        return custCota;
    }

    /**
     * Define o valor da propriedade custCota.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setCustCota(PrestServicoIdentificationComplexType value) {
        this.custCota = value;
    }

    /**
     * Obtém o valor da propriedade custDeriv.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getCustDeriv() {
        return custDeriv;
    }

    /**
     * Define o valor da propriedade custDeriv.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setCustDeriv(PrestServicoIdentificationComplexType value) {
        this.custDeriv = value;
    }

    /**
     * Obtém o valor da propriedade custRF.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getCustRF() {
        return custRF;
    }

    /**
     * Define o valor da propriedade custRF.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setCustRF(PrestServicoIdentificationComplexType value) {
        this.custRF = value;
    }

    /**
     * Obtém o valor da propriedade custRV.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getCustRV() {
        return custRV;
    }

    /**
     * Define o valor da propriedade custRV.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setCustRV(PrestServicoIdentificationComplexType value) {
        this.custRV = value;
    }

    /**
     * Obtém o valor da propriedade custUnico.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getCustUnico() {
        return custUnico;
    }

    /**
     * Define o valor da propriedade custUnico.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setCustUnico(PrestServicoIdentificationComplexType value) {
        this.custUnico = value;
    }

    /**
     * Obtém o valor da propriedade empAvRisco.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getEmpAvRisco() {
        return empAvRisco;
    }

    /**
     * Define o valor da propriedade empAvRisco.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setEmpAvRisco(PrestServicoIdentificationComplexType value) {
        this.empAvRisco = value;
    }

    /**
     * Obtém o valor da propriedade gestCota.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getGestCota() {
        return gestCota;
    }

    /**
     * Define o valor da propriedade gestCota.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setGestCota(PrestServicoIdentificationComplexType value) {
        this.gestCota = value;
    }

    /**
     * Obtém o valor da propriedade gestDeriv.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getGestDeriv() {
        return gestDeriv;
    }

    /**
     * Define o valor da propriedade gestDeriv.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setGestDeriv(PrestServicoIdentificationComplexType value) {
        this.gestDeriv = value;
    }

    /**
     * Obtém o valor da propriedade gestRF.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getGestRF() {
        return gestRF;
    }

    /**
     * Define o valor da propriedade gestRF.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setGestRF(PrestServicoIdentificationComplexType value) {
        this.gestRF = value;
    }

    /**
     * Obtém o valor da propriedade gestRV.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getGestRV() {
        return gestRV;
    }

    /**
     * Define o valor da propriedade gestRV.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setGestRV(PrestServicoIdentificationComplexType value) {
        this.gestRV = value;
    }

    /**
     * Obtém o valor da propriedade gestUnico.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getGestUnico() {
        return gestUnico;
    }

    /**
     * Define o valor da propriedade gestUnico.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setGestUnico(PrestServicoIdentificationComplexType value) {
        this.gestUnico = value;
    }

}
