package br.com.stianbid.schemacarteira;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Mensagem de Retorno de Carteiras Correção.
 * <p>
 * <p>
 * <p>Classe Java de MessageResponseConsumirCarteirasCorrecao complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageResponseConsumirCarteirasCorrecao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idCampo" type="{http://www.stianbid.com.br/SchemaCarteira}CampoIdentifier"/>
 *         &lt;element name="nmCampo" type="{http://www.stianbid.com.br/SchemaCarteira}NomeCampo"/>
 *         &lt;element name="dsJustificativa" type="{http://www.stianbid.com.br/SchemaCarteira}DescricaoJustificativa" minOccurs="0"/>
 *         &lt;element name="listaItensCodAutoReg" type="{http://www.stianbid.com.br/SchemaCarteira}RevCodAutoRegComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageResponseConsumirCarteirasCorrecao",
         propOrder = {"idCampo", "nmCampo", "dsJustificativa", "listaItensCodAutoReg"})
public class MessageResponseConsumirCarteirasCorrecao {

    protected int idCampo;
    @XmlElement(required = true) protected String nmCampo;
    protected String dsJustificativa;
    protected RevCodAutoRegComplexType listaItensCodAutoReg;

    /**
     * Obtém o valor da propriedade dsJustificativa.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDsJustificativa() {
        return dsJustificativa;
    }

    /**
     * Define o valor da propriedade dsJustificativa.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDsJustificativa(String value) {
        this.dsJustificativa = value;
    }

    /**
     * Obtém o valor da propriedade idCampo.
     */
    public int getIdCampo() {
        return idCampo;
    }

    /**
     * Define o valor da propriedade idCampo.
     */
    public void setIdCampo(int value) {
        this.idCampo = value;
    }

    /**
     * Obtém o valor da propriedade listaItensCodAutoReg.
     *
     * @return possible object is
     * {@link RevCodAutoRegComplexType }
     */
    public RevCodAutoRegComplexType getListaItensCodAutoReg() {
        return listaItensCodAutoReg;
    }

    /**
     * Define o valor da propriedade listaItensCodAutoReg.
     *
     * @param value allowed object is
     *              {@link RevCodAutoRegComplexType }
     */
    public void setListaItensCodAutoReg(RevCodAutoRegComplexType value) {
        this.listaItensCodAutoReg = value;
    }

    /**
     * Obtém o valor da propriedade nmCampo.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNmCampo() {
        return nmCampo;
    }

    /**
     * Define o valor da propriedade nmCampo.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNmCampo(String value) {
        this.nmCampo = value;
    }

}
