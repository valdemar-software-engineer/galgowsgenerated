package br.com.stianbid.schemafundo;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlType;


/**
 * Atualização de Documentos de Fundos.
 * <p>
 * <p>Classe Java de EnvDocAtuBinaryComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="EnvDocAtuBinaryComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dsDescDocto" type="{http://www.stianbid.com.br/SchemaFundo}DescricaoDocumento" minOccurs="0"/>
 *         &lt;element name="dsNomeDocumento" type="{http://www.stianbid.com.br/SchemaFundo}NomeDocumento"/>
 *         &lt;element name="idIdioma" type="{http://www.stianbid.com.br/SchemaFundo}IdiomaIdentifier"/>
 *         &lt;element name="binaryArquivo" type="{http://www.stianbid.com.br/SchemaFundo}ArquivoBinario"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnvDocAtuBinaryComplexType",
         propOrder = {"dsDescDocto", "dsNomeDocumento", "idIdioma", "binaryArquivo"})
public class EnvDocAtuBinaryComplexType {

    protected String dsDescDocto;
    @XmlElement(required = true) protected String dsNomeDocumento;
    protected int idIdioma;
    @XmlElement(required = true) @XmlMimeType("application/octet-stream") protected DataHandler binaryArquivo;

    /**
     * Obtém o valor da propriedade binaryArquivo.
     *
     * @return possible object is
     * {@link DataHandler }
     */
    public DataHandler getBinaryArquivo() {
        return binaryArquivo;
    }

    /**
     * Define o valor da propriedade binaryArquivo.
     *
     * @param value allowed object is
     *              {@link DataHandler }
     */
    public void setBinaryArquivo(DataHandler value) {
        this.binaryArquivo = value;
    }

    /**
     * Obtém o valor da propriedade dsDescDocto.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDsDescDocto() {
        return dsDescDocto;
    }

    /**
     * Define o valor da propriedade dsDescDocto.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDsDescDocto(String value) {
        this.dsDescDocto = value;
    }

    /**
     * Obtém o valor da propriedade dsNomeDocumento.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDsNomeDocumento() {
        return dsNomeDocumento;
    }

    /**
     * Define o valor da propriedade dsNomeDocumento.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDsNomeDocumento(String value) {
        this.dsNomeDocumento = value;
    }

    /**
     * Obtém o valor da propriedade idIdioma.
     */
    public int getIdIdioma() {
        return idIdioma;
    }

    /**
     * Define o valor da propriedade idIdioma.
     */
    public void setIdIdioma(int value) {
        this.idIdioma = value;
    }

}
