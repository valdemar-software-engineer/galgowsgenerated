package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Datas
 * <p>
 * <p>Classe Java de DatasComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="DatasComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PreOper" type="{http://www.stianbid.com.br/Common}YesNoIndicator"/>
 *         &lt;element name="DtInicAtiv" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtRegCvm" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtPublInicOf" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatasComplexType", propOrder = {"preOper", "dtInicAtiv", "dtRegCvm", "dtPublInicOf"})
public class DatasComplexType {

    @XmlElement(name = "PreOper", required = true) protected YesNoIndicator preOper;
    @XmlElement(name = "DtInicAtiv") protected XMLGregorianCalendar dtInicAtiv;
    @XmlElement(name = "DtRegCvm") protected XMLGregorianCalendar dtRegCvm;
    @XmlElement(name = "DtPublInicOf") protected XMLGregorianCalendar dtPublInicOf;

    /**
     * Obtém o valor da propriedade dtInicAtiv.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInicAtiv() {
        return dtInicAtiv;
    }

    /**
     * Define o valor da propriedade dtInicAtiv.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInicAtiv(XMLGregorianCalendar value) {
        this.dtInicAtiv = value;
    }

    /**
     * Obtém o valor da propriedade dtPublInicOf.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtPublInicOf() {
        return dtPublInicOf;
    }

    /**
     * Define o valor da propriedade dtPublInicOf.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtPublInicOf(XMLGregorianCalendar value) {
        this.dtPublInicOf = value;
    }

    /**
     * Obtém o valor da propriedade dtRegCvm.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtRegCvm() {
        return dtRegCvm;
    }

    /**
     * Define o valor da propriedade dtRegCvm.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtRegCvm(XMLGregorianCalendar value) {
        this.dtRegCvm = value;
    }

    /**
     * Obtém o valor da propriedade preOper.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getPreOper() {
        return preOper;
    }

    /**
     * Define o valor da propriedade preOper.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setPreOper(YesNoIndicator value) {
        this.preOper = value;
    }

}
