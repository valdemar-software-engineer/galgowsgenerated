package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Alterações nos Parâmetros de Movimentação
 * <p>
 * <p>
 * <p>Classe Java de AltParMovimComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="AltParMovimComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DtPrazoConvAplic" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtPrazoConvResg" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtPrazoPagtoResg" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtRegraPagtoResg" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtDescRegraPagtoResg" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtFechCapt" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AltParMovimComplexType",
         propOrder = {"dtPrazoConvAplic", "dtPrazoConvResg", "dtPrazoPagtoResg", "dtRegraPagtoResg", "dtDescRegraPagtoResg", "dtFechCapt"})
public class AltParMovimComplexType {

    @XmlElement(name = "DtPrazoConvAplic") protected XMLGregorianCalendar dtPrazoConvAplic;
    @XmlElement(name = "DtPrazoConvResg") protected XMLGregorianCalendar dtPrazoConvResg;
    @XmlElement(name = "DtPrazoPagtoResg") protected XMLGregorianCalendar dtPrazoPagtoResg;
    @XmlElement(name = "DtRegraPagtoResg") protected XMLGregorianCalendar dtRegraPagtoResg;
    @XmlElement(name = "DtDescRegraPagtoResg") protected XMLGregorianCalendar dtDescRegraPagtoResg;
    @XmlElement(name = "DtFechCapt") protected XMLGregorianCalendar dtFechCapt;

    /**
     * Obtém o valor da propriedade dtDescRegraPagtoResg.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtDescRegraPagtoResg() {
        return dtDescRegraPagtoResg;
    }

    /**
     * Define o valor da propriedade dtDescRegraPagtoResg.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtDescRegraPagtoResg(XMLGregorianCalendar value) {
        this.dtDescRegraPagtoResg = value;
    }

    /**
     * Obtém o valor da propriedade dtFechCapt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtFechCapt() {
        return dtFechCapt;
    }

    /**
     * Define o valor da propriedade dtFechCapt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtFechCapt(XMLGregorianCalendar value) {
        this.dtFechCapt = value;
    }

    /**
     * Obtém o valor da propriedade dtPrazoConvAplic.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtPrazoConvAplic() {
        return dtPrazoConvAplic;
    }

    /**
     * Define o valor da propriedade dtPrazoConvAplic.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtPrazoConvAplic(XMLGregorianCalendar value) {
        this.dtPrazoConvAplic = value;
    }

    /**
     * Obtém o valor da propriedade dtPrazoConvResg.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtPrazoConvResg() {
        return dtPrazoConvResg;
    }

    /**
     * Define o valor da propriedade dtPrazoConvResg.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtPrazoConvResg(XMLGregorianCalendar value) {
        this.dtPrazoConvResg = value;
    }

    /**
     * Obtém o valor da propriedade dtPrazoPagtoResg.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtPrazoPagtoResg() {
        return dtPrazoPagtoResg;
    }

    /**
     * Define o valor da propriedade dtPrazoPagtoResg.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtPrazoPagtoResg(XMLGregorianCalendar value) {
        this.dtPrazoPagtoResg = value;
    }

    /**
     * Obtém o valor da propriedade dtRegraPagtoResg.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtRegraPagtoResg() {
        return dtRegraPagtoResg;
    }

    /**
     * Define o valor da propriedade dtRegraPagtoResg.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtRegraPagtoResg(XMLGregorianCalendar value) {
        this.dtRegraPagtoResg = value;
    }

}
