package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * Outras Taxas
 * <p>
 * <p>Classe Java de OutrasTaxasComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="OutrasTaxasComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TpTxEntr" type="{http://www.stianbid.com.br/SchemaFundo}TpTxEntrSaidaCode"/>
 *         &lt;element name="VlTxEntr" type="{http://www.stianbid.com.br/SchemaFundo}DecimalNumber_13_4" minOccurs="0"/>
 *         &lt;element name="InfoTxEntr" type="{http://www.stianbid.com.br/Common}Max500Text" minOccurs="0"/>
 *         &lt;element name="TpTxSaida" type="{http://www.stianbid.com.br/SchemaFundo}TpTxEntrSaidaCode"/>
 *         &lt;element name="VlTxSaida" type="{http://www.stianbid.com.br/SchemaFundo}DecimalNumber_13_4" minOccurs="0"/>
 *         &lt;element name="InfoTxSaida" type="{http://www.stianbid.com.br/Common}Max500Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutrasTaxasComplexType",
         propOrder = {"tpTxEntr", "vlTxEntr", "infoTxEntr", "tpTxSaida", "vlTxSaida", "infoTxSaida"})
public class OutrasTaxasComplexType {

    @XmlElement(name = "TpTxEntr") protected int tpTxEntr;
    @XmlElement(name = "VlTxEntr") protected BigDecimal vlTxEntr;
    @XmlElement(name = "InfoTxEntr") protected String infoTxEntr;
    @XmlElement(name = "TpTxSaida") protected int tpTxSaida;
    @XmlElement(name = "VlTxSaida") protected BigDecimal vlTxSaida;
    @XmlElement(name = "InfoTxSaida") protected String infoTxSaida;

    /**
     * Obtém o valor da propriedade infoTxEntr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInfoTxEntr() {
        return infoTxEntr;
    }

    /**
     * Define o valor da propriedade infoTxEntr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setInfoTxEntr(String value) {
        this.infoTxEntr = value;
    }

    /**
     * Obtém o valor da propriedade infoTxSaida.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInfoTxSaida() {
        return infoTxSaida;
    }

    /**
     * Define o valor da propriedade infoTxSaida.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setInfoTxSaida(String value) {
        this.infoTxSaida = value;
    }

    /**
     * Obtém o valor da propriedade tpTxEntr.
     */
    public int getTpTxEntr() {
        return tpTxEntr;
    }

    /**
     * Define o valor da propriedade tpTxEntr.
     */
    public void setTpTxEntr(int value) {
        this.tpTxEntr = value;
    }

    /**
     * Obtém o valor da propriedade tpTxSaida.
     */
    public int getTpTxSaida() {
        return tpTxSaida;
    }

    /**
     * Define o valor da propriedade tpTxSaida.
     */
    public void setTpTxSaida(int value) {
        this.tpTxSaida = value;
    }

    /**
     * Obtém o valor da propriedade vlTxEntr.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getVlTxEntr() {
        return vlTxEntr;
    }

    /**
     * Define o valor da propriedade vlTxEntr.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setVlTxEntr(BigDecimal value) {
        this.vlTxEntr = value;
    }

    /**
     * Obtém o valor da propriedade vlTxSaida.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getVlTxSaida() {
        return vlTxSaida;
    }

    /**
     * Define o valor da propriedade vlTxSaida.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setVlTxSaida(BigDecimal value) {
        this.vlTxSaida = value;
    }

}
