package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dados para Alteração do Fundo
 * <p>
 * <p>Classe Java de DadosAltComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="DadosAltComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AltIdentif" type="{http://www.stianbid.com.br/SchemaFundo}AltIdentifComplexType" minOccurs="0"/>
 *         &lt;element name="AltPerfil" type="{http://www.stianbid.com.br/SchemaFundo}AltPerfilComplexType" minOccurs="0"/>
 *         &lt;element name="AltDatas" type="{http://www.stianbid.com.br/SchemaFundo}AltDatasComplexType" minOccurs="0"/>
 *         &lt;element name="AltPrestServ" type="{http://www.stianbid.com.br/SchemaFundo}AltPrestServComplexType" minOccurs="0"/>
 *         &lt;element name="AltTaxas" type="{http://www.stianbid.com.br/SchemaFundo}AltTaxasComplexType" minOccurs="0"/>
 *         &lt;element name="AltParMovim" type="{http://www.stianbid.com.br/SchemaFundo}AltParMovimComplexType" minOccurs="0"/>
 *         &lt;element name="AltDoctos" type="{http://www.stianbid.com.br/SchemaFundo}AltDoctosComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DadosAltComplexType",
         propOrder = {"altIdentif", "altPerfil", "altDatas", "altPrestServ", "altTaxas", "altParMovim", "altDoctos"})
public class DadosAltComplexType {

    @XmlElement(name = "AltIdentif") protected AltIdentifComplexType altIdentif;
    @XmlElement(name = "AltPerfil") protected AltPerfilComplexType altPerfil;
    @XmlElement(name = "AltDatas") protected AltDatasComplexType altDatas;
    @XmlElement(name = "AltPrestServ") protected AltPrestServComplexType altPrestServ;
    @XmlElement(name = "AltTaxas") protected AltTaxasComplexType altTaxas;
    @XmlElement(name = "AltParMovim") protected AltParMovimComplexType altParMovim;
    @XmlElement(name = "AltDoctos") protected AltDoctosComplexType altDoctos;

    /**
     * Obtém o valor da propriedade altDatas.
     *
     * @return possible object is
     * {@link AltDatasComplexType }
     */
    public AltDatasComplexType getAltDatas() {
        return altDatas;
    }

    /**
     * Define o valor da propriedade altDatas.
     *
     * @param value allowed object is
     *              {@link AltDatasComplexType }
     */
    public void setAltDatas(AltDatasComplexType value) {
        this.altDatas = value;
    }

    /**
     * Obtém o valor da propriedade altDoctos.
     *
     * @return possible object is
     * {@link AltDoctosComplexType }
     */
    public AltDoctosComplexType getAltDoctos() {
        return altDoctos;
    }

    /**
     * Define o valor da propriedade altDoctos.
     *
     * @param value allowed object is
     *              {@link AltDoctosComplexType }
     */
    public void setAltDoctos(AltDoctosComplexType value) {
        this.altDoctos = value;
    }

    /**
     * Obtém o valor da propriedade altIdentif.
     *
     * @return possible object is
     * {@link AltIdentifComplexType }
     */
    public AltIdentifComplexType getAltIdentif() {
        return altIdentif;
    }

    /**
     * Define o valor da propriedade altIdentif.
     *
     * @param value allowed object is
     *              {@link AltIdentifComplexType }
     */
    public void setAltIdentif(AltIdentifComplexType value) {
        this.altIdentif = value;
    }

    /**
     * Obtém o valor da propriedade altParMovim.
     *
     * @return possible object is
     * {@link AltParMovimComplexType }
     */
    public AltParMovimComplexType getAltParMovim() {
        return altParMovim;
    }

    /**
     * Define o valor da propriedade altParMovim.
     *
     * @param value allowed object is
     *              {@link AltParMovimComplexType }
     */
    public void setAltParMovim(AltParMovimComplexType value) {
        this.altParMovim = value;
    }

    /**
     * Obtém o valor da propriedade altPerfil.
     *
     * @return possible object is
     * {@link AltPerfilComplexType }
     */
    public AltPerfilComplexType getAltPerfil() {
        return altPerfil;
    }

    /**
     * Define o valor da propriedade altPerfil.
     *
     * @param value allowed object is
     *              {@link AltPerfilComplexType }
     */
    public void setAltPerfil(AltPerfilComplexType value) {
        this.altPerfil = value;
    }

    /**
     * Obtém o valor da propriedade altPrestServ.
     *
     * @return possible object is
     * {@link AltPrestServComplexType }
     */
    public AltPrestServComplexType getAltPrestServ() {
        return altPrestServ;
    }

    /**
     * Define o valor da propriedade altPrestServ.
     *
     * @param value allowed object is
     *              {@link AltPrestServComplexType }
     */
    public void setAltPrestServ(AltPrestServComplexType value) {
        this.altPrestServ = value;
    }

    /**
     * Obtém o valor da propriedade altTaxas.
     *
     * @return possible object is
     * {@link AltTaxasComplexType }
     */
    public AltTaxasComplexType getAltTaxas() {
        return altTaxas;
    }

    /**
     * Define o valor da propriedade altTaxas.
     *
     * @param value allowed object is
     *              {@link AltTaxasComplexType }
     */
    public void setAltTaxas(AltTaxasComplexType value) {
        this.altTaxas = value;
    }

}
