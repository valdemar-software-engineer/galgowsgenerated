package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Identificação do FIDC de Origem
 * <p>
 * <p>
 * <p>Classe Java de IdentFidcOrigemComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="IdentFidcOrigemComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CdSti" type="{http://www.stianbid.com.br/SchemaFundo}STIIdentifier"/>
 *         &lt;element name="Cnpj" type="{http://www.stianbid.com.br/SchemaFundo}CNPJIdentifier"/>
 *         &lt;element name="RazSocial" type="{http://www.stianbid.com.br/Common}Max150Text"/>
 *         &lt;element name="Sigla" type="{http://www.stianbid.com.br/Common}Max35Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentFidcOrigemComplexType", propOrder = {"cdSti", "cnpj", "razSocial", "sigla"})
public class IdentFidcOrigemComplexType {

    @XmlElement(name = "CdSti") protected int cdSti;
    @XmlElement(name = "Cnpj") protected long cnpj;
    @XmlElement(name = "RazSocial", required = true) protected String razSocial;
    @XmlElement(name = "Sigla") protected String sigla;

    /**
     * Obtém o valor da propriedade cdSti.
     */
    public int getCdSti() {
        return cdSti;
    }

    /**
     * Define o valor da propriedade cdSti.
     */
    public void setCdSti(int value) {
        this.cdSti = value;
    }

    /**
     * Obtém o valor da propriedade cnpj.
     */
    public long getCnpj() {
        return cnpj;
    }

    /**
     * Define o valor da propriedade cnpj.
     */
    public void setCnpj(long value) {
        this.cnpj = value;
    }

    /**
     * Obtém o valor da propriedade razSocial.
     *
     * @return possible object is
     * {@link String }
     */
    public String getRazSocial() {
        return razSocial;
    }

    /**
     * Define o valor da propriedade razSocial.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setRazSocial(String value) {
        this.razSocial = value;
    }

    /**
     * Obtém o valor da propriedade sigla.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSigla() {
        return sigla;
    }

    /**
     * Define o valor da propriedade sigla.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSigla(String value) {
        this.sigla = value;
    }

}
