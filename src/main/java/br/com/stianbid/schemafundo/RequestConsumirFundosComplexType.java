package br.com.stianbid.schemafundo;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Metodo - Consumir Fundos Correcao
 * <p>
 * <p>
 * <p>Classe Java de RequestConsumirFundosComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="RequestConsumirFundosComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType">
 *       &lt;sequence>
 *         &lt;element name="CdSti" type="{http://www.stianbid.com.br/SchemaFundo}STIIdentifier"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestConsumirFundosComplexType", propOrder = {"cdSti"})
public class RequestConsumirFundosComplexType extends MessageRequestComplexType {

    @XmlElement(name = "CdSti") protected int cdSti;

    /**
     * Obtém o valor da propriedade cdSti.
     */
    public int getCdSti() {
        return cdSti;
    }

    /**
     * Define o valor da propriedade cdSti.
     */
    public void setCdSti(int value) {
        this.cdSti = value;
    }

}
