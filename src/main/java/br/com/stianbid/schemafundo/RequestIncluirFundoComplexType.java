package br.com.stianbid.schemafundo;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Incluir Fundos.
 * <p>
 * <p>Classe Java de RequestIncluirFundoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="RequestIncluirFundoComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType">
 *       &lt;sequence>
 *         &lt;element name="fundos" type="{http://www.stianbid.com.br/SchemaFundo}MessageFundoComplexType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestIncluirFundoComplexType", propOrder = {"fundos"})
public class RequestIncluirFundoComplexType extends MessageRequestComplexType {

    @XmlElement(required = true) protected List<MessageFundoComplexType> fundos;

    /**
     * Gets the value of the fundos property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fundos property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFundos().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MessageFundoComplexType }
     */
    public List<MessageFundoComplexType> getFundos() {
        if (fundos == null) {
            fundos = new ArrayList<MessageFundoComplexType>();
        }
        return this.fundos;
    }

}
