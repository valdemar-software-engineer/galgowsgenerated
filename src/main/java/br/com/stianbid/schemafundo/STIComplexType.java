package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Dados do STI (Fundos , Serie e Cota).
 * <p>
 * <p>
 * <p>Classe Java de STIComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="STIComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Operacao" type="{http://www.stianbid.com.br/SchemaFundo}OperationCode"/>
 *         &lt;element name="IdFdo" type="{http://www.stianbid.com.br/SchemaFundo}IdentificacaoFundoComplexType"/>
 *         &lt;element name="Eventos" type="{http://www.stianbid.com.br/SchemaFundo}EventosComplexType" minOccurs="0"/>
 *         &lt;element name="Perfil" type="{http://www.stianbid.com.br/SchemaFundo}PerfilComplexType"/>
 *         &lt;element name="PerfilFidc" type="{http://www.stianbid.com.br/SchemaFundo}PerfilFidcComplexType" minOccurs="0"/>
 *         &lt;element name="Datas" type="{http://www.stianbid.com.br/SchemaFundo}DatasComplexType" minOccurs="0"/>
 *         &lt;element name="PrestServico" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoComplexType"/>
 *         &lt;element name="Taxas" type="{http://www.stianbid.com.br/SchemaFundo}TaxasComplexType"/>
 *         &lt;element name="Movimentacao" type="{http://www.stianbid.com.br/SchemaFundo}ParamMovimentacaoComplexType"/>
 *         &lt;element name="Amortizacao" type="{http://www.stianbid.com.br/SchemaFundo}AmortizacaoComplexType" minOccurs="0"/>
 *         &lt;element name="Contas" type="{http://www.stianbid.com.br/SchemaFundo}ContasComplexType" minOccurs="0"/>
 *         &lt;element name="FidcSerie" type="{http://www.stianbid.com.br/SchemaFundo}SerieFIDCComplexType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CotaSubord" type="{http://www.stianbid.com.br/SchemaFundo}CotaSubordFIDCComplexType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DadosAlt" type="{http://www.stianbid.com.br/SchemaFundo}DadosAltComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "STIComplexType",
         propOrder = {"operacao", "idFdo", "eventos", "perfil", "perfilFidc", "datas", "prestServico", "taxas", "movimentacao", "amortizacao", "contas", "fidcSerie", "cotaSubord", "dadosAlt"})
public class STIComplexType {

    @XmlElement(name = "Operacao", required = true) protected String operacao;
    @XmlElement(name = "IdFdo", required = true) protected IdentificacaoFundoComplexType idFdo;
    @XmlElement(name = "Eventos") protected EventosComplexType eventos;
    @XmlElement(name = "Perfil", required = true) protected PerfilComplexType perfil;
    @XmlElement(name = "PerfilFidc") protected PerfilFidcComplexType perfilFidc;
    @XmlElement(name = "Datas") protected DatasComplexType datas;
    @XmlElement(name = "PrestServico", required = true) protected PrestServicoComplexType prestServico;
    @XmlElement(name = "Taxas", required = true) protected TaxasComplexType taxas;
    @XmlElement(name = "Movimentacao", required = true) protected ParamMovimentacaoComplexType movimentacao;
    @XmlElement(name = "Amortizacao") protected AmortizacaoComplexType amortizacao;
    @XmlElement(name = "Contas") protected ContasComplexType contas;
    @XmlElement(name = "FidcSerie") protected List<SerieFIDCComplexType> fidcSerie;
    @XmlElement(name = "CotaSubord") protected List<CotaSubordFIDCComplexType> cotaSubord;
    @XmlElement(name = "DadosAlt") protected DadosAltComplexType dadosAlt;

    /**
     * Obtém o valor da propriedade amortizacao.
     *
     * @return possible object is
     * {@link AmortizacaoComplexType }
     */
    public AmortizacaoComplexType getAmortizacao() {
        return amortizacao;
    }

    /**
     * Define o valor da propriedade amortizacao.
     *
     * @param value allowed object is
     *              {@link AmortizacaoComplexType }
     */
    public void setAmortizacao(AmortizacaoComplexType value) {
        this.amortizacao = value;
    }

    /**
     * Obtém o valor da propriedade contas.
     *
     * @return possible object is
     * {@link ContasComplexType }
     */
    public ContasComplexType getContas() {
        return contas;
    }

    /**
     * Define o valor da propriedade contas.
     *
     * @param value allowed object is
     *              {@link ContasComplexType }
     */
    public void setContas(ContasComplexType value) {
        this.contas = value;
    }

    /**
     * Gets the value of the cotaSubord property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cotaSubord property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCotaSubord().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CotaSubordFIDCComplexType }
     */
    public List<CotaSubordFIDCComplexType> getCotaSubord() {
        if (cotaSubord == null) {
            cotaSubord = new ArrayList<CotaSubordFIDCComplexType>();
        }
        return this.cotaSubord;
    }

    /**
     * Obtém o valor da propriedade dadosAlt.
     *
     * @return possible object is
     * {@link DadosAltComplexType }
     */
    public DadosAltComplexType getDadosAlt() {
        return dadosAlt;
    }

    /**
     * Define o valor da propriedade dadosAlt.
     *
     * @param value allowed object is
     *              {@link DadosAltComplexType }
     */
    public void setDadosAlt(DadosAltComplexType value) {
        this.dadosAlt = value;
    }

    /**
     * Obtém o valor da propriedade datas.
     *
     * @return possible object is
     * {@link DatasComplexType }
     */
    public DatasComplexType getDatas() {
        return datas;
    }

    /**
     * Define o valor da propriedade datas.
     *
     * @param value allowed object is
     *              {@link DatasComplexType }
     */
    public void setDatas(DatasComplexType value) {
        this.datas = value;
    }

    /**
     * Obtém o valor da propriedade eventos.
     *
     * @return possible object is
     * {@link EventosComplexType }
     */
    public EventosComplexType getEventos() {
        return eventos;
    }

    /**
     * Define o valor da propriedade eventos.
     *
     * @param value allowed object is
     *              {@link EventosComplexType }
     */
    public void setEventos(EventosComplexType value) {
        this.eventos = value;
    }

    /**
     * Gets the value of the fidcSerie property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fidcSerie property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFidcSerie().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SerieFIDCComplexType }
     */
    public List<SerieFIDCComplexType> getFidcSerie() {
        if (fidcSerie == null) {
            fidcSerie = new ArrayList<SerieFIDCComplexType>();
        }
        return this.fidcSerie;
    }

    /**
     * Obtém o valor da propriedade idFdo.
     *
     * @return possible object is
     * {@link IdentificacaoFundoComplexType }
     */
    public IdentificacaoFundoComplexType getIdFdo() {
        return idFdo;
    }

    /**
     * Define o valor da propriedade idFdo.
     *
     * @param value allowed object is
     *              {@link IdentificacaoFundoComplexType }
     */
    public void setIdFdo(IdentificacaoFundoComplexType value) {
        this.idFdo = value;
    }

    /**
     * Obtém o valor da propriedade movimentacao.
     *
     * @return possible object is
     * {@link ParamMovimentacaoComplexType }
     */
    public ParamMovimentacaoComplexType getMovimentacao() {
        return movimentacao;
    }

    /**
     * Define o valor da propriedade movimentacao.
     *
     * @param value allowed object is
     *              {@link ParamMovimentacaoComplexType }
     */
    public void setMovimentacao(ParamMovimentacaoComplexType value) {
        this.movimentacao = value;
    }

    /**
     * Obtém o valor da propriedade operacao.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOperacao() {
        return operacao;
    }

    /**
     * Define o valor da propriedade operacao.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setOperacao(String value) {
        this.operacao = value;
    }

    /**
     * Obtém o valor da propriedade perfil.
     *
     * @return possible object is
     * {@link PerfilComplexType }
     */
    public PerfilComplexType getPerfil() {
        return perfil;
    }

    /**
     * Define o valor da propriedade perfil.
     *
     * @param value allowed object is
     *              {@link PerfilComplexType }
     */
    public void setPerfil(PerfilComplexType value) {
        this.perfil = value;
    }

    /**
     * Obtém o valor da propriedade perfilFidc.
     *
     * @return possible object is
     * {@link PerfilFidcComplexType }
     */
    public PerfilFidcComplexType getPerfilFidc() {
        return perfilFidc;
    }

    /**
     * Define o valor da propriedade perfilFidc.
     *
     * @param value allowed object is
     *              {@link PerfilFidcComplexType }
     */
    public void setPerfilFidc(PerfilFidcComplexType value) {
        this.perfilFidc = value;
    }

    /**
     * Obtém o valor da propriedade prestServico.
     *
     * @return possible object is
     * {@link PrestServicoComplexType }
     */
    public PrestServicoComplexType getPrestServico() {
        return prestServico;
    }

    /**
     * Define o valor da propriedade prestServico.
     *
     * @param value allowed object is
     *              {@link PrestServicoComplexType }
     */
    public void setPrestServico(PrestServicoComplexType value) {
        this.prestServico = value;
    }

    /**
     * Obtém o valor da propriedade taxas.
     *
     * @return possible object is
     * {@link TaxasComplexType }
     */
    public TaxasComplexType getTaxas() {
        return taxas;
    }

    /**
     * Define o valor da propriedade taxas.
     *
     * @param value allowed object is
     *              {@link TaxasComplexType }
     */
    public void setTaxas(TaxasComplexType value) {
        this.taxas = value;
    }

}
