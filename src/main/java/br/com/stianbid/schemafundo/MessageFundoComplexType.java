package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Mensagem de Fundos de Investimentos
 * <p>
 * <p>
 * <p>Classe Java de MessageFundoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageFundoComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UpdtdDt" type="{http://www.stianbid.com.br/SchemaFundo}UpdatedDateComplexType" minOccurs="0"/>
 *         &lt;element name="FinInstrmDtls" type="{http://www.stianbid.com.br/SchemaFundo}FinancialInstrumentDetailsComplexType" minOccurs="0"/>
 *         &lt;element name="ValtnDealgChrtcs" type="{http://www.stianbid.com.br/SchemaFundo}ValuationDealingCharacteristicsComplexType" minOccurs="0"/>
 *         &lt;element name="InstrmRstrctns" type="{http://www.stianbid.com.br/SchemaFundo}InstrumentRestrictionsComplexType" minOccurs="0"/>
 *         &lt;element name="SttlmDtls" type="{http://www.stianbid.com.br/SchemaFundo}SettlementDetailsComplexType" maxOccurs="5" minOccurs="0"/>
 *         &lt;element name="FndMgmtCpny" type="{http://www.stianbid.com.br/SchemaFundo}FundManagementCompanyComplexType" minOccurs="0"/>
 *         &lt;element name="MainFndOrdrDsk" type="{http://www.stianbid.com.br/SchemaFundo}MainFundOrderDeskComplexType" minOccurs="0"/>
 *         &lt;element name="LclOrdrDsk" type="{http://www.stianbid.com.br/SchemaFundo}LocalOrderDeskComplexType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DadosSTI" type="{http://www.stianbid.com.br/SchemaFundo}STIComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageFundoComplexType",
         propOrder = {"updtdDt", "finInstrmDtls", "valtnDealgChrtcs", "instrmRstrctns", "sttlmDtls", "fndMgmtCpny", "mainFndOrdrDsk", "lclOrdrDsk", "dadosSTI"})
public class MessageFundoComplexType {

    @XmlElement(name = "UpdtdDt") protected UpdatedDateComplexType updtdDt;
    @XmlElement(name = "FinInstrmDtls") protected FinancialInstrumentDetailsComplexType finInstrmDtls;
    @XmlElement(name = "ValtnDealgChrtcs") protected ValuationDealingCharacteristicsComplexType valtnDealgChrtcs;
    @XmlElement(name = "InstrmRstrctns") protected InstrumentRestrictionsComplexType instrmRstrctns;
    @XmlElement(name = "SttlmDtls") protected List<SettlementDetailsComplexType> sttlmDtls;
    @XmlElement(name = "FndMgmtCpny") protected FundManagementCompanyComplexType fndMgmtCpny;
    @XmlElement(name = "MainFndOrdrDsk") protected MainFundOrderDeskComplexType mainFndOrdrDsk;
    @XmlElement(name = "LclOrdrDsk") protected List<LocalOrderDeskComplexType> lclOrdrDsk;
    @XmlElement(name = "DadosSTI") protected STIComplexType dadosSTI;

    /**
     * Obtém o valor da propriedade dadosSTI.
     *
     * @return possible object is
     * {@link STIComplexType }
     */
    public STIComplexType getDadosSTI() {
        return dadosSTI;
    }

    /**
     * Define o valor da propriedade dadosSTI.
     *
     * @param value allowed object is
     *              {@link STIComplexType }
     */
    public void setDadosSTI(STIComplexType value) {
        this.dadosSTI = value;
    }

    /**
     * Obtém o valor da propriedade finInstrmDtls.
     *
     * @return possible object is
     * {@link FinancialInstrumentDetailsComplexType }
     */
    public FinancialInstrumentDetailsComplexType getFinInstrmDtls() {
        return finInstrmDtls;
    }

    /**
     * Define o valor da propriedade finInstrmDtls.
     *
     * @param value allowed object is
     *              {@link FinancialInstrumentDetailsComplexType }
     */
    public void setFinInstrmDtls(FinancialInstrumentDetailsComplexType value) {
        this.finInstrmDtls = value;
    }

    /**
     * Obtém o valor da propriedade fndMgmtCpny.
     *
     * @return possible object is
     * {@link FundManagementCompanyComplexType }
     */
    public FundManagementCompanyComplexType getFndMgmtCpny() {
        return fndMgmtCpny;
    }

    /**
     * Define o valor da propriedade fndMgmtCpny.
     *
     * @param value allowed object is
     *              {@link FundManagementCompanyComplexType }
     */
    public void setFndMgmtCpny(FundManagementCompanyComplexType value) {
        this.fndMgmtCpny = value;
    }

    /**
     * Obtém o valor da propriedade instrmRstrctns.
     *
     * @return possible object is
     * {@link InstrumentRestrictionsComplexType }
     */
    public InstrumentRestrictionsComplexType getInstrmRstrctns() {
        return instrmRstrctns;
    }

    /**
     * Define o valor da propriedade instrmRstrctns.
     *
     * @param value allowed object is
     *              {@link InstrumentRestrictionsComplexType }
     */
    public void setInstrmRstrctns(InstrumentRestrictionsComplexType value) {
        this.instrmRstrctns = value;
    }

    /**
     * Gets the value of the lclOrdrDsk property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lclOrdrDsk property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLclOrdrDsk().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocalOrderDeskComplexType }
     */
    public List<LocalOrderDeskComplexType> getLclOrdrDsk() {
        if (lclOrdrDsk == null) {
            lclOrdrDsk = new ArrayList<LocalOrderDeskComplexType>();
        }
        return this.lclOrdrDsk;
    }

    /**
     * Obtém o valor da propriedade mainFndOrdrDsk.
     *
     * @return possible object is
     * {@link MainFundOrderDeskComplexType }
     */
    public MainFundOrderDeskComplexType getMainFndOrdrDsk() {
        return mainFndOrdrDsk;
    }

    /**
     * Define o valor da propriedade mainFndOrdrDsk.
     *
     * @param value allowed object is
     *              {@link MainFundOrderDeskComplexType }
     */
    public void setMainFndOrdrDsk(MainFundOrderDeskComplexType value) {
        this.mainFndOrdrDsk = value;
    }

    /**
     * Gets the value of the sttlmDtls property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sttlmDtls property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSttlmDtls().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SettlementDetailsComplexType }
     */
    public List<SettlementDetailsComplexType> getSttlmDtls() {
        if (sttlmDtls == null) {
            sttlmDtls = new ArrayList<SettlementDetailsComplexType>();
        }
        return this.sttlmDtls;
    }

    /**
     * Obtém o valor da propriedade updtdDt.
     *
     * @return possible object is
     * {@link UpdatedDateComplexType }
     */
    public UpdatedDateComplexType getUpdtdDt() {
        return updtdDt;
    }

    /**
     * Define o valor da propriedade updtdDt.
     *
     * @param value allowed object is
     *              {@link UpdatedDateComplexType }
     */
    public void setUpdtdDt(UpdatedDateComplexType value) {
        this.updtdDt = value;
    }

    /**
     * Obtém o valor da propriedade valtnDealgChrtcs.
     *
     * @return possible object is
     * {@link ValuationDealingCharacteristicsComplexType }
     */
    public ValuationDealingCharacteristicsComplexType getValtnDealgChrtcs() {
        return valtnDealgChrtcs;
    }

    /**
     * Define o valor da propriedade valtnDealgChrtcs.
     *
     * @param value allowed object is
     *              {@link ValuationDealingCharacteristicsComplexType }
     */
    public void setValtnDealgChrtcs(ValuationDealingCharacteristicsComplexType value) {
        this.valtnDealgChrtcs = value;
    }

}
