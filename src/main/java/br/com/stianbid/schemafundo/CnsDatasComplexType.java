package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Consulta de Datas
 * <p>
 * <p>Classe Java de CnsDatasComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="CnsDatasComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DtInicioInclusao" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtFimInclusao" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtIniVigencia" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtFimVicencia" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CnsDatasComplexType",
         propOrder = {"dtInicioInclusao", "dtFimInclusao", "dtIniVigencia", "dtFimVicencia"})
public class CnsDatasComplexType {

    @XmlElement(name = "DtInicioInclusao") protected XMLGregorianCalendar dtInicioInclusao;
    @XmlElement(name = "DtFimInclusao") protected XMLGregorianCalendar dtFimInclusao;
    @XmlElement(name = "DtIniVigencia") protected XMLGregorianCalendar dtIniVigencia;
    @XmlElement(name = "DtFimVicencia") protected XMLGregorianCalendar dtFimVicencia;

    /**
     * Obtém o valor da propriedade dtFimInclusao.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtFimInclusao() {
        return dtFimInclusao;
    }

    /**
     * Define o valor da propriedade dtFimInclusao.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtFimInclusao(XMLGregorianCalendar value) {
        this.dtFimInclusao = value;
    }

    /**
     * Obtém o valor da propriedade dtFimVicencia.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtFimVicencia() {
        return dtFimVicencia;
    }

    /**
     * Define o valor da propriedade dtFimVicencia.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtFimVicencia(XMLGregorianCalendar value) {
        this.dtFimVicencia = value;
    }

    /**
     * Obtém o valor da propriedade dtIniVigencia.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtIniVigencia() {
        return dtIniVigencia;
    }

    /**
     * Define o valor da propriedade dtIniVigencia.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtIniVigencia(XMLGregorianCalendar value) {
        this.dtIniVigencia = value;
    }

    /**
     * Obtém o valor da propriedade dtInicioInclusao.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInicioInclusao() {
        return dtInicioInclusao;
    }

    /**
     * Define o valor da propriedade dtInicioInclusao.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInicioInclusao(XMLGregorianCalendar value) {
        this.dtInicioInclusao = value;
    }

}
