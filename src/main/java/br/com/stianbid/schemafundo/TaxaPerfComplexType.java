package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Taxa de Performance
 * <p>
 * <p>Classe Java de TaxaPerfComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="TaxaPerfComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CobraTxPerf" type="{http://www.stianbid.com.br/Common}YesNoIndicator"/>
 *         &lt;element name="FaixaTxPerf" type="{http://www.stianbid.com.br/SchemaFundo}FaixaTxPerfComplexType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CupomTxperf" type="{http://www.stianbid.com.br/Common}Max500Text" minOccurs="0"/>
 *         &lt;element name="PeriodCobTxPerf" type="{http://www.stianbid.com.br/SchemaFundo}PeriodicidadeTxCode" minOccurs="0"/>
 *         &lt;element name="PlBaseTxPerf" type="{http://www.stianbid.com.br/SchemaFundo}PlBaseTxPerfCode" minOccurs="0"/>
 *         &lt;element name="DtBasePl" type="{http://www.stianbid.com.br/SchemaFundo}Number_2" minOccurs="0"/>
 *         &lt;element name="LinhaDagua" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="ZeraPerf" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="BaseCalcTxPerf" type="{http://www.stianbid.com.br/SchemaFundo}BaseCalcTxPerfCode" minOccurs="0"/>
 *         &lt;element name="AjustePerfCot" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="InfoTxPerf" type="{http://www.stianbid.com.br/Common}Max500Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxaPerfComplexType",
         propOrder = {"cobraTxPerf", "faixaTxPerf", "cupomTxperf", "periodCobTxPerf", "plBaseTxPerf", "dtBasePl", "linhaDagua", "zeraPerf", "baseCalcTxPerf", "ajustePerfCot", "infoTxPerf"})
public class TaxaPerfComplexType {

    @XmlElement(name = "CobraTxPerf", required = true) protected YesNoIndicator cobraTxPerf;
    @XmlElement(name = "FaixaTxPerf") protected List<FaixaTxPerfComplexType> faixaTxPerf;
    @XmlElement(name = "CupomTxperf") protected String cupomTxperf;
    @XmlElement(name = "PeriodCobTxPerf") protected Integer periodCobTxPerf;
    @XmlElement(name = "PlBaseTxPerf") protected Integer plBaseTxPerf;
    @XmlElement(name = "DtBasePl") protected BigDecimal dtBasePl;
    @XmlElement(name = "LinhaDagua") protected YesNoIndicator linhaDagua;
    @XmlElement(name = "ZeraPerf") protected YesNoIndicator zeraPerf;
    @XmlElement(name = "BaseCalcTxPerf") protected Integer baseCalcTxPerf;
    @XmlElement(name = "AjustePerfCot") protected YesNoIndicator ajustePerfCot;
    @XmlElement(name = "InfoTxPerf") protected String infoTxPerf;

    /**
     * Obtém o valor da propriedade ajustePerfCot.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getAjustePerfCot() {
        return ajustePerfCot;
    }

    /**
     * Define o valor da propriedade ajustePerfCot.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setAjustePerfCot(YesNoIndicator value) {
        this.ajustePerfCot = value;
    }

    /**
     * Obtém o valor da propriedade baseCalcTxPerf.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getBaseCalcTxPerf() {
        return baseCalcTxPerf;
    }

    /**
     * Define o valor da propriedade baseCalcTxPerf.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setBaseCalcTxPerf(Integer value) {
        this.baseCalcTxPerf = value;
    }

    /**
     * Obtém o valor da propriedade cobraTxPerf.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getCobraTxPerf() {
        return cobraTxPerf;
    }

    /**
     * Define o valor da propriedade cobraTxPerf.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setCobraTxPerf(YesNoIndicator value) {
        this.cobraTxPerf = value;
    }

    /**
     * Obtém o valor da propriedade cupomTxperf.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCupomTxperf() {
        return cupomTxperf;
    }

    /**
     * Define o valor da propriedade cupomTxperf.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCupomTxperf(String value) {
        this.cupomTxperf = value;
    }

    /**
     * Obtém o valor da propriedade dtBasePl.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getDtBasePl() {
        return dtBasePl;
    }

    /**
     * Define o valor da propriedade dtBasePl.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setDtBasePl(BigDecimal value) {
        this.dtBasePl = value;
    }

    /**
     * Gets the value of the faixaTxPerf property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the faixaTxPerf property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFaixaTxPerf().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FaixaTxPerfComplexType }
     */
    public List<FaixaTxPerfComplexType> getFaixaTxPerf() {
        if (faixaTxPerf == null) {
            faixaTxPerf = new ArrayList<FaixaTxPerfComplexType>();
        }
        return this.faixaTxPerf;
    }

    /**
     * Obtém o valor da propriedade infoTxPerf.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInfoTxPerf() {
        return infoTxPerf;
    }

    /**
     * Define o valor da propriedade infoTxPerf.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setInfoTxPerf(String value) {
        this.infoTxPerf = value;
    }

    /**
     * Obtém o valor da propriedade linhaDagua.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getLinhaDagua() {
        return linhaDagua;
    }

    /**
     * Define o valor da propriedade linhaDagua.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setLinhaDagua(YesNoIndicator value) {
        this.linhaDagua = value;
    }

    /**
     * Obtém o valor da propriedade periodCobTxPerf.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getPeriodCobTxPerf() {
        return periodCobTxPerf;
    }

    /**
     * Define o valor da propriedade periodCobTxPerf.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setPeriodCobTxPerf(Integer value) {
        this.periodCobTxPerf = value;
    }

    /**
     * Obtém o valor da propriedade plBaseTxPerf.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getPlBaseTxPerf() {
        return plBaseTxPerf;
    }

    /**
     * Define o valor da propriedade plBaseTxPerf.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setPlBaseTxPerf(Integer value) {
        this.plBaseTxPerf = value;
    }

    /**
     * Obtém o valor da propriedade zeraPerf.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getZeraPerf() {
        return zeraPerf;
    }

    /**
     * Define o valor da propriedade zeraPerf.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setZeraPerf(YesNoIndicator value) {
        this.zeraPerf = value;
    }

}
