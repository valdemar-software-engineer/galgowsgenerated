package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Alterações no Perfil
 * <p>
 * <p>Classe Java de AltPerfilComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="AltPerfilComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DtCalcCota" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtComposicao" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtCtgAnbid" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtTpAnbid" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="AltHistTpAnbid" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DtFocoAtua" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtAplicAuto" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtPlanoPrevi" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtClassCvm" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtPrevidenciario" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtCredPrivado" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtInvExterior" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtPublAlvo" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtLimitesEmiss" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtTributPerseg" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtInvQualif" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtAlavancado" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtRestrInvest" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AltPerfilComplexType",
         propOrder = {"dtCalcCota", "dtComposicao", "dtCtgAnbid", "dtTpAnbid", "altHistTpAnbid", "dtFocoAtua", "dtAplicAuto", "dtPlanoPrevi", "dtClassCvm", "dtPrevidenciario", "dtCredPrivado", "dtInvExterior", "dtPublAlvo", "dtLimitesEmiss", "dtTributPerseg", "dtInvQualif", "dtAlavancado", "dtRestrInvest"})
public class AltPerfilComplexType {

    @XmlElement(name = "DtCalcCota") protected XMLGregorianCalendar dtCalcCota;
    @XmlElement(name = "DtComposicao") protected XMLGregorianCalendar dtComposicao;
    @XmlElement(name = "DtCtgAnbid") protected XMLGregorianCalendar dtCtgAnbid;
    @XmlElement(name = "DtTpAnbid") protected XMLGregorianCalendar dtTpAnbid;
    @XmlElement(name = "AltHistTpAnbid") protected YesNoIndicator altHistTpAnbid;
    @XmlElement(name = "DtFocoAtua") protected XMLGregorianCalendar dtFocoAtua;
    @XmlElement(name = "DtAplicAuto") protected XMLGregorianCalendar dtAplicAuto;
    @XmlElement(name = "DtPlanoPrevi") protected XMLGregorianCalendar dtPlanoPrevi;
    @XmlElement(name = "DtClassCvm") protected XMLGregorianCalendar dtClassCvm;
    @XmlElement(name = "DtPrevidenciario") protected XMLGregorianCalendar dtPrevidenciario;
    @XmlElement(name = "DtCredPrivado") protected XMLGregorianCalendar dtCredPrivado;
    @XmlElement(name = "DtInvExterior") protected XMLGregorianCalendar dtInvExterior;
    @XmlElement(name = "DtPublAlvo") protected XMLGregorianCalendar dtPublAlvo;
    @XmlElement(name = "DtLimitesEmiss") protected XMLGregorianCalendar dtLimitesEmiss;
    @XmlElement(name = "DtTributPerseg") protected XMLGregorianCalendar dtTributPerseg;
    @XmlElement(name = "DtInvQualif") protected XMLGregorianCalendar dtInvQualif;
    @XmlElement(name = "DtAlavancado") protected XMLGregorianCalendar dtAlavancado;
    @XmlElement(name = "DtRestrInvest") protected XMLGregorianCalendar dtRestrInvest;

    /**
     * Obtém o valor da propriedade altHistTpAnbid.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getAltHistTpAnbid() {
        return altHistTpAnbid;
    }

    /**
     * Define o valor da propriedade altHistTpAnbid.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setAltHistTpAnbid(YesNoIndicator value) {
        this.altHistTpAnbid = value;
    }

    /**
     * Obtém o valor da propriedade dtAlavancado.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtAlavancado() {
        return dtAlavancado;
    }

    /**
     * Define o valor da propriedade dtAlavancado.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtAlavancado(XMLGregorianCalendar value) {
        this.dtAlavancado = value;
    }

    /**
     * Obtém o valor da propriedade dtAplicAuto.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtAplicAuto() {
        return dtAplicAuto;
    }

    /**
     * Define o valor da propriedade dtAplicAuto.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtAplicAuto(XMLGregorianCalendar value) {
        this.dtAplicAuto = value;
    }

    /**
     * Obtém o valor da propriedade dtCalcCota.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtCalcCota() {
        return dtCalcCota;
    }

    /**
     * Define o valor da propriedade dtCalcCota.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtCalcCota(XMLGregorianCalendar value) {
        this.dtCalcCota = value;
    }

    /**
     * Obtém o valor da propriedade dtClassCvm.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtClassCvm() {
        return dtClassCvm;
    }

    /**
     * Define o valor da propriedade dtClassCvm.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtClassCvm(XMLGregorianCalendar value) {
        this.dtClassCvm = value;
    }

    /**
     * Obtém o valor da propriedade dtComposicao.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtComposicao() {
        return dtComposicao;
    }

    /**
     * Define o valor da propriedade dtComposicao.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtComposicao(XMLGregorianCalendar value) {
        this.dtComposicao = value;
    }

    /**
     * Obtém o valor da propriedade dtCredPrivado.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtCredPrivado() {
        return dtCredPrivado;
    }

    /**
     * Define o valor da propriedade dtCredPrivado.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtCredPrivado(XMLGregorianCalendar value) {
        this.dtCredPrivado = value;
    }

    /**
     * Obtém o valor da propriedade dtCtgAnbid.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtCtgAnbid() {
        return dtCtgAnbid;
    }

    /**
     * Define o valor da propriedade dtCtgAnbid.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtCtgAnbid(XMLGregorianCalendar value) {
        this.dtCtgAnbid = value;
    }

    /**
     * Obtém o valor da propriedade dtFocoAtua.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtFocoAtua() {
        return dtFocoAtua;
    }

    /**
     * Define o valor da propriedade dtFocoAtua.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtFocoAtua(XMLGregorianCalendar value) {
        this.dtFocoAtua = value;
    }

    /**
     * Obtém o valor da propriedade dtInvExterior.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInvExterior() {
        return dtInvExterior;
    }

    /**
     * Define o valor da propriedade dtInvExterior.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInvExterior(XMLGregorianCalendar value) {
        this.dtInvExterior = value;
    }

    /**
     * Obtém o valor da propriedade dtInvQualif.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInvQualif() {
        return dtInvQualif;
    }

    /**
     * Define o valor da propriedade dtInvQualif.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInvQualif(XMLGregorianCalendar value) {
        this.dtInvQualif = value;
    }

    /**
     * Obtém o valor da propriedade dtLimitesEmiss.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtLimitesEmiss() {
        return dtLimitesEmiss;
    }

    /**
     * Define o valor da propriedade dtLimitesEmiss.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtLimitesEmiss(XMLGregorianCalendar value) {
        this.dtLimitesEmiss = value;
    }

    /**
     * Obtém o valor da propriedade dtPlanoPrevi.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtPlanoPrevi() {
        return dtPlanoPrevi;
    }

    /**
     * Define o valor da propriedade dtPlanoPrevi.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtPlanoPrevi(XMLGregorianCalendar value) {
        this.dtPlanoPrevi = value;
    }

    /**
     * Obtém o valor da propriedade dtPrevidenciario.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtPrevidenciario() {
        return dtPrevidenciario;
    }

    /**
     * Define o valor da propriedade dtPrevidenciario.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtPrevidenciario(XMLGregorianCalendar value) {
        this.dtPrevidenciario = value;
    }

    /**
     * Obtém o valor da propriedade dtPublAlvo.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtPublAlvo() {
        return dtPublAlvo;
    }

    /**
     * Define o valor da propriedade dtPublAlvo.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtPublAlvo(XMLGregorianCalendar value) {
        this.dtPublAlvo = value;
    }

    /**
     * Obtém o valor da propriedade dtRestrInvest.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtRestrInvest() {
        return dtRestrInvest;
    }

    /**
     * Define o valor da propriedade dtRestrInvest.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtRestrInvest(XMLGregorianCalendar value) {
        this.dtRestrInvest = value;
    }

    /**
     * Obtém o valor da propriedade dtTpAnbid.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtTpAnbid() {
        return dtTpAnbid;
    }

    /**
     * Define o valor da propriedade dtTpAnbid.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtTpAnbid(XMLGregorianCalendar value) {
        this.dtTpAnbid = value;
    }

    /**
     * Obtém o valor da propriedade dtTributPerseg.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtTributPerseg() {
        return dtTributPerseg;
    }

    /**
     * Define o valor da propriedade dtTributPerseg.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtTributPerseg(XMLGregorianCalendar value) {
        this.dtTributPerseg = value;
    }

}
