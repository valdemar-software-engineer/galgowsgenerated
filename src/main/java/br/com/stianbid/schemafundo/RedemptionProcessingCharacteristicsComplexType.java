package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Redemption Processing Characteristic (Campo ISO não
 * utilizado no momento)
 * <p>
 * <p>
 * <p>Classe Java de RedemptionProcessingCharacteristicsComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="RedemptionProcessingCharacteristicsComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RedAmtVal" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="RedAmtUnits" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DealgCutOffTm" type="{http://www.stianbid.com.br/Common}ISOTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RedemptionProcessingCharacteristicsComplexType",
         propOrder = {"redAmtVal", "redAmtUnits", "dealgCutOffTm"})
public class RedemptionProcessingCharacteristicsComplexType {

    @XmlElement(name = "RedAmtVal") protected YesNoIndicator redAmtVal;
    @XmlElement(name = "RedAmtUnits") protected YesNoIndicator redAmtUnits;
    @XmlElement(name = "DealgCutOffTm") protected XMLGregorianCalendar dealgCutOffTm;

    /**
     * Obtém o valor da propriedade dealgCutOffTm.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDealgCutOffTm() {
        return dealgCutOffTm;
    }

    /**
     * Define o valor da propriedade dealgCutOffTm.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDealgCutOffTm(XMLGregorianCalendar value) {
        this.dealgCutOffTm = value;
    }

    /**
     * Obtém o valor da propriedade redAmtUnits.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getRedAmtUnits() {
        return redAmtUnits;
    }

    /**
     * Define o valor da propriedade redAmtUnits.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setRedAmtUnits(YesNoIndicator value) {
        this.redAmtUnits = value;
    }

    /**
     * Obtém o valor da propriedade redAmtVal.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getRedAmtVal() {
        return redAmtVal;
    }

    /**
     * Define o valor da propriedade redAmtVal.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setRedAmtVal(YesNoIndicator value) {
        this.redAmtVal = value;
    }

}
