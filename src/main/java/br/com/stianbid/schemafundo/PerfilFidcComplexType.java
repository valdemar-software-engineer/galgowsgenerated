package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * Perfil do FIDC
 * <p>
 * <p>Classe Java de PerfilFidcComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PerfilFidcComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TpEmiss" type="{http://www.stianbid.com.br/SchemaFundo}TipoEmissaoFIDCCode"/>
 *         &lt;element name="ClasseCota" type="{http://www.stianbid.com.br/SchemaFundo}ClasseCotaFIDCCode"/>
 *         &lt;element name="SubordTotal" type="{http://www.stianbid.com.br/SchemaFundo}DecimalNumber_7_4" minOccurs="0"/>
 *         &lt;element name="QtdClasseSubord" type="{http://www.stianbid.com.br/SchemaFundo}Number_3" minOccurs="0"/>
 *         &lt;element name="Originador" type="{http://www.stianbid.com.br/SchemaFundo}OriginadorFIDCCode"/>
 *         &lt;element name="Cedente" type="{http://www.stianbid.com.br/SchemaFundo}CedenteFIDCCode"/>
 *         &lt;element name="Sacado" type="{http://www.stianbid.com.br/SchemaFundo}SacadoFIDCCode"/>
 *         &lt;element name="TpDirCredit" type="{http://www.stianbid.com.br/SchemaFundo}TpDirCreditFIDCCode"/>
 *         &lt;element name="UtilizDerivativos" type="{http://www.stianbid.com.br/Common}YesNoIndicator"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PerfilFidcComplexType",
         propOrder = {"tpEmiss", "classeCota", "subordTotal", "qtdClasseSubord", "originador", "cedente", "sacado", "tpDirCredit", "utilizDerivativos"})
public class PerfilFidcComplexType {

    @XmlElement(name = "TpEmiss") protected int tpEmiss;
    @XmlElement(name = "ClasseCota") protected int classeCota;
    @XmlElement(name = "SubordTotal") protected BigDecimal subordTotal;
    @XmlElement(name = "QtdClasseSubord") protected BigDecimal qtdClasseSubord;
    @XmlElement(name = "Originador") protected int originador;
    @XmlElement(name = "Cedente") protected int cedente;
    @XmlElement(name = "Sacado") protected int sacado;
    @XmlElement(name = "TpDirCredit") protected int tpDirCredit;
    @XmlElement(name = "UtilizDerivativos", required = true) protected YesNoIndicator utilizDerivativos;

    /**
     * Obtém o valor da propriedade cedente.
     */
    public int getCedente() {
        return cedente;
    }

    /**
     * Define o valor da propriedade cedente.
     */
    public void setCedente(int value) {
        this.cedente = value;
    }

    /**
     * Obtém o valor da propriedade classeCota.
     */
    public int getClasseCota() {
        return classeCota;
    }

    /**
     * Define o valor da propriedade classeCota.
     */
    public void setClasseCota(int value) {
        this.classeCota = value;
    }

    /**
     * Obtém o valor da propriedade originador.
     */
    public int getOriginador() {
        return originador;
    }

    /**
     * Define o valor da propriedade originador.
     */
    public void setOriginador(int value) {
        this.originador = value;
    }

    /**
     * Obtém o valor da propriedade qtdClasseSubord.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getQtdClasseSubord() {
        return qtdClasseSubord;
    }

    /**
     * Define o valor da propriedade qtdClasseSubord.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setQtdClasseSubord(BigDecimal value) {
        this.qtdClasseSubord = value;
    }

    /**
     * Obtém o valor da propriedade sacado.
     */
    public int getSacado() {
        return sacado;
    }

    /**
     * Define o valor da propriedade sacado.
     */
    public void setSacado(int value) {
        this.sacado = value;
    }

    /**
     * Obtém o valor da propriedade subordTotal.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getSubordTotal() {
        return subordTotal;
    }

    /**
     * Define o valor da propriedade subordTotal.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setSubordTotal(BigDecimal value) {
        this.subordTotal = value;
    }

    /**
     * Obtém o valor da propriedade tpDirCredit.
     */
    public int getTpDirCredit() {
        return tpDirCredit;
    }

    /**
     * Define o valor da propriedade tpDirCredit.
     */
    public void setTpDirCredit(int value) {
        this.tpDirCredit = value;
    }

    /**
     * Obtém o valor da propriedade tpEmiss.
     */
    public int getTpEmiss() {
        return tpEmiss;
    }

    /**
     * Define o valor da propriedade tpEmiss.
     */
    public void setTpEmiss(int value) {
        this.tpEmiss = value;
    }

    /**
     * Obtém o valor da propriedade utilizDerivativos.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getUtilizDerivativos() {
        return utilizDerivativos;
    }

    /**
     * Define o valor da propriedade utilizDerivativos.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setUtilizDerivativos(YesNoIndicator value) {
        this.utilizDerivativos = value;
    }

}
