package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Finalizar Fundo de Investimento
 * <p>
 * <p>
 * <p>Classe Java de FinalizarComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="FinalizarComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="CdSti" type="{http://www.stianbid.com.br/SchemaFundo}STIIdentifier"/>
 *           &lt;element name="NrProtocolo" type="{http://www.stianbid.com.br/SchemaFundo}NumeroProtocolo"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinalizarComplexType", propOrder = {"cdSti", "nrProtocolo"})
public class FinalizarComplexType {

    @XmlElement(name = "CdSti") protected Integer cdSti;
    @XmlElement(name = "NrProtocolo") protected Long nrProtocolo;

    /**
     * Obtém o valor da propriedade cdSti.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCdSti() {
        return cdSti;
    }

    /**
     * Define o valor da propriedade cdSti.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCdSti(Integer value) {
        this.cdSti = value;
    }

    /**
     * Obtém o valor da propriedade nrProtocolo.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getNrProtocolo() {
        return nrProtocolo;
    }

    /**
     * Define o valor da propriedade nrProtocolo.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setNrProtocolo(Long value) {
        this.nrProtocolo = value;
    }

}
