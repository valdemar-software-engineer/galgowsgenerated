package br.com.stianbid.schemafundo;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Atualizacao Fundos Correcao
 * <p>
 * <p>Classe Java de RequestAtualizarFundosComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="RequestAtualizarFundosComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType">
 *       &lt;sequence>
 *         &lt;element name="CdSti" type="{http://www.stianbid.com.br/SchemaFundo}STIIdentifier"/>
 *         &lt;element name="IdAnaliseCorrecao" type="{http://www.stianbid.com.br/SchemaFundo}IdAnaliseCorrecaoSimpleType" minOccurs="0"/>
 *         &lt;element name="FundosCorrecao" type="{http://www.stianbid.com.br/SchemaFundo}MessageAtualizarFundosCorrecaoComplexType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestAtualizarFundosComplexType", propOrder = {"cdSti", "idAnaliseCorrecao", "fundosCorrecao"})
public class RequestAtualizarFundosComplexType extends MessageRequestComplexType {

    @XmlElement(name = "CdSti") protected int cdSti;
    @XmlElement(name = "IdAnaliseCorrecao") protected Integer idAnaliseCorrecao;
    @XmlElement(name = "FundosCorrecao") protected List<MessageAtualizarFundosCorrecaoComplexType> fundosCorrecao;

    /**
     * Obtém o valor da propriedade cdSti.
     */
    public int getCdSti() {
        return cdSti;
    }

    /**
     * Define o valor da propriedade cdSti.
     */
    public void setCdSti(int value) {
        this.cdSti = value;
    }

    /**
     * Gets the value of the fundosCorrecao property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fundosCorrecao property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFundosCorrecao().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MessageAtualizarFundosCorrecaoComplexType }
     */
    public List<MessageAtualizarFundosCorrecaoComplexType> getFundosCorrecao() {
        if (fundosCorrecao == null) {
            fundosCorrecao = new ArrayList<MessageAtualizarFundosCorrecaoComplexType>();
        }
        return this.fundosCorrecao;
    }

    /**
     * Obtém o valor da propriedade idAnaliseCorrecao.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getIdAnaliseCorrecao() {
        return idAnaliseCorrecao;
    }

    /**
     * Define o valor da propriedade idAnaliseCorrecao.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setIdAnaliseCorrecao(Integer value) {
        this.idAnaliseCorrecao = value;
    }

}
