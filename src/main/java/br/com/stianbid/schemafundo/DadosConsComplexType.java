package br.com.stianbid.schemafundo;

import br.com.stianbid.common.MessageRequestComplexType;
import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dados para Consulta de Fundos.
 * <p>
 * <p>
 * <p>Classe Java de DadosConsComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="DadosConsComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType">
 *       &lt;sequence>
 *         &lt;element name="listaIdRegistro" type="{http://www.stianbid.com.br/SchemaFundo}OtherIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="listaNrProtocolo" type="{http://www.stianbid.com.br/SchemaFundo}ListaNrProtocoloComplexType" minOccurs="0"/>
 *         &lt;element name="ConsDatas" type="{http://www.stianbid.com.br/SchemaFundo}CnsDatasComplexType" minOccurs="0"/>
 *         &lt;element name="icHistorico" type="{http://www.stianbid.com.br/Common}YesNoIndicator"/>
 *         &lt;element name="icRegistroAutomatico" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DadosConsComplexType",
         propOrder = {"listaIdRegistro", "listaNrProtocolo", "consDatas", "icHistorico", "icRegistroAutomatico"})
public class DadosConsComplexType extends MessageRequestComplexType {

    protected OtherIdentificationComplexType listaIdRegistro;
    protected ListaNrProtocoloComplexType listaNrProtocolo;
    @XmlElement(name = "ConsDatas") protected CnsDatasComplexType consDatas;
    @XmlElement(required = true) protected YesNoIndicator icHistorico;
    protected YesNoIndicator icRegistroAutomatico;

    /**
     * Obtém o valor da propriedade consDatas.
     *
     * @return possible object is
     * {@link CnsDatasComplexType }
     */
    public CnsDatasComplexType getConsDatas() {
        return consDatas;
    }

    /**
     * Define o valor da propriedade consDatas.
     *
     * @param value allowed object is
     *              {@link CnsDatasComplexType }
     */
    public void setConsDatas(CnsDatasComplexType value) {
        this.consDatas = value;
    }

    /**
     * Obtém o valor da propriedade icHistorico.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getIcHistorico() {
        return icHistorico;
    }

    /**
     * Define o valor da propriedade icHistorico.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setIcHistorico(YesNoIndicator value) {
        this.icHistorico = value;
    }

    /**
     * Obtém o valor da propriedade icRegistroAutomatico.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getIcRegistroAutomatico() {
        return icRegistroAutomatico;
    }

    /**
     * Define o valor da propriedade icRegistroAutomatico.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setIcRegistroAutomatico(YesNoIndicator value) {
        this.icRegistroAutomatico = value;
    }

    /**
     * Obtém o valor da propriedade listaIdRegistro.
     *
     * @return possible object is
     * {@link OtherIdentificationComplexType }
     */
    public OtherIdentificationComplexType getListaIdRegistro() {
        return listaIdRegistro;
    }

    /**
     * Define o valor da propriedade listaIdRegistro.
     *
     * @param value allowed object is
     *              {@link OtherIdentificationComplexType }
     */
    public void setListaIdRegistro(OtherIdentificationComplexType value) {
        this.listaIdRegistro = value;
    }

    /**
     * Obtém o valor da propriedade listaNrProtocolo.
     *
     * @return possible object is
     * {@link ListaNrProtocoloComplexType }
     */
    public ListaNrProtocoloComplexType getListaNrProtocolo() {
        return listaNrProtocolo;
    }

    /**
     * Define o valor da propriedade listaNrProtocolo.
     *
     * @param value allowed object is
     *              {@link ListaNrProtocoloComplexType }
     */
    public void setListaNrProtocolo(ListaNrProtocoloComplexType value) {
        this.listaNrProtocolo = value;
    }

}
