package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dados de Conta - Settlement Details
 * <p>
 * <p>
 * <p>Classe Java de SettlementDetailsComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="SettlementDetailsComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Ccy" type="{http://www.stianbid.com.br/Common}CurrencyCode" minOccurs="0"/>
 *         &lt;element name="AcctSvcr" type="{http://www.stianbid.com.br/SchemaFundo}AccountServicerComplexType" minOccurs="0"/>
 *         &lt;element name="AcctId" type="{http://www.stianbid.com.br/SchemaFundo}AccountIdentificationComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SettlementDetailsComplexType", propOrder = {"ccy", "acctSvcr", "acctId"})
public class SettlementDetailsComplexType {

    @XmlElement(name = "Ccy") protected String ccy;
    @XmlElement(name = "AcctSvcr") protected AccountServicerComplexType acctSvcr;
    @XmlElement(name = "AcctId") protected AccountIdentificationComplexType acctId;

    /**
     * Obtém o valor da propriedade acctId.
     *
     * @return possible object is
     * {@link AccountIdentificationComplexType }
     */
    public AccountIdentificationComplexType getAcctId() {
        return acctId;
    }

    /**
     * Define o valor da propriedade acctId.
     *
     * @param value allowed object is
     *              {@link AccountIdentificationComplexType }
     */
    public void setAcctId(AccountIdentificationComplexType value) {
        this.acctId = value;
    }

    /**
     * Obtém o valor da propriedade acctSvcr.
     *
     * @return possible object is
     * {@link AccountServicerComplexType }
     */
    public AccountServicerComplexType getAcctSvcr() {
        return acctSvcr;
    }

    /**
     * Define o valor da propriedade acctSvcr.
     *
     * @param value allowed object is
     *              {@link AccountServicerComplexType }
     */
    public void setAcctSvcr(AccountServicerComplexType value) {
        this.acctSvcr = value;
    }

    /**
     * Obtém o valor da propriedade ccy.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * Define o valor da propriedade ccy.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCcy(String value) {
        this.ccy = value;
    }

}
