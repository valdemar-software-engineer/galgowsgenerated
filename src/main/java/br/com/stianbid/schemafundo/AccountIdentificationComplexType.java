package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Identificação da Conta
 * <p>
 * <p>Classe Java de AccountIdentificationComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="AccountIdentificationComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="IBAN" type="{http://www.stianbid.com.br/Common}IBANIdentifier" minOccurs="0"/>
 *           &lt;element name="BBAN" type="{http://www.stianbid.com.br/Common}BBANIdentifier" minOccurs="0"/>
 *           &lt;element name="UPIC" type="{http://www.stianbid.com.br/Common}UPICIdentifier" minOccurs="0"/>
 *           &lt;element name="DmstAcct" type="{http://www.stianbid.com.br/SchemaFundo}DomesticAccountComplexType" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountIdentificationComplexType", propOrder = {"iban", "bban", "upic", "dmstAcct"})
public class AccountIdentificationComplexType {

    @XmlElement(name = "IBAN") protected String iban;
    @XmlElement(name = "BBAN") protected String bban;
    @XmlElement(name = "UPIC") protected String upic;
    @XmlElement(name = "DmstAcct") protected DomesticAccountComplexType dmstAcct;

    /**
     * Obtém o valor da propriedade bban.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBBAN() {
        return bban;
    }

    /**
     * Define o valor da propriedade bban.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBBAN(String value) {
        this.bban = value;
    }

    /**
     * Obtém o valor da propriedade dmstAcct.
     *
     * @return possible object is
     * {@link DomesticAccountComplexType }
     */
    public DomesticAccountComplexType getDmstAcct() {
        return dmstAcct;
    }

    /**
     * Define o valor da propriedade dmstAcct.
     *
     * @param value allowed object is
     *              {@link DomesticAccountComplexType }
     */
    public void setDmstAcct(DomesticAccountComplexType value) {
        this.dmstAcct = value;
    }

    /**
     * Obtém o valor da propriedade iban.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIBAN() {
        return iban;
    }

    /**
     * Define o valor da propriedade iban.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIBAN(String value) {
        this.iban = value;
    }

    /**
     * Obtém o valor da propriedade upic.
     *
     * @return possible object is
     * {@link String }
     */
    public String getUPIC() {
        return upic;
    }

    /**
     * Define o valor da propriedade upic.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setUPIC(String value) {
        this.upic = value;
    }

}
