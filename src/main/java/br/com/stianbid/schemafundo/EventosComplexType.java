package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Eventos
 * <p>
 * <p>Classe Java de EventosComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="EventosComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FdoInicEvCorp" type="{http://www.stianbid.com.br/Common}YesNoIndicator"/>
 *         &lt;element name="EvCorp" type="{http://www.stianbid.com.br/SchemaFundo}EventoCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CnpjOrigem" type="{http://www.stianbid.com.br/SchemaFundo}CNPJIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventosComplexType", propOrder = {"fdoInicEvCorp", "evCorp", "cnpjOrigem"})
public class EventosComplexType {

    @XmlElement(name = "FdoInicEvCorp", required = true) protected YesNoIndicator fdoInicEvCorp;
    @XmlElement(name = "EvCorp", type = Integer.class) protected List<Integer> evCorp;
    @XmlElement(name = "CnpjOrigem", type = Long.class) protected List<Long> cnpjOrigem;

    /**
     * Gets the value of the cnpjOrigem property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cnpjOrigem property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCnpjOrigem().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Long }
     */
    public List<Long> getCnpjOrigem() {
        if (cnpjOrigem == null) {
            cnpjOrigem = new ArrayList<Long>();
        }
        return this.cnpjOrigem;
    }

    /**
     * Gets the value of the evCorp property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the evCorp property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEvCorp().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     */
    public List<Integer> getEvCorp() {
        if (evCorp == null) {
            evCorp = new ArrayList<Integer>();
        }
        return this.evCorp;
    }

    /**
     * Obtém o valor da propriedade fdoInicEvCorp.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getFdoInicEvCorp() {
        return fdoInicEvCorp;
    }

    /**
     * Define o valor da propriedade fdoInicEvCorp.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setFdoInicEvCorp(YesNoIndicator value) {
        this.fdoInicEvCorp = value;
    }

}
