package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Application Forms LOD (Campo ISO não utilizado no
 * momento)
 * <p>
 * <p>
 * <p>Classe Java de ApplicationFormsLODType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ApplicationFormsLODType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InitlApplForm" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="SgntrReqrdInitlSbcpt" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="SbsqApplForm" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="SgntrReqrdSubsqSbcpt" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="SgntrReqrdRed" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationFormsLODType",
         propOrder = {"initlApplForm", "sgntrReqrdInitlSbcpt", "sbsqApplForm", "sgntrReqrdSubsqSbcpt", "sgntrReqrdRed"})
public class ApplicationFormsLODType {

    @XmlElement(name = "InitlApplForm") protected YesNoIndicator initlApplForm;
    @XmlElement(name = "SgntrReqrdInitlSbcpt") protected YesNoIndicator sgntrReqrdInitlSbcpt;
    @XmlElement(name = "SbsqApplForm") protected YesNoIndicator sbsqApplForm;
    @XmlElement(name = "SgntrReqrdSubsqSbcpt") protected YesNoIndicator sgntrReqrdSubsqSbcpt;
    @XmlElement(name = "SgntrReqrdRed") protected YesNoIndicator sgntrReqrdRed;

    /**
     * Obtém o valor da propriedade initlApplForm.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getInitlApplForm() {
        return initlApplForm;
    }

    /**
     * Define o valor da propriedade initlApplForm.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setInitlApplForm(YesNoIndicator value) {
        this.initlApplForm = value;
    }

    /**
     * Obtém o valor da propriedade sbsqApplForm.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getSbsqApplForm() {
        return sbsqApplForm;
    }

    /**
     * Define o valor da propriedade sbsqApplForm.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setSbsqApplForm(YesNoIndicator value) {
        this.sbsqApplForm = value;
    }

    /**
     * Obtém o valor da propriedade sgntrReqrdInitlSbcpt.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getSgntrReqrdInitlSbcpt() {
        return sgntrReqrdInitlSbcpt;
    }

    /**
     * Define o valor da propriedade sgntrReqrdInitlSbcpt.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setSgntrReqrdInitlSbcpt(YesNoIndicator value) {
        this.sgntrReqrdInitlSbcpt = value;
    }

    /**
     * Obtém o valor da propriedade sgntrReqrdRed.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getSgntrReqrdRed() {
        return sgntrReqrdRed;
    }

    /**
     * Define o valor da propriedade sgntrReqrdRed.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setSgntrReqrdRed(YesNoIndicator value) {
        this.sgntrReqrdRed = value;
    }

    /**
     * Obtém o valor da propriedade sgntrReqrdSubsqSbcpt.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getSgntrReqrdSubsqSbcpt() {
        return sgntrReqrdSubsqSbcpt;
    }

    /**
     * Define o valor da propriedade sgntrReqrdSubsqSbcpt.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setSgntrReqrdSubsqSbcpt(YesNoIndicator value) {
        this.sgntrReqrdSubsqSbcpt = value;
    }

}
