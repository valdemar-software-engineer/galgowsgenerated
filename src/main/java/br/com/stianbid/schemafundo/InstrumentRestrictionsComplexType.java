package br.com.stianbid.schemafundo;

import br.com.stianbid.common.ActiveCurrencyAndAmount;
import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * Instrument Restrictions
 * <p>
 * <p>Classe Java de InstrumentRestrictionsComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="InstrumentRestrictionsComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PHYSBEAR" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DmtrlsdBEAR" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="PHYSRegd" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DmtrlsdRegd" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="MaxRedAmt" type="{http://www.stianbid.com.br/Common}ActiveCurrencyAndAmount" minOccurs="0"/>
 *         &lt;element name="MaxRedUnits" type="{http://www.stianbid.com.br/Common}Number" minOccurs="0"/>
 *         &lt;element name="OthrRedRstrctns" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/>
 *         &lt;element name="MinHldgAmt" type="{http://www.stianbid.com.br/Common}ActiveCurrencyAndAmount" minOccurs="0"/>
 *         &lt;element name="MinHldgUnits" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="18"/>
 *               &lt;fractionDigits value="17"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MinHldgPrd" type="{http://www.stianbid.com.br/Common}Max70Text" minOccurs="0"/>
 *         &lt;element name="HldgTrfblInd" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InstrumentRestrictionsComplexType",
         propOrder = {"physbear", "dmtrlsdBEAR", "physRegd", "dmtrlsdRegd", "maxRedAmt", "maxRedUnits", "othrRedRstrctns", "minHldgAmt", "minHldgUnits", "minHldgPrd", "hldgTrfblInd"})
public class InstrumentRestrictionsComplexType {

    @XmlElement(name = "PHYSBEAR") protected YesNoIndicator physbear;
    @XmlElement(name = "DmtrlsdBEAR") protected YesNoIndicator dmtrlsdBEAR;
    @XmlElement(name = "PHYSRegd") protected YesNoIndicator physRegd;
    @XmlElement(name = "DmtrlsdRegd") protected YesNoIndicator dmtrlsdRegd;
    @XmlElement(name = "MaxRedAmt") protected ActiveCurrencyAndAmount maxRedAmt;
    @XmlElement(name = "MaxRedUnits") protected BigDecimal maxRedUnits;
    @XmlElement(name = "OthrRedRstrctns") protected String othrRedRstrctns;
    @XmlElement(name = "MinHldgAmt") protected ActiveCurrencyAndAmount minHldgAmt;
    @XmlElement(name = "MinHldgUnits") protected BigDecimal minHldgUnits;
    @XmlElement(name = "MinHldgPrd") protected String minHldgPrd;
    @XmlElement(name = "HldgTrfblInd") protected YesNoIndicator hldgTrfblInd;

    /**
     * Obtém o valor da propriedade dmtrlsdBEAR.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getDmtrlsdBEAR() {
        return dmtrlsdBEAR;
    }

    /**
     * Define o valor da propriedade dmtrlsdBEAR.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setDmtrlsdBEAR(YesNoIndicator value) {
        this.dmtrlsdBEAR = value;
    }

    /**
     * Obtém o valor da propriedade dmtrlsdRegd.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getDmtrlsdRegd() {
        return dmtrlsdRegd;
    }

    /**
     * Define o valor da propriedade dmtrlsdRegd.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setDmtrlsdRegd(YesNoIndicator value) {
        this.dmtrlsdRegd = value;
    }

    /**
     * Obtém o valor da propriedade hldgTrfblInd.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getHldgTrfblInd() {
        return hldgTrfblInd;
    }

    /**
     * Define o valor da propriedade hldgTrfblInd.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setHldgTrfblInd(YesNoIndicator value) {
        this.hldgTrfblInd = value;
    }

    /**
     * Obtém o valor da propriedade maxRedAmt.
     *
     * @return possible object is
     * {@link ActiveCurrencyAndAmount }
     */
    public ActiveCurrencyAndAmount getMaxRedAmt() {
        return maxRedAmt;
    }

    /**
     * Define o valor da propriedade maxRedAmt.
     *
     * @param value allowed object is
     *              {@link ActiveCurrencyAndAmount }
     */
    public void setMaxRedAmt(ActiveCurrencyAndAmount value) {
        this.maxRedAmt = value;
    }

    /**
     * Obtém o valor da propriedade maxRedUnits.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getMaxRedUnits() {
        return maxRedUnits;
    }

    /**
     * Define o valor da propriedade maxRedUnits.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setMaxRedUnits(BigDecimal value) {
        this.maxRedUnits = value;
    }

    /**
     * Obtém o valor da propriedade minHldgAmt.
     *
     * @return possible object is
     * {@link ActiveCurrencyAndAmount }
     */
    public ActiveCurrencyAndAmount getMinHldgAmt() {
        return minHldgAmt;
    }

    /**
     * Define o valor da propriedade minHldgAmt.
     *
     * @param value allowed object is
     *              {@link ActiveCurrencyAndAmount }
     */
    public void setMinHldgAmt(ActiveCurrencyAndAmount value) {
        this.minHldgAmt = value;
    }

    /**
     * Obtém o valor da propriedade minHldgPrd.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMinHldgPrd() {
        return minHldgPrd;
    }

    /**
     * Define o valor da propriedade minHldgPrd.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMinHldgPrd(String value) {
        this.minHldgPrd = value;
    }

    /**
     * Obtém o valor da propriedade minHldgUnits.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getMinHldgUnits() {
        return minHldgUnits;
    }

    /**
     * Define o valor da propriedade minHldgUnits.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setMinHldgUnits(BigDecimal value) {
        this.minHldgUnits = value;
    }

    /**
     * Obtém o valor da propriedade othrRedRstrctns.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOthrRedRstrctns() {
        return othrRedRstrctns;
    }

    /**
     * Define o valor da propriedade othrRedRstrctns.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setOthrRedRstrctns(String value) {
        this.othrRedRstrctns = value;
    }

    /**
     * Obtém o valor da propriedade physbear.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getPHYSBEAR() {
        return physbear;
    }

    /**
     * Define o valor da propriedade physbear.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setPHYSBEAR(YesNoIndicator value) {
        this.physbear = value;
    }

    /**
     * Obtém o valor da propriedade physRegd.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getPHYSRegd() {
        return physRegd;
    }

    /**
     * Define o valor da propriedade physRegd.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setPHYSRegd(YesNoIndicator value) {
        this.physRegd = value;
    }

}
