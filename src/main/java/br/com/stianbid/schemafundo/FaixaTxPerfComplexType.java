package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Faixas para Taxa de Performance
 * <p>
 * <p>
 * <p>Classe Java de FaixaTxPerfComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="FaixaTxPerfComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IndCompAltTx" type="{http://www.stianbid.com.br/SchemaFundo}IndEOUType" minOccurs="0"/>
 *         &lt;element name="PercTxPerf" type="{http://www.stianbid.com.br/SchemaFundo}DecimalNumber_7_4" minOccurs="0"/>
 *         &lt;element name="IndexRef" type="{http://www.stianbid.com.br/SchemaFundo}IndexRefComplexType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaixaTxPerfComplexType", propOrder = {"indCompAltTx", "percTxPerf", "indexRef"})
public class FaixaTxPerfComplexType {

    @XmlElement(name = "IndCompAltTx") protected IndEOUType indCompAltTx;
    @XmlElement(name = "PercTxPerf") protected BigDecimal percTxPerf;
    @XmlElement(name = "IndexRef", required = true) protected List<IndexRefComplexType> indexRef;

    /**
     * Obtém o valor da propriedade indCompAltTx.
     *
     * @return possible object is
     * {@link IndEOUType }
     */
    public IndEOUType getIndCompAltTx() {
        return indCompAltTx;
    }

    /**
     * Define o valor da propriedade indCompAltTx.
     *
     * @param value allowed object is
     *              {@link IndEOUType }
     */
    public void setIndCompAltTx(IndEOUType value) {
        this.indCompAltTx = value;
    }

    /**
     * Gets the value of the indexRef property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the indexRef property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIndexRef().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IndexRefComplexType }
     */
    public List<IndexRefComplexType> getIndexRef() {
        if (indexRef == null) {
            indexRef = new ArrayList<IndexRefComplexType>();
        }
        return this.indexRef;
    }

    /**
     * Obtém o valor da propriedade percTxPerf.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getPercTxPerf() {
        return percTxPerf;
    }

    /**
     * Define o valor da propriedade percTxPerf.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setPercTxPerf(BigDecimal value) {
        this.percTxPerf = value;
    }

}
