package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * Dados da Série
 * <p>
 * <p>Classe Java de DadosSerieFIDCComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="DadosSerieFIDCComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CdStiSerie" type="{http://www.stianbid.com.br/SchemaFundo}STIIdentifier" minOccurs="0"/>
 *         &lt;element name="NumSerie" type="{http://www.stianbid.com.br/SchemaFundo}Number_3"/>
 *         &lt;element name="NmFantSerieImp" type="{http://www.stianbid.com.br/SchemaFundo}NomeFantasia" minOccurs="0"/>
 *         &lt;element name="Isin" type="{http://www.stianbid.com.br/Common}Max16Text" minOccurs="0"/>
 *         &lt;element name="DurSerie" type="{http://www.stianbid.com.br/SchemaFundo}DuracaoFundoCode"/>
 *         &lt;element name="UnidDuracaoSerie" type="{http://www.stianbid.com.br/SchemaFundo}UnidadeDuracaoFundoCode" minOccurs="0"/>
 *         &lt;element name="PrzDuracaoSerie" type="{http://www.stianbid.com.br/SchemaFundo}Number_3" minOccurs="0"/>
 *         &lt;element name="BaseDuracSerie" type="{http://www.stianbid.com.br/SchemaFundo}BaseDuracaoFdoCode"/>
 *         &lt;element name="PossuiRentAlvo" type="{http://www.stianbid.com.br/Common}YesNoIndicator"/>
 *         &lt;element name="RentAlvo" type="{http://www.stianbid.com.br/SchemaFundo}DecimalNumber_7_4" minOccurs="0"/>
 *         &lt;element name="IndexRentAlvo" type="{http://www.stianbid.com.br/SchemaFundo}IndexCorrTxCode" minOccurs="0"/>
 *         &lt;element name="CupomRentAlvo" type="{http://www.stianbid.com.br/Common}Max150Text" minOccurs="0"/>
 *         &lt;element name="Bookbuilding" type="{http://www.stianbid.com.br/Common}YesNoIndicator"/>
 *         &lt;element name="VlEmissao" type="{http://www.stianbid.com.br/SchemaFundo}DecimalNumber_15_2"/>
 *         &lt;element name="QtdCotaSenior" type="{http://www.stianbid.com.br/SchemaFundo}Number_10" minOccurs="0"/>
 *         &lt;element name="QtdCotaUnica" type="{http://www.stianbid.com.br/SchemaFundo}Number_10" minOccurs="0"/>
 *         &lt;element name="QtdDistrMinCotaSenior" type="{http://www.stianbid.com.br/SchemaFundo}Number_10" minOccurs="0"/>
 *         &lt;element name="CarenciaInicial" type="{http://www.stianbid.com.br/SchemaFundo}Number_5" minOccurs="0"/>
 *         &lt;element name="PossuiMecAmort" type="{http://www.stianbid.com.br/Common}YesNoIndicator"/>
 *         &lt;element name="DtPrimAmort" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="PeriodAmort" type="{http://www.stianbid.com.br/SchemaFundo}PeriodAmortCode" minOccurs="0"/>
 *         &lt;element name="DtBaseAmort" type="{http://www.stianbid.com.br/SchemaFundo}DtBaseAmortCode" minOccurs="0"/>
 *         &lt;element name="DiaAmort" type="{http://www.stianbid.com.br/SchemaFundo}Number_2" minOccurs="0"/>
 *         &lt;element name="ObsAmort" type="{http://www.stianbid.com.br/Common}Max500Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DadosSerieFIDCComplexType",
         propOrder = {"cdStiSerie", "numSerie", "nmFantSerieImp", "isin", "durSerie", "unidDuracaoSerie", "przDuracaoSerie", "baseDuracSerie", "possuiRentAlvo", "rentAlvo", "indexRentAlvo", "cupomRentAlvo", "bookbuilding", "vlEmissao", "qtdCotaSenior", "qtdCotaUnica", "qtdDistrMinCotaSenior", "carenciaInicial", "possuiMecAmort", "dtPrimAmort", "periodAmort", "dtBaseAmort", "diaAmort", "obsAmort"})
public class DadosSerieFIDCComplexType {

    @XmlElement(name = "CdStiSerie") protected Integer cdStiSerie;
    @XmlElement(name = "NumSerie", required = true) protected BigDecimal numSerie;
    @XmlElement(name = "NmFantSerieImp") protected String nmFantSerieImp;
    @XmlElement(name = "Isin") protected String isin;
    @XmlElement(name = "DurSerie") protected int durSerie;
    @XmlElement(name = "UnidDuracaoSerie") protected Integer unidDuracaoSerie;
    @XmlElement(name = "PrzDuracaoSerie") protected BigDecimal przDuracaoSerie;
    @XmlElement(name = "BaseDuracSerie") protected int baseDuracSerie;
    @XmlElement(name = "PossuiRentAlvo", required = true) protected YesNoIndicator possuiRentAlvo;
    @XmlElement(name = "RentAlvo") protected BigDecimal rentAlvo;
    @XmlElement(name = "IndexRentAlvo") protected Integer indexRentAlvo;
    @XmlElement(name = "CupomRentAlvo") protected String cupomRentAlvo;
    @XmlElement(name = "Bookbuilding", required = true) protected YesNoIndicator bookbuilding;
    @XmlElement(name = "VlEmissao", required = true) protected BigDecimal vlEmissao;
    @XmlElement(name = "QtdCotaSenior") protected BigDecimal qtdCotaSenior;
    @XmlElement(name = "QtdCotaUnica") protected BigDecimal qtdCotaUnica;
    @XmlElement(name = "QtdDistrMinCotaSenior") protected BigDecimal qtdDistrMinCotaSenior;
    @XmlElement(name = "CarenciaInicial") protected BigDecimal carenciaInicial;
    @XmlElement(name = "PossuiMecAmort", required = true) protected YesNoIndicator possuiMecAmort;
    @XmlElement(name = "DtPrimAmort") protected XMLGregorianCalendar dtPrimAmort;
    @XmlElement(name = "PeriodAmort") protected Integer periodAmort;
    @XmlElement(name = "DtBaseAmort") protected Integer dtBaseAmort;
    @XmlElement(name = "DiaAmort") protected BigDecimal diaAmort;
    @XmlElement(name = "ObsAmort") protected String obsAmort;

    /**
     * Obtém o valor da propriedade baseDuracSerie.
     */
    public int getBaseDuracSerie() {
        return baseDuracSerie;
    }

    /**
     * Define o valor da propriedade baseDuracSerie.
     */
    public void setBaseDuracSerie(int value) {
        this.baseDuracSerie = value;
    }

    /**
     * Obtém o valor da propriedade bookbuilding.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getBookbuilding() {
        return bookbuilding;
    }

    /**
     * Define o valor da propriedade bookbuilding.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setBookbuilding(YesNoIndicator value) {
        this.bookbuilding = value;
    }

    /**
     * Obtém o valor da propriedade carenciaInicial.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getCarenciaInicial() {
        return carenciaInicial;
    }

    /**
     * Define o valor da propriedade carenciaInicial.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setCarenciaInicial(BigDecimal value) {
        this.carenciaInicial = value;
    }

    /**
     * Obtém o valor da propriedade cdStiSerie.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCdStiSerie() {
        return cdStiSerie;
    }

    /**
     * Define o valor da propriedade cdStiSerie.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCdStiSerie(Integer value) {
        this.cdStiSerie = value;
    }

    /**
     * Obtém o valor da propriedade cupomRentAlvo.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCupomRentAlvo() {
        return cupomRentAlvo;
    }

    /**
     * Define o valor da propriedade cupomRentAlvo.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCupomRentAlvo(String value) {
        this.cupomRentAlvo = value;
    }

    /**
     * Obtém o valor da propriedade diaAmort.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getDiaAmort() {
        return diaAmort;
    }

    /**
     * Define o valor da propriedade diaAmort.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setDiaAmort(BigDecimal value) {
        this.diaAmort = value;
    }

    /**
     * Obtém o valor da propriedade dtBaseAmort.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getDtBaseAmort() {
        return dtBaseAmort;
    }

    /**
     * Define o valor da propriedade dtBaseAmort.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setDtBaseAmort(Integer value) {
        this.dtBaseAmort = value;
    }

    /**
     * Obtém o valor da propriedade dtPrimAmort.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtPrimAmort() {
        return dtPrimAmort;
    }

    /**
     * Define o valor da propriedade dtPrimAmort.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtPrimAmort(XMLGregorianCalendar value) {
        this.dtPrimAmort = value;
    }

    /**
     * Obtém o valor da propriedade durSerie.
     */
    public int getDurSerie() {
        return durSerie;
    }

    /**
     * Define o valor da propriedade durSerie.
     */
    public void setDurSerie(int value) {
        this.durSerie = value;
    }

    /**
     * Obtém o valor da propriedade indexRentAlvo.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getIndexRentAlvo() {
        return indexRentAlvo;
    }

    /**
     * Define o valor da propriedade indexRentAlvo.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setIndexRentAlvo(Integer value) {
        this.indexRentAlvo = value;
    }

    /**
     * Obtém o valor da propriedade isin.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIsin() {
        return isin;
    }

    /**
     * Define o valor da propriedade isin.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIsin(String value) {
        this.isin = value;
    }

    /**
     * Obtém o valor da propriedade nmFantSerieImp.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNmFantSerieImp() {
        return nmFantSerieImp;
    }

    /**
     * Define o valor da propriedade nmFantSerieImp.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNmFantSerieImp(String value) {
        this.nmFantSerieImp = value;
    }

    /**
     * Obtém o valor da propriedade numSerie.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getNumSerie() {
        return numSerie;
    }

    /**
     * Define o valor da propriedade numSerie.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setNumSerie(BigDecimal value) {
        this.numSerie = value;
    }

    /**
     * Obtém o valor da propriedade obsAmort.
     *
     * @return possible object is
     * {@link String }
     */
    public String getObsAmort() {
        return obsAmort;
    }

    /**
     * Define o valor da propriedade obsAmort.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setObsAmort(String value) {
        this.obsAmort = value;
    }

    /**
     * Obtém o valor da propriedade periodAmort.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getPeriodAmort() {
        return periodAmort;
    }

    /**
     * Define o valor da propriedade periodAmort.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setPeriodAmort(Integer value) {
        this.periodAmort = value;
    }

    /**
     * Obtém o valor da propriedade possuiMecAmort.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getPossuiMecAmort() {
        return possuiMecAmort;
    }

    /**
     * Define o valor da propriedade possuiMecAmort.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setPossuiMecAmort(YesNoIndicator value) {
        this.possuiMecAmort = value;
    }

    /**
     * Obtém o valor da propriedade possuiRentAlvo.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getPossuiRentAlvo() {
        return possuiRentAlvo;
    }

    /**
     * Define o valor da propriedade possuiRentAlvo.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setPossuiRentAlvo(YesNoIndicator value) {
        this.possuiRentAlvo = value;
    }

    /**
     * Obtém o valor da propriedade przDuracaoSerie.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getPrzDuracaoSerie() {
        return przDuracaoSerie;
    }

    /**
     * Define o valor da propriedade przDuracaoSerie.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setPrzDuracaoSerie(BigDecimal value) {
        this.przDuracaoSerie = value;
    }

    /**
     * Obtém o valor da propriedade qtdCotaSenior.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getQtdCotaSenior() {
        return qtdCotaSenior;
    }

    /**
     * Define o valor da propriedade qtdCotaSenior.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setQtdCotaSenior(BigDecimal value) {
        this.qtdCotaSenior = value;
    }

    /**
     * Obtém o valor da propriedade qtdCotaUnica.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getQtdCotaUnica() {
        return qtdCotaUnica;
    }

    /**
     * Define o valor da propriedade qtdCotaUnica.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setQtdCotaUnica(BigDecimal value) {
        this.qtdCotaUnica = value;
    }

    /**
     * Obtém o valor da propriedade qtdDistrMinCotaSenior.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getQtdDistrMinCotaSenior() {
        return qtdDistrMinCotaSenior;
    }

    /**
     * Define o valor da propriedade qtdDistrMinCotaSenior.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setQtdDistrMinCotaSenior(BigDecimal value) {
        this.qtdDistrMinCotaSenior = value;
    }

    /**
     * Obtém o valor da propriedade rentAlvo.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getRentAlvo() {
        return rentAlvo;
    }

    /**
     * Define o valor da propriedade rentAlvo.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setRentAlvo(BigDecimal value) {
        this.rentAlvo = value;
    }

    /**
     * Obtém o valor da propriedade unidDuracaoSerie.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getUnidDuracaoSerie() {
        return unidDuracaoSerie;
    }

    /**
     * Define o valor da propriedade unidDuracaoSerie.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setUnidDuracaoSerie(Integer value) {
        this.unidDuracaoSerie = value;
    }

    /**
     * Obtém o valor da propriedade vlEmissao.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getVlEmissao() {
        return vlEmissao;
    }

    /**
     * Define o valor da propriedade vlEmissao.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setVlEmissao(BigDecimal value) {
        this.vlEmissao = value;
    }

}
