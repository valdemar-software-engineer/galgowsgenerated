package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * Parâmetros de Movimentação
 * <p>
 * <p>Classe Java de ParamMovimentacaoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ParamMovimentacaoComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EstabValMin" type="{http://www.stianbid.com.br/Common}YesNoIndicator"/>
 *         &lt;element name="CritPrazoConverCotas" type="{http://www.stianbid.com.br/SchemaFundo}CritPrazoCode"/>
 *         &lt;element name="CritPrazoConverResgate" type="{http://www.stianbid.com.br/SchemaFundo}CritPrazoCode"/>
 *         &lt;element name="CritPrazoPagtoResgate" type="{http://www.stianbid.com.br/SchemaFundo}CritPrazoCode"/>
 *         &lt;element name="PrazoApliNPadr" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DescPrazoApliNPadr" type="{http://www.stianbid.com.br/Common}Max500Text" minOccurs="0"/>
 *         &lt;element name="AbertoCap" type="{http://www.stianbid.com.br/Common}YesNoIndicator"/>
 *         &lt;element name="DtAbertCap" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="PrazoResgNPadr" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DescPrazoResgNPadr" type="{http://www.stianbid.com.br/Common}Max500Text" minOccurs="0"/>
 *         &lt;element name="PrazoConvResg" type="{http://www.stianbid.com.br/SchemaFundo}Number_3"/>
 *         &lt;element name="PrazoPagResg" type="{http://www.stianbid.com.br/SchemaFundo}Number_3"/>
 *         &lt;element name="CarenciaCicl" type="{http://www.stianbid.com.br/SchemaFundo}Number_3" minOccurs="0"/>
 *         &lt;element name="SaldoMin" type="{http://www.stianbid.com.br/SchemaFundo}DecimalNumber_15_2" minOccurs="0"/>
 *         &lt;element name="PossRegraPgtoResg" type="{http://www.stianbid.com.br/Common}YesNoIndicator"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParamMovimentacaoComplexType",
         propOrder = {"estabValMin", "critPrazoConverCotas", "critPrazoConverResgate", "critPrazoPagtoResgate", "prazoApliNPadr", "descPrazoApliNPadr", "abertoCap", "dtAbertCap", "prazoResgNPadr", "descPrazoResgNPadr", "prazoConvResg", "prazoPagResg", "carenciaCicl", "saldoMin", "possRegraPgtoResg"})
public class ParamMovimentacaoComplexType {

    @XmlElement(name = "EstabValMin", required = true) protected YesNoIndicator estabValMin;
    @XmlElement(name = "CritPrazoConverCotas") protected int critPrazoConverCotas;
    @XmlElement(name = "CritPrazoConverResgate") protected int critPrazoConverResgate;
    @XmlElement(name = "CritPrazoPagtoResgate") protected int critPrazoPagtoResgate;
    @XmlElement(name = "PrazoApliNPadr") protected YesNoIndicator prazoApliNPadr;
    @XmlElement(name = "DescPrazoApliNPadr") protected String descPrazoApliNPadr;
    @XmlElement(name = "AbertoCap", required = true) protected YesNoIndicator abertoCap;
    @XmlElement(name = "DtAbertCap") protected XMLGregorianCalendar dtAbertCap;
    @XmlElement(name = "PrazoResgNPadr") protected YesNoIndicator prazoResgNPadr;
    @XmlElement(name = "DescPrazoResgNPadr") protected String descPrazoResgNPadr;
    @XmlElement(name = "PrazoConvResg", required = true) protected BigDecimal prazoConvResg;
    @XmlElement(name = "PrazoPagResg", required = true) protected BigDecimal prazoPagResg;
    @XmlElement(name = "CarenciaCicl") protected BigDecimal carenciaCicl;
    @XmlElement(name = "SaldoMin") protected BigDecimal saldoMin;
    @XmlElement(name = "PossRegraPgtoResg", required = true) protected YesNoIndicator possRegraPgtoResg;

    /**
     * Obtém o valor da propriedade abertoCap.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getAbertoCap() {
        return abertoCap;
    }

    /**
     * Define o valor da propriedade abertoCap.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setAbertoCap(YesNoIndicator value) {
        this.abertoCap = value;
    }

    /**
     * Obtém o valor da propriedade carenciaCicl.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getCarenciaCicl() {
        return carenciaCicl;
    }

    /**
     * Define o valor da propriedade carenciaCicl.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setCarenciaCicl(BigDecimal value) {
        this.carenciaCicl = value;
    }

    /**
     * Obtém o valor da propriedade critPrazoConverCotas.
     */
    public int getCritPrazoConverCotas() {
        return critPrazoConverCotas;
    }

    /**
     * Define o valor da propriedade critPrazoConverCotas.
     */
    public void setCritPrazoConverCotas(int value) {
        this.critPrazoConverCotas = value;
    }

    /**
     * Obtém o valor da propriedade critPrazoConverResgate.
     */
    public int getCritPrazoConverResgate() {
        return critPrazoConverResgate;
    }

    /**
     * Define o valor da propriedade critPrazoConverResgate.
     */
    public void setCritPrazoConverResgate(int value) {
        this.critPrazoConverResgate = value;
    }

    /**
     * Obtém o valor da propriedade critPrazoPagtoResgate.
     */
    public int getCritPrazoPagtoResgate() {
        return critPrazoPagtoResgate;
    }

    /**
     * Define o valor da propriedade critPrazoPagtoResgate.
     */
    public void setCritPrazoPagtoResgate(int value) {
        this.critPrazoPagtoResgate = value;
    }

    /**
     * Obtém o valor da propriedade descPrazoApliNPadr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDescPrazoApliNPadr() {
        return descPrazoApliNPadr;
    }

    /**
     * Define o valor da propriedade descPrazoApliNPadr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDescPrazoApliNPadr(String value) {
        this.descPrazoApliNPadr = value;
    }

    /**
     * Obtém o valor da propriedade descPrazoResgNPadr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDescPrazoResgNPadr() {
        return descPrazoResgNPadr;
    }

    /**
     * Define o valor da propriedade descPrazoResgNPadr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDescPrazoResgNPadr(String value) {
        this.descPrazoResgNPadr = value;
    }

    /**
     * Obtém o valor da propriedade dtAbertCap.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtAbertCap() {
        return dtAbertCap;
    }

    /**
     * Define o valor da propriedade dtAbertCap.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtAbertCap(XMLGregorianCalendar value) {
        this.dtAbertCap = value;
    }

    /**
     * Obtém o valor da propriedade estabValMin.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getEstabValMin() {
        return estabValMin;
    }

    /**
     * Define o valor da propriedade estabValMin.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setEstabValMin(YesNoIndicator value) {
        this.estabValMin = value;
    }

    /**
     * Obtém o valor da propriedade possRegraPgtoResg.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getPossRegraPgtoResg() {
        return possRegraPgtoResg;
    }

    /**
     * Define o valor da propriedade possRegraPgtoResg.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setPossRegraPgtoResg(YesNoIndicator value) {
        this.possRegraPgtoResg = value;
    }

    /**
     * Obtém o valor da propriedade prazoApliNPadr.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getPrazoApliNPadr() {
        return prazoApliNPadr;
    }

    /**
     * Define o valor da propriedade prazoApliNPadr.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setPrazoApliNPadr(YesNoIndicator value) {
        this.prazoApliNPadr = value;
    }

    /**
     * Obtém o valor da propriedade prazoConvResg.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getPrazoConvResg() {
        return prazoConvResg;
    }

    /**
     * Define o valor da propriedade prazoConvResg.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setPrazoConvResg(BigDecimal value) {
        this.prazoConvResg = value;
    }

    /**
     * Obtém o valor da propriedade prazoPagResg.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getPrazoPagResg() {
        return prazoPagResg;
    }

    /**
     * Define o valor da propriedade prazoPagResg.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setPrazoPagResg(BigDecimal value) {
        this.prazoPagResg = value;
    }

    /**
     * Obtém o valor da propriedade prazoResgNPadr.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getPrazoResgNPadr() {
        return prazoResgNPadr;
    }

    /**
     * Define o valor da propriedade prazoResgNPadr.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setPrazoResgNPadr(YesNoIndicator value) {
        this.prazoResgNPadr = value;
    }

    /**
     * Obtém o valor da propriedade saldoMin.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getSaldoMin() {
        return saldoMin;
    }

    /**
     * Define o valor da propriedade saldoMin.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setSaldoMin(BigDecimal value) {
        this.saldoMin = value;
    }

}
