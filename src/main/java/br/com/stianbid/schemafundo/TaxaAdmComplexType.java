package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Taxa de Administração
 * <p>
 * <p>Classe Java de TaxaAdmComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="TaxaAdmComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PerfilTxAdm" type="{http://www.stianbid.com.br/SchemaFundo}PerfilTxAdmCode"/>
 *         &lt;element name="UnidTxAdm" type="{http://www.stianbid.com.br/SchemaFundo}UnidTxAdmCode"/>
 *         &lt;element name="VlTxAdm" type="{http://www.stianbid.com.br/SchemaFundo}DecimalNumber_13_4" minOccurs="0"/>
 *         &lt;element name="PlBaseCalcTx" type="{http://www.stianbid.com.br/SchemaFundo}PlBaseCalcTxCode"/>
 *         &lt;element name="VlPisoTxAdm" type="{http://www.stianbid.com.br/SchemaFundo}DecimalNumber_15_2" minOccurs="0"/>
 *         &lt;element name="IndexCorrTx" type="{http://www.stianbid.com.br/SchemaFundo}IndexCorrTxCode" minOccurs="0"/>
 *         &lt;element name="PerCorrTx" type="{http://www.stianbid.com.br/SchemaFundo}PeriodicidadeTxCode" minOccurs="0"/>
 *         &lt;element name="AplicOutrosFds" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="TxMax" type="{http://www.stianbid.com.br/SchemaFundo}DecimalNumber_7_4" minOccurs="0"/>
 *         &lt;element name="FaixaTxEscalCasc" type="{http://www.stianbid.com.br/SchemaFundo}FaixaTxEscalCascComplexType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PeriodCobrTx" type="{http://www.stianbid.com.br/SchemaFundo}PeriodCobrTxCode" minOccurs="0"/>
 *         &lt;element name="InfoTxNPadr" type="{http://www.stianbid.com.br/Common}Max500Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxaAdmComplexType",
         propOrder = {"perfilTxAdm", "unidTxAdm", "vlTxAdm", "plBaseCalcTx", "vlPisoTxAdm", "indexCorrTx", "perCorrTx", "aplicOutrosFds", "txMax", "faixaTxEscalCasc", "periodCobrTx", "infoTxNPadr"})
public class TaxaAdmComplexType {

    @XmlElement(name = "PerfilTxAdm") protected int perfilTxAdm;
    @XmlElement(name = "UnidTxAdm") protected int unidTxAdm;
    @XmlElement(name = "VlTxAdm") protected BigDecimal vlTxAdm;
    @XmlElement(name = "PlBaseCalcTx") protected int plBaseCalcTx;
    @XmlElement(name = "VlPisoTxAdm") protected BigDecimal vlPisoTxAdm;
    @XmlElement(name = "IndexCorrTx") protected Integer indexCorrTx;
    @XmlElement(name = "PerCorrTx") protected Integer perCorrTx;
    @XmlElement(name = "AplicOutrosFds") protected YesNoIndicator aplicOutrosFds;
    @XmlElement(name = "TxMax") protected BigDecimal txMax;
    @XmlElement(name = "FaixaTxEscalCasc") protected List<FaixaTxEscalCascComplexType> faixaTxEscalCasc;
    @XmlElement(name = "PeriodCobrTx") protected Integer periodCobrTx;
    @XmlElement(name = "InfoTxNPadr") protected String infoTxNPadr;

    /**
     * Obtém o valor da propriedade aplicOutrosFds.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getAplicOutrosFds() {
        return aplicOutrosFds;
    }

    /**
     * Define o valor da propriedade aplicOutrosFds.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setAplicOutrosFds(YesNoIndicator value) {
        this.aplicOutrosFds = value;
    }

    /**
     * Gets the value of the faixaTxEscalCasc property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the faixaTxEscalCasc property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFaixaTxEscalCasc().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FaixaTxEscalCascComplexType }
     */
    public List<FaixaTxEscalCascComplexType> getFaixaTxEscalCasc() {
        if (faixaTxEscalCasc == null) {
            faixaTxEscalCasc = new ArrayList<FaixaTxEscalCascComplexType>();
        }
        return this.faixaTxEscalCasc;
    }

    /**
     * Obtém o valor da propriedade indexCorrTx.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getIndexCorrTx() {
        return indexCorrTx;
    }

    /**
     * Define o valor da propriedade indexCorrTx.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setIndexCorrTx(Integer value) {
        this.indexCorrTx = value;
    }

    /**
     * Obtém o valor da propriedade infoTxNPadr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInfoTxNPadr() {
        return infoTxNPadr;
    }

    /**
     * Define o valor da propriedade infoTxNPadr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setInfoTxNPadr(String value) {
        this.infoTxNPadr = value;
    }

    /**
     * Obtém o valor da propriedade perCorrTx.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getPerCorrTx() {
        return perCorrTx;
    }

    /**
     * Define o valor da propriedade perCorrTx.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setPerCorrTx(Integer value) {
        this.perCorrTx = value;
    }

    /**
     * Obtém o valor da propriedade perfilTxAdm.
     */
    public int getPerfilTxAdm() {
        return perfilTxAdm;
    }

    /**
     * Define o valor da propriedade perfilTxAdm.
     */
    public void setPerfilTxAdm(int value) {
        this.perfilTxAdm = value;
    }

    /**
     * Obtém o valor da propriedade periodCobrTx.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getPeriodCobrTx() {
        return periodCobrTx;
    }

    /**
     * Define o valor da propriedade periodCobrTx.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setPeriodCobrTx(Integer value) {
        this.periodCobrTx = value;
    }

    /**
     * Obtém o valor da propriedade plBaseCalcTx.
     */
    public int getPlBaseCalcTx() {
        return plBaseCalcTx;
    }

    /**
     * Define o valor da propriedade plBaseCalcTx.
     */
    public void setPlBaseCalcTx(int value) {
        this.plBaseCalcTx = value;
    }

    /**
     * Obtém o valor da propriedade txMax.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getTxMax() {
        return txMax;
    }

    /**
     * Define o valor da propriedade txMax.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setTxMax(BigDecimal value) {
        this.txMax = value;
    }

    /**
     * Obtém o valor da propriedade unidTxAdm.
     */
    public int getUnidTxAdm() {
        return unidTxAdm;
    }

    /**
     * Define o valor da propriedade unidTxAdm.
     */
    public void setUnidTxAdm(int value) {
        this.unidTxAdm = value;
    }

    /**
     * Obtém o valor da propriedade vlPisoTxAdm.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getVlPisoTxAdm() {
        return vlPisoTxAdm;
    }

    /**
     * Define o valor da propriedade vlPisoTxAdm.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setVlPisoTxAdm(BigDecimal value) {
        this.vlPisoTxAdm = value;
    }

    /**
     * Obtém o valor da propriedade vlTxAdm.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getVlTxAdm() {
        return vlTxAdm;
    }

    /**
     * Define o valor da propriedade vlTxAdm.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setVlTxAdm(BigDecimal value) {
        this.vlTxAdm = value;
    }

}
