package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Série do FIDC
 * <p>
 * <p>Classe Java de SerieFIDCComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="SerieFIDCComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentFidcOrigem" type="{http://www.stianbid.com.br/SchemaFundo}IdentFidcOrigemComplexType"/>
 *         &lt;element name="DadosSerie" type="{http://www.stianbid.com.br/SchemaFundo}DadosSerieFIDCComplexType" minOccurs="0"/>
 *         &lt;element name="Datas" type="{http://www.stianbid.com.br/SchemaFundo}DatasFIDCComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SerieFIDCComplexType", propOrder = {"identFidcOrigem", "dadosSerie", "datas"})
public class SerieFIDCComplexType {

    @XmlElement(name = "IdentFidcOrigem", required = true) protected IdentFidcOrigemComplexType identFidcOrigem;
    @XmlElement(name = "DadosSerie") protected DadosSerieFIDCComplexType dadosSerie;
    @XmlElement(name = "Datas") protected DatasFIDCComplexType datas;

    /**
     * Obtém o valor da propriedade dadosSerie.
     *
     * @return possible object is
     * {@link DadosSerieFIDCComplexType }
     */
    public DadosSerieFIDCComplexType getDadosSerie() {
        return dadosSerie;
    }

    /**
     * Define o valor da propriedade dadosSerie.
     *
     * @param value allowed object is
     *              {@link DadosSerieFIDCComplexType }
     */
    public void setDadosSerie(DadosSerieFIDCComplexType value) {
        this.dadosSerie = value;
    }

    /**
     * Obtém o valor da propriedade datas.
     *
     * @return possible object is
     * {@link DatasFIDCComplexType }
     */
    public DatasFIDCComplexType getDatas() {
        return datas;
    }

    /**
     * Define o valor da propriedade datas.
     *
     * @param value allowed object is
     *              {@link DatasFIDCComplexType }
     */
    public void setDatas(DatasFIDCComplexType value) {
        this.datas = value;
    }

    /**
     * Obtém o valor da propriedade identFidcOrigem.
     *
     * @return possible object is
     * {@link IdentFidcOrigemComplexType }
     */
    public IdentFidcOrigemComplexType getIdentFidcOrigem() {
        return identFidcOrigem;
    }

    /**
     * Define o valor da propriedade identFidcOrigem.
     *
     * @param value allowed object is
     *              {@link IdentFidcOrigemComplexType }
     */
    public void setIdentFidcOrigem(IdentFidcOrigemComplexType value) {
        this.identFidcOrigem = value;
    }

}
