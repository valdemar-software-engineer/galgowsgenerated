package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informações de Identificação do Fundo (Utilizado somente
 * na consulta).
 * <p>
 * <p>
 * <p>Classe Java de IdentificacaoFundoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="IdentificacaoFundoComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CdSti" type="{http://www.stianbid.com.br/SchemaFundo}STIIdentifier" minOccurs="0"/>
 *         &lt;element name="CNPJ" type="{http://www.stianbid.com.br/SchemaFundo}CNPJIdentifier" minOccurs="0"/>
 *         &lt;element name="RazaoSocial" type="{http://www.stianbid.com.br/Common}Max150Text"/>
 *         &lt;element name="NmFantasiaImp" type="{http://www.stianbid.com.br/SchemaFundo}NomeFantasia"/>
 *         &lt;element name="Sigla" type="{http://www.stianbid.com.br/Common}Max35Text" minOccurs="0"/>
 *         &lt;element name="MnmCetip" type="{http://www.stianbid.com.br/Common}Max16Text" minOccurs="0"/>
 *         &lt;element name="Cusip" type="{http://www.stianbid.com.br/SchemaFundo}CusipCode" minOccurs="0"/>
 *         &lt;element name="DivAnbid" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="StatusMerc" type="{http://www.stianbid.com.br/SchemaFundo}StatusMercadoCode" minOccurs="0"/>
 *         &lt;element name="StatusAnbid" type="{http://www.stianbid.com.br/SchemaFundo}StatusANBIDCode" minOccurs="0"/>
 *         &lt;element name="StatusDoc" type="{http://www.stianbid.com.br/SchemaFundo}StatusDocCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificacaoFundoComplexType",
         propOrder = {"cdSti", "cnpj", "razaoSocial", "nmFantasiaImp", "sigla", "mnmCetip", "cusip", "divAnbid", "statusMerc", "statusAnbid", "statusDoc"})
public class IdentificacaoFundoComplexType {

    @XmlElement(name = "CdSti") protected Integer cdSti;
    @XmlElement(name = "CNPJ") protected Long cnpj;
    @XmlElement(name = "RazaoSocial", required = true) protected String razaoSocial;
    @XmlElement(name = "NmFantasiaImp", required = true) protected String nmFantasiaImp;
    @XmlElement(name = "Sigla") protected String sigla;
    @XmlElement(name = "MnmCetip") protected String mnmCetip;
    @XmlElement(name = "Cusip") protected String cusip;
    @XmlElement(name = "DivAnbid") protected YesNoIndicator divAnbid;
    @XmlElement(name = "StatusMerc") protected Integer statusMerc;
    @XmlElement(name = "StatusAnbid") protected Integer statusAnbid;
    @XmlElement(name = "StatusDoc") protected Integer statusDoc;

    /**
     * Obtém o valor da propriedade cnpj.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getCNPJ() {
        return cnpj;
    }

    /**
     * Define o valor da propriedade cnpj.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setCNPJ(Long value) {
        this.cnpj = value;
    }

    /**
     * Obtém o valor da propriedade cdSti.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCdSti() {
        return cdSti;
    }

    /**
     * Define o valor da propriedade cdSti.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCdSti(Integer value) {
        this.cdSti = value;
    }

    /**
     * Obtém o valor da propriedade cusip.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCusip() {
        return cusip;
    }

    /**
     * Define o valor da propriedade cusip.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCusip(String value) {
        this.cusip = value;
    }

    /**
     * Obtém o valor da propriedade divAnbid.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getDivAnbid() {
        return divAnbid;
    }

    /**
     * Define o valor da propriedade divAnbid.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setDivAnbid(YesNoIndicator value) {
        this.divAnbid = value;
    }

    /**
     * Obtém o valor da propriedade mnmCetip.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMnmCetip() {
        return mnmCetip;
    }

    /**
     * Define o valor da propriedade mnmCetip.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMnmCetip(String value) {
        this.mnmCetip = value;
    }

    /**
     * Obtém o valor da propriedade nmFantasiaImp.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNmFantasiaImp() {
        return nmFantasiaImp;
    }

    /**
     * Define o valor da propriedade nmFantasiaImp.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNmFantasiaImp(String value) {
        this.nmFantasiaImp = value;
    }

    /**
     * Obtém o valor da propriedade razaoSocial.
     *
     * @return possible object is
     * {@link String }
     */
    public String getRazaoSocial() {
        return razaoSocial;
    }

    /**
     * Define o valor da propriedade razaoSocial.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setRazaoSocial(String value) {
        this.razaoSocial = value;
    }

    /**
     * Obtém o valor da propriedade sigla.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSigla() {
        return sigla;
    }

    /**
     * Define o valor da propriedade sigla.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSigla(String value) {
        this.sigla = value;
    }

    /**
     * Obtém o valor da propriedade statusAnbid.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getStatusAnbid() {
        return statusAnbid;
    }

    /**
     * Define o valor da propriedade statusAnbid.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setStatusAnbid(Integer value) {
        this.statusAnbid = value;
    }

    /**
     * Obtém o valor da propriedade statusDoc.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getStatusDoc() {
        return statusDoc;
    }

    /**
     * Define o valor da propriedade statusDoc.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setStatusDoc(Integer value) {
        this.statusDoc = value;
    }

    /**
     * Obtém o valor da propriedade statusMerc.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getStatusMerc() {
        return statusMerc;
    }

    /**
     * Define o valor da propriedade statusMerc.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setStatusMerc(Integer value) {
        this.statusMerc = value;
    }

}
