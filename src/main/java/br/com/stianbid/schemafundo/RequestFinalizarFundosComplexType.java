package br.com.stianbid.schemafundo;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Finalizar Fundo de Investimento
 * <p>
 * <p>
 * <p>Classe Java de RequestFinalizarFundosComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="RequestFinalizarFundosComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType">
 *       &lt;sequence>
 *         &lt;element name="Finalizar" type="{http://www.stianbid.com.br/SchemaFundo}FinalizarComplexType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestFinalizarFundosComplexType", propOrder = {"finalizar"})
public class RequestFinalizarFundosComplexType extends MessageRequestComplexType {

    @XmlElement(name = "Finalizar", required = true) protected FinalizarComplexType finalizar;

    /**
     * Obtém o valor da propriedade finalizar.
     *
     * @return possible object is
     * {@link FinalizarComplexType }
     */
    public FinalizarComplexType getFinalizar() {
        return finalizar;
    }

    /**
     * Define o valor da propriedade finalizar.
     *
     * @param value allowed object is
     *              {@link FinalizarComplexType }
     */
    public void setFinalizar(FinalizarComplexType value) {
        this.finalizar = value;
    }

}
