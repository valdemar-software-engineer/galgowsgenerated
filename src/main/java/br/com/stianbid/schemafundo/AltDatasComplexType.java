package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Alteração de Datas
 * <p>
 * <p>Classe Java de AltDatasComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="AltDatasComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DtHistTpAnbid" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtEncerr" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="JustEncerr" type="{http://www.stianbid.com.br/Common}Max500Text" minOccurs="0"/>
 *         &lt;element name="DtEncerrCvm" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="FdoAtivo" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DtInativ" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="JustInativ" type="{http://www.stianbid.com.br/Common}Max500Text" minOccurs="0"/>
 *         &lt;element name="DtVincAnbid" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtDesvAnbid" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtAssembl" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtProtCvm" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtAprovAnbid" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtInicValAlt" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtRefAltEsp" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtInicVersao" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtFimVersao" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AltDatasComplexType",
         propOrder = {"dtHistTpAnbid", "dtEncerr", "justEncerr", "dtEncerrCvm", "fdoAtivo", "dtInativ", "justInativ", "dtVincAnbid", "dtDesvAnbid", "dtAssembl", "dtProtCvm", "dtAprovAnbid", "dtInicValAlt", "dtRefAltEsp", "dtInicVersao", "dtFimVersao"})
public class AltDatasComplexType {

    @XmlElement(name = "DtHistTpAnbid") protected XMLGregorianCalendar dtHistTpAnbid;
    @XmlElement(name = "DtEncerr") protected XMLGregorianCalendar dtEncerr;
    @XmlElement(name = "JustEncerr") protected String justEncerr;
    @XmlElement(name = "DtEncerrCvm") protected XMLGregorianCalendar dtEncerrCvm;
    @XmlElement(name = "FdoAtivo") protected YesNoIndicator fdoAtivo;
    @XmlElement(name = "DtInativ") protected XMLGregorianCalendar dtInativ;
    @XmlElement(name = "JustInativ") protected String justInativ;
    @XmlElement(name = "DtVincAnbid") protected XMLGregorianCalendar dtVincAnbid;
    @XmlElement(name = "DtDesvAnbid") protected XMLGregorianCalendar dtDesvAnbid;
    @XmlElement(name = "DtAssembl") protected XMLGregorianCalendar dtAssembl;
    @XmlElement(name = "DtProtCvm") protected XMLGregorianCalendar dtProtCvm;
    @XmlElement(name = "DtAprovAnbid") protected XMLGregorianCalendar dtAprovAnbid;
    @XmlElement(name = "DtInicValAlt") protected XMLGregorianCalendar dtInicValAlt;
    @XmlElement(name = "DtRefAltEsp") protected XMLGregorianCalendar dtRefAltEsp;
    @XmlElement(name = "DtInicVersao") protected XMLGregorianCalendar dtInicVersao;
    @XmlElement(name = "DtFimVersao") protected XMLGregorianCalendar dtFimVersao;

    /**
     * Obtém o valor da propriedade dtAprovAnbid.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtAprovAnbid() {
        return dtAprovAnbid;
    }

    /**
     * Define o valor da propriedade dtAprovAnbid.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtAprovAnbid(XMLGregorianCalendar value) {
        this.dtAprovAnbid = value;
    }

    /**
     * Obtém o valor da propriedade dtAssembl.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtAssembl() {
        return dtAssembl;
    }

    /**
     * Define o valor da propriedade dtAssembl.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtAssembl(XMLGregorianCalendar value) {
        this.dtAssembl = value;
    }

    /**
     * Obtém o valor da propriedade dtDesvAnbid.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtDesvAnbid() {
        return dtDesvAnbid;
    }

    /**
     * Define o valor da propriedade dtDesvAnbid.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtDesvAnbid(XMLGregorianCalendar value) {
        this.dtDesvAnbid = value;
    }

    /**
     * Obtém o valor da propriedade dtEncerr.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtEncerr() {
        return dtEncerr;
    }

    /**
     * Define o valor da propriedade dtEncerr.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtEncerr(XMLGregorianCalendar value) {
        this.dtEncerr = value;
    }

    /**
     * Obtém o valor da propriedade dtEncerrCvm.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtEncerrCvm() {
        return dtEncerrCvm;
    }

    /**
     * Define o valor da propriedade dtEncerrCvm.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtEncerrCvm(XMLGregorianCalendar value) {
        this.dtEncerrCvm = value;
    }

    /**
     * Obtém o valor da propriedade dtFimVersao.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtFimVersao() {
        return dtFimVersao;
    }

    /**
     * Define o valor da propriedade dtFimVersao.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtFimVersao(XMLGregorianCalendar value) {
        this.dtFimVersao = value;
    }

    /**
     * Obtém o valor da propriedade dtHistTpAnbid.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtHistTpAnbid() {
        return dtHistTpAnbid;
    }

    /**
     * Define o valor da propriedade dtHistTpAnbid.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtHistTpAnbid(XMLGregorianCalendar value) {
        this.dtHistTpAnbid = value;
    }

    /**
     * Obtém o valor da propriedade dtInativ.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInativ() {
        return dtInativ;
    }

    /**
     * Define o valor da propriedade dtInativ.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInativ(XMLGregorianCalendar value) {
        this.dtInativ = value;
    }

    /**
     * Obtém o valor da propriedade dtInicValAlt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInicValAlt() {
        return dtInicValAlt;
    }

    /**
     * Define o valor da propriedade dtInicValAlt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInicValAlt(XMLGregorianCalendar value) {
        this.dtInicValAlt = value;
    }

    /**
     * Obtém o valor da propriedade dtInicVersao.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInicVersao() {
        return dtInicVersao;
    }

    /**
     * Define o valor da propriedade dtInicVersao.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInicVersao(XMLGregorianCalendar value) {
        this.dtInicVersao = value;
    }

    /**
     * Obtém o valor da propriedade dtProtCvm.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtProtCvm() {
        return dtProtCvm;
    }

    /**
     * Define o valor da propriedade dtProtCvm.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtProtCvm(XMLGregorianCalendar value) {
        this.dtProtCvm = value;
    }

    /**
     * Obtém o valor da propriedade dtRefAltEsp.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtRefAltEsp() {
        return dtRefAltEsp;
    }

    /**
     * Define o valor da propriedade dtRefAltEsp.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtRefAltEsp(XMLGregorianCalendar value) {
        this.dtRefAltEsp = value;
    }

    /**
     * Obtém o valor da propriedade dtVincAnbid.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtVincAnbid() {
        return dtVincAnbid;
    }

    /**
     * Define o valor da propriedade dtVincAnbid.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtVincAnbid(XMLGregorianCalendar value) {
        this.dtVincAnbid = value;
    }

    /**
     * Obtém o valor da propriedade fdoAtivo.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getFdoAtivo() {
        return fdoAtivo;
    }

    /**
     * Define o valor da propriedade fdoAtivo.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setFdoAtivo(YesNoIndicator value) {
        this.fdoAtivo = value;
    }

    /**
     * Obtém o valor da propriedade justEncerr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getJustEncerr() {
        return justEncerr;
    }

    /**
     * Define o valor da propriedade justEncerr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setJustEncerr(String value) {
        this.justEncerr = value;
    }

    /**
     * Obtém o valor da propriedade justInativ.
     *
     * @return possible object is
     * {@link String }
     */
    public String getJustInativ() {
        return justInativ;
    }

    /**
     * Define o valor da propriedade justInativ.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setJustInativ(String value) {
        this.justInativ = value;
    }

}
