package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Lista de Documentos
 * <p>
 * <p>Classe Java de ListaEnvDocAtuComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ListaEnvDocAtuComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DocAtu" type="{http://www.stianbid.com.br/SchemaFundo}EnvDocAtuBinaryComplexType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListaEnvDocAtuComplexType", propOrder = {"docAtu"})
public class ListaEnvDocAtuComplexType {

    @XmlElement(name = "DocAtu", required = true) protected List<EnvDocAtuBinaryComplexType> docAtu;

    /**
     * Gets the value of the docAtu property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the docAtu property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocAtu().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnvDocAtuBinaryComplexType }
     */
    public List<EnvDocAtuBinaryComplexType> getDocAtu() {
        if (docAtu == null) {
            docAtu = new ArrayList<EnvDocAtuBinaryComplexType>();
        }
        return this.docAtu;
    }

}
