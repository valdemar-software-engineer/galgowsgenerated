package br.com.stianbid.schemafundo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the br.com.stianbid.schemafundo package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FPPRprts_QNAME = new QName("http://www.stianbid.com.br/SchemaFundo", "FPPRprts");
    private final static QName _FPPRprt_QNAME = new QName("http://www.stianbid.com.br/SchemaFundo", "FPPRprt");
    private final static QName _RequisicaoConsulta_QNAME = new QName("http://www.stianbid.com.br/SchemaFundo",
        "RequisicaoConsulta");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.stianbid.schemafundo
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AccountIdentificationComplexType }
     */
    public AccountIdentificationComplexType createAccountIdentificationComplexType() {
        return new AccountIdentificationComplexType();
    }

    /**
     * Create an instance of {@link AccountServicerComplexType }
     */
    public AccountServicerComplexType createAccountServicerComplexType() {
        return new AccountServicerComplexType();
    }

    /**
     * Create an instance of {@link AltDatasComplexType }
     */
    public AltDatasComplexType createAltDatasComplexType() {
        return new AltDatasComplexType();
    }

    /**
     * Create an instance of {@link AltDoctosComplexType }
     */
    public AltDoctosComplexType createAltDoctosComplexType() {
        return new AltDoctosComplexType();
    }

    /**
     * Create an instance of {@link AltIdentifComplexType }
     */
    public AltIdentifComplexType createAltIdentifComplexType() {
        return new AltIdentifComplexType();
    }

    /**
     * Create an instance of {@link AltParMovimComplexType }
     */
    public AltParMovimComplexType createAltParMovimComplexType() {
        return new AltParMovimComplexType();
    }

    /**
     * Create an instance of {@link AltPerfilComplexType }
     */
    public AltPerfilComplexType createAltPerfilComplexType() {
        return new AltPerfilComplexType();
    }

    /**
     * Create an instance of {@link AltPrestServComplexType }
     */
    public AltPrestServComplexType createAltPrestServComplexType() {
        return new AltPrestServComplexType();
    }

    /**
     * Create an instance of {@link AltTaxasComplexType }
     */
    public AltTaxasComplexType createAltTaxasComplexType() {
        return new AltTaxasComplexType();
    }

    /**
     * Create an instance of {@link AmortizacaoComplexType }
     */
    public AmortizacaoComplexType createAmortizacaoComplexType() {
        return new AmortizacaoComplexType();
    }

    /**
     * Create an instance of {@link ApplicationFormsComplexType }
     */
    public ApplicationFormsComplexType createApplicationFormsComplexType() {
        return new ApplicationFormsComplexType();
    }

    /**
     * Create an instance of {@link ApplicationFormsLODType }
     */
    public ApplicationFormsLODType createApplicationFormsLODType() {
        return new ApplicationFormsLODType();
    }

    /**
     * Create an instance of {@link CnsDatasComplexType }
     */
    public CnsDatasComplexType createCnsDatasComplexType() {
        return new CnsDatasComplexType();
    }

    /**
     * Create an instance of {@link ContasComplexType }
     */
    public ContasComplexType createContasComplexType() {
        return new ContasComplexType();
    }

    /**
     * Create an instance of {@link CotaSubordFIDCComplexType }
     */
    public CotaSubordFIDCComplexType createCotaSubordFIDCComplexType() {
        return new CotaSubordFIDCComplexType();
    }

    /**
     * Create an instance of {@link DadoSubordFIDCComplexType }
     */
    public DadoSubordFIDCComplexType createDadoSubordFIDCComplexType() {
        return new DadoSubordFIDCComplexType();
    }

    /**
     * Create an instance of {@link DadosAltComplexType }
     */
    public DadosAltComplexType createDadosAltComplexType() {
        return new DadosAltComplexType();
    }

    /**
     * Create an instance of {@link DadosConsComplexType }
     */
    public DadosConsComplexType createDadosConsComplexType() {
        return new DadosConsComplexType();
    }

    /**
     * Create an instance of {@link DadosSerieFIDCComplexType }
     */
    public DadosSerieFIDCComplexType createDadosSerieFIDCComplexType() {
        return new DadosSerieFIDCComplexType();
    }

    /**
     * Create an instance of {@link DataSubordFIDCComplexType }
     */
    public DataSubordFIDCComplexType createDataSubordFIDCComplexType() {
        return new DataSubordFIDCComplexType();
    }

    /**
     * Create an instance of {@link DatasComplexType }
     */
    public DatasComplexType createDatasComplexType() {
        return new DatasComplexType();
    }

    /**
     * Create an instance of {@link DatasFIDCComplexType }
     */
    public DatasFIDCComplexType createDatasFIDCComplexType() {
        return new DatasFIDCComplexType();
    }

    /**
     * Create an instance of {@link DocBinaryComplexType }
     */
    public DocBinaryComplexType createDocBinaryComplexType() {
        return new DocBinaryComplexType();
    }

    /**
     * Create an instance of {@link DomesticAccountComplexType }
     */
    public DomesticAccountComplexType createDomesticAccountComplexType() {
        return new DomesticAccountComplexType();
    }

    /**
     * Create an instance of {@link EnvDocAtuBinaryComplexType }
     */
    public EnvDocAtuBinaryComplexType createEnvDocAtuBinaryComplexType() {
        return new EnvDocAtuBinaryComplexType();
    }

    /**
     * Create an instance of {@link EnvDocBinaryComplexType }
     */
    public EnvDocBinaryComplexType createEnvDocBinaryComplexType() {
        return new EnvDocBinaryComplexType();
    }

    /**
     * Create an instance of {@link EventosComplexType }
     */
    public EventosComplexType createEventosComplexType() {
        return new EventosComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageFundoComplexType }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.stianbid.com.br/SchemaFundo", name = "FPPRprt")
    public JAXBElement<MessageFundoComplexType> createFPPRprt(MessageFundoComplexType value) {
        return new JAXBElement<MessageFundoComplexType>(_FPPRprt_QNAME, MessageFundoComplexType.class, null, value);
    }

    /**
     * Create an instance of {@link FPPRprtComplexType }
     */
    public FPPRprtComplexType createFPPRprtComplexType() {
        return new FPPRprtComplexType();
    }

    /**
     * Create an instance of {@link FPPRprtPendComplexType }
     */
    public FPPRprtPendComplexType createFPPRprtPendComplexType() {
        return new FPPRprtPendComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FPPRprtComplexType }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.stianbid.com.br/SchemaFundo", name = "FPPRprts")
    public JAXBElement<FPPRprtComplexType> createFPPRprts(FPPRprtComplexType value) {
        return new JAXBElement<FPPRprtComplexType>(_FPPRprts_QNAME, FPPRprtComplexType.class, null, value);
    }

    /**
     * Create an instance of {@link FaixaTxEscalCascComplexType }
     */
    public FaixaTxEscalCascComplexType createFaixaTxEscalCascComplexType() {
        return new FaixaTxEscalCascComplexType();
    }

    /**
     * Create an instance of {@link FaixaTxPerfComplexType }
     */
    public FaixaTxPerfComplexType createFaixaTxPerfComplexType() {
        return new FaixaTxPerfComplexType();
    }

    /**
     * Create an instance of {@link FinalizarComplexType }
     */
    public FinalizarComplexType createFinalizarComplexType() {
        return new FinalizarComplexType();
    }

    /**
     * Create an instance of {@link FinancialInstrumentDetailsComplexType }
     */
    public FinancialInstrumentDetailsComplexType createFinancialInstrumentDetailsComplexType() {
        return new FinancialInstrumentDetailsComplexType();
    }

    /**
     * Create an instance of {@link FundManagementCompanyComplexType }
     */
    public FundManagementCompanyComplexType createFundManagementCompanyComplexType() {
        return new FundManagementCompanyComplexType();
    }

    /**
     * Create an instance of {@link FundManagerPostalAddressComplexType }
     */
    public FundManagerPostalAddressComplexType createFundManagerPostalAddressComplexType() {
        return new FundManagerPostalAddressComplexType();
    }

    /**
     * Create an instance of {@link IdentFidcOrigemComplexType }
     */
    public IdentFidcOrigemComplexType createIdentFidcOrigemComplexType() {
        return new IdentFidcOrigemComplexType();
    }

    /**
     * Create an instance of {@link IdentificacaoFundoComplexType }
     */
    public IdentificacaoFundoComplexType createIdentificacaoFundoComplexType() {
        return new IdentificacaoFundoComplexType();
    }

    /**
     * Create an instance of {@link IdentificationComplexType }
     */
    public IdentificationComplexType createIdentificationComplexType() {
        return new IdentificationComplexType();
    }

    /**
     * Create an instance of {@link IndexRefComplexType }
     */
    public IndexRefComplexType createIndexRefComplexType() {
        return new IndexRefComplexType();
    }

    /**
     * Create an instance of {@link InstrumentRestrictionsComplexType }
     */
    public InstrumentRestrictionsComplexType createInstrumentRestrictionsComplexType() {
        return new InstrumentRestrictionsComplexType();
    }

    /**
     * Create an instance of {@link ItensRevCodComplexType }
     */
    public ItensRevCodComplexType createItensRevCodComplexType() {
        return new ItensRevCodComplexType();
    }

    /**
     * Create an instance of {@link ListaEnvDocAtuComplexType }
     */
    public ListaEnvDocAtuComplexType createListaEnvDocAtuComplexType() {
        return new ListaEnvDocAtuComplexType();
    }

    /**
     * Create an instance of {@link ListaNrProtocoloComplexType }
     */
    public ListaNrProtocoloComplexType createListaNrProtocoloComplexType() {
        return new ListaNrProtocoloComplexType();
    }

    /**
     * Create an instance of {@link LocalOrderDeskComplexType }
     */
    public LocalOrderDeskComplexType createLocalOrderDeskComplexType() {
        return new LocalOrderDeskComplexType();
    }

    /**
     * Create an instance of {@link MainFundOrderDeskComplexType }
     */
    public MainFundOrderDeskComplexType createMainFundOrderDeskComplexType() {
        return new MainFundOrderDeskComplexType();
    }

    /**
     * Create an instance of {@link MessageAtualizarFundosCorrecaoComplexType }
     */
    public MessageAtualizarFundosCorrecaoComplexType createMessageAtualizarFundosCorrecaoComplexType() {
        return new MessageAtualizarFundosCorrecaoComplexType();
    }

    /**
     * Create an instance of {@link MessageFundoComplexType }
     */
    public MessageFundoComplexType createMessageFundoComplexType() {
        return new MessageFundoComplexType();
    }

    /**
     * Create an instance of {@link MessageListaValoresCorretos }
     */
    public MessageListaValoresCorretos createMessageListaValoresCorretos() {
        return new MessageListaValoresCorretos();
    }

    /**
     * Create an instance of {@link MessageResponseConsumirFundosCorrecao }
     */
    public MessageResponseConsumirFundosCorrecao createMessageResponseConsumirFundosCorrecao() {
        return new MessageResponseConsumirFundosCorrecao();
    }

    /**
     * Create an instance of {@link OtherIdentificationComplexType }
     */
    public OtherIdentificationComplexType createOtherIdentificationComplexType() {
        return new OtherIdentificationComplexType();
    }

    /**
     * Create an instance of {@link OutrasTaxasComplexType }
     */
    public OutrasTaxasComplexType createOutrasTaxasComplexType() {
        return new OutrasTaxasComplexType();
    }

    /**
     * Create an instance of {@link ParamMovimentacaoComplexType }
     */
    public ParamMovimentacaoComplexType createParamMovimentacaoComplexType() {
        return new ParamMovimentacaoComplexType();
    }

    /**
     * Create an instance of {@link PerfilComplexType }
     */
    public PerfilComplexType createPerfilComplexType() {
        return new PerfilComplexType();
    }

    /**
     * Create an instance of {@link PerfilFidcComplexType }
     */
    public PerfilFidcComplexType createPerfilFidcComplexType() {
        return new PerfilFidcComplexType();
    }

    /**
     * Create an instance of {@link PostalAddressComplexType }
     */
    public PostalAddressComplexType createPostalAddressComplexType() {
        return new PostalAddressComplexType();
    }

    /**
     * Create an instance of {@link PrestServicoComplexType }
     */
    public PrestServicoComplexType createPrestServicoComplexType() {
        return new PrestServicoComplexType();
    }

    /**
     * Create an instance of {@link PrestServicoIdentification2ComplexType }
     */
    public PrestServicoIdentification2ComplexType createPrestServicoIdentification2ComplexType() {
        return new PrestServicoIdentification2ComplexType();
    }

    /**
     * Create an instance of {@link PrestServicoIdentification3ComplexType }
     */
    public PrestServicoIdentification3ComplexType createPrestServicoIdentification3ComplexType() {
        return new PrestServicoIdentification3ComplexType();
    }

    /**
     * Create an instance of {@link PrestServicoIdentification4ComplexType }
     */
    public PrestServicoIdentification4ComplexType createPrestServicoIdentification4ComplexType() {
        return new PrestServicoIdentification4ComplexType();
    }

    /**
     * Create an instance of {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType createPrestServicoIdentificationComplexType() {
        return new PrestServicoIdentificationComplexType();
    }

    /**
     * Create an instance of {@link RedemptionProcessingCharacteristicsComplexType }
     */
    public RedemptionProcessingCharacteristicsComplexType createRedemptionProcessingCharacteristicsComplexType() {
        return new RedemptionProcessingCharacteristicsComplexType();
    }

    /**
     * Create an instance of {@link RequestAlterarFundoComplexType }
     */
    public RequestAlterarFundoComplexType createRequestAlterarFundoComplexType() {
        return new RequestAlterarFundoComplexType();
    }

    /**
     * Create an instance of {@link RequestAtualizarFundosComplexType }
     */
    public RequestAtualizarFundosComplexType createRequestAtualizarFundosComplexType() {
        return new RequestAtualizarFundosComplexType();
    }

    /**
     * Create an instance of {@link RequestConsultaDocumentoComplexType }
     */
    public RequestConsultaDocumentoComplexType createRequestConsultaDocumentoComplexType() {
        return new RequestConsultaDocumentoComplexType();
    }

    /**
     * Create an instance of {@link RequestConsumirFundosComplexType }
     */
    public RequestConsumirFundosComplexType createRequestConsumirFundosComplexType() {
        return new RequestConsumirFundosComplexType();
    }

    /**
     * Create an instance of {@link RequestConsumirVersoesFundoComplexType }
     */
    public RequestConsumirVersoesFundoComplexType createRequestConsumirVersoesFundoComplexType() {
        return new RequestConsumirVersoesFundoComplexType();
    }

    /**
     * Create an instance of {@link RequestEnviaDocComplexType }
     */
    public RequestEnviaDocComplexType createRequestEnviaDocComplexType() {
        return new RequestEnviaDocComplexType();
    }

    /**
     * Create an instance of {@link RequestFinalizarFundosComplexType }
     */
    public RequestFinalizarFundosComplexType createRequestFinalizarFundosComplexType() {
        return new RequestFinalizarFundosComplexType();
    }

    /**
     * Create an instance of {@link RequestIncluirFundoComplexType }
     */
    public RequestIncluirFundoComplexType createRequestIncluirFundoComplexType() {
        return new RequestIncluirFundoComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestIncluirFundoComplexType }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.stianbid.com.br/SchemaFundo", name = "RequisicaoConsulta")
    public JAXBElement<RequestIncluirFundoComplexType> createRequisicaoConsulta(RequestIncluirFundoComplexType value) {
        return new JAXBElement<RequestIncluirFundoComplexType>(_RequisicaoConsulta_QNAME,
            RequestIncluirFundoComplexType.class, null, value);
    }

    /**
     * Create an instance of {@link ResponseConsultaDocumentoComplexType }
     */
    public ResponseConsultaDocumentoComplexType createResponseConsultaDocumentoComplexType() {
        return new ResponseConsultaDocumentoComplexType();
    }

    /**
     * Create an instance of {@link ResponseConsumirFundosComplexType }
     */
    public ResponseConsumirFundosComplexType createResponseConsumirFundosComplexType() {
        return new ResponseConsumirFundosComplexType();
    }

    /**
     * Create an instance of {@link RevCodAutoRegComplexType }
     */
    public RevCodAutoRegComplexType createRevCodAutoRegComplexType() {
        return new RevCodAutoRegComplexType();
    }

    /**
     * Create an instance of {@link STIComplexType }
     */
    public STIComplexType createSTIComplexType() {
        return new STIComplexType();
    }

    /**
     * Create an instance of {@link STIConsumirFundosCorrecaoComplexType }
     */
    public STIConsumirFundosCorrecaoComplexType createSTIConsumirFundosCorrecaoComplexType() {
        return new STIConsumirFundosCorrecaoComplexType();
    }

    /**
     * Create an instance of {@link SerieFIDCComplexType }
     */
    public SerieFIDCComplexType createSerieFIDCComplexType() {
        return new SerieFIDCComplexType();
    }

    /**
     * Create an instance of {@link SettlementDetailsComplexType }
     */
    public SettlementDetailsComplexType createSettlementDetailsComplexType() {
        return new SettlementDetailsComplexType();
    }

    /**
     * Create an instance of {@link SubscriptionProcessingCharacteristicsComplexType }
     */
    public SubscriptionProcessingCharacteristicsComplexType createSubscriptionProcessingCharacteristicsComplexType() {
        return new SubscriptionProcessingCharacteristicsComplexType();
    }

    /**
     * Create an instance of {@link TaxaAdmComplexType }
     */
    public TaxaAdmComplexType createTaxaAdmComplexType() {
        return new TaxaAdmComplexType();
    }

    /**
     * Create an instance of {@link TaxaPerfComplexType }
     */
    public TaxaPerfComplexType createTaxaPerfComplexType() {
        return new TaxaPerfComplexType();
    }

    /**
     * Create an instance of {@link TaxasComplexType }
     */
    public TaxasComplexType createTaxasComplexType() {
        return new TaxasComplexType();
    }

    /**
     * Create an instance of {@link UpdatedDateComplexType }
     */
    public UpdatedDateComplexType createUpdatedDateComplexType() {
        return new UpdatedDateComplexType();
    }

    /**
     * Create an instance of {@link ValuationDealingCharacteristicsComplexType }
     */
    public ValuationDealingCharacteristicsComplexType createValuationDealingCharacteristicsComplexType() {
        return new ValuationDealingCharacteristicsComplexType();
    }

}
