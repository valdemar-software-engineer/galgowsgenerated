package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Lista de Números de Protocolos
 * <p>
 * <p>
 * <p>Classe Java de ListaNrProtocoloComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ListaNrProtocoloComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NrProtocolo" type="{http://www.stianbid.com.br/SchemaFundo}NumeroProtocolo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListaNrProtocoloComplexType", propOrder = {"nrProtocolo"})
public class ListaNrProtocoloComplexType {

    @XmlElement(name = "NrProtocolo", type = Long.class) protected List<Long> nrProtocolo;

    /**
     * Gets the value of the nrProtocolo property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nrProtocolo property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNrProtocolo().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Long }
     */
    public List<Long> getNrProtocolo() {
        if (nrProtocolo == null) {
            nrProtocolo = new ArrayList<Long>();
        }
        return this.nrProtocolo;
    }

}
