package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Account Servicer (Campo ISO não utilizado no momento)
 * <p>
 * <p>
 * <p>Classe Java de AccountServicerComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="AccountServicerComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BIC" type="{http://www.stianbid.com.br/Common}BICIdentification1" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountServicerComplexType", propOrder = {"bic"})
public class AccountServicerComplexType {

    @XmlElement(name = "BIC") protected String bic;

    /**
     * Obtém o valor da propriedade bic.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBIC() {
        return bic;
    }

    /**
     * Define o valor da propriedade bic.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBIC(String value) {
        this.bic = value;
    }

}
