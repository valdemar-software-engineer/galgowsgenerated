package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Alterações de Identificação
 * <p>
 * <p>Classe Java de AltIdentifComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="AltIdentifComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DtNmFantasia" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtRazaoSocial" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AltIdentifComplexType", propOrder = {"dtNmFantasia", "dtRazaoSocial"})
public class AltIdentifComplexType {

    @XmlElement(name = "DtNmFantasia") protected XMLGregorianCalendar dtNmFantasia;
    @XmlElement(name = "DtRazaoSocial") protected XMLGregorianCalendar dtRazaoSocial;

    /**
     * Obtém o valor da propriedade dtNmFantasia.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtNmFantasia() {
        return dtNmFantasia;
    }

    /**
     * Define o valor da propriedade dtNmFantasia.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtNmFantasia(XMLGregorianCalendar value) {
        this.dtNmFantasia = value;
    }

    /**
     * Obtém o valor da propriedade dtRazaoSocial.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtRazaoSocial() {
        return dtRazaoSocial;
    }

    /**
     * Define o valor da propriedade dtRazaoSocial.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtRazaoSocial(XMLGregorianCalendar value) {
        this.dtRazaoSocial = value;
    }

}
