package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Taxas
 * <p>
 * <p>Classe Java de TaxasComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="TaxasComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TxAdm" type="{http://www.stianbid.com.br/SchemaFundo}TaxaAdmComplexType"/>
 *         &lt;element name="TxPerf" type="{http://www.stianbid.com.br/SchemaFundo}TaxaPerfComplexType"/>
 *         &lt;element name="OutrasTx" type="{http://www.stianbid.com.br/SchemaFundo}OutrasTaxasComplexType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxasComplexType", propOrder = {"txAdm", "txPerf", "outrasTx"})
public class TaxasComplexType {

    @XmlElement(name = "TxAdm", required = true) protected TaxaAdmComplexType txAdm;
    @XmlElement(name = "TxPerf", required = true) protected TaxaPerfComplexType txPerf;
    @XmlElement(name = "OutrasTx", required = true) protected OutrasTaxasComplexType outrasTx;

    /**
     * Obtém o valor da propriedade outrasTx.
     *
     * @return possible object is
     * {@link OutrasTaxasComplexType }
     */
    public OutrasTaxasComplexType getOutrasTx() {
        return outrasTx;
    }

    /**
     * Define o valor da propriedade outrasTx.
     *
     * @param value allowed object is
     *              {@link OutrasTaxasComplexType }
     */
    public void setOutrasTx(OutrasTaxasComplexType value) {
        this.outrasTx = value;
    }

    /**
     * Obtém o valor da propriedade txAdm.
     *
     * @return possible object is
     * {@link TaxaAdmComplexType }
     */
    public TaxaAdmComplexType getTxAdm() {
        return txAdm;
    }

    /**
     * Define o valor da propriedade txAdm.
     *
     * @param value allowed object is
     *              {@link TaxaAdmComplexType }
     */
    public void setTxAdm(TaxaAdmComplexType value) {
        this.txAdm = value;
    }

    /**
     * Obtém o valor da propriedade txPerf.
     *
     * @return possible object is
     * {@link TaxaPerfComplexType }
     */
    public TaxaPerfComplexType getTxPerf() {
        return txPerf;
    }

    /**
     * Define o valor da propriedade txPerf.
     *
     * @param value allowed object is
     *              {@link TaxaPerfComplexType }
     */
    public void setTxPerf(TaxaPerfComplexType value) {
        this.txPerf = value;
    }

}
