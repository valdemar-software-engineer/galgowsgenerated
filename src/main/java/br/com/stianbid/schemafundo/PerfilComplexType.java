package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Perfil
 * <p>
 * <p>Classe Java de PerfilComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PerfilComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClassCvm" type="{http://www.stianbid.com.br/SchemaFundo}ClassCVMCode"/>
 *         &lt;element name="CtgAnbid" type="{http://www.stianbid.com.br/SchemaFundo}CtgAnbidCode" minOccurs="0"/>
 *         &lt;element name="TpAnbid" type="{http://www.stianbid.com.br/SchemaFundo}TipoAnbidCode" minOccurs="0"/>
 *         &lt;element name="FocoAtua" type="{http://www.stianbid.com.br/SchemaFundo}FocoAtuaCode" minOccurs="0"/>
 *         &lt;element name="AplicAuto" type="{http://www.stianbid.com.br/SchemaFundo}AplicAutoCode" minOccurs="0"/>
 *         &lt;element name="PlanoPrev" type="{http://www.stianbid.com.br/SchemaFundo}PlanoPrevCode" minOccurs="0"/>
 *         &lt;element name="CalcCota" type="{http://www.stianbid.com.br/SchemaFundo}CalcCotaCode" minOccurs="0"/>
 *         &lt;element name="HrComprCota" type="{http://www.stianbid.com.br/Common}ISOTime"/>
 *         &lt;element name="HrComprAtivos" type="{http://www.stianbid.com.br/Common}ISOTime"/>
 *         &lt;element name="AbertoEstat" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DuracaoFdo" type="{http://www.stianbid.com.br/SchemaFundo}DuracaoFundoCode" minOccurs="0"/>
 *         &lt;element name="UnidDuracaoFdo" type="{http://www.stianbid.com.br/SchemaFundo}UnidadeDuracaoFundoCode" minOccurs="0"/>
 *         &lt;element name="PerDuracaoFdo" type="{http://www.stianbid.com.br/SchemaFundo}Number_3" minOccurs="0"/>
 *         &lt;element name="BaseDuracaoFdo" type="{http://www.stianbid.com.br/SchemaFundo}BaseDuracaoFdoCode" minOccurs="0"/>
 *         &lt;element name="Composicao" type="{http://www.stianbid.com.br/SchemaFundo}ComposicaoCode"/>
 *         &lt;element name="InvQualif" type="{http://www.stianbid.com.br/Common}YesNoIndicator"/>
 *         &lt;element name="Alavancado" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="Previdenciario" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="CredPrivado" type="{http://www.stianbid.com.br/SchemaFundo}YesNoIndicatorSTI" minOccurs="0"/>
 *         &lt;element name="InvExterior" type="{http://www.stianbid.com.br/SchemaFundo}YesNoIndicatorSTI" minOccurs="0"/>
 *         &lt;element name="RestrInvest" type="{http://www.stianbid.com.br/SchemaFundo}RestrInvestCode" minOccurs="0"/>
 *         &lt;element name="PublAlvo" type="{http://www.stianbid.com.br/SchemaFundo}PublAlvoCode" maxOccurs="unbounded"/>
 *         &lt;element name="DivulgImp" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="LimitesEmiss" type="{http://www.stianbid.com.br/SchemaFundo}YesNoIndicatorSTI" minOccurs="0"/>
 *         &lt;element name="TributPerseg" type="{http://www.stianbid.com.br/SchemaFundo}TributPersegCode"/>
 *         &lt;element name="PossuiProsp" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="PossuiContrCess" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="PossuiSumRating" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PerfilComplexType",
         propOrder = {"classCvm", "ctgAnbid", "tpAnbid", "focoAtua", "aplicAuto", "planoPrev", "calcCota", "hrComprCota", "hrComprAtivos", "abertoEstat", "duracaoFdo", "unidDuracaoFdo", "perDuracaoFdo", "baseDuracaoFdo", "composicao", "invQualif", "alavancado", "previdenciario", "credPrivado", "invExterior", "restrInvest", "publAlvo", "divulgImp", "limitesEmiss", "tributPerseg", "possuiProsp", "possuiContrCess", "possuiSumRating"})
public class PerfilComplexType {

    @XmlElement(name = "ClassCvm") protected int classCvm;
    @XmlElement(name = "CtgAnbid") protected Integer ctgAnbid;
    @XmlElement(name = "TpAnbid") protected Integer tpAnbid;
    @XmlElement(name = "FocoAtua") protected Integer focoAtua;
    @XmlElement(name = "AplicAuto") protected Integer aplicAuto;
    @XmlElement(name = "PlanoPrev") protected Integer planoPrev;
    @XmlElement(name = "CalcCota") protected Integer calcCota;
    @XmlElement(name = "HrComprCota", required = true) protected XMLGregorianCalendar hrComprCota;
    @XmlElement(name = "HrComprAtivos", required = true) protected XMLGregorianCalendar hrComprAtivos;
    @XmlElement(name = "AbertoEstat") protected YesNoIndicator abertoEstat;
    @XmlElement(name = "DuracaoFdo") protected Integer duracaoFdo;
    @XmlElement(name = "UnidDuracaoFdo") protected Integer unidDuracaoFdo;
    @XmlElement(name = "PerDuracaoFdo") protected BigDecimal perDuracaoFdo;
    @XmlElement(name = "BaseDuracaoFdo") protected Integer baseDuracaoFdo;
    @XmlElement(name = "Composicao") protected int composicao;
    @XmlElement(name = "InvQualif", required = true) protected YesNoIndicator invQualif;
    @XmlElement(name = "Alavancado") protected YesNoIndicator alavancado;
    @XmlElement(name = "Previdenciario") protected YesNoIndicator previdenciario;
    @XmlElement(name = "CredPrivado") protected Integer credPrivado;
    @XmlElement(name = "InvExterior") protected Integer invExterior;
    @XmlElement(name = "RestrInvest") protected Integer restrInvest;
    @XmlElement(name = "PublAlvo", type = Integer.class) protected List<Integer> publAlvo;
    @XmlElement(name = "DivulgImp") protected YesNoIndicator divulgImp;
    @XmlElement(name = "LimitesEmiss") protected Integer limitesEmiss;
    @XmlElement(name = "TributPerseg") protected int tributPerseg;
    @XmlElement(name = "PossuiProsp") protected YesNoIndicator possuiProsp;
    @XmlElement(name = "PossuiContrCess") protected YesNoIndicator possuiContrCess;
    @XmlElement(name = "PossuiSumRating") protected YesNoIndicator possuiSumRating;

    /**
     * Obtém o valor da propriedade abertoEstat.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getAbertoEstat() {
        return abertoEstat;
    }

    /**
     * Define o valor da propriedade abertoEstat.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setAbertoEstat(YesNoIndicator value) {
        this.abertoEstat = value;
    }

    /**
     * Obtém o valor da propriedade alavancado.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getAlavancado() {
        return alavancado;
    }

    /**
     * Define o valor da propriedade alavancado.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setAlavancado(YesNoIndicator value) {
        this.alavancado = value;
    }

    /**
     * Obtém o valor da propriedade aplicAuto.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getAplicAuto() {
        return aplicAuto;
    }

    /**
     * Define o valor da propriedade aplicAuto.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setAplicAuto(Integer value) {
        this.aplicAuto = value;
    }

    /**
     * Obtém o valor da propriedade baseDuracaoFdo.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getBaseDuracaoFdo() {
        return baseDuracaoFdo;
    }

    /**
     * Define o valor da propriedade baseDuracaoFdo.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setBaseDuracaoFdo(Integer value) {
        this.baseDuracaoFdo = value;
    }

    /**
     * Obtém o valor da propriedade calcCota.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCalcCota() {
        return calcCota;
    }

    /**
     * Define o valor da propriedade calcCota.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCalcCota(Integer value) {
        this.calcCota = value;
    }

    /**
     * Obtém o valor da propriedade classCvm.
     */
    public int getClassCvm() {
        return classCvm;
    }

    /**
     * Define o valor da propriedade classCvm.
     */
    public void setClassCvm(int value) {
        this.classCvm = value;
    }

    /**
     * Obtém o valor da propriedade composicao.
     */
    public int getComposicao() {
        return composicao;
    }

    /**
     * Define o valor da propriedade composicao.
     */
    public void setComposicao(int value) {
        this.composicao = value;
    }

    /**
     * Obtém o valor da propriedade credPrivado.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCredPrivado() {
        return credPrivado;
    }

    /**
     * Define o valor da propriedade credPrivado.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCredPrivado(Integer value) {
        this.credPrivado = value;
    }

    /**
     * Obtém o valor da propriedade ctgAnbid.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCtgAnbid() {
        return ctgAnbid;
    }

    /**
     * Define o valor da propriedade ctgAnbid.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCtgAnbid(Integer value) {
        this.ctgAnbid = value;
    }

    /**
     * Obtém o valor da propriedade divulgImp.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getDivulgImp() {
        return divulgImp;
    }

    /**
     * Define o valor da propriedade divulgImp.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setDivulgImp(YesNoIndicator value) {
        this.divulgImp = value;
    }

    /**
     * Obtém o valor da propriedade duracaoFdo.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getDuracaoFdo() {
        return duracaoFdo;
    }

    /**
     * Define o valor da propriedade duracaoFdo.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setDuracaoFdo(Integer value) {
        this.duracaoFdo = value;
    }

    /**
     * Obtém o valor da propriedade focoAtua.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getFocoAtua() {
        return focoAtua;
    }

    /**
     * Define o valor da propriedade focoAtua.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setFocoAtua(Integer value) {
        this.focoAtua = value;
    }

    /**
     * Obtém o valor da propriedade hrComprAtivos.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getHrComprAtivos() {
        return hrComprAtivos;
    }

    /**
     * Define o valor da propriedade hrComprAtivos.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setHrComprAtivos(XMLGregorianCalendar value) {
        this.hrComprAtivos = value;
    }

    /**
     * Obtém o valor da propriedade hrComprCota.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getHrComprCota() {
        return hrComprCota;
    }

    /**
     * Define o valor da propriedade hrComprCota.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setHrComprCota(XMLGregorianCalendar value) {
        this.hrComprCota = value;
    }

    /**
     * Obtém o valor da propriedade invExterior.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getInvExterior() {
        return invExterior;
    }

    /**
     * Define o valor da propriedade invExterior.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setInvExterior(Integer value) {
        this.invExterior = value;
    }

    /**
     * Obtém o valor da propriedade invQualif.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getInvQualif() {
        return invQualif;
    }

    /**
     * Define o valor da propriedade invQualif.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setInvQualif(YesNoIndicator value) {
        this.invQualif = value;
    }

    /**
     * Obtém o valor da propriedade limitesEmiss.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getLimitesEmiss() {
        return limitesEmiss;
    }

    /**
     * Define o valor da propriedade limitesEmiss.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setLimitesEmiss(Integer value) {
        this.limitesEmiss = value;
    }

    /**
     * Obtém o valor da propriedade perDuracaoFdo.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getPerDuracaoFdo() {
        return perDuracaoFdo;
    }

    /**
     * Define o valor da propriedade perDuracaoFdo.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setPerDuracaoFdo(BigDecimal value) {
        this.perDuracaoFdo = value;
    }

    /**
     * Obtém o valor da propriedade planoPrev.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getPlanoPrev() {
        return planoPrev;
    }

    /**
     * Define o valor da propriedade planoPrev.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setPlanoPrev(Integer value) {
        this.planoPrev = value;
    }

    /**
     * Obtém o valor da propriedade possuiContrCess.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getPossuiContrCess() {
        return possuiContrCess;
    }

    /**
     * Define o valor da propriedade possuiContrCess.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setPossuiContrCess(YesNoIndicator value) {
        this.possuiContrCess = value;
    }

    /**
     * Obtém o valor da propriedade possuiProsp.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getPossuiProsp() {
        return possuiProsp;
    }

    /**
     * Define o valor da propriedade possuiProsp.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setPossuiProsp(YesNoIndicator value) {
        this.possuiProsp = value;
    }

    /**
     * Obtém o valor da propriedade possuiSumRating.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getPossuiSumRating() {
        return possuiSumRating;
    }

    /**
     * Define o valor da propriedade possuiSumRating.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setPossuiSumRating(YesNoIndicator value) {
        this.possuiSumRating = value;
    }

    /**
     * Obtém o valor da propriedade previdenciario.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getPrevidenciario() {
        return previdenciario;
    }

    /**
     * Define o valor da propriedade previdenciario.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setPrevidenciario(YesNoIndicator value) {
        this.previdenciario = value;
    }

    /**
     * Gets the value of the publAlvo property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the publAlvo property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPublAlvo().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     */
    public List<Integer> getPublAlvo() {
        if (publAlvo == null) {
            publAlvo = new ArrayList<Integer>();
        }
        return this.publAlvo;
    }

    /**
     * Obtém o valor da propriedade restrInvest.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getRestrInvest() {
        return restrInvest;
    }

    /**
     * Define o valor da propriedade restrInvest.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setRestrInvest(Integer value) {
        this.restrInvest = value;
    }

    /**
     * Obtém o valor da propriedade tpAnbid.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getTpAnbid() {
        return tpAnbid;
    }

    /**
     * Define o valor da propriedade tpAnbid.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setTpAnbid(Integer value) {
        this.tpAnbid = value;
    }

    /**
     * Obtém o valor da propriedade tributPerseg.
     */
    public int getTributPerseg() {
        return tributPerseg;
    }

    /**
     * Define o valor da propriedade tributPerseg.
     */
    public void setTributPerseg(int value) {
        this.tributPerseg = value;
    }

    /**
     * Obtém o valor da propriedade unidDuracaoFdo.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getUnidDuracaoFdo() {
        return unidDuracaoFdo;
    }

    /**
     * Define o valor da propriedade unidDuracaoFdo.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setUnidDuracaoFdo(Integer value) {
        this.unidDuracaoFdo = value;
    }

}
