package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * Amortização
 * <p>
 * <p>Classe Java de AmortizacaoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="AmortizacaoComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PossuiMecAmort" type="{http://www.stianbid.com.br/Common}YesNoIndicator"/>
 *         &lt;element name="DtPrimAmort" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="PeriodAmort" type="{http://www.stianbid.com.br/SchemaFundo}PeriodAmortCode" minOccurs="0"/>
 *         &lt;element name="DtBaseAmort" type="{http://www.stianbid.com.br/SchemaFundo}DtBaseAmortCode" minOccurs="0"/>
 *         &lt;element name="DiaAmort" type="{http://www.stianbid.com.br/SchemaFundo}Number_2" minOccurs="0"/>
 *         &lt;element name="InfAmort" type="{http://www.stianbid.com.br/Common}Max500Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AmortizacaoComplexType",
         propOrder = {"possuiMecAmort", "dtPrimAmort", "periodAmort", "dtBaseAmort", "diaAmort", "infAmort"})
public class AmortizacaoComplexType {

    @XmlElement(name = "PossuiMecAmort", required = true) protected YesNoIndicator possuiMecAmort;
    @XmlElement(name = "DtPrimAmort") protected XMLGregorianCalendar dtPrimAmort;
    @XmlElement(name = "PeriodAmort") protected Integer periodAmort;
    @XmlElement(name = "DtBaseAmort") protected Integer dtBaseAmort;
    @XmlElement(name = "DiaAmort") protected BigDecimal diaAmort;
    @XmlElement(name = "InfAmort") protected String infAmort;

    /**
     * Obtém o valor da propriedade diaAmort.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getDiaAmort() {
        return diaAmort;
    }

    /**
     * Define o valor da propriedade diaAmort.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setDiaAmort(BigDecimal value) {
        this.diaAmort = value;
    }

    /**
     * Obtém o valor da propriedade dtBaseAmort.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getDtBaseAmort() {
        return dtBaseAmort;
    }

    /**
     * Define o valor da propriedade dtBaseAmort.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setDtBaseAmort(Integer value) {
        this.dtBaseAmort = value;
    }

    /**
     * Obtém o valor da propriedade dtPrimAmort.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtPrimAmort() {
        return dtPrimAmort;
    }

    /**
     * Define o valor da propriedade dtPrimAmort.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtPrimAmort(XMLGregorianCalendar value) {
        this.dtPrimAmort = value;
    }

    /**
     * Obtém o valor da propriedade infAmort.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInfAmort() {
        return infAmort;
    }

    /**
     * Define o valor da propriedade infAmort.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setInfAmort(String value) {
        this.infAmort = value;
    }

    /**
     * Obtém o valor da propriedade periodAmort.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getPeriodAmort() {
        return periodAmort;
    }

    /**
     * Define o valor da propriedade periodAmort.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setPeriodAmort(Integer value) {
        this.periodAmort = value;
    }

    /**
     * Obtém o valor da propriedade possuiMecAmort.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getPossuiMecAmort() {
        return possuiMecAmort;
    }

    /**
     * Define o valor da propriedade possuiMecAmort.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setPossuiMecAmort(YesNoIndicator value) {
        this.possuiMecAmort = value;
    }

}
