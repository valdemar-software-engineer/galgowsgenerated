package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Cota Subordinada do FIDC
 * <p>
 * <p>Classe Java de CotaSubordFIDCComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="CotaSubordFIDCComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentFidicOrigem" type="{http://www.stianbid.com.br/SchemaFundo}IdentFidcOrigemComplexType"/>
 *         &lt;element name="DadoSubord" type="{http://www.stianbid.com.br/SchemaFundo}DadoSubordFIDCComplexType" minOccurs="0"/>
 *         &lt;element name="DataSubord" type="{http://www.stianbid.com.br/SchemaFundo}DataSubordFIDCComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CotaSubordFIDCComplexType", propOrder = {"identFidicOrigem", "dadoSubord", "dataSubord"})
public class CotaSubordFIDCComplexType {

    @XmlElement(name = "IdentFidicOrigem", required = true) protected IdentFidcOrigemComplexType identFidicOrigem;
    @XmlElement(name = "DadoSubord") protected DadoSubordFIDCComplexType dadoSubord;
    @XmlElement(name = "DataSubord") protected DataSubordFIDCComplexType dataSubord;

    /**
     * Obtém o valor da propriedade dadoSubord.
     *
     * @return possible object is
     * {@link DadoSubordFIDCComplexType }
     */
    public DadoSubordFIDCComplexType getDadoSubord() {
        return dadoSubord;
    }

    /**
     * Define o valor da propriedade dadoSubord.
     *
     * @param value allowed object is
     *              {@link DadoSubordFIDCComplexType }
     */
    public void setDadoSubord(DadoSubordFIDCComplexType value) {
        this.dadoSubord = value;
    }

    /**
     * Obtém o valor da propriedade dataSubord.
     *
     * @return possible object is
     * {@link DataSubordFIDCComplexType }
     */
    public DataSubordFIDCComplexType getDataSubord() {
        return dataSubord;
    }

    /**
     * Define o valor da propriedade dataSubord.
     *
     * @param value allowed object is
     *              {@link DataSubordFIDCComplexType }
     */
    public void setDataSubord(DataSubordFIDCComplexType value) {
        this.dataSubord = value;
    }

    /**
     * Obtém o valor da propriedade identFidicOrigem.
     *
     * @return possible object is
     * {@link IdentFidcOrigemComplexType }
     */
    public IdentFidcOrigemComplexType getIdentFidicOrigem() {
        return identFidicOrigem;
    }

    /**
     * Define o valor da propriedade identFidicOrigem.
     *
     * @param value allowed object is
     *              {@link IdentFidcOrigemComplexType }
     */
    public void setIdentFidicOrigem(IdentFidcOrigemComplexType value) {
        this.identFidicOrigem = value;
    }

}
