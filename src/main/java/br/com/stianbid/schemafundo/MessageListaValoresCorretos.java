package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Lista de Valores para correção
 * <p>
 * Enviar a lista de campos necessários para atualização.
 * <p>
 * Exemplo:
 * <p>
 * Para inclusão de um novo registro na lista de múltiplos
 * valores (/listaValorCorreto/vlCorreto).
 * <p>
 * Valor: A (Já existe) – O Sistema irá manter este valor
 * já cadastrado.
 * <p>
 * B (Já existe) - O Sistema irá manter este valor já
 * cadastrado.
 * <p>
 * C (Valor novo) - O valor será incluído no sistema.
 * <p>
 * <p>
 * <p>
 * <p>
 * <p>
 * <p>
 * Para exclusão de um registro na lista de múltiplos
 * valores consirerando o exemplo acima.
 * <p>
 * <p>
 * Valor: A (Já existe)
 * <p>
 * C (Já existe)
 * <p>
 * Como o valor “B” nãofoi enviado será excluído.
 * <p>
 * <p>
 * <p>
 * <p>Classe Java de MessageListaValoresCorretos complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageListaValoresCorretos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vlCorreto" type="{http://www.stianbid.com.br/SchemaFundo}ValorCampoCorreto"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageListaValoresCorretos", propOrder = {"vlCorreto"})
public class MessageListaValoresCorretos {

    @XmlElement(required = true) protected String vlCorreto;

    /**
     * Obtém o valor da propriedade vlCorreto.
     *
     * @return possible object is
     * {@link String }
     */
    public String getVlCorreto() {
        return vlCorreto;
    }

    /**
     * Define o valor da propriedade vlCorreto.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setVlCorreto(String value) {
        this.vlCorreto = value;
    }

}
