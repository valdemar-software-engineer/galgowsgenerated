package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Consumir Fundos Correção.
 * <p>
 * <p>Classe Java de STIConsumirFundosCorrecaoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="STIConsumirFundosCorrecaoComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CdSti" type="{http://www.stianbid.com.br/SchemaFundo}STIIdentifier"/>
 *         &lt;element name="FundosCorrecao" type="{http://www.stianbid.com.br/SchemaFundo}MessageResponseConsumirFundosCorrecao" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dsObservacao" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "STIConsumirFundosCorrecaoComplexType", propOrder = {"cdSti", "fundosCorrecao", "dsObservacao"})
public class STIConsumirFundosCorrecaoComplexType {

    @XmlElement(name = "CdSti") protected int cdSti;
    @XmlElement(name = "FundosCorrecao") protected List<MessageResponseConsumirFundosCorrecao> fundosCorrecao;
    protected String dsObservacao;

    /**
     * Obtém o valor da propriedade cdSti.
     */
    public int getCdSti() {
        return cdSti;
    }

    /**
     * Define o valor da propriedade cdSti.
     */
    public void setCdSti(int value) {
        this.cdSti = value;
    }

    /**
     * Obtém o valor da propriedade dsObservacao.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDsObservacao() {
        return dsObservacao;
    }

    /**
     * Define o valor da propriedade dsObservacao.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDsObservacao(String value) {
        this.dsObservacao = value;
    }

    /**
     * Gets the value of the fundosCorrecao property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fundosCorrecao property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFundosCorrecao().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MessageResponseConsumirFundosCorrecao }
     */
    public List<MessageResponseConsumirFundosCorrecao> getFundosCorrecao() {
        if (fundosCorrecao == null) {
            fundosCorrecao = new ArrayList<MessageResponseConsumirFundosCorrecao>();
        }
        return this.fundosCorrecao;
    }

}
