package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dados da Subordinada
 * <p>
 * <p>Classe Java de DadoSubordFIDCComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="DadoSubordFIDCComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CdStiSubord" type="{http://www.stianbid.com.br/SchemaFundo}STIIdentifier" minOccurs="0"/>
 *         &lt;element name="NmSubord" type="{http://www.stianbid.com.br/Common}Max70Text"/>
 *         &lt;element name="NmFantSubImp" type="{http://www.stianbid.com.br/SchemaFundo}NomeFantasia" minOccurs="0"/>
 *         &lt;element name="Isin" type="{http://www.stianbid.com.br/Common}ISINIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DadoSubordFIDCComplexType", propOrder = {"cdStiSubord", "nmSubord", "nmFantSubImp", "isin"})
public class DadoSubordFIDCComplexType {

    @XmlElement(name = "CdStiSubord") protected Integer cdStiSubord;
    @XmlElement(name = "NmSubord", required = true) protected String nmSubord;
    @XmlElement(name = "NmFantSubImp") protected String nmFantSubImp;
    @XmlElement(name = "Isin") protected String isin;

    /**
     * Obtém o valor da propriedade cdStiSubord.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCdStiSubord() {
        return cdStiSubord;
    }

    /**
     * Define o valor da propriedade cdStiSubord.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCdStiSubord(Integer value) {
        this.cdStiSubord = value;
    }

    /**
     * Obtém o valor da propriedade isin.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIsin() {
        return isin;
    }

    /**
     * Define o valor da propriedade isin.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIsin(String value) {
        this.isin = value;
    }

    /**
     * Obtém o valor da propriedade nmFantSubImp.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNmFantSubImp() {
        return nmFantSubImp;
    }

    /**
     * Define o valor da propriedade nmFantSubImp.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNmFantSubImp(String value) {
        this.nmFantSubImp = value;
    }

    /**
     * Obtém o valor da propriedade nmSubord.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNmSubord() {
        return nmSubord;
    }

    /**
     * Define o valor da propriedade nmSubord.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNmSubord(String value) {
        this.nmSubord = value;
    }

}
