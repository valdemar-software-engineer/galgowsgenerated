package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dados do Gestor
 * <p>
 * <p>Classe Java de FundManagementCompanyComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="FundManagementCompanyComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Nm" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/>
 *         &lt;element name="FndMgrPstlAdr" type="{http://www.stianbid.com.br/SchemaFundo}FundManagerPostalAddressComplexType" minOccurs="0"/>
 *         &lt;element name="PhneNb" type="{http://www.stianbid.com.br/Common}PhoneNumber" minOccurs="0"/>
 *         &lt;element name="FaxNb" type="{http://www.stianbid.com.br/Common}PhoneNumber" minOccurs="0"/>
 *         &lt;element name="EmailAdr" type="{http://www.stianbid.com.br/Common}Max256Text" minOccurs="0"/>
 *         &lt;element name="URLAdr" type="{http://www.stianbid.com.br/Common}Max256Text" minOccurs="0"/>
 *         &lt;element name="Id" type="{http://www.stianbid.com.br/Common}BICIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FundManagementCompanyComplexType",
         propOrder = {"nm", "fndMgrPstlAdr", "phneNb", "faxNb", "emailAdr", "urlAdr", "id"})
public class FundManagementCompanyComplexType {

    @XmlElement(name = "Nm") protected String nm;
    @XmlElement(name = "FndMgrPstlAdr") protected FundManagerPostalAddressComplexType fndMgrPstlAdr;
    @XmlElement(name = "PhneNb") protected String phneNb;
    @XmlElement(name = "FaxNb") protected String faxNb;
    @XmlElement(name = "EmailAdr") protected String emailAdr;
    @XmlElement(name = "URLAdr") protected String urlAdr;
    @XmlElement(name = "Id") protected String id;

    /**
     * Obtém o valor da propriedade emailAdr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getEmailAdr() {
        return emailAdr;
    }

    /**
     * Define o valor da propriedade emailAdr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setEmailAdr(String value) {
        this.emailAdr = value;
    }

    /**
     * Obtém o valor da propriedade faxNb.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFaxNb() {
        return faxNb;
    }

    /**
     * Define o valor da propriedade faxNb.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFaxNb(String value) {
        this.faxNb = value;
    }

    /**
     * Obtém o valor da propriedade fndMgrPstlAdr.
     *
     * @return possible object is
     * {@link FundManagerPostalAddressComplexType }
     */
    public FundManagerPostalAddressComplexType getFndMgrPstlAdr() {
        return fndMgrPstlAdr;
    }

    /**
     * Define o valor da propriedade fndMgrPstlAdr.
     *
     * @param value allowed object is
     *              {@link FundManagerPostalAddressComplexType }
     */
    public void setFndMgrPstlAdr(FundManagerPostalAddressComplexType value) {
        this.fndMgrPstlAdr = value;
    }

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link String }
     */
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade nm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNm() {
        return nm;
    }

    /**
     * Define o valor da propriedade nm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNm(String value) {
        this.nm = value;
    }

    /**
     * Obtém o valor da propriedade phneNb.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPhneNb() {
        return phneNb;
    }

    /**
     * Define o valor da propriedade phneNb.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPhneNb(String value) {
        this.phneNb = value;
    }

    /**
     * Obtém o valor da propriedade urlAdr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getURLAdr() {
        return urlAdr;
    }

    /**
     * Define o valor da propriedade urlAdr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setURLAdr(String value) {
        this.urlAdr = value;
    }

}
