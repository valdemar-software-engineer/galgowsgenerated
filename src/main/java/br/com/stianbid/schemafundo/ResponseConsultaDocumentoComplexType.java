package br.com.stianbid.schemafundo;

import br.com.stianbid.common.MessageBatchComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Consultar Documentos.
 * <p>
 * <p>Classe Java de ResponseConsultaDocumentoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ResponseConsultaDocumentoComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageBatchComplexType">
 *       &lt;sequence>
 *         &lt;element name="Documentos" type="{http://www.stianbid.com.br/SchemaFundo}DocBinaryComplexType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseConsultaDocumentoComplexType", propOrder = {"documentos"})
public class ResponseConsultaDocumentoComplexType extends MessageBatchComplexType {

    @XmlElement(name = "Documentos") protected List<DocBinaryComplexType> documentos;

    /**
     * Gets the value of the documentos property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentos property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentos().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocBinaryComplexType }
     */
    public List<DocBinaryComplexType> getDocumentos() {
        if (documentos == null) {
            documentos = new ArrayList<DocBinaryComplexType>();
        }
        return this.documentos;
    }

}
