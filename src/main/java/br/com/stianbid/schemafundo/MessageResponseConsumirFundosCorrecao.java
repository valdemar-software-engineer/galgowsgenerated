package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Mensagem de Retorno de Fundos Correção.
 * <p>
 * <p>
 * <p>Classe Java de MessageResponseConsumirFundosCorrecao complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageResponseConsumirFundosCorrecao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idCampo" type="{http://www.stianbid.com.br/SchemaFundo}CampoIdentifier"/>
 *         &lt;element name="nmCampo" type="{http://www.stianbid.com.br/SchemaFundo}NomeCampo"/>
 *         &lt;element name="dsJustificativa" type="{http://www.stianbid.com.br/SchemaFundo}DescricaoJustificativa" minOccurs="0"/>
 *         &lt;element name="listaItensCodAutoReg" type="{http://www.stianbid.com.br/SchemaFundo}RevCodAutoRegComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageResponseConsumirFundosCorrecao",
         propOrder = {"idCampo", "nmCampo", "dsJustificativa", "listaItensCodAutoReg"})
public class MessageResponseConsumirFundosCorrecao {

    protected int idCampo;
    @XmlElement(required = true) protected String nmCampo;
    protected String dsJustificativa;
    protected RevCodAutoRegComplexType listaItensCodAutoReg;

    /**
     * Obtém o valor da propriedade dsJustificativa.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDsJustificativa() {
        return dsJustificativa;
    }

    /**
     * Define o valor da propriedade dsJustificativa.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDsJustificativa(String value) {
        this.dsJustificativa = value;
    }

    /**
     * Obtém o valor da propriedade idCampo.
     */
    public int getIdCampo() {
        return idCampo;
    }

    /**
     * Define o valor da propriedade idCampo.
     */
    public void setIdCampo(int value) {
        this.idCampo = value;
    }

    /**
     * Obtém o valor da propriedade listaItensCodAutoReg.
     *
     * @return possible object is
     * {@link RevCodAutoRegComplexType }
     */
    public RevCodAutoRegComplexType getListaItensCodAutoReg() {
        return listaItensCodAutoReg;
    }

    /**
     * Define o valor da propriedade listaItensCodAutoReg.
     *
     * @param value allowed object is
     *              {@link RevCodAutoRegComplexType }
     */
    public void setListaItensCodAutoReg(RevCodAutoRegComplexType value) {
        this.listaItensCodAutoReg = value;
    }

    /**
     * Obtém o valor da propriedade nmCampo.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNmCampo() {
        return nmCampo;
    }

    /**
     * Define o valor da propriedade nmCampo.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNmCampo(String value) {
        this.nmCampo = value;
    }

}
