package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Prestadores de Serviço
 * <p>
 * <p>Classe Java de PrestServicoIdentification4ComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PrestServicoIdentification4ComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="CNPJ" type="{http://www.stianbid.com.br/SchemaFundo}CNPJIdentifier" minOccurs="0"/>
 *           &lt;element name="CPF" type="{http://www.stianbid.com.br/SchemaFundo}CPFIdentifier" minOccurs="0"/>
 *           &lt;element name="IdentifEstrang" type="{http://www.stianbid.com.br/Common}Max35Text" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrestServicoIdentification4ComplexType", propOrder = {"cnpj", "cpf", "identifEstrang"})
public class PrestServicoIdentification4ComplexType {

    @XmlElement(name = "CNPJ") protected Long cnpj;
    @XmlElement(name = "CPF") protected Long cpf;
    @XmlElement(name = "IdentifEstrang") protected String identifEstrang;

    /**
     * Obtém o valor da propriedade cnpj.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getCNPJ() {
        return cnpj;
    }

    /**
     * Define o valor da propriedade cnpj.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setCNPJ(Long value) {
        this.cnpj = value;
    }

    /**
     * Obtém o valor da propriedade cpf.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getCPF() {
        return cpf;
    }

    /**
     * Define o valor da propriedade cpf.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setCPF(Long value) {
        this.cpf = value;
    }

    /**
     * Obtém o valor da propriedade identifEstrang.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIdentifEstrang() {
        return identifEstrang;
    }

    /**
     * Define o valor da propriedade identifEstrang.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIdentifEstrang(String value) {
        this.identifEstrang = value;
    }

}
