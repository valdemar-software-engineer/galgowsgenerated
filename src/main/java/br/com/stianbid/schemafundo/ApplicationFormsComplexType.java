package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Application Forms (Campo ISO não utilizado no momento)
 * <p>
 * <p>
 * <p>Classe Java de ApplicationFormsComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ApplicationFormsComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplReqrdInitlSbcpt" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="ApplReqrdSubsqSbcpt" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="SgntrReqrdInitlSbcpt" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="SgntrReqrdSubsqSbcpt" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="RedForm" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="SgntrReqrdRed" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationFormsComplexType",
         propOrder = {"applReqrdInitlSbcpt", "applReqrdSubsqSbcpt", "sgntrReqrdInitlSbcpt", "sgntrReqrdSubsqSbcpt", "redForm", "sgntrReqrdRed"})
public class ApplicationFormsComplexType {

    @XmlElement(name = "ApplReqrdInitlSbcpt") protected YesNoIndicator applReqrdInitlSbcpt;
    @XmlElement(name = "ApplReqrdSubsqSbcpt") protected YesNoIndicator applReqrdSubsqSbcpt;
    @XmlElement(name = "SgntrReqrdInitlSbcpt") protected YesNoIndicator sgntrReqrdInitlSbcpt;
    @XmlElement(name = "SgntrReqrdSubsqSbcpt") protected YesNoIndicator sgntrReqrdSubsqSbcpt;
    @XmlElement(name = "RedForm") protected YesNoIndicator redForm;
    @XmlElement(name = "SgntrReqrdRed") protected YesNoIndicator sgntrReqrdRed;

    /**
     * Obtém o valor da propriedade applReqrdInitlSbcpt.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getApplReqrdInitlSbcpt() {
        return applReqrdInitlSbcpt;
    }

    /**
     * Define o valor da propriedade applReqrdInitlSbcpt.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setApplReqrdInitlSbcpt(YesNoIndicator value) {
        this.applReqrdInitlSbcpt = value;
    }

    /**
     * Obtém o valor da propriedade applReqrdSubsqSbcpt.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getApplReqrdSubsqSbcpt() {
        return applReqrdSubsqSbcpt;
    }

    /**
     * Define o valor da propriedade applReqrdSubsqSbcpt.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setApplReqrdSubsqSbcpt(YesNoIndicator value) {
        this.applReqrdSubsqSbcpt = value;
    }

    /**
     * Obtém o valor da propriedade redForm.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getRedForm() {
        return redForm;
    }

    /**
     * Define o valor da propriedade redForm.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setRedForm(YesNoIndicator value) {
        this.redForm = value;
    }

    /**
     * Obtém o valor da propriedade sgntrReqrdInitlSbcpt.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getSgntrReqrdInitlSbcpt() {
        return sgntrReqrdInitlSbcpt;
    }

    /**
     * Define o valor da propriedade sgntrReqrdInitlSbcpt.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setSgntrReqrdInitlSbcpt(YesNoIndicator value) {
        this.sgntrReqrdInitlSbcpt = value;
    }

    /**
     * Obtém o valor da propriedade sgntrReqrdRed.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getSgntrReqrdRed() {
        return sgntrReqrdRed;
    }

    /**
     * Define o valor da propriedade sgntrReqrdRed.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setSgntrReqrdRed(YesNoIndicator value) {
        this.sgntrReqrdRed = value;
    }

    /**
     * Obtém o valor da propriedade sgntrReqrdSubsqSbcpt.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getSgntrReqrdSubsqSbcpt() {
        return sgntrReqrdSubsqSbcpt;
    }

    /**
     * Define o valor da propriedade sgntrReqrdSubsqSbcpt.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setSgntrReqrdSubsqSbcpt(YesNoIndicator value) {
        this.sgntrReqrdSubsqSbcpt = value;
    }

}
