package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Alterações em Taxas
 * <p>
 * <p>Classe Java de AltTaxasComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="AltTaxasComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DtTxAdm" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtTxPerf" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtTxEntr" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtTxSaida" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AltTaxasComplexType", propOrder = {"dtTxAdm", "dtTxPerf", "dtTxEntr", "dtTxSaida"})
public class AltTaxasComplexType {

    @XmlElement(name = "DtTxAdm") protected XMLGregorianCalendar dtTxAdm;
    @XmlElement(name = "DtTxPerf") protected XMLGregorianCalendar dtTxPerf;
    @XmlElement(name = "DtTxEntr") protected XMLGregorianCalendar dtTxEntr;
    @XmlElement(name = "DtTxSaida") protected XMLGregorianCalendar dtTxSaida;

    /**
     * Obtém o valor da propriedade dtTxAdm.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtTxAdm() {
        return dtTxAdm;
    }

    /**
     * Define o valor da propriedade dtTxAdm.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtTxAdm(XMLGregorianCalendar value) {
        this.dtTxAdm = value;
    }

    /**
     * Obtém o valor da propriedade dtTxEntr.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtTxEntr() {
        return dtTxEntr;
    }

    /**
     * Define o valor da propriedade dtTxEntr.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtTxEntr(XMLGregorianCalendar value) {
        this.dtTxEntr = value;
    }

    /**
     * Obtém o valor da propriedade dtTxPerf.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtTxPerf() {
        return dtTxPerf;
    }

    /**
     * Define o valor da propriedade dtTxPerf.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtTxPerf(XMLGregorianCalendar value) {
        this.dtTxPerf = value;
    }

    /**
     * Obtém o valor da propriedade dtTxSaida.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtTxSaida() {
        return dtTxSaida;
    }

    /**
     * Define o valor da propriedade dtTxSaida.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtTxSaida(XMLGregorianCalendar value) {
        this.dtTxSaida = value;
    }

}
