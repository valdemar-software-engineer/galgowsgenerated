package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Prestador de Serviço
 * <p>
 * <p>Classe Java de PrestServicoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PrestServicoComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Administrador" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentification2ComplexType"/>
 *         &lt;element name="AgClassRisco" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="Auditoria" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="Cons" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Contab" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="ControlAtivo" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType"/>
 *         &lt;element name="ControlPass" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType"/>
 *         &lt;element name="CoordLider" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="CustCota" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="CustDeriv" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="CustRF" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="CustRV" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="CustUnico" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="Distribuidor" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="EmpAvRisco" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="EscCotas" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="GestCota" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentification3ComplexType" minOccurs="0"/>
 *         &lt;element name="GestDeriv" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentification3ComplexType" minOccurs="0"/>
 *         &lt;element name="GestRF" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentification3ComplexType" minOccurs="0"/>
 *         &lt;element name="GestRV" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentification3ComplexType" minOccurs="0"/>
 *         &lt;element name="GestUnico" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentification4ComplexType" minOccurs="0"/>
 *         &lt;element name="InfoPlCota" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="InfoAtivos" type="{http://www.stianbid.com.br/SchemaFundo}PrestServicoIdentificationComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrestServicoComplexType",
         propOrder = {"administrador", "agClassRisco", "auditoria", "cons", "contab", "controlAtivo", "controlPass", "coordLider", "custCota", "custDeriv", "custRF", "custRV", "custUnico", "distribuidor", "empAvRisco", "escCotas", "gestCota", "gestDeriv", "gestRF", "gestRV", "gestUnico", "infoPlCota", "infoAtivos"})
public class PrestServicoComplexType {

    @XmlElement(name = "Administrador", required = true) protected PrestServicoIdentification2ComplexType administrador;
    @XmlElement(name = "AgClassRisco") protected PrestServicoIdentificationComplexType agClassRisco;
    @XmlElement(name = "Auditoria") protected PrestServicoIdentificationComplexType auditoria;
    @XmlElement(name = "Cons") protected List<PrestServicoIdentificationComplexType> cons;
    @XmlElement(name = "Contab") protected PrestServicoIdentificationComplexType contab;
    @XmlElement(name = "ControlAtivo", required = true) protected PrestServicoIdentificationComplexType controlAtivo;
    @XmlElement(name = "ControlPass", required = true) protected PrestServicoIdentificationComplexType controlPass;
    @XmlElement(name = "CoordLider") protected PrestServicoIdentificationComplexType coordLider;
    @XmlElement(name = "CustCota") protected PrestServicoIdentificationComplexType custCota;
    @XmlElement(name = "CustDeriv") protected PrestServicoIdentificationComplexType custDeriv;
    @XmlElement(name = "CustRF") protected PrestServicoIdentificationComplexType custRF;
    @XmlElement(name = "CustRV") protected PrestServicoIdentificationComplexType custRV;
    @XmlElement(name = "CustUnico") protected PrestServicoIdentificationComplexType custUnico;
    @XmlElement(name = "Distribuidor") protected List<PrestServicoIdentificationComplexType> distribuidor;
    @XmlElement(name = "EmpAvRisco") protected PrestServicoIdentificationComplexType empAvRisco;
    @XmlElement(name = "EscCotas") protected PrestServicoIdentificationComplexType escCotas;
    @XmlElement(name = "GestCota") protected PrestServicoIdentification3ComplexType gestCota;
    @XmlElement(name = "GestDeriv") protected PrestServicoIdentification3ComplexType gestDeriv;
    @XmlElement(name = "GestRF") protected PrestServicoIdentification3ComplexType gestRF;
    @XmlElement(name = "GestRV") protected PrestServicoIdentification3ComplexType gestRV;
    @XmlElement(name = "GestUnico") protected PrestServicoIdentification4ComplexType gestUnico;
    @XmlElement(name = "InfoPlCota") protected PrestServicoIdentificationComplexType infoPlCota;
    @XmlElement(name = "InfoAtivos") protected PrestServicoIdentificationComplexType infoAtivos;

    /**
     * Obtém o valor da propriedade administrador.
     *
     * @return possible object is
     * {@link PrestServicoIdentification2ComplexType }
     */
    public PrestServicoIdentification2ComplexType getAdministrador() {
        return administrador;
    }

    /**
     * Define o valor da propriedade administrador.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentification2ComplexType }
     */
    public void setAdministrador(PrestServicoIdentification2ComplexType value) {
        this.administrador = value;
    }

    /**
     * Obtém o valor da propriedade agClassRisco.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getAgClassRisco() {
        return agClassRisco;
    }

    /**
     * Define o valor da propriedade agClassRisco.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setAgClassRisco(PrestServicoIdentificationComplexType value) {
        this.agClassRisco = value;
    }

    /**
     * Obtém o valor da propriedade auditoria.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getAuditoria() {
        return auditoria;
    }

    /**
     * Define o valor da propriedade auditoria.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setAuditoria(PrestServicoIdentificationComplexType value) {
        this.auditoria = value;
    }

    /**
     * Gets the value of the cons property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cons property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCons().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrestServicoIdentificationComplexType }
     */
    public List<PrestServicoIdentificationComplexType> getCons() {
        if (cons == null) {
            cons = new ArrayList<PrestServicoIdentificationComplexType>();
        }
        return this.cons;
    }

    /**
     * Obtém o valor da propriedade contab.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getContab() {
        return contab;
    }

    /**
     * Define o valor da propriedade contab.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setContab(PrestServicoIdentificationComplexType value) {
        this.contab = value;
    }

    /**
     * Obtém o valor da propriedade controlAtivo.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getControlAtivo() {
        return controlAtivo;
    }

    /**
     * Define o valor da propriedade controlAtivo.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setControlAtivo(PrestServicoIdentificationComplexType value) {
        this.controlAtivo = value;
    }

    /**
     * Obtém o valor da propriedade controlPass.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getControlPass() {
        return controlPass;
    }

    /**
     * Define o valor da propriedade controlPass.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setControlPass(PrestServicoIdentificationComplexType value) {
        this.controlPass = value;
    }

    /**
     * Obtém o valor da propriedade coordLider.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getCoordLider() {
        return coordLider;
    }

    /**
     * Define o valor da propriedade coordLider.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setCoordLider(PrestServicoIdentificationComplexType value) {
        this.coordLider = value;
    }

    /**
     * Obtém o valor da propriedade custCota.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getCustCota() {
        return custCota;
    }

    /**
     * Define o valor da propriedade custCota.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setCustCota(PrestServicoIdentificationComplexType value) {
        this.custCota = value;
    }

    /**
     * Obtém o valor da propriedade custDeriv.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getCustDeriv() {
        return custDeriv;
    }

    /**
     * Define o valor da propriedade custDeriv.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setCustDeriv(PrestServicoIdentificationComplexType value) {
        this.custDeriv = value;
    }

    /**
     * Obtém o valor da propriedade custRF.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getCustRF() {
        return custRF;
    }

    /**
     * Define o valor da propriedade custRF.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setCustRF(PrestServicoIdentificationComplexType value) {
        this.custRF = value;
    }

    /**
     * Obtém o valor da propriedade custRV.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getCustRV() {
        return custRV;
    }

    /**
     * Define o valor da propriedade custRV.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setCustRV(PrestServicoIdentificationComplexType value) {
        this.custRV = value;
    }

    /**
     * Obtém o valor da propriedade custUnico.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getCustUnico() {
        return custUnico;
    }

    /**
     * Define o valor da propriedade custUnico.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setCustUnico(PrestServicoIdentificationComplexType value) {
        this.custUnico = value;
    }

    /**
     * Gets the value of the distribuidor property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the distribuidor property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDistribuidor().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrestServicoIdentificationComplexType }
     */
    public List<PrestServicoIdentificationComplexType> getDistribuidor() {
        if (distribuidor == null) {
            distribuidor = new ArrayList<PrestServicoIdentificationComplexType>();
        }
        return this.distribuidor;
    }

    /**
     * Obtém o valor da propriedade empAvRisco.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getEmpAvRisco() {
        return empAvRisco;
    }

    /**
     * Define o valor da propriedade empAvRisco.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setEmpAvRisco(PrestServicoIdentificationComplexType value) {
        this.empAvRisco = value;
    }

    /**
     * Obtém o valor da propriedade escCotas.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getEscCotas() {
        return escCotas;
    }

    /**
     * Define o valor da propriedade escCotas.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setEscCotas(PrestServicoIdentificationComplexType value) {
        this.escCotas = value;
    }

    /**
     * Obtém o valor da propriedade gestCota.
     *
     * @return possible object is
     * {@link PrestServicoIdentification3ComplexType }
     */
    public PrestServicoIdentification3ComplexType getGestCota() {
        return gestCota;
    }

    /**
     * Define o valor da propriedade gestCota.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentification3ComplexType }
     */
    public void setGestCota(PrestServicoIdentification3ComplexType value) {
        this.gestCota = value;
    }

    /**
     * Obtém o valor da propriedade gestDeriv.
     *
     * @return possible object is
     * {@link PrestServicoIdentification3ComplexType }
     */
    public PrestServicoIdentification3ComplexType getGestDeriv() {
        return gestDeriv;
    }

    /**
     * Define o valor da propriedade gestDeriv.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentification3ComplexType }
     */
    public void setGestDeriv(PrestServicoIdentification3ComplexType value) {
        this.gestDeriv = value;
    }

    /**
     * Obtém o valor da propriedade gestRF.
     *
     * @return possible object is
     * {@link PrestServicoIdentification3ComplexType }
     */
    public PrestServicoIdentification3ComplexType getGestRF() {
        return gestRF;
    }

    /**
     * Define o valor da propriedade gestRF.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentification3ComplexType }
     */
    public void setGestRF(PrestServicoIdentification3ComplexType value) {
        this.gestRF = value;
    }

    /**
     * Obtém o valor da propriedade gestRV.
     *
     * @return possible object is
     * {@link PrestServicoIdentification3ComplexType }
     */
    public PrestServicoIdentification3ComplexType getGestRV() {
        return gestRV;
    }

    /**
     * Define o valor da propriedade gestRV.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentification3ComplexType }
     */
    public void setGestRV(PrestServicoIdentification3ComplexType value) {
        this.gestRV = value;
    }

    /**
     * Obtém o valor da propriedade gestUnico.
     *
     * @return possible object is
     * {@link PrestServicoIdentification4ComplexType }
     */
    public PrestServicoIdentification4ComplexType getGestUnico() {
        return gestUnico;
    }

    /**
     * Define o valor da propriedade gestUnico.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentification4ComplexType }
     */
    public void setGestUnico(PrestServicoIdentification4ComplexType value) {
        this.gestUnico = value;
    }

    /**
     * Obtém o valor da propriedade infoAtivos.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getInfoAtivos() {
        return infoAtivos;
    }

    /**
     * Define o valor da propriedade infoAtivos.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setInfoAtivos(PrestServicoIdentificationComplexType value) {
        this.infoAtivos = value;
    }

    /**
     * Obtém o valor da propriedade infoPlCota.
     *
     * @return possible object is
     * {@link PrestServicoIdentificationComplexType }
     */
    public PrestServicoIdentificationComplexType getInfoPlCota() {
        return infoPlCota;
    }

    /**
     * Define o valor da propriedade infoPlCota.
     *
     * @param value allowed object is
     *              {@link PrestServicoIdentificationComplexType }
     */
    public void setInfoPlCota(PrestServicoIdentificationComplexType value) {
        this.infoPlCota = value;
    }

}
