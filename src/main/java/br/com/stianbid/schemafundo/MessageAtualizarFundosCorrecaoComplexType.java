package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Mensagem Atualizar Fundos para Correção.
 * <p>
 * <p>
 * <p>Classe Java de MessageAtualizarFundosCorrecaoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageAtualizarFundosCorrecaoComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idCampo" type="{http://www.stianbid.com.br/SchemaFundo}CampoIdentifier"/>
 *         &lt;choice>
 *           &lt;element name="listaValorCorreto" type="{http://www.stianbid.com.br/SchemaFundo}MessageListaValoresCorretos" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="Documentos" type="{http://www.stianbid.com.br/SchemaFundo}ListaEnvDocAtuComplexType" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageAtualizarFundosCorrecaoComplexType", propOrder = {"idCampo", "listaValorCorreto", "documentos"})
public class MessageAtualizarFundosCorrecaoComplexType {

    protected int idCampo;
    protected List<MessageListaValoresCorretos> listaValorCorreto;
    @XmlElement(name = "Documentos") protected ListaEnvDocAtuComplexType documentos;

    /**
     * Obtém o valor da propriedade documentos.
     *
     * @return possible object is
     * {@link ListaEnvDocAtuComplexType }
     */
    public ListaEnvDocAtuComplexType getDocumentos() {
        return documentos;
    }

    /**
     * Define o valor da propriedade documentos.
     *
     * @param value allowed object is
     *              {@link ListaEnvDocAtuComplexType }
     */
    public void setDocumentos(ListaEnvDocAtuComplexType value) {
        this.documentos = value;
    }

    /**
     * Obtém o valor da propriedade idCampo.
     */
    public int getIdCampo() {
        return idCampo;
    }

    /**
     * Define o valor da propriedade idCampo.
     */
    public void setIdCampo(int value) {
        this.idCampo = value;
    }

    /**
     * Gets the value of the listaValorCorreto property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaValorCorreto property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaValorCorreto().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MessageListaValoresCorretos }
     */
    public List<MessageListaValoresCorretos> getListaValorCorreto() {
        if (listaValorCorreto == null) {
            listaValorCorreto = new ArrayList<MessageListaValoresCorretos>();
        }
        return this.listaValorCorreto;
    }

}
