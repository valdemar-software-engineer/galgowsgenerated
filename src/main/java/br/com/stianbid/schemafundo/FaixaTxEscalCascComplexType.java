package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * Faixas para Taxa Escalonada ou Cascata
 * <p>
 * <p>
 * <p>Classe Java de FaixaTxEscalCascComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="FaixaTxEscalCascComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VlIniPl" type="{http://www.stianbid.com.br/SchemaFundo}DecimalNumber_15_2" minOccurs="0"/>
 *         &lt;element name="VlFimPl" type="{http://www.stianbid.com.br/SchemaFundo}DecimalNumber_15_2" minOccurs="0"/>
 *         &lt;element name="TxFaixa" type="{http://www.stianbid.com.br/SchemaFundo}DecimalNumber_17_4" minOccurs="0"/>
 *         &lt;element name="UnitTxFaixa" type="{http://www.stianbid.com.br/SchemaFundo}UnitTxFaixaCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaixaTxEscalCascComplexType", propOrder = {"vlIniPl", "vlFimPl", "txFaixa", "unitTxFaixa"})
public class FaixaTxEscalCascComplexType {

    @XmlElement(name = "VlIniPl") protected BigDecimal vlIniPl;
    @XmlElement(name = "VlFimPl") protected BigDecimal vlFimPl;
    @XmlElement(name = "TxFaixa") protected BigDecimal txFaixa;
    @XmlElement(name = "UnitTxFaixa") protected Integer unitTxFaixa;

    /**
     * Obtém o valor da propriedade txFaixa.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getTxFaixa() {
        return txFaixa;
    }

    /**
     * Define o valor da propriedade txFaixa.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setTxFaixa(BigDecimal value) {
        this.txFaixa = value;
    }

    /**
     * Obtém o valor da propriedade unitTxFaixa.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getUnitTxFaixa() {
        return unitTxFaixa;
    }

    /**
     * Define o valor da propriedade unitTxFaixa.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setUnitTxFaixa(Integer value) {
        this.unitTxFaixa = value;
    }

    /**
     * Obtém o valor da propriedade vlFimPl.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getVlFimPl() {
        return vlFimPl;
    }

    /**
     * Define o valor da propriedade vlFimPl.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setVlFimPl(BigDecimal value) {
        this.vlFimPl = value;
    }

    /**
     * Obtém o valor da propriedade vlIniPl.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getVlIniPl() {
        return vlIniPl;
    }

    /**
     * Define o valor da propriedade vlIniPl.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setVlIniPl(BigDecimal value) {
        this.vlIniPl = value;
    }

}
