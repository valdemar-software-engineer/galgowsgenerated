package br.com.stianbid.schemafundo;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Alteracao do Fundos de Investimentos
 * <p>
 * <p>
 * <p>Classe Java de RequestAlterarFundoComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="RequestAlterarFundoComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType">
 *       &lt;sequence>
 *         &lt;element name="Alterafundo" type="{http://www.stianbid.com.br/SchemaFundo}MessageFundoComplexType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestAlterarFundoComplexType", propOrder = {"alterafundo"})
public class RequestAlterarFundoComplexType extends MessageRequestComplexType {

    @XmlElement(name = "Alterafundo", required = true) protected List<MessageFundoComplexType> alterafundo;

    /**
     * Gets the value of the alterafundo property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alterafundo property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlterafundo().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MessageFundoComplexType }
     */
    public List<MessageFundoComplexType> getAlterafundo() {
        if (alterafundo == null) {
            alterafundo = new ArrayList<MessageFundoComplexType>();
        }
        return this.alterafundo;
    }

}
