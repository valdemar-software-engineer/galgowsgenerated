package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Alterações nos Documentos
 * <p>
 * <p>Classe Java de AltDoctosComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="AltDoctosComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AltAnaliseProsp" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="AltImpacBaseAnbid" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DocAntAta" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DocAntCartaIsen" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DocAntContCessao" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DocAntProsp" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DocAntRegul" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DocAntSumRating" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DocAntCompPagto" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AltDoctosComplexType",
         propOrder = {"altAnaliseProsp", "altImpacBaseAnbid", "docAntAta", "docAntCartaIsen", "docAntContCessao", "docAntProsp", "docAntRegul", "docAntSumRating", "docAntCompPagto"})
public class AltDoctosComplexType {

    @XmlElement(name = "AltAnaliseProsp") protected YesNoIndicator altAnaliseProsp;
    @XmlElement(name = "AltImpacBaseAnbid") protected YesNoIndicator altImpacBaseAnbid;
    @XmlElement(name = "DocAntAta") protected YesNoIndicator docAntAta;
    @XmlElement(name = "DocAntCartaIsen") protected YesNoIndicator docAntCartaIsen;
    @XmlElement(name = "DocAntContCessao") protected YesNoIndicator docAntContCessao;
    @XmlElement(name = "DocAntProsp") protected YesNoIndicator docAntProsp;
    @XmlElement(name = "DocAntRegul") protected YesNoIndicator docAntRegul;
    @XmlElement(name = "DocAntSumRating") protected YesNoIndicator docAntSumRating;
    @XmlElement(name = "DocAntCompPagto") protected YesNoIndicator docAntCompPagto;

    /**
     * Obtém o valor da propriedade altAnaliseProsp.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getAltAnaliseProsp() {
        return altAnaliseProsp;
    }

    /**
     * Define o valor da propriedade altAnaliseProsp.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setAltAnaliseProsp(YesNoIndicator value) {
        this.altAnaliseProsp = value;
    }

    /**
     * Obtém o valor da propriedade altImpacBaseAnbid.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getAltImpacBaseAnbid() {
        return altImpacBaseAnbid;
    }

    /**
     * Define o valor da propriedade altImpacBaseAnbid.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setAltImpacBaseAnbid(YesNoIndicator value) {
        this.altImpacBaseAnbid = value;
    }

    /**
     * Obtém o valor da propriedade docAntAta.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getDocAntAta() {
        return docAntAta;
    }

    /**
     * Define o valor da propriedade docAntAta.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setDocAntAta(YesNoIndicator value) {
        this.docAntAta = value;
    }

    /**
     * Obtém o valor da propriedade docAntCartaIsen.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getDocAntCartaIsen() {
        return docAntCartaIsen;
    }

    /**
     * Define o valor da propriedade docAntCartaIsen.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setDocAntCartaIsen(YesNoIndicator value) {
        this.docAntCartaIsen = value;
    }

    /**
     * Obtém o valor da propriedade docAntCompPagto.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getDocAntCompPagto() {
        return docAntCompPagto;
    }

    /**
     * Define o valor da propriedade docAntCompPagto.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setDocAntCompPagto(YesNoIndicator value) {
        this.docAntCompPagto = value;
    }

    /**
     * Obtém o valor da propriedade docAntContCessao.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getDocAntContCessao() {
        return docAntContCessao;
    }

    /**
     * Define o valor da propriedade docAntContCessao.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setDocAntContCessao(YesNoIndicator value) {
        this.docAntContCessao = value;
    }

    /**
     * Obtém o valor da propriedade docAntProsp.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getDocAntProsp() {
        return docAntProsp;
    }

    /**
     * Define o valor da propriedade docAntProsp.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setDocAntProsp(YesNoIndicator value) {
        this.docAntProsp = value;
    }

    /**
     * Obtém o valor da propriedade docAntRegul.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getDocAntRegul() {
        return docAntRegul;
    }

    /**
     * Define o valor da propriedade docAntRegul.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setDocAntRegul(YesNoIndicator value) {
        this.docAntRegul = value;
    }

    /**
     * Obtém o valor da propriedade docAntSumRating.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getDocAntSumRating() {
        return docAntSumRating;
    }

    /**
     * Define o valor da propriedade docAntSumRating.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setDocAntSumRating(YesNoIndicator value) {
        this.docAntSumRating = value;
    }

}
