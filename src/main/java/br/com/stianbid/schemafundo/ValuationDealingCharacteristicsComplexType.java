package br.com.stianbid.schemafundo;

import br.com.stianbid.common.ActiveCurrencyAndAmount;
import br.com.stianbid.common.EventFrequency2Code;
import br.com.stianbid.common.PriceMethod1Code;
import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Valuation Dealing Characteristics
 * <p>
 * <p>
 * <p>Classe Java de ValuationDealingCharacteristicsComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ValuationDealingCharacteristicsComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ValtnFrqcy" type="{http://www.stianbid.com.br/Common}EventFrequency2Code" minOccurs="0"/>
 *         &lt;element name="ValtnFrqcyDesc" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/>
 *         &lt;element name="SbcptDealgFrqcy" type="{http://www.stianbid.com.br/Common}EventFrequency2Code" minOccurs="0"/>
 *         &lt;element name="SbcptDealFrqcyDesc" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/>
 *         &lt;element name="LtdSbcptPrd" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/>
 *         &lt;element name="InitlSbcptMinVal" type="{http://www.stianbid.com.br/Common}ActiveCurrencyAndAmount" minOccurs="0"/>
 *         &lt;element name="InitlSbcptMinUnits" type="{http://www.stianbid.com.br/Common}DecimalNumber" minOccurs="0"/>
 *         &lt;element name="SbsqntSbcptMinVal" type="{http://www.stianbid.com.br/Common}ActiveCurrencyAndAmount" minOccurs="0"/>
 *         &lt;element name="SbsqntSbcptMinUnits" type="{http://www.stianbid.com.br/Common}DecimalNumber" minOccurs="0"/>
 *         &lt;element name="SbcptSttlmCycl" type="{http://www.stianbid.com.br/Common}Number"/>
 *         &lt;element name="RedDealgFrqcy" type="{http://www.stianbid.com.br/Common}EventFrequency2Code" minOccurs="0"/>
 *         &lt;element name="RedDealFrqcyDesc" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/>
 *         &lt;element name="LtdRedPrd" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/>
 *         &lt;element name="RedSttlmCycl" type="{http://www.stianbid.com.br/Common}Number" minOccurs="0"/>
 *         &lt;element name="DcmlstnPric" type="{http://www.stianbid.com.br/Common}Number" minOccurs="0"/>
 *         &lt;element name="DcmlstnUnits" type="{http://www.stianbid.com.br/Common}Number" minOccurs="0"/>
 *         &lt;element name="DualFndInd" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="PricMtd" type="{http://www.stianbid.com.br/Common}PriceMethod1Code" minOccurs="0"/>
 *         &lt;element name="PricCcy" type="{http://www.stianbid.com.br/Common}ActiveCurrencyCode" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValuationDealingCharacteristicsComplexType",
         propOrder = {"valtnFrqcy", "valtnFrqcyDesc", "sbcptDealgFrqcy", "sbcptDealFrqcyDesc", "ltdSbcptPrd", "initlSbcptMinVal", "initlSbcptMinUnits", "sbsqntSbcptMinVal", "sbsqntSbcptMinUnits", "sbcptSttlmCycl", "redDealgFrqcy", "redDealFrqcyDesc", "ltdRedPrd", "redSttlmCycl", "dcmlstnPric", "dcmlstnUnits", "dualFndInd", "pricMtd", "pricCcy"})
public class ValuationDealingCharacteristicsComplexType {

    @XmlElement(name = "ValtnFrqcy") protected EventFrequency2Code valtnFrqcy;
    @XmlElement(name = "ValtnFrqcyDesc") protected String valtnFrqcyDesc;
    @XmlElement(name = "SbcptDealgFrqcy") protected EventFrequency2Code sbcptDealgFrqcy;
    @XmlElement(name = "SbcptDealFrqcyDesc") protected String sbcptDealFrqcyDesc;
    @XmlElement(name = "LtdSbcptPrd") protected String ltdSbcptPrd;
    @XmlElement(name = "InitlSbcptMinVal") protected ActiveCurrencyAndAmount initlSbcptMinVal;
    @XmlElement(name = "InitlSbcptMinUnits") protected BigDecimal initlSbcptMinUnits;
    @XmlElement(name = "SbsqntSbcptMinVal") protected ActiveCurrencyAndAmount sbsqntSbcptMinVal;
    @XmlElement(name = "SbsqntSbcptMinUnits") protected BigDecimal sbsqntSbcptMinUnits;
    @XmlElement(name = "SbcptSttlmCycl", required = true) protected BigDecimal sbcptSttlmCycl;
    @XmlElement(name = "RedDealgFrqcy") protected EventFrequency2Code redDealgFrqcy;
    @XmlElement(name = "RedDealFrqcyDesc") protected String redDealFrqcyDesc;
    @XmlElement(name = "LtdRedPrd") protected String ltdRedPrd;
    @XmlElement(name = "RedSttlmCycl") protected BigDecimal redSttlmCycl;
    @XmlElement(name = "DcmlstnPric") protected BigDecimal dcmlstnPric;
    @XmlElement(name = "DcmlstnUnits") protected BigDecimal dcmlstnUnits;
    @XmlElement(name = "DualFndInd") protected YesNoIndicator dualFndInd;
    @XmlElement(name = "PricMtd") protected PriceMethod1Code pricMtd;
    @XmlElement(name = "PricCcy") protected List<String> pricCcy;

    /**
     * Obtém o valor da propriedade dcmlstnPric.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getDcmlstnPric() {
        return dcmlstnPric;
    }

    /**
     * Define o valor da propriedade dcmlstnPric.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setDcmlstnPric(BigDecimal value) {
        this.dcmlstnPric = value;
    }

    /**
     * Obtém o valor da propriedade dcmlstnUnits.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getDcmlstnUnits() {
        return dcmlstnUnits;
    }

    /**
     * Define o valor da propriedade dcmlstnUnits.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setDcmlstnUnits(BigDecimal value) {
        this.dcmlstnUnits = value;
    }

    /**
     * Obtém o valor da propriedade dualFndInd.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getDualFndInd() {
        return dualFndInd;
    }

    /**
     * Define o valor da propriedade dualFndInd.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setDualFndInd(YesNoIndicator value) {
        this.dualFndInd = value;
    }

    /**
     * Obtém o valor da propriedade initlSbcptMinUnits.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getInitlSbcptMinUnits() {
        return initlSbcptMinUnits;
    }

    /**
     * Define o valor da propriedade initlSbcptMinUnits.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setInitlSbcptMinUnits(BigDecimal value) {
        this.initlSbcptMinUnits = value;
    }

    /**
     * Obtém o valor da propriedade initlSbcptMinVal.
     *
     * @return possible object is
     * {@link ActiveCurrencyAndAmount }
     */
    public ActiveCurrencyAndAmount getInitlSbcptMinVal() {
        return initlSbcptMinVal;
    }

    /**
     * Define o valor da propriedade initlSbcptMinVal.
     *
     * @param value allowed object is
     *              {@link ActiveCurrencyAndAmount }
     */
    public void setInitlSbcptMinVal(ActiveCurrencyAndAmount value) {
        this.initlSbcptMinVal = value;
    }

    /**
     * Obtém o valor da propriedade ltdRedPrd.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLtdRedPrd() {
        return ltdRedPrd;
    }

    /**
     * Define o valor da propriedade ltdRedPrd.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLtdRedPrd(String value) {
        this.ltdRedPrd = value;
    }

    /**
     * Obtém o valor da propriedade ltdSbcptPrd.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLtdSbcptPrd() {
        return ltdSbcptPrd;
    }

    /**
     * Define o valor da propriedade ltdSbcptPrd.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLtdSbcptPrd(String value) {
        this.ltdSbcptPrd = value;
    }

    /**
     * Gets the value of the pricCcy property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pricCcy property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPricCcy().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     */
    public List<String> getPricCcy() {
        if (pricCcy == null) {
            pricCcy = new ArrayList<String>();
        }
        return this.pricCcy;
    }

    /**
     * Obtém o valor da propriedade pricMtd.
     *
     * @return possible object is
     * {@link PriceMethod1Code }
     */
    public PriceMethod1Code getPricMtd() {
        return pricMtd;
    }

    /**
     * Define o valor da propriedade pricMtd.
     *
     * @param value allowed object is
     *              {@link PriceMethod1Code }
     */
    public void setPricMtd(PriceMethod1Code value) {
        this.pricMtd = value;
    }

    /**
     * Obtém o valor da propriedade redDealFrqcyDesc.
     *
     * @return possible object is
     * {@link String }
     */
    public String getRedDealFrqcyDesc() {
        return redDealFrqcyDesc;
    }

    /**
     * Define o valor da propriedade redDealFrqcyDesc.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setRedDealFrqcyDesc(String value) {
        this.redDealFrqcyDesc = value;
    }

    /**
     * Obtém o valor da propriedade redDealgFrqcy.
     *
     * @return possible object is
     * {@link EventFrequency2Code }
     */
    public EventFrequency2Code getRedDealgFrqcy() {
        return redDealgFrqcy;
    }

    /**
     * Define o valor da propriedade redDealgFrqcy.
     *
     * @param value allowed object is
     *              {@link EventFrequency2Code }
     */
    public void setRedDealgFrqcy(EventFrequency2Code value) {
        this.redDealgFrqcy = value;
    }

    /**
     * Obtém o valor da propriedade redSttlmCycl.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getRedSttlmCycl() {
        return redSttlmCycl;
    }

    /**
     * Define o valor da propriedade redSttlmCycl.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setRedSttlmCycl(BigDecimal value) {
        this.redSttlmCycl = value;
    }

    /**
     * Obtém o valor da propriedade sbcptDealFrqcyDesc.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSbcptDealFrqcyDesc() {
        return sbcptDealFrqcyDesc;
    }

    /**
     * Define o valor da propriedade sbcptDealFrqcyDesc.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSbcptDealFrqcyDesc(String value) {
        this.sbcptDealFrqcyDesc = value;
    }

    /**
     * Obtém o valor da propriedade sbcptDealgFrqcy.
     *
     * @return possible object is
     * {@link EventFrequency2Code }
     */
    public EventFrequency2Code getSbcptDealgFrqcy() {
        return sbcptDealgFrqcy;
    }

    /**
     * Define o valor da propriedade sbcptDealgFrqcy.
     *
     * @param value allowed object is
     *              {@link EventFrequency2Code }
     */
    public void setSbcptDealgFrqcy(EventFrequency2Code value) {
        this.sbcptDealgFrqcy = value;
    }

    /**
     * Obtém o valor da propriedade sbcptSttlmCycl.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getSbcptSttlmCycl() {
        return sbcptSttlmCycl;
    }

    /**
     * Define o valor da propriedade sbcptSttlmCycl.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setSbcptSttlmCycl(BigDecimal value) {
        this.sbcptSttlmCycl = value;
    }

    /**
     * Obtém o valor da propriedade sbsqntSbcptMinUnits.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getSbsqntSbcptMinUnits() {
        return sbsqntSbcptMinUnits;
    }

    /**
     * Define o valor da propriedade sbsqntSbcptMinUnits.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setSbsqntSbcptMinUnits(BigDecimal value) {
        this.sbsqntSbcptMinUnits = value;
    }

    /**
     * Obtém o valor da propriedade sbsqntSbcptMinVal.
     *
     * @return possible object is
     * {@link ActiveCurrencyAndAmount }
     */
    public ActiveCurrencyAndAmount getSbsqntSbcptMinVal() {
        return sbsqntSbcptMinVal;
    }

    /**
     * Define o valor da propriedade sbsqntSbcptMinVal.
     *
     * @param value allowed object is
     *              {@link ActiveCurrencyAndAmount }
     */
    public void setSbsqntSbcptMinVal(ActiveCurrencyAndAmount value) {
        this.sbsqntSbcptMinVal = value;
    }

    /**
     * Obtém o valor da propriedade valtnFrqcy.
     *
     * @return possible object is
     * {@link EventFrequency2Code }
     */
    public EventFrequency2Code getValtnFrqcy() {
        return valtnFrqcy;
    }

    /**
     * Define o valor da propriedade valtnFrqcy.
     *
     * @param value allowed object is
     *              {@link EventFrequency2Code }
     */
    public void setValtnFrqcy(EventFrequency2Code value) {
        this.valtnFrqcy = value;
    }

    /**
     * Obtém o valor da propriedade valtnFrqcyDesc.
     *
     * @return possible object is
     * {@link String }
     */
    public String getValtnFrqcyDesc() {
        return valtnFrqcyDesc;
    }

    /**
     * Define o valor da propriedade valtnFrqcyDesc.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setValtnFrqcyDesc(String value) {
        this.valtnFrqcyDesc = value;
    }

}
