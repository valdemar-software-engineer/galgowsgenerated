package br.com.stianbid.schemafundo;

import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * Subscription Processing Characteristic (Campo ISO não
 * utilizado no momento)
 * <p>
 * <p>
 * <p>Classe Java de SubscriptionProcessingCharacteristicsComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="SubscriptionProcessingCharacteristicsComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SbcptRedCcy" type="{http://www.stianbid.com.br/Common}ActiveCurrencyCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SbcptAmtVal" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="SbcptAmtUnits" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DealgCutOffTm" type="{http://www.stianbid.com.br/Common}ISOTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubscriptionProcessingCharacteristicsComplexType",
         propOrder = {"sbcptRedCcy", "sbcptAmtVal", "sbcptAmtUnits", "dealgCutOffTm"})
public class SubscriptionProcessingCharacteristicsComplexType {

    @XmlElement(name = "SbcptRedCcy") protected List<String> sbcptRedCcy;
    @XmlElement(name = "SbcptAmtVal") protected YesNoIndicator sbcptAmtVal;
    @XmlElement(name = "SbcptAmtUnits") protected YesNoIndicator sbcptAmtUnits;
    @XmlElement(name = "DealgCutOffTm") protected XMLGregorianCalendar dealgCutOffTm;

    /**
     * Obtém o valor da propriedade dealgCutOffTm.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDealgCutOffTm() {
        return dealgCutOffTm;
    }

    /**
     * Define o valor da propriedade dealgCutOffTm.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDealgCutOffTm(XMLGregorianCalendar value) {
        this.dealgCutOffTm = value;
    }

    /**
     * Obtém o valor da propriedade sbcptAmtUnits.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getSbcptAmtUnits() {
        return sbcptAmtUnits;
    }

    /**
     * Define o valor da propriedade sbcptAmtUnits.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setSbcptAmtUnits(YesNoIndicator value) {
        this.sbcptAmtUnits = value;
    }

    /**
     * Obtém o valor da propriedade sbcptAmtVal.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getSbcptAmtVal() {
        return sbcptAmtVal;
    }

    /**
     * Define o valor da propriedade sbcptAmtVal.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setSbcptAmtVal(YesNoIndicator value) {
        this.sbcptAmtVal = value;
    }

    /**
     * Gets the value of the sbcptRedCcy property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sbcptRedCcy property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSbcptRedCcy().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     */
    public List<String> getSbcptRedCcy() {
        if (sbcptRedCcy == null) {
            sbcptRedCcy = new ArrayList<String>();
        }
        return this.sbcptRedCcy;
    }

}
