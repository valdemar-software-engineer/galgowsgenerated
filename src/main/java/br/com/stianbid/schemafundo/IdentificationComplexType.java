package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Identificador
 * <p>
 * <p>Classe Java de IdentificationComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="IdentificationComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="ISIN" type="{http://www.stianbid.com.br/Common}ISINIdentifier"/>
 *         &lt;element name="OthrId" type="{http://www.stianbid.com.br/SchemaFundo}OtherIdentificationComplexType" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificationComplexType", propOrder = {"isin", "othrId"})
public class IdentificationComplexType {

    @XmlElement(name = "ISIN") protected String isin;
    @XmlElement(name = "OthrId") protected OtherIdentificationComplexType othrId;

    /**
     * Obtém o valor da propriedade isin.
     *
     * @return possible object is
     * {@link String }
     */
    public String getISIN() {
        return isin;
    }

    /**
     * Define o valor da propriedade isin.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setISIN(String value) {
        this.isin = value;
    }

    /**
     * Obtém o valor da propriedade othrId.
     *
     * @return possible object is
     * {@link OtherIdentificationComplexType }
     */
    public OtherIdentificationComplexType getOthrId() {
        return othrId;
    }

    /**
     * Define o valor da propriedade othrId.
     *
     * @param value allowed object is
     *              {@link OtherIdentificationComplexType }
     */
    public void setOthrId(OtherIdentificationComplexType value) {
        this.othrId = value;
    }

}
