package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dados da Entidade Administradora
 * <p>
 * <p>
 * <p>Classe Java de MainFundOrderDeskComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MainFundOrderDeskComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Nm" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/>
 *         &lt;element name="PstlAdr" type="{http://www.stianbid.com.br/SchemaFundo}PostalAddressComplexType" minOccurs="0"/>
 *         &lt;element name="PhneNb" type="{http://www.stianbid.com.br/Common}PhoneNumber" minOccurs="0"/>
 *         &lt;element name="FaxNb" type="{http://www.stianbid.com.br/Common}PhoneNumber" minOccurs="0"/>
 *         &lt;element name="EmailAdr" type="{http://www.stianbid.com.br/Common}Max256Text" minOccurs="0"/>
 *         &lt;element name="Id" type="{http://www.stianbid.com.br/Common}BICIdentifier" minOccurs="0"/>
 *         &lt;element name="ApplFrms" type="{http://www.stianbid.com.br/SchemaFundo}ApplicationFormsComplexType" minOccurs="0"/>
 *         &lt;element name="SbcptPrcgChrtcs" type="{http://www.stianbid.com.br/SchemaFundo}SubscriptionProcessingCharacteristicsComplexType" minOccurs="0"/>
 *         &lt;element name="RedPrcgChrtcs" type="{http://www.stianbid.com.br/SchemaFundo}RedemptionProcessingCharacteristicsComplexType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MainFundOrderDeskComplexType",
         propOrder = {"nm", "pstlAdr", "phneNb", "faxNb", "emailAdr", "id", "applFrms", "sbcptPrcgChrtcs", "redPrcgChrtcs"})
public class MainFundOrderDeskComplexType {

    @XmlElement(name = "Nm") protected String nm;
    @XmlElement(name = "PstlAdr") protected PostalAddressComplexType pstlAdr;
    @XmlElement(name = "PhneNb") protected String phneNb;
    @XmlElement(name = "FaxNb") protected String faxNb;
    @XmlElement(name = "EmailAdr") protected String emailAdr;
    @XmlElement(name = "Id") protected String id;
    @XmlElement(name = "ApplFrms") protected ApplicationFormsComplexType applFrms;
    @XmlElement(name = "SbcptPrcgChrtcs") protected SubscriptionProcessingCharacteristicsComplexType sbcptPrcgChrtcs;
    @XmlElement(name = "RedPrcgChrtcs") protected RedemptionProcessingCharacteristicsComplexType redPrcgChrtcs;

    /**
     * Obtém o valor da propriedade applFrms.
     *
     * @return possible object is
     * {@link ApplicationFormsComplexType }
     */
    public ApplicationFormsComplexType getApplFrms() {
        return applFrms;
    }

    /**
     * Define o valor da propriedade applFrms.
     *
     * @param value allowed object is
     *              {@link ApplicationFormsComplexType }
     */
    public void setApplFrms(ApplicationFormsComplexType value) {
        this.applFrms = value;
    }

    /**
     * Obtém o valor da propriedade emailAdr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getEmailAdr() {
        return emailAdr;
    }

    /**
     * Define o valor da propriedade emailAdr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setEmailAdr(String value) {
        this.emailAdr = value;
    }

    /**
     * Obtém o valor da propriedade faxNb.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFaxNb() {
        return faxNb;
    }

    /**
     * Define o valor da propriedade faxNb.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFaxNb(String value) {
        this.faxNb = value;
    }

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link String }
     */
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade nm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNm() {
        return nm;
    }

    /**
     * Define o valor da propriedade nm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNm(String value) {
        this.nm = value;
    }

    /**
     * Obtém o valor da propriedade phneNb.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPhneNb() {
        return phneNb;
    }

    /**
     * Define o valor da propriedade phneNb.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPhneNb(String value) {
        this.phneNb = value;
    }

    /**
     * Obtém o valor da propriedade pstlAdr.
     *
     * @return possible object is
     * {@link PostalAddressComplexType }
     */
    public PostalAddressComplexType getPstlAdr() {
        return pstlAdr;
    }

    /**
     * Define o valor da propriedade pstlAdr.
     *
     * @param value allowed object is
     *              {@link PostalAddressComplexType }
     */
    public void setPstlAdr(PostalAddressComplexType value) {
        this.pstlAdr = value;
    }

    /**
     * Obtém o valor da propriedade redPrcgChrtcs.
     *
     * @return possible object is
     * {@link RedemptionProcessingCharacteristicsComplexType }
     */
    public RedemptionProcessingCharacteristicsComplexType getRedPrcgChrtcs() {
        return redPrcgChrtcs;
    }

    /**
     * Define o valor da propriedade redPrcgChrtcs.
     *
     * @param value allowed object is
     *              {@link RedemptionProcessingCharacteristicsComplexType }
     */
    public void setRedPrcgChrtcs(RedemptionProcessingCharacteristicsComplexType value) {
        this.redPrcgChrtcs = value;
    }

    /**
     * Obtém o valor da propriedade sbcptPrcgChrtcs.
     *
     * @return possible object is
     * {@link SubscriptionProcessingCharacteristicsComplexType }
     */
    public SubscriptionProcessingCharacteristicsComplexType getSbcptPrcgChrtcs() {
        return sbcptPrcgChrtcs;
    }

    /**
     * Define o valor da propriedade sbcptPrcgChrtcs.
     *
     * @param value allowed object is
     *              {@link SubscriptionProcessingCharacteristicsComplexType }
     */
    public void setSbcptPrcgChrtcs(SubscriptionProcessingCharacteristicsComplexType value) {
        this.sbcptPrcgChrtcs = value;
    }

}
