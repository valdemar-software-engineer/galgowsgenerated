package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de IndEOUType.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="IndEOUType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="E"/>
 *     &lt;enumeration value="OU"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "IndEOUType")
@XmlEnum
public enum IndEOUType {

    E,
    OU;

    public static IndEOUType fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
