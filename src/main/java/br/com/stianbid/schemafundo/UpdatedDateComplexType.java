package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Data de Alteração
 * <p>
 * <p>Classe Java de UpdatedDateComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="UpdatedDateComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Dt" type="{http://www.stianbid.com.br/Common}ISODate"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdatedDateComplexType", propOrder = {"dt"})
public class UpdatedDateComplexType {

    @XmlElement(name = "Dt", required = true) protected XMLGregorianCalendar dt;

    /**
     * Obtém o valor da propriedade dt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDt() {
        return dt;
    }

    /**
     * Define o valor da propriedade dt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDt(XMLGregorianCalendar value) {
        this.dt = value;
    }

}
