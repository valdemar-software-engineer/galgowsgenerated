package br.com.stianbid.schemafundo;

import br.com.stianbid.common.MessageBatchComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Consumir Fundos.
 * <p>
 * <p>Classe Java de ResponseConsumirFundosComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ResponseConsumirFundosComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageBatchComplexType">
 *       &lt;sequence>
 *         &lt;element name="Fundos" type="{http://www.stianbid.com.br/SchemaFundo}STIConsumirFundosCorrecaoComplexType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseConsumirFundosComplexType", propOrder = {"fundos"})
public class ResponseConsumirFundosComplexType extends MessageBatchComplexType {

    @XmlElement(name = "Fundos", required = true) protected STIConsumirFundosCorrecaoComplexType fundos;

    /**
     * Obtém o valor da propriedade fundos.
     *
     * @return possible object is
     * {@link STIConsumirFundosCorrecaoComplexType }
     */
    public STIConsumirFundosCorrecaoComplexType getFundos() {
        return fundos;
    }

    /**
     * Define o valor da propriedade fundos.
     *
     * @param value allowed object is
     *              {@link STIConsumirFundosCorrecaoComplexType }
     */
    public void setFundos(STIConsumirFundosCorrecaoComplexType value) {
        this.fundos = value;
    }

}
