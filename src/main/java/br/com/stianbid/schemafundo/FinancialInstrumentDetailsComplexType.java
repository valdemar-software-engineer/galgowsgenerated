package br.com.stianbid.schemafundo;

import br.com.stianbid.common.DistributionPolicy1Code;
import br.com.stianbid.common.EUSavingsDirective1Code;
import br.com.stianbid.common.EventFrequency2Code;
import br.com.stianbid.common.YesNoIndicator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Financial Instrument Details
 * <p>
 * <p>Classe Java de FinancialInstrumentDetailsComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="FinancialInstrumentDetailsComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.stianbid.com.br/SchemaFundo}IdentificationComplexType" minOccurs="0"/>
 *         &lt;element name="Nm" type="{http://www.stianbid.com.br/Common}Max350Text" minOccurs="0"/>
 *         &lt;element name="UmbrllNm" type="{http://www.stianbid.com.br/Common}Max35Text" minOccurs="0"/>
 *         &lt;element name="CtryOfDmcl" type="{http://www.stianbid.com.br/Common}CountryCode" minOccurs="0"/>
 *         &lt;element name="RegdDstrbtnCtry" type="{http://www.stianbid.com.br/Common}CountryCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Ctry" type="{http://www.stianbid.com.br/Common}CountryCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DnmtnCcy" type="{http://www.stianbid.com.br/Common}ActiveCurrencyCode" minOccurs="0"/>
 *         &lt;element name="DstrbtnPlcy" type="{http://www.stianbid.com.br/Common}DistributionPolicy1Code" minOccurs="0"/>
 *         &lt;element name="CshDstrbtn" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DstrbtnRinvstmt" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="DvddFrqcy" type="{http://www.stianbid.com.br/Common}EventFrequency2Code" minOccurs="0"/>
 *         &lt;element name="RinvstmtFrqcy" type="{http://www.stianbid.com.br/Common}EventFrequency2Code" minOccurs="0"/>
 *         &lt;element name="EUSvgsDrctv" type="{http://www.stianbid.com.br/Common}EUSavingsDirective1Code" minOccurs="0"/>
 *         &lt;element name="FrntEndLdInd" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="BckEndLdInd" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *         &lt;element name="SwtchFeeInd" type="{http://www.stianbid.com.br/Common}YesNoIndicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialInstrumentDetailsComplexType",
         propOrder = {"id", "nm", "umbrllNm", "ctryOfDmcl", "regdDstrbtnCtry", "ctry", "dnmtnCcy", "dstrbtnPlcy", "cshDstrbtn", "dstrbtnRinvstmt", "dvddFrqcy", "rinvstmtFrqcy", "euSvgsDrctv", "frntEndLdInd", "bckEndLdInd", "swtchFeeInd"})
public class FinancialInstrumentDetailsComplexType {

    @XmlElement(name = "Id") protected IdentificationComplexType id;
    @XmlElement(name = "Nm") protected String nm;
    @XmlElement(name = "UmbrllNm") protected String umbrllNm;
    @XmlElement(name = "CtryOfDmcl") protected String ctryOfDmcl;
    @XmlElement(name = "RegdDstrbtnCtry") protected List<String> regdDstrbtnCtry;
    @XmlElement(name = "Ctry") protected List<String> ctry;
    @XmlElement(name = "DnmtnCcy") protected String dnmtnCcy;
    @XmlElement(name = "DstrbtnPlcy") protected DistributionPolicy1Code dstrbtnPlcy;
    @XmlElement(name = "CshDstrbtn") protected YesNoIndicator cshDstrbtn;
    @XmlElement(name = "DstrbtnRinvstmt") protected YesNoIndicator dstrbtnRinvstmt;
    @XmlElement(name = "DvddFrqcy") protected EventFrequency2Code dvddFrqcy;
    @XmlElement(name = "RinvstmtFrqcy") protected EventFrequency2Code rinvstmtFrqcy;
    @XmlElement(name = "EUSvgsDrctv") protected EUSavingsDirective1Code euSvgsDrctv;
    @XmlElement(name = "FrntEndLdInd") protected YesNoIndicator frntEndLdInd;
    @XmlElement(name = "BckEndLdInd") protected YesNoIndicator bckEndLdInd;
    @XmlElement(name = "SwtchFeeInd") protected YesNoIndicator swtchFeeInd;

    /**
     * Obtém o valor da propriedade bckEndLdInd.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getBckEndLdInd() {
        return bckEndLdInd;
    }

    /**
     * Define o valor da propriedade bckEndLdInd.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setBckEndLdInd(YesNoIndicator value) {
        this.bckEndLdInd = value;
    }

    /**
     * Obtém o valor da propriedade cshDstrbtn.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getCshDstrbtn() {
        return cshDstrbtn;
    }

    /**
     * Define o valor da propriedade cshDstrbtn.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setCshDstrbtn(YesNoIndicator value) {
        this.cshDstrbtn = value;
    }

    /**
     * Gets the value of the ctry property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ctry property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCtry().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     */
    public List<String> getCtry() {
        if (ctry == null) {
            ctry = new ArrayList<String>();
        }
        return this.ctry;
    }

    /**
     * Obtém o valor da propriedade ctryOfDmcl.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCtryOfDmcl() {
        return ctryOfDmcl;
    }

    /**
     * Define o valor da propriedade ctryOfDmcl.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCtryOfDmcl(String value) {
        this.ctryOfDmcl = value;
    }

    /**
     * Obtém o valor da propriedade dnmtnCcy.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDnmtnCcy() {
        return dnmtnCcy;
    }

    /**
     * Define o valor da propriedade dnmtnCcy.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDnmtnCcy(String value) {
        this.dnmtnCcy = value;
    }

    /**
     * Obtém o valor da propriedade dstrbtnPlcy.
     *
     * @return possible object is
     * {@link DistributionPolicy1Code }
     */
    public DistributionPolicy1Code getDstrbtnPlcy() {
        return dstrbtnPlcy;
    }

    /**
     * Define o valor da propriedade dstrbtnPlcy.
     *
     * @param value allowed object is
     *              {@link DistributionPolicy1Code }
     */
    public void setDstrbtnPlcy(DistributionPolicy1Code value) {
        this.dstrbtnPlcy = value;
    }

    /**
     * Obtém o valor da propriedade dstrbtnRinvstmt.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getDstrbtnRinvstmt() {
        return dstrbtnRinvstmt;
    }

    /**
     * Define o valor da propriedade dstrbtnRinvstmt.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setDstrbtnRinvstmt(YesNoIndicator value) {
        this.dstrbtnRinvstmt = value;
    }

    /**
     * Obtém o valor da propriedade dvddFrqcy.
     *
     * @return possible object is
     * {@link EventFrequency2Code }
     */
    public EventFrequency2Code getDvddFrqcy() {
        return dvddFrqcy;
    }

    /**
     * Define o valor da propriedade dvddFrqcy.
     *
     * @param value allowed object is
     *              {@link EventFrequency2Code }
     */
    public void setDvddFrqcy(EventFrequency2Code value) {
        this.dvddFrqcy = value;
    }

    /**
     * Obtém o valor da propriedade euSvgsDrctv.
     *
     * @return possible object is
     * {@link EUSavingsDirective1Code }
     */
    public EUSavingsDirective1Code getEUSvgsDrctv() {
        return euSvgsDrctv;
    }

    /**
     * Define o valor da propriedade euSvgsDrctv.
     *
     * @param value allowed object is
     *              {@link EUSavingsDirective1Code }
     */
    public void setEUSvgsDrctv(EUSavingsDirective1Code value) {
        this.euSvgsDrctv = value;
    }

    /**
     * Obtém o valor da propriedade frntEndLdInd.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getFrntEndLdInd() {
        return frntEndLdInd;
    }

    /**
     * Define o valor da propriedade frntEndLdInd.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setFrntEndLdInd(YesNoIndicator value) {
        this.frntEndLdInd = value;
    }

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link IdentificationComplexType }
     */
    public IdentificationComplexType getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link IdentificationComplexType }
     */
    public void setId(IdentificationComplexType value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade nm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNm() {
        return nm;
    }

    /**
     * Define o valor da propriedade nm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNm(String value) {
        this.nm = value;
    }

    /**
     * Gets the value of the regdDstrbtnCtry property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regdDstrbtnCtry property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegdDstrbtnCtry().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     */
    public List<String> getRegdDstrbtnCtry() {
        if (regdDstrbtnCtry == null) {
            regdDstrbtnCtry = new ArrayList<String>();
        }
        return this.regdDstrbtnCtry;
    }

    /**
     * Obtém o valor da propriedade rinvstmtFrqcy.
     *
     * @return possible object is
     * {@link EventFrequency2Code }
     */
    public EventFrequency2Code getRinvstmtFrqcy() {
        return rinvstmtFrqcy;
    }

    /**
     * Define o valor da propriedade rinvstmtFrqcy.
     *
     * @param value allowed object is
     *              {@link EventFrequency2Code }
     */
    public void setRinvstmtFrqcy(EventFrequency2Code value) {
        this.rinvstmtFrqcy = value;
    }

    /**
     * Obtém o valor da propriedade swtchFeeInd.
     *
     * @return possible object is
     * {@link YesNoIndicator }
     */
    public YesNoIndicator getSwtchFeeInd() {
        return swtchFeeInd;
    }

    /**
     * Define o valor da propriedade swtchFeeInd.
     *
     * @param value allowed object is
     *              {@link YesNoIndicator }
     */
    public void setSwtchFeeInd(YesNoIndicator value) {
        this.swtchFeeInd = value;
    }

    /**
     * Obtém o valor da propriedade umbrllNm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getUmbrllNm() {
        return umbrllNm;
    }

    /**
     * Define o valor da propriedade umbrllNm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setUmbrllNm(String value) {
        this.umbrllNm = value;
    }

}
