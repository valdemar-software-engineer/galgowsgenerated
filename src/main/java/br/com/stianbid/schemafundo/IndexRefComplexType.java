package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Classe Java de IndexRefComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="IndexRefComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IndCompAltIndex" type="{http://www.stianbid.com.br/SchemaFundo}IndEOUType" minOccurs="0"/>
 *         &lt;element name="PercIndexRef" type="{http://www.stianbid.com.br/SchemaFundo}DecimalNumber_7_4"/>
 *         &lt;element name="IndexRefTxPerf" type="{http://www.stianbid.com.br/SchemaFundo}IndexCorrTxCode"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndexRefComplexType", propOrder = {"indCompAltIndex", "percIndexRef", "indexRefTxPerf"})
public class IndexRefComplexType {

    @XmlElement(name = "IndCompAltIndex") protected IndEOUType indCompAltIndex;
    @XmlElement(name = "PercIndexRef", required = true) protected BigDecimal percIndexRef;
    @XmlElement(name = "IndexRefTxPerf") protected int indexRefTxPerf;

    /**
     * Obtém o valor da propriedade indCompAltIndex.
     *
     * @return possible object is
     * {@link IndEOUType }
     */
    public IndEOUType getIndCompAltIndex() {
        return indCompAltIndex;
    }

    /**
     * Define o valor da propriedade indCompAltIndex.
     *
     * @param value allowed object is
     *              {@link IndEOUType }
     */
    public void setIndCompAltIndex(IndEOUType value) {
        this.indCompAltIndex = value;
    }

    /**
     * Obtém o valor da propriedade indexRefTxPerf.
     */
    public int getIndexRefTxPerf() {
        return indexRefTxPerf;
    }

    /**
     * Define o valor da propriedade indexRefTxPerf.
     */
    public void setIndexRefTxPerf(int value) {
        this.indexRefTxPerf = value;
    }

    /**
     * Obtém o valor da propriedade percIndexRef.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getPercIndexRef() {
        return percIndexRef;
    }

    /**
     * Define o valor da propriedade percIndexRef.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setPercIndexRef(BigDecimal value) {
        this.percIndexRef = value;
    }

}
