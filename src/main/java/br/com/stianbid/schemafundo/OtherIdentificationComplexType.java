package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Outras Identificações. (Campo ISO não utilizado no
 * momento)
 * <p>
 * <p>
 * <p>Classe Java de OtherIdentificationComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="OtherIdentificationComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.stianbid.com.br/SchemaFundo}ListaIdRegistro" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="DmstIdSrc" type="{http://www.stianbid.com.br/Common}CountryCode" minOccurs="0"/>
 *           &lt;element name="PrtryIdSrc" type="{http://www.stianbid.com.br/Common}Max35Text" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OtherIdentificationComplexType", propOrder = {"id", "dmstIdSrc", "prtryIdSrc"})
public class OtherIdentificationComplexType {

    @XmlElement(name = "Id") protected String id;
    @XmlElement(name = "DmstIdSrc") protected String dmstIdSrc;
    @XmlElement(name = "PrtryIdSrc") protected String prtryIdSrc;

    /**
     * Obtém o valor da propriedade dmstIdSrc.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDmstIdSrc() {
        return dmstIdSrc;
    }

    /**
     * Define o valor da propriedade dmstIdSrc.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDmstIdSrc(String value) {
        this.dmstIdSrc = value;
    }

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link String }
     */
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade prtryIdSrc.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPrtryIdSrc() {
        return prtryIdSrc;
    }

    /**
     * Define o valor da propriedade prtryIdSrc.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPrtryIdSrc(String value) {
        this.prtryIdSrc = value;
    }

}
