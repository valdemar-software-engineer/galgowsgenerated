package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Lista de Códigos de Auto Regulação a serem revisados.
 * <p>
 * <p>
 * <p>Classe Java de RevCodAutoRegComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="RevCodAutoRegComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemCodAutoReg" type="{http://www.stianbid.com.br/SchemaFundo}itensRevCodComplexType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RevCodAutoRegComplexType", propOrder = {"itemCodAutoReg"})
public class RevCodAutoRegComplexType {

    @XmlElement(name = "ItemCodAutoReg") protected List<ItensRevCodComplexType> itemCodAutoReg;

    /**
     * Gets the value of the itemCodAutoReg property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemCodAutoReg property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemCodAutoReg().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItensRevCodComplexType }
     */
    public List<ItensRevCodComplexType> getItemCodAutoReg() {
        if (itemCodAutoReg == null) {
            itemCodAutoReg = new ArrayList<ItensRevCodComplexType>();
        }
        return this.itemCodAutoReg;
    }

}
