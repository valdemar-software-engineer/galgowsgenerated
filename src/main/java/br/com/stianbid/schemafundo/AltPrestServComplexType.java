package br.com.stianbid.schemafundo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Alterações em Prestadores de Serviço
 * <p>
 * <p>
 * <p>Classe Java de AltPrestServComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="AltPrestServComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DtAdm" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtAudit" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtControlAtivo" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtControlPass" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtCustCota" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtCustDeriv" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtCustRF" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtCustRV" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtCustUnico" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtGestCota" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtGestDeriv" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtGestRF" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtGestRV" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtGestUnico" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtInfoPlCota" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *         &lt;element name="DtInfoAtivos" type="{http://www.stianbid.com.br/Common}ISODate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AltPrestServComplexType",
         propOrder = {"dtAdm", "dtAudit", "dtControlAtivo", "dtControlPass", "dtCustCota", "dtCustDeriv", "dtCustRF", "dtCustRV", "dtCustUnico", "dtGestCota", "dtGestDeriv", "dtGestRF", "dtGestRV", "dtGestUnico", "dtInfoPlCota", "dtInfoAtivos"})
public class AltPrestServComplexType {

    @XmlElement(name = "DtAdm") protected XMLGregorianCalendar dtAdm;
    @XmlElement(name = "DtAudit") protected XMLGregorianCalendar dtAudit;
    @XmlElement(name = "DtControlAtivo") protected XMLGregorianCalendar dtControlAtivo;
    @XmlElement(name = "DtControlPass") protected XMLGregorianCalendar dtControlPass;
    @XmlElement(name = "DtCustCota") protected XMLGregorianCalendar dtCustCota;
    @XmlElement(name = "DtCustDeriv") protected XMLGregorianCalendar dtCustDeriv;
    @XmlElement(name = "DtCustRF") protected XMLGregorianCalendar dtCustRF;
    @XmlElement(name = "DtCustRV") protected XMLGregorianCalendar dtCustRV;
    @XmlElement(name = "DtCustUnico") protected XMLGregorianCalendar dtCustUnico;
    @XmlElement(name = "DtGestCota") protected XMLGregorianCalendar dtGestCota;
    @XmlElement(name = "DtGestDeriv") protected XMLGregorianCalendar dtGestDeriv;
    @XmlElement(name = "DtGestRF") protected XMLGregorianCalendar dtGestRF;
    @XmlElement(name = "DtGestRV") protected XMLGregorianCalendar dtGestRV;
    @XmlElement(name = "DtGestUnico") protected XMLGregorianCalendar dtGestUnico;
    @XmlElement(name = "DtInfoPlCota") protected XMLGregorianCalendar dtInfoPlCota;
    @XmlElement(name = "DtInfoAtivos") protected XMLGregorianCalendar dtInfoAtivos;

    /**
     * Obtém o valor da propriedade dtAdm.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtAdm() {
        return dtAdm;
    }

    /**
     * Define o valor da propriedade dtAdm.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtAdm(XMLGregorianCalendar value) {
        this.dtAdm = value;
    }

    /**
     * Obtém o valor da propriedade dtAudit.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtAudit() {
        return dtAudit;
    }

    /**
     * Define o valor da propriedade dtAudit.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtAudit(XMLGregorianCalendar value) {
        this.dtAudit = value;
    }

    /**
     * Obtém o valor da propriedade dtControlAtivo.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtControlAtivo() {
        return dtControlAtivo;
    }

    /**
     * Define o valor da propriedade dtControlAtivo.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtControlAtivo(XMLGregorianCalendar value) {
        this.dtControlAtivo = value;
    }

    /**
     * Obtém o valor da propriedade dtControlPass.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtControlPass() {
        return dtControlPass;
    }

    /**
     * Define o valor da propriedade dtControlPass.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtControlPass(XMLGregorianCalendar value) {
        this.dtControlPass = value;
    }

    /**
     * Obtém o valor da propriedade dtCustCota.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtCustCota() {
        return dtCustCota;
    }

    /**
     * Define o valor da propriedade dtCustCota.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtCustCota(XMLGregorianCalendar value) {
        this.dtCustCota = value;
    }

    /**
     * Obtém o valor da propriedade dtCustDeriv.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtCustDeriv() {
        return dtCustDeriv;
    }

    /**
     * Define o valor da propriedade dtCustDeriv.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtCustDeriv(XMLGregorianCalendar value) {
        this.dtCustDeriv = value;
    }

    /**
     * Obtém o valor da propriedade dtCustRF.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtCustRF() {
        return dtCustRF;
    }

    /**
     * Define o valor da propriedade dtCustRF.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtCustRF(XMLGregorianCalendar value) {
        this.dtCustRF = value;
    }

    /**
     * Obtém o valor da propriedade dtCustRV.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtCustRV() {
        return dtCustRV;
    }

    /**
     * Define o valor da propriedade dtCustRV.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtCustRV(XMLGregorianCalendar value) {
        this.dtCustRV = value;
    }

    /**
     * Obtém o valor da propriedade dtCustUnico.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtCustUnico() {
        return dtCustUnico;
    }

    /**
     * Define o valor da propriedade dtCustUnico.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtCustUnico(XMLGregorianCalendar value) {
        this.dtCustUnico = value;
    }

    /**
     * Obtém o valor da propriedade dtGestCota.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtGestCota() {
        return dtGestCota;
    }

    /**
     * Define o valor da propriedade dtGestCota.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtGestCota(XMLGregorianCalendar value) {
        this.dtGestCota = value;
    }

    /**
     * Obtém o valor da propriedade dtGestDeriv.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtGestDeriv() {
        return dtGestDeriv;
    }

    /**
     * Define o valor da propriedade dtGestDeriv.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtGestDeriv(XMLGregorianCalendar value) {
        this.dtGestDeriv = value;
    }

    /**
     * Obtém o valor da propriedade dtGestRF.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtGestRF() {
        return dtGestRF;
    }

    /**
     * Define o valor da propriedade dtGestRF.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtGestRF(XMLGregorianCalendar value) {
        this.dtGestRF = value;
    }

    /**
     * Obtém o valor da propriedade dtGestRV.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtGestRV() {
        return dtGestRV;
    }

    /**
     * Define o valor da propriedade dtGestRV.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtGestRV(XMLGregorianCalendar value) {
        this.dtGestRV = value;
    }

    /**
     * Obtém o valor da propriedade dtGestUnico.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtGestUnico() {
        return dtGestUnico;
    }

    /**
     * Define o valor da propriedade dtGestUnico.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtGestUnico(XMLGregorianCalendar value) {
        this.dtGestUnico = value;
    }

    /**
     * Obtém o valor da propriedade dtInfoAtivos.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInfoAtivos() {
        return dtInfoAtivos;
    }

    /**
     * Define o valor da propriedade dtInfoAtivos.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInfoAtivos(XMLGregorianCalendar value) {
        this.dtInfoAtivos = value;
    }

    /**
     * Obtém o valor da propriedade dtInfoPlCota.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtInfoPlCota() {
        return dtInfoPlCota;
    }

    /**
     * Define o valor da propriedade dtInfoPlCota.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtInfoPlCota(XMLGregorianCalendar value) {
        this.dtInfoPlCota = value;
    }

}
