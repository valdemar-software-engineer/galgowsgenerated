package br.com.stianbid.schemaanbid;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Utilizado para atualizacao do Status do Processamento
 * para os Registros (Código STI)
 * <p>
 * <p>
 * <p>Classe Java de MessageUpdateComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageUpdateComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType">
 *       &lt;sequence>
 *         &lt;element name="UpdateCodANBID" type="{http://www.stianbid.com.br/SchemaANBID}STIRecordComplexType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageUpdateComplexType", propOrder = {"updateCodANBID"})
public class MessageUpdateComplexType extends MessageRequestComplexType {

    @XmlElement(name = "UpdateCodANBID", required = true) protected List<STIRecordComplexType> updateCodANBID;

    /**
     * Gets the value of the updateCodANBID property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updateCodANBID property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdateCodANBID().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link STIRecordComplexType }
     */
    public List<STIRecordComplexType> getUpdateCodANBID() {
        if (updateCodANBID == null) {
            updateCodANBID = new ArrayList<STIRecordComplexType>();
        }
        return this.updateCodANBID;
    }

}
