package br.com.stianbid.schemaanbid;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the br.com.stianbid.schemaanbid package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UpdateCodANBID_QNAME = new QName("http://www.stianbid.com.br/SchemaANBID",
        "UpdateCodANBID");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.stianbid.schemaanbid
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MessageRequestPendIncFinComplexType }
     */
    public MessageRequestPendIncFinComplexType createMessageRequestPendIncFinComplexType() {
        return new MessageRequestPendIncFinComplexType();
    }

    /**
     * Create an instance of {@link MessageUpdateComplexType }
     */
    public MessageUpdateComplexType createMessageUpdateComplexType() {
        return new MessageUpdateComplexType();
    }

    /**
     * Create an instance of {@link STIRecordComplexType }
     */
    public STIRecordComplexType createSTIRecordComplexType() {
        return new STIRecordComplexType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageUpdateComplexType }{@code >}}
     */
    @XmlElementDecl(namespace = "http://www.stianbid.com.br/SchemaANBID", name = "UpdateCodANBID")
    public JAXBElement<MessageUpdateComplexType> createUpdateCodANBID(MessageUpdateComplexType value) {
        return new JAXBElement<MessageUpdateComplexType>(_UpdateCodANBID_QNAME, MessageUpdateComplexType.class, null,
            value);
    }

}
