package br.com.stianbid.schemaanbid;

import br.com.stianbid.common.MessageRequestComplexType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Marcador
 * <p>
 * <p>Classe Java de MessageRequestPendIncFinComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="MessageRequestPendIncFinComplexType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.stianbid.com.br/Common}MessageRequestComplexType">
 *       &lt;sequence>
 *         &lt;element name="MarcadorInic" type="{http://www.stianbid.com.br/Common}MarcadorType" minOccurs="0"/>
 *         &lt;element name="MarcadorFin" type="{http://www.stianbid.com.br/Common}MarcadorType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageRequestPendIncFinComplexType", propOrder = {"marcadorInic", "marcadorFin"})
public class MessageRequestPendIncFinComplexType extends MessageRequestComplexType {

    @XmlElement(name = "MarcadorInic") protected String marcadorInic;
    @XmlElement(name = "MarcadorFin") protected String marcadorFin;

    /**
     * Obtém o valor da propriedade marcadorFin.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMarcadorFin() {
        return marcadorFin;
    }

    /**
     * Define o valor da propriedade marcadorFin.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMarcadorFin(String value) {
        this.marcadorFin = value;
    }

    /**
     * Obtém o valor da propriedade marcadorInic.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMarcadorInic() {
        return marcadorInic;
    }

    /**
     * Define o valor da propriedade marcadorInic.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMarcadorInic(String value) {
        this.marcadorInic = value;
    }

}
