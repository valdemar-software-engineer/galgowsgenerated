package br.com.stianbid.schemaanbid;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Cada registro que terá o Status atualizado
 * <p>
 * <p>
 * <p>Classe Java de STIRecordComplexType complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="STIRecordComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdProcessStatus" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ErrorDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CdSti">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;maxInclusive value="9999999"/>
 *               &lt;minInclusive value="0"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CdAnbid" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="999999"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Reason" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="1"/>
 *               &lt;enumeration value="2"/>
 *               &lt;enumeration value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DtCodGeneration" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "STIRecordComplexType",
         propOrder = {"idProcessStatus", "errorDescription", "cdSti", "cdAnbid", "reason", "dtCodGeneration"})
public class STIRecordComplexType {

    @XmlElement(name = "IdProcessStatus") protected int idProcessStatus;
    @XmlElement(name = "ErrorDescription") protected String errorDescription;
    @XmlElement(name = "CdSti") protected int cdSti;
    @XmlElement(name = "CdAnbid") protected Integer cdAnbid;
    @XmlElement(name = "Reason") protected String reason;
    @XmlElement(name = "DtCodGeneration") @XmlSchemaType(name = "date") protected XMLGregorianCalendar dtCodGeneration;

    /**
     * Obtém o valor da propriedade cdAnbid.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getCdAnbid() {
        return cdAnbid;
    }

    /**
     * Define o valor da propriedade cdAnbid.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setCdAnbid(Integer value) {
        this.cdAnbid = value;
    }

    /**
     * Obtém o valor da propriedade cdSti.
     */
    public int getCdSti() {
        return cdSti;
    }

    /**
     * Define o valor da propriedade cdSti.
     */
    public void setCdSti(int value) {
        this.cdSti = value;
    }

    /**
     * Obtém o valor da propriedade dtCodGeneration.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtCodGeneration() {
        return dtCodGeneration;
    }

    /**
     * Define o valor da propriedade dtCodGeneration.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtCodGeneration(XMLGregorianCalendar value) {
        this.dtCodGeneration = value;
    }

    /**
     * Obtém o valor da propriedade errorDescription.
     *
     * @return possible object is
     * {@link String }
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * Define o valor da propriedade errorDescription.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setErrorDescription(String value) {
        this.errorDescription = value;
    }

    /**
     * Obtém o valor da propriedade idProcessStatus.
     */
    public int getIdProcessStatus() {
        return idProcessStatus;
    }

    /**
     * Define o valor da propriedade idProcessStatus.
     */
    public void setIdProcessStatus(int value) {
        this.idProcessStatus = value;
    }

    /**
     * Obtém o valor da propriedade reason.
     *
     * @return possible object is
     * {@link String }
     */
    public String getReason() {
        return reason;
    }

    /**
     * Define o valor da propriedade reason.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
