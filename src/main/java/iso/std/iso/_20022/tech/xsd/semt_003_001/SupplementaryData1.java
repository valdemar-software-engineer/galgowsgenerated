package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de SupplementaryData1 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="SupplementaryData1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PlcAndNm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max350Text" minOccurs="0"/&gt;
 *         &lt;element name="Envlp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SupplementaryDataEnvelope1"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SupplementaryData1", propOrder = {"plcAndNm", "envlp"})
public class SupplementaryData1 {

    @XmlElement(name = "PlcAndNm") protected String plcAndNm;
    @XmlElement(name = "Envlp", required = true) protected SupplementaryDataEnvelope1 envlp;

    /**
     * Obtém o valor da propriedade envlp.
     *
     * @return possible object is
     * {@link SupplementaryDataEnvelope1 }
     */
    public SupplementaryDataEnvelope1 getEnvlp() {
        return envlp;
    }

    /**
     * Define o valor da propriedade envlp.
     *
     * @param value allowed object is
     *              {@link SupplementaryDataEnvelope1 }
     */
    public void setEnvlp(SupplementaryDataEnvelope1 value) {
        this.envlp = value;
    }

    /**
     * Obtém o valor da propriedade plcAndNm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPlcAndNm() {
        return plcAndNm;
    }

    /**
     * Define o valor da propriedade plcAndNm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPlcAndNm(String value) {
        this.plcAndNm = value;
    }

}
