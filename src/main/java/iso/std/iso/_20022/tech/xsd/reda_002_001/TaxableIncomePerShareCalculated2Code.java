package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de TaxableIncomePerShareCalculated2Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="TaxableIncomePerShareCalculated2Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="TSIY"/&gt;
 *     &lt;enumeration value="TSIN"/&gt;
 *     &lt;enumeration value="UKWN"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "TaxableIncomePerShareCalculated2Code")
@XmlEnum
public enum TaxableIncomePerShareCalculated2Code {

    TSIY,
    TSIN,
    UKWN;

    public static TaxableIncomePerShareCalculated2Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
