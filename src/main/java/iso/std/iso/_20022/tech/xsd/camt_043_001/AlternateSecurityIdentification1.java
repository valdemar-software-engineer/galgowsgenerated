package iso.std.iso._20022.tech.xsd.camt_043_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de AlternateSecurityIdentification1 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="AlternateSecurityIdentification1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Max35Text"/>
 *         &lt;choice>
 *           &lt;element name="DmstIdSrc" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}CountryCode"/>
 *           &lt;element name="PrtryIdSrc" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Max35Text"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlternateSecurityIdentification1", propOrder = {"id", "dmstIdSrc", "prtryIdSrc"})
public class AlternateSecurityIdentification1 {

    @XmlElement(name = "Id", required = true) protected String id;
    @XmlElement(name = "DmstIdSrc") protected String dmstIdSrc;
    @XmlElement(name = "PrtryIdSrc") protected String prtryIdSrc;

    /**
     * Obtém o valor da propriedade dmstIdSrc.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDmstIdSrc() {
        return dmstIdSrc;
    }

    /**
     * Define o valor da propriedade dmstIdSrc.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDmstIdSrc(String value) {
        this.dmstIdSrc = value;
    }

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link String }
     */
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade prtryIdSrc.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPrtryIdSrc() {
        return prtryIdSrc;
    }

    /**
     * Define o valor da propriedade prtryIdSrc.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPrtryIdSrc(String value) {
        this.prtryIdSrc = value;
    }

}
