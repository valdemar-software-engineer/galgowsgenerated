package iso.std.iso._20022.tech.xsd.camt_045_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de NameAndAddress5 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="NameAndAddress5">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Nm" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}Max350Text"/>
 *         &lt;element name="Adr" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}PostalAddress1" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NameAndAddress5", propOrder = {"nm", "adr"})
public class NameAndAddress5 {

    @XmlElement(name = "Nm", required = true) protected String nm;
    @XmlElement(name = "Adr") protected PostalAddress1 adr;

    /**
     * Obtém o valor da propriedade adr.
     *
     * @return possible object is
     * {@link PostalAddress1 }
     */
    public PostalAddress1 getAdr() {
        return adr;
    }

    /**
     * Define o valor da propriedade adr.
     *
     * @param value allowed object is
     *              {@link PostalAddress1 }
     */
    public void setAdr(PostalAddress1 value) {
        this.adr = value;
    }

    /**
     * Obtém o valor da propriedade nm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNm() {
        return nm;
    }

    /**
     * Define o valor da propriedade nm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNm(String value) {
        this.nm = value;
    }

}
