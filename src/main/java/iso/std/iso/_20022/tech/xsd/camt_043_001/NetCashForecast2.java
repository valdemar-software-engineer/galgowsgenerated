package iso.std.iso._20022.tech.xsd.camt_043_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de NetCashForecast2 complex type.
 *
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 *
 * <pre>
 * &lt;complexType name="NetCashForecast2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CshSttlmDt" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}ISODate"/&gt;
 *         &lt;element name="NetAmt" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}ActiveOrHistoricCurrencyAndAmount" minOccurs="0"/&gt;
 *         &lt;element name="NetUnitsNb" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}FinancialInstrumentQuantity1" minOccurs="0"/&gt;
 *         &lt;element name="FlowDrctn" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}FlowDirectionType1Code"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NetCashForecast2", propOrder = {
    "cshSttlmDt",
    "netAmt",
    "netUnitsNb",
    "flowDrctn"
})
public class NetCashForecast2 {

    @XmlElement(name = "CshSttlmDt", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar cshSttlmDt;
    @XmlElement(name = "NetAmt")
    protected ActiveOrHistoricCurrencyAndAmount netAmt;
    @XmlElement(name = "NetUnitsNb")
    protected FinancialInstrumentQuantity1 netUnitsNb;
    @XmlElement(name = "FlowDrctn", required = true)
    @XmlSchemaType(name = "string")
    protected FlowDirectionType1Code flowDrctn;

    /**
     * Obtém o valor da propriedade cshSttlmDt.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getCshSttlmDt() {
        return cshSttlmDt;
    }

    /**
     * Define o valor da propriedade cshSttlmDt.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setCshSttlmDt(XMLGregorianCalendar value) {
        this.cshSttlmDt = value;
    }

    /**
     * Obtém o valor da propriedade flowDrctn.
     *
     * @return
     *     possible object is
     *     {@link FlowDirectionType1Code }
     *
     */
    public FlowDirectionType1Code getFlowDrctn() {
        return flowDrctn;
    }

    /**
     * Define o valor da propriedade flowDrctn.
     *
     * @param value
     *     allowed object is
     *     {@link FlowDirectionType1Code }
     *
     */
    public void setFlowDrctn(FlowDirectionType1Code value) {
        this.flowDrctn = value;
    }

    /**
     * Obtém o valor da propriedade netAmt.
     *
     * @return
     *     possible object is
     *     {@link ActiveOrHistoricCurrencyAndAmount }
     *
     */
    public ActiveOrHistoricCurrencyAndAmount getNetAmt() {
        return netAmt;
    }

    /**
     * Define o valor da propriedade netAmt.
     *
     * @param value
     *     allowed object is
     *     {@link ActiveOrHistoricCurrencyAndAmount }
     *
     */
    public void setNetAmt(ActiveOrHistoricCurrencyAndAmount value) {
        this.netAmt = value;
    }

    /**
     * Obtém o valor da propriedade netUnitsNb.
     *
     * @return
     *     possible object is
     *     {@link FinancialInstrumentQuantity1 }
     *
     */
    public FinancialInstrumentQuantity1 getNetUnitsNb() {
        return netUnitsNb;
    }

    /**
     * Define o valor da propriedade netUnitsNb.
     *
     * @param value
     *     allowed object is
     *     {@link FinancialInstrumentQuantity1 }
     *
     */
    public void setNetUnitsNb(FinancialInstrumentQuantity1 value) {
        this.netUnitsNb = value;
    }

}
