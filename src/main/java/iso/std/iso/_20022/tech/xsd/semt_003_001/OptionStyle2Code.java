package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de OptionStyle2Code.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;simpleType name="OptionStyle2Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AMER"/&gt;
 *     &lt;enumeration value="EURO"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "OptionStyle2Code")
@XmlEnum
public enum OptionStyle2Code {

    AMER,
    EURO;

    public static OptionStyle2Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
