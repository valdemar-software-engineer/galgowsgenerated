package iso.std.iso._20022.tech.xsd.head_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de FinancialInstitutionIdentification8 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="FinancialInstitutionIdentification8"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BICFI" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}BICFIIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="ClrSysMmbId" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}ClearingSystemMemberIdentification2" minOccurs="0"/&gt;
 *         &lt;element name="Nm" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}Max140Text" minOccurs="0"/&gt;
 *         &lt;element name="PstlAdr" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}PostalAddress6" minOccurs="0"/&gt;
 *         &lt;element name="Othr" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}GenericFinancialIdentification1" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialInstitutionIdentification8", propOrder = {"bicfi", "clrSysMmbId", "nm", "pstlAdr", "othr"})
public class FinancialInstitutionIdentification8 {

    @XmlElement(name = "BICFI") protected String bicfi;
    @XmlElement(name = "ClrSysMmbId") protected ClearingSystemMemberIdentification2 clrSysMmbId;
    @XmlElement(name = "Nm") protected String nm;
    @XmlElement(name = "PstlAdr") protected PostalAddress6 pstlAdr;
    @XmlElement(name = "Othr") protected GenericFinancialIdentification1 othr;

    /**
     * Obtém o valor da propriedade bicfi.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBICFI() {
        return bicfi;
    }

    /**
     * Define o valor da propriedade bicfi.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBICFI(String value) {
        this.bicfi = value;
    }

    /**
     * Obtém o valor da propriedade clrSysMmbId.
     *
     * @return possible object is
     * {@link ClearingSystemMemberIdentification2 }
     */
    public ClearingSystemMemberIdentification2 getClrSysMmbId() {
        return clrSysMmbId;
    }

    /**
     * Define o valor da propriedade clrSysMmbId.
     *
     * @param value allowed object is
     *              {@link ClearingSystemMemberIdentification2 }
     */
    public void setClrSysMmbId(ClearingSystemMemberIdentification2 value) {
        this.clrSysMmbId = value;
    }

    /**
     * Obtém o valor da propriedade nm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNm() {
        return nm;
    }

    /**
     * Define o valor da propriedade nm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNm(String value) {
        this.nm = value;
    }

    /**
     * Obtém o valor da propriedade othr.
     *
     * @return possible object is
     * {@link GenericFinancialIdentification1 }
     */
    public GenericFinancialIdentification1 getOthr() {
        return othr;
    }

    /**
     * Define o valor da propriedade othr.
     *
     * @param value allowed object is
     *              {@link GenericFinancialIdentification1 }
     */
    public void setOthr(GenericFinancialIdentification1 value) {
        this.othr = value;
    }

    /**
     * Obtém o valor da propriedade pstlAdr.
     *
     * @return possible object is
     * {@link PostalAddress6 }
     */
    public PostalAddress6 getPstlAdr() {
        return pstlAdr;
    }

    /**
     * Define o valor da propriedade pstlAdr.
     *
     * @param value allowed object is
     *              {@link PostalAddress6 }
     */
    public void setPstlAdr(PostalAddress6 value) {
        this.pstlAdr = value;
    }

}
