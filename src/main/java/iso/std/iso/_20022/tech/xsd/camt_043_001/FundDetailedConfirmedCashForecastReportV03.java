package iso.std.iso._20022.tech.xsd.camt_043_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de FundDetailedConfirmedCashForecastReportV03 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="FundDetailedConfirmedCashForecastReportV03">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MsgId" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}MessageIdentification1"/>
 *         &lt;element name="PoolRef" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}AdditionalReference3" minOccurs="0"/>
 *         &lt;element name="PrvsRef" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}AdditionalReference3" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RltdRef" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}AdditionalReference3" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MsgPgntn" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Pagination"/>
 *         &lt;element name="FndCshFcstDtls" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}FundCashForecast4" maxOccurs="unbounded"/>
 *         &lt;element name="CnsltdNetCshFcst" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}NetCashForecast3" minOccurs="0"/>
 *         &lt;element name="Xtnsn" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Extension1" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FundDetailedConfirmedCashForecastReportV03",
         propOrder = {"msgId", "poolRef", "prvsRef", "rltdRef", "msgPgntn", "fndCshFcstDtls", "cnsltdNetCshFcst", "xtnsn"})
public class FundDetailedConfirmedCashForecastReportV03 {

    @XmlElement(name = "MsgId", required = true) protected MessageIdentification1 msgId;
    @XmlElement(name = "PoolRef") protected AdditionalReference3 poolRef;
    @XmlElement(name = "PrvsRef") protected List<AdditionalReference3> prvsRef;
    @XmlElement(name = "RltdRef") protected List<AdditionalReference3> rltdRef;
    @XmlElement(name = "MsgPgntn", required = true) protected Pagination msgPgntn;
    @XmlElement(name = "FndCshFcstDtls", required = true) protected List<FundCashForecast4> fndCshFcstDtls;
    @XmlElement(name = "CnsltdNetCshFcst") protected NetCashForecast3 cnsltdNetCshFcst;
    @XmlElement(name = "Xtnsn") protected List<Extension1> xtnsn;

    /**
     * Obtém o valor da propriedade cnsltdNetCshFcst.
     *
     * @return possible object is
     * {@link NetCashForecast3 }
     */
    public NetCashForecast3 getCnsltdNetCshFcst() {
        return cnsltdNetCshFcst;
    }

    /**
     * Define o valor da propriedade cnsltdNetCshFcst.
     *
     * @param value allowed object is
     *              {@link NetCashForecast3 }
     */
    public void setCnsltdNetCshFcst(NetCashForecast3 value) {
        this.cnsltdNetCshFcst = value;
    }

    /**
     * Gets the value of the fndCshFcstDtls property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fndCshFcstDtls property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFndCshFcstDtls().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FundCashForecast4 }
     */
    public List<FundCashForecast4> getFndCshFcstDtls() {
        if (fndCshFcstDtls == null) {
            fndCshFcstDtls = new ArrayList<FundCashForecast4>();
        }
        return this.fndCshFcstDtls;
    }

    /**
     * Obtém o valor da propriedade msgId.
     *
     * @return possible object is
     * {@link MessageIdentification1 }
     */
    public MessageIdentification1 getMsgId() {
        return msgId;
    }

    /**
     * Define o valor da propriedade msgId.
     *
     * @param value allowed object is
     *              {@link MessageIdentification1 }
     */
    public void setMsgId(MessageIdentification1 value) {
        this.msgId = value;
    }

    /**
     * Obtém o valor da propriedade msgPgntn.
     *
     * @return possible object is
     * {@link Pagination }
     */
    public Pagination getMsgPgntn() {
        return msgPgntn;
    }

    /**
     * Define o valor da propriedade msgPgntn.
     *
     * @param value allowed object is
     *              {@link Pagination }
     */
    public void setMsgPgntn(Pagination value) {
        this.msgPgntn = value;
    }

    /**
     * Obtém o valor da propriedade poolRef.
     *
     * @return possible object is
     * {@link AdditionalReference3 }
     */
    public AdditionalReference3 getPoolRef() {
        return poolRef;
    }

    /**
     * Define o valor da propriedade poolRef.
     *
     * @param value allowed object is
     *              {@link AdditionalReference3 }
     */
    public void setPoolRef(AdditionalReference3 value) {
        this.poolRef = value;
    }

    /**
     * Gets the value of the prvsRef property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prvsRef property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrvsRef().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalReference3 }
     */
    public List<AdditionalReference3> getPrvsRef() {
        if (prvsRef == null) {
            prvsRef = new ArrayList<AdditionalReference3>();
        }
        return this.prvsRef;
    }

    /**
     * Gets the value of the rltdRef property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rltdRef property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRltdRef().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalReference3 }
     */
    public List<AdditionalReference3> getRltdRef() {
        if (rltdRef == null) {
            rltdRef = new ArrayList<AdditionalReference3>();
        }
        return this.rltdRef;
    }

    /**
     * Gets the value of the xtnsn property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the xtnsn property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getXtnsn().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Extension1 }
     */
    public List<Extension1> getXtnsn() {
        if (xtnsn == null) {
            xtnsn = new ArrayList<Extension1>();
        }
        return this.xtnsn;
    }

}
