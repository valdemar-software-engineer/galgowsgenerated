package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de DateOrDateTimePeriodChoice complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="DateOrDateTimePeriodChoice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Dt" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}DatePeriodDetails"/&gt;
 *           &lt;element name="DtTm" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}DateTimePeriodDetails"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DateOrDateTimePeriodChoice", propOrder = {"dt", "dtTm"})
public class DateOrDateTimePeriodChoice {

    @XmlElement(name = "Dt") protected DatePeriodDetails dt;
    @XmlElement(name = "DtTm") protected DateTimePeriodDetails dtTm;

    /**
     * Obtém o valor da propriedade dt.
     *
     * @return possible object is
     * {@link DatePeriodDetails }
     */
    public DatePeriodDetails getDt() {
        return dt;
    }

    /**
     * Define o valor da propriedade dt.
     *
     * @param value allowed object is
     *              {@link DatePeriodDetails }
     */
    public void setDt(DatePeriodDetails value) {
        this.dt = value;
    }

    /**
     * Obtém o valor da propriedade dtTm.
     *
     * @return possible object is
     * {@link DateTimePeriodDetails }
     */
    public DateTimePeriodDetails getDtTm() {
        return dtTm;
    }

    /**
     * Define o valor da propriedade dtTm.
     *
     * @param value allowed object is
     *              {@link DateTimePeriodDetails }
     */
    public void setDtTm(DateTimePeriodDetails value) {
        this.dtTm = value;
    }

}
