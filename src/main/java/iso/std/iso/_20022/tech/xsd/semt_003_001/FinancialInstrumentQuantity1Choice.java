package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Classe Java de FinancialInstrumentQuantity1Choice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="FinancialInstrumentQuantity1Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Unit" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}DecimalNumber"/&gt;
 *           &lt;element name="FaceAmt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ImpliedCurrencyAndAmount"/&gt;
 *           &lt;element name="AmtsdVal" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ImpliedCurrencyAndAmount"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialInstrumentQuantity1Choice", propOrder = {"unit", "faceAmt", "amtsdVal"})
public class FinancialInstrumentQuantity1Choice {

    @XmlElement(name = "Unit") protected BigDecimal unit;
    @XmlElement(name = "FaceAmt") protected BigDecimal faceAmt;
    @XmlElement(name = "AmtsdVal") protected BigDecimal amtsdVal;

    /**
     * Obtém o valor da propriedade amtsdVal.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getAmtsdVal() {
        return amtsdVal;
    }

    /**
     * Define o valor da propriedade amtsdVal.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setAmtsdVal(BigDecimal value) {
        this.amtsdVal = value;
    }

    /**
     * Obtém o valor da propriedade faceAmt.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getFaceAmt() {
        return faceAmt;
    }

    /**
     * Define o valor da propriedade faceAmt.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setFaceAmt(BigDecimal value) {
        this.faceAmt = value;
    }

    /**
     * Obtém o valor da propriedade unit.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getUnit() {
        return unit;
    }

    /**
     * Define o valor da propriedade unit.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setUnit(BigDecimal value) {
        this.unit = value;
    }

}
