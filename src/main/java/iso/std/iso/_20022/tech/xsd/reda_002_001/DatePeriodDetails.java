package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de DatePeriodDetails complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="DatePeriodDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FrDt" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}ISODate"/&gt;
 *         &lt;element name="ToDt" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}ISODate"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatePeriodDetails", propOrder = {"frDt", "toDt"})
public class DatePeriodDetails {

    @XmlElement(name = "FrDt", required = true) @XmlSchemaType(name = "date") protected XMLGregorianCalendar frDt;
    @XmlElement(name = "ToDt", required = true) @XmlSchemaType(name = "date") protected XMLGregorianCalendar toDt;

    /**
     * Obtém o valor da propriedade frDt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getFrDt() {
        return frDt;
    }

    /**
     * Define o valor da propriedade frDt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setFrDt(XMLGregorianCalendar value) {
        this.frDt = value;
    }

    /**
     * Obtém o valor da propriedade toDt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getToDt() {
        return toDt;
    }

    /**
     * Define o valor da propriedade toDt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setToDt(XMLGregorianCalendar value) {
        this.toDt = value;
    }

}
