package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de SecurityIdentification3Choice complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="SecurityIdentification3Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="ISIN" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}ISINIdentifier"/&gt;
 *           &lt;element name="SEDOL" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}SEDOLIdentifier"/&gt;
 *           &lt;element name="CUSIP" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}CUSIPIdentifier"/&gt;
 *           &lt;element name="RIC" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}RICIdentifier"/&gt;
 *           &lt;element name="TckrSymb" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}TickerIdentifier"/&gt;
 *           &lt;element name="Blmbrg" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}BloombergIdentifier"/&gt;
 *           &lt;element name="CTA" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}ConsolidatedTapeAssociationIdentifier"/&gt;
 *           &lt;element name="QUICK" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}QUICKIdentifier"/&gt;
 *           &lt;element name="Wrtppr" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}WertpapierIdentifier"/&gt;
 *           &lt;element name="Dtch" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}DutchIdentifier"/&gt;
 *           &lt;element name="Vlrn" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}ValorenIdentifier"/&gt;
 *           &lt;element name="SCVM" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}SicovamIdentifier"/&gt;
 *           &lt;element name="Belgn" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}BelgianIdentifier"/&gt;
 *           &lt;element name="Cmon" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}EuroclearClearstreamIdentifier"/&gt;
 *           &lt;element name="OthrPrtryId" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}AlternateSecurityIdentification1"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SecurityIdentification3Choice",
         propOrder = {"isin", "sedol", "cusip", "ric", "tckrSymb", "blmbrg", "cta", "quick", "wrtppr", "dtch", "vlrn", "scvm", "belgn", "cmon", "othrPrtryId"})
public class SecurityIdentification3Choice {

    @XmlElement(name = "ISIN") protected String isin;
    @XmlElement(name = "SEDOL") protected String sedol;
    @XmlElement(name = "CUSIP") protected String cusip;
    @XmlElement(name = "RIC") protected String ric;
    @XmlElement(name = "TckrSymb") protected String tckrSymb;
    @XmlElement(name = "Blmbrg") protected String blmbrg;
    @XmlElement(name = "CTA") protected String cta;
    @XmlElement(name = "QUICK") protected String quick;
    @XmlElement(name = "Wrtppr") protected String wrtppr;
    @XmlElement(name = "Dtch") protected String dtch;
    @XmlElement(name = "Vlrn") protected String vlrn;
    @XmlElement(name = "SCVM") protected String scvm;
    @XmlElement(name = "Belgn") protected String belgn;
    @XmlElement(name = "Cmon") protected String cmon;
    @XmlElement(name = "OthrPrtryId") protected AlternateSecurityIdentification1 othrPrtryId;

    /**
     * Obtém o valor da propriedade belgn.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBelgn() {
        return belgn;
    }

    /**
     * Define o valor da propriedade belgn.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBelgn(String value) {
        this.belgn = value;
    }

    /**
     * Obtém o valor da propriedade blmbrg.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBlmbrg() {
        return blmbrg;
    }

    /**
     * Define o valor da propriedade blmbrg.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBlmbrg(String value) {
        this.blmbrg = value;
    }

    /**
     * Obtém o valor da propriedade cta.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCTA() {
        return cta;
    }

    /**
     * Define o valor da propriedade cta.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCTA(String value) {
        this.cta = value;
    }

    /**
     * Obtém o valor da propriedade cusip.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCUSIP() {
        return cusip;
    }

    /**
     * Define o valor da propriedade cusip.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCUSIP(String value) {
        this.cusip = value;
    }

    /**
     * Obtém o valor da propriedade cmon.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCmon() {
        return cmon;
    }

    /**
     * Define o valor da propriedade cmon.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCmon(String value) {
        this.cmon = value;
    }

    /**
     * Obtém o valor da propriedade dtch.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDtch() {
        return dtch;
    }

    /**
     * Define o valor da propriedade dtch.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDtch(String value) {
        this.dtch = value;
    }

    /**
     * Obtém o valor da propriedade isin.
     *
     * @return possible object is
     * {@link String }
     */
    public String getISIN() {
        return isin;
    }

    /**
     * Define o valor da propriedade isin.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setISIN(String value) {
        this.isin = value;
    }

    /**
     * Obtém o valor da propriedade othrPrtryId.
     *
     * @return possible object is
     * {@link AlternateSecurityIdentification1 }
     */
    public AlternateSecurityIdentification1 getOthrPrtryId() {
        return othrPrtryId;
    }

    /**
     * Define o valor da propriedade othrPrtryId.
     *
     * @param value allowed object is
     *              {@link AlternateSecurityIdentification1 }
     */
    public void setOthrPrtryId(AlternateSecurityIdentification1 value) {
        this.othrPrtryId = value;
    }

    /**
     * Obtém o valor da propriedade quick.
     *
     * @return possible object is
     * {@link String }
     */
    public String getQUICK() {
        return quick;
    }

    /**
     * Define o valor da propriedade quick.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setQUICK(String value) {
        this.quick = value;
    }

    /**
     * Obtém o valor da propriedade ric.
     *
     * @return possible object is
     * {@link String }
     */
    public String getRIC() {
        return ric;
    }

    /**
     * Define o valor da propriedade ric.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setRIC(String value) {
        this.ric = value;
    }

    /**
     * Obtém o valor da propriedade scvm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSCVM() {
        return scvm;
    }

    /**
     * Define o valor da propriedade scvm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSCVM(String value) {
        this.scvm = value;
    }

    /**
     * Obtém o valor da propriedade sedol.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSEDOL() {
        return sedol;
    }

    /**
     * Define o valor da propriedade sedol.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSEDOL(String value) {
        this.sedol = value;
    }

    /**
     * Obtém o valor da propriedade tckrSymb.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTckrSymb() {
        return tckrSymb;
    }

    /**
     * Define o valor da propriedade tckrSymb.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTckrSymb(String value) {
        this.tckrSymb = value;
    }

    /**
     * Obtém o valor da propriedade vlrn.
     *
     * @return possible object is
     * {@link String }
     */
    public String getVlrn() {
        return vlrn;
    }

    /**
     * Define o valor da propriedade vlrn.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setVlrn(String value) {
        this.vlrn = value;
    }

    /**
     * Obtém o valor da propriedade wrtppr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWrtppr() {
        return wrtppr;
    }

    /**
     * Define o valor da propriedade wrtppr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWrtppr(String value) {
        this.wrtppr = value;
    }

}
