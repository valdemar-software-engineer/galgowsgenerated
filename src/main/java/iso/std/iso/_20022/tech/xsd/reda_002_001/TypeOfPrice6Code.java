package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de TypeOfPrice6Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeOfPrice6Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="BIDE"/&gt;
 *     &lt;enumeration value="OFFR"/&gt;
 *     &lt;enumeration value="NAVL"/&gt;
 *     &lt;enumeration value="CREA"/&gt;
 *     &lt;enumeration value="CANC"/&gt;
 *     &lt;enumeration value="INTE"/&gt;
 *     &lt;enumeration value="SWNG"/&gt;
 *     &lt;enumeration value="OTHR"/&gt;
 *     &lt;enumeration value="MIDD"/&gt;
 *     &lt;enumeration value="RINV"/&gt;
 *     &lt;enumeration value="SWIC"/&gt;
 *     &lt;enumeration value="DDVR"/&gt;
 *     &lt;enumeration value="ACTU"/&gt;
 *     &lt;enumeration value="NAUP"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "TypeOfPrice6Code")
@XmlEnum
public enum TypeOfPrice6Code {

    BIDE,
    OFFR,
    NAVL,
    CREA,
    CANC,
    INTE,
    SWNG,
    OTHR,
    MIDD,
    RINV,
    SWIC,
    DDVR,
    ACTU,
    NAUP;

    public static TypeOfPrice6Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
