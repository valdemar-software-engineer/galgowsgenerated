package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de EventFrequency1Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="EventFrequency1Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="YEAR"/&gt;
 *     &lt;enumeration value="SEMI"/&gt;
 *     &lt;enumeration value="QUTR"/&gt;
 *     &lt;enumeration value="TOMN"/&gt;
 *     &lt;enumeration value="MNTH"/&gt;
 *     &lt;enumeration value="TWMN"/&gt;
 *     &lt;enumeration value="TOWK"/&gt;
 *     &lt;enumeration value="WEEK"/&gt;
 *     &lt;enumeration value="DAIL"/&gt;
 *     &lt;enumeration value="ADHO"/&gt;
 *     &lt;enumeration value="INDA"/&gt;
 *     &lt;enumeration value="OVNG"/&gt;
 *     &lt;enumeration value="ONDE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "EventFrequency1Code")
@XmlEnum
public enum EventFrequency1Code {

    YEAR,
    SEMI,
    QUTR,
    TOMN,
    MNTH,
    TWMN,
    TOWK,
    WEEK,
    DAIL,
    ADHO,
    INDA,
    OVNG,
    ONDE;

    public static EventFrequency1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
