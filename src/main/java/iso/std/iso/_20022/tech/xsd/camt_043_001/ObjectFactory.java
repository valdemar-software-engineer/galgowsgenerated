package iso.std.iso._20022.tech.xsd.camt_043_001;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the iso.std.iso._20022.tech.xsd.camt_043_001 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Document_QNAME = new QName("urn:iso:std:iso:20022:tech:xsd:camt.043.001.03", "Document");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: iso.std.iso._20022.tech.xsd.camt_043_001
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActiveCurrencyAnd13DecimalAmount }
     *
     */
    public ActiveCurrencyAnd13DecimalAmount createActiveCurrencyAnd13DecimalAmount() {
        return new ActiveCurrencyAnd13DecimalAmount();
    }

    /**
     * Create an instance of {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     *
     */
    public ActiveOrHistoricCurrencyAnd13DecimalAmount createActiveOrHistoricCurrencyAnd13DecimalAmount() {
        return new ActiveOrHistoricCurrencyAnd13DecimalAmount();
    }

    /**
     * Create an instance of {@link ActiveOrHistoricCurrencyAndAmount }
     *
     */
    public ActiveOrHistoricCurrencyAndAmount createActiveOrHistoricCurrencyAndAmount() {
        return new ActiveOrHistoricCurrencyAndAmount();
    }

    /**
     * Create an instance of {@link AdditionalParameters1 }
     *
     */
    public AdditionalParameters1 createAdditionalParameters1() {
        return new AdditionalParameters1();
    }

    /**
     * Create an instance of {@link AdditionalReference3 }
     *
     */
    public AdditionalReference3 createAdditionalReference3() {
        return new AdditionalReference3();
    }

    /**
     * Create an instance of {@link AlternateSecurityIdentification1 }
     *
     */
    public AlternateSecurityIdentification1 createAlternateSecurityIdentification1() {
        return new AlternateSecurityIdentification1();
    }

    /**
     * Create an instance of {@link BreakdownByCountry1 }
     *
     */
    public BreakdownByCountry1 createBreakdownByCountry1() {
        return new BreakdownByCountry1();
    }

    /**
     * Create an instance of {@link BreakdownByCurrency1 }
     *
     */
    public BreakdownByCurrency1 createBreakdownByCurrency1() {
        return new BreakdownByCurrency1();
    }

    /**
     * Create an instance of {@link BreakdownByParty1 }
     *
     */
    public BreakdownByParty1 createBreakdownByParty1() {
        return new BreakdownByParty1();
    }

    /**
     * Create an instance of {@link BreakdownByUserDefinedParameter1 }
     *
     */
    public BreakdownByUserDefinedParameter1 createBreakdownByUserDefinedParameter1() {
        return new BreakdownByUserDefinedParameter1();
    }

    /**
     * Create an instance of {@link CashInForecast3 }
     *
     */
    public CashInForecast3 createCashInForecast3() {
        return new CashInForecast3();
    }

    /**
     * Create an instance of {@link CashOutForecast3 }
     *
     */
    public CashOutForecast3 createCashOutForecast3() {
        return new CashOutForecast3();
    }

    /**
     * Create an instance of {@link Charge16 }
     *
     */
    public Charge16 createCharge16() {
        return new Charge16();
    }

    /**
     * Create an instance of {@link Commission9 }
     *
     */
    public Commission9 createCommission9() {
        return new Commission9();
    }

    /**
     * Create an instance of {@link DataFormat2Choice }
     *
     */
    public DataFormat2Choice createDataFormat2Choice() {
        return new DataFormat2Choice();
    }

    /**
     * Create an instance of {@link DateAndDateTimeChoice }
     *
     */
    public DateAndDateTimeChoice createDateAndDateTimeChoice() {
        return new DateAndDateTimeChoice();
    }

    /**
     * Create an instance of {@link Document }
     *
     */
    public Document createDocument() {
        return new Document();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Document }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "urn:iso:std:iso:20022:tech:xsd:camt.043.001.03", name = "Document")
    public JAXBElement<Document> createDocument(Document value) {
        return new JAXBElement<Document>(_Document_QNAME, Document.class, null, value);
    }

    /**
     * Create an instance of {@link Extension1 }
     *
     */
    public Extension1 createExtension1() {
        return new Extension1();
    }

    /**
     * Create an instance of {@link FinancialInstrument9 }
     *
     */
    public FinancialInstrument9 createFinancialInstrument9() {
        return new FinancialInstrument9();
    }

    /**
     * Create an instance of {@link FinancialInstrumentQuantity1 }
     *
     */
    public FinancialInstrumentQuantity1 createFinancialInstrumentQuantity1() {
        return new FinancialInstrumentQuantity1();
    }

    /**
     * Create an instance of {@link FundCashForecast4 }
     *
     */
    public FundCashForecast4 createFundCashForecast4() {
        return new FundCashForecast4();
    }

    /**
     * Create an instance of {@link FundCashInBreakdown2 }
     *
     */
    public FundCashInBreakdown2 createFundCashInBreakdown2() {
        return new FundCashInBreakdown2();
    }

    /**
     * Create an instance of {@link FundCashOutBreakdown2 }
     *
     */
    public FundCashOutBreakdown2 createFundCashOutBreakdown2() {
        return new FundCashOutBreakdown2();
    }

    /**
     * Create an instance of {@link FundDetailedConfirmedCashForecastReportV03 }
     *
     */
    public FundDetailedConfirmedCashForecastReportV03 createFundDetailedConfirmedCashForecastReportV03() {
        return new FundDetailedConfirmedCashForecastReportV03();
    }

    /**
     * Create an instance of {@link GenericIdentification1 }
     *
     */
    public GenericIdentification1 createGenericIdentification1() {
        return new GenericIdentification1();
    }

    /**
     * Create an instance of {@link MessageIdentification1 }
     *
     */
    public MessageIdentification1 createMessageIdentification1() {
        return new MessageIdentification1();
    }

    /**
     * Create an instance of {@link NameAndAddress5 }
     *
     */
    public NameAndAddress5 createNameAndAddress5() {
        return new NameAndAddress5();
    }

    /**
     * Create an instance of {@link NetCashForecast2 }
     *
     */
    public NetCashForecast2 createNetCashForecast2() {
        return new NetCashForecast2();
    }

    /**
     * Create an instance of {@link NetCashForecast3 }
     *
     */
    public NetCashForecast3 createNetCashForecast3() {
        return new NetCashForecast3();
    }

    /**
     * Create an instance of {@link Pagination }
     *
     */
    public Pagination createPagination() {
        return new Pagination();
    }

    /**
     * Create an instance of {@link PartyIdentification2Choice }
     *
     */
    public PartyIdentification2Choice createPartyIdentification2Choice() {
        return new PartyIdentification2Choice();
    }

    /**
     * Create an instance of {@link PostalAddress1 }
     *
     */
    public PostalAddress1 createPostalAddress1() {
        return new PostalAddress1();
    }

    /**
     * Create an instance of {@link SecurityIdentification3Choice }
     *
     */
    public SecurityIdentification3Choice createSecurityIdentification3Choice() {
        return new SecurityIdentification3Choice();
    }

}
