package iso.std.iso._20022.tech.xsd.reda_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Classe Java de PriceValueChange1 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PriceValueChange1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}ActiveOrHistoricCurrencyAnd13DecimalAmount" minOccurs="0"/&gt;
 *         &lt;element name="AmtSgn" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PlusOrMinusIndicator" minOccurs="0"/&gt;
 *         &lt;element name="Rate" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PercentageRate" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceValueChange1", propOrder = {"amt", "amtSgn", "rate"})
public class PriceValueChange1 {

    @XmlElement(name = "Amt") protected ActiveOrHistoricCurrencyAnd13DecimalAmount amt;
    @XmlElement(name = "AmtSgn") protected Boolean amtSgn;
    @XmlElement(name = "Rate") protected BigDecimal rate;

    /**
     * Obtém o valor da propriedade amt.
     *
     * @return possible object is
     * {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     */
    public ActiveOrHistoricCurrencyAnd13DecimalAmount getAmt() {
        return amt;
    }

    /**
     * Define o valor da propriedade amt.
     *
     * @param value allowed object is
     *              {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     */
    public void setAmt(ActiveOrHistoricCurrencyAnd13DecimalAmount value) {
        this.amt = value;
    }

    /**
     * Obtém o valor da propriedade rate.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Define o valor da propriedade rate.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setRate(BigDecimal value) {
        this.rate = value;
    }

    /**
     * Obtém o valor da propriedade amtSgn.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isAmtSgn() {
        return amtSgn;
    }

    /**
     * Define o valor da propriedade amtSgn.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setAmtSgn(Boolean value) {
        this.amtSgn = value;
    }

}
