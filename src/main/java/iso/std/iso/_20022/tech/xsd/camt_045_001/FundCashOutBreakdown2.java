package iso.std.iso._20022.tech.xsd.camt_045_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de FundCashOutBreakdown2 complex type.
 *
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 *
 * <pre>
 * &lt;complexType name="FundCashOutBreakdown2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}ActiveOrHistoricCurrencyAndAmount" minOccurs="0"/&gt;
 *         &lt;element name="UnitsNb" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}FinancialInstrumentQuantity1" minOccurs="0"/&gt;
 *         &lt;element name="NewAmtInd" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}YesNoIndicator" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="InvstmtFndTxOutTp" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}InvestmentFundTransactionOutType1Code"/&gt;
 *           &lt;element name="XtndedInvstmtFndTxOutTp" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}Extended350Code"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice&gt;
 *           &lt;element name="OrgnlOrdrQtyTp" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}OrderQuantityType2Code"/&gt;
 *           &lt;element name="XtndedOrgnlOrdrQtyTp" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}Extended350Code"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="ChrgDtls" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}Charge16" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ComssnDtls" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}Commission9" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FundCashOutBreakdown2", propOrder = {
    "amt",
    "unitsNb",
    "newAmtInd",
    "invstmtFndTxOutTp",
    "xtndedInvstmtFndTxOutTp",
    "orgnlOrdrQtyTp",
    "xtndedOrgnlOrdrQtyTp",
    "chrgDtls",
    "comssnDtls"
})
public class FundCashOutBreakdown2 {

    @XmlElement(name = "Amt")
    protected ActiveOrHistoricCurrencyAndAmount amt;
    @XmlElement(name = "UnitsNb")
    protected FinancialInstrumentQuantity1 unitsNb;
    @XmlElement(name = "NewAmtInd")
    protected Boolean newAmtInd;
    @XmlElement(name = "InvstmtFndTxOutTp")
    @XmlSchemaType(name = "string")
    protected InvestmentFundTransactionOutType1Code invstmtFndTxOutTp;
    @XmlElement(name = "XtndedInvstmtFndTxOutTp")
    protected String xtndedInvstmtFndTxOutTp;
    @XmlElement(name = "OrgnlOrdrQtyTp")
    @XmlSchemaType(name = "string")
    protected OrderQuantityType2Code orgnlOrdrQtyTp;
    @XmlElement(name = "XtndedOrgnlOrdrQtyTp")
    protected String xtndedOrgnlOrdrQtyTp;
    @XmlElement(name = "ChrgDtls")
    protected List<Charge16> chrgDtls;
    @XmlElement(name = "ComssnDtls")
    protected List<Commission9> comssnDtls;

    /**
     * Obtém o valor da propriedade amt.
     *
     * @return
     *     possible object is
     *     {@link ActiveOrHistoricCurrencyAndAmount }
     *
     */
    public ActiveOrHistoricCurrencyAndAmount getAmt() {
        return amt;
    }

    /**
     * Define o valor da propriedade amt.
     *
     * @param value
     *     allowed object is
     *     {@link ActiveOrHistoricCurrencyAndAmount }
     *
     */
    public void setAmt(ActiveOrHistoricCurrencyAndAmount value) {
        this.amt = value;
    }

    /**
     * Gets the value of the chrgDtls property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chrgDtls property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChrgDtls().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Charge16 }
     *
     *
     */
    public List<Charge16> getChrgDtls() {
        if (chrgDtls == null) {
            chrgDtls = new ArrayList<Charge16>();
        }
        return this.chrgDtls;
    }

    /**
     * Gets the value of the comssnDtls property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comssnDtls property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComssnDtls().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Commission9 }
     *
     *
     */
    public List<Commission9> getComssnDtls() {
        if (comssnDtls == null) {
            comssnDtls = new ArrayList<Commission9>();
        }
        return this.comssnDtls;
    }

    /**
     * Obtém o valor da propriedade invstmtFndTxOutTp.
     *
     * @return
     *     possible object is
     *     {@link InvestmentFundTransactionOutType1Code }
     *
     */
    public InvestmentFundTransactionOutType1Code getInvstmtFndTxOutTp() {
        return invstmtFndTxOutTp;
    }

    /**
     * Define o valor da propriedade invstmtFndTxOutTp.
     *
     * @param value
     *     allowed object is
     *     {@link InvestmentFundTransactionOutType1Code }
     *
     */
    public void setInvstmtFndTxOutTp(InvestmentFundTransactionOutType1Code value) {
        this.invstmtFndTxOutTp = value;
    }

    /**
     * Obtém o valor da propriedade orgnlOrdrQtyTp.
     *
     * @return
     *     possible object is
     *     {@link OrderQuantityType2Code }
     *
     */
    public OrderQuantityType2Code getOrgnlOrdrQtyTp() {
        return orgnlOrdrQtyTp;
    }

    /**
     * Define o valor da propriedade orgnlOrdrQtyTp.
     *
     * @param value
     *     allowed object is
     *     {@link OrderQuantityType2Code }
     *
     */
    public void setOrgnlOrdrQtyTp(OrderQuantityType2Code value) {
        this.orgnlOrdrQtyTp = value;
    }

    /**
     * Obtém o valor da propriedade unitsNb.
     *
     * @return
     *     possible object is
     *     {@link FinancialInstrumentQuantity1 }
     *
     */
    public FinancialInstrumentQuantity1 getUnitsNb() {
        return unitsNb;
    }

    /**
     * Define o valor da propriedade unitsNb.
     *
     * @param value
     *     allowed object is
     *     {@link FinancialInstrumentQuantity1 }
     *
     */
    public void setUnitsNb(FinancialInstrumentQuantity1 value) {
        this.unitsNb = value;
    }

    /**
     * Obtém o valor da propriedade xtndedInvstmtFndTxOutTp.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getXtndedInvstmtFndTxOutTp() {
        return xtndedInvstmtFndTxOutTp;
    }

    /**
     * Define o valor da propriedade xtndedInvstmtFndTxOutTp.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setXtndedInvstmtFndTxOutTp(String value) {
        this.xtndedInvstmtFndTxOutTp = value;
    }

    /**
     * Obtém o valor da propriedade xtndedOrgnlOrdrQtyTp.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getXtndedOrgnlOrdrQtyTp() {
        return xtndedOrgnlOrdrQtyTp;
    }

    /**
     * Define o valor da propriedade xtndedOrgnlOrdrQtyTp.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setXtndedOrgnlOrdrQtyTp(String value) {
        this.xtndedOrgnlOrdrQtyTp = value;
    }

    /**
     * Obtém o valor da propriedade newAmtInd.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public Boolean isNewAmtInd() {
        return newAmtInd;
    }

    /**
     * Define o valor da propriedade newAmtInd.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setNewAmtInd(Boolean value) {
        this.newAmtInd = value;
    }

}
