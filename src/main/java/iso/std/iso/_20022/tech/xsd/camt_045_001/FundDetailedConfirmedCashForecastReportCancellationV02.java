package iso.std.iso._20022.tech.xsd.camt_045_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de FundDetailedConfirmedCashForecastReportCancellationV02 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="FundDetailedConfirmedCashForecastReportCancellationV02">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MsgId" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}MessageIdentification1"/>
 *         &lt;element name="PoolRef" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}AdditionalReference3" minOccurs="0"/>
 *         &lt;element name="PrvsRef" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}AdditionalReference3" minOccurs="0"/>
 *         &lt;element name="RltdRef" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}AdditionalReference3" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MsgPgntn" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}Pagination"/>
 *         &lt;element name="CshFcstRptToBeCanc" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}FundDetailedConfirmedCashForecastReport2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FundDetailedConfirmedCashForecastReportCancellationV02",
         propOrder = {"msgId", "poolRef", "prvsRef", "rltdRef", "msgPgntn", "cshFcstRptToBeCanc"})
public class FundDetailedConfirmedCashForecastReportCancellationV02 {

    @XmlElement(name = "MsgId", required = true) protected MessageIdentification1 msgId;
    @XmlElement(name = "PoolRef") protected AdditionalReference3 poolRef;
    @XmlElement(name = "PrvsRef") protected AdditionalReference3 prvsRef;
    @XmlElement(name = "RltdRef") protected List<AdditionalReference3> rltdRef;
    @XmlElement(name = "MsgPgntn", required = true) protected Pagination msgPgntn;
    @XmlElement(name = "CshFcstRptToBeCanc") protected FundDetailedConfirmedCashForecastReport2 cshFcstRptToBeCanc;

    /**
     * Obtém o valor da propriedade cshFcstRptToBeCanc.
     *
     * @return possible object is
     * {@link FundDetailedConfirmedCashForecastReport2 }
     */
    public FundDetailedConfirmedCashForecastReport2 getCshFcstRptToBeCanc() {
        return cshFcstRptToBeCanc;
    }

    /**
     * Define o valor da propriedade cshFcstRptToBeCanc.
     *
     * @param value allowed object is
     *              {@link FundDetailedConfirmedCashForecastReport2 }
     */
    public void setCshFcstRptToBeCanc(FundDetailedConfirmedCashForecastReport2 value) {
        this.cshFcstRptToBeCanc = value;
    }

    /**
     * Obtém o valor da propriedade msgId.
     *
     * @return possible object is
     * {@link MessageIdentification1 }
     */
    public MessageIdentification1 getMsgId() {
        return msgId;
    }

    /**
     * Define o valor da propriedade msgId.
     *
     * @param value allowed object is
     *              {@link MessageIdentification1 }
     */
    public void setMsgId(MessageIdentification1 value) {
        this.msgId = value;
    }

    /**
     * Obtém o valor da propriedade msgPgntn.
     *
     * @return possible object is
     * {@link Pagination }
     */
    public Pagination getMsgPgntn() {
        return msgPgntn;
    }

    /**
     * Define o valor da propriedade msgPgntn.
     *
     * @param value allowed object is
     *              {@link Pagination }
     */
    public void setMsgPgntn(Pagination value) {
        this.msgPgntn = value;
    }

    /**
     * Obtém o valor da propriedade poolRef.
     *
     * @return possible object is
     * {@link AdditionalReference3 }
     */
    public AdditionalReference3 getPoolRef() {
        return poolRef;
    }

    /**
     * Define o valor da propriedade poolRef.
     *
     * @param value allowed object is
     *              {@link AdditionalReference3 }
     */
    public void setPoolRef(AdditionalReference3 value) {
        this.poolRef = value;
    }

    /**
     * Obtém o valor da propriedade prvsRef.
     *
     * @return possible object is
     * {@link AdditionalReference3 }
     */
    public AdditionalReference3 getPrvsRef() {
        return prvsRef;
    }

    /**
     * Define o valor da propriedade prvsRef.
     *
     * @param value allowed object is
     *              {@link AdditionalReference3 }
     */
    public void setPrvsRef(AdditionalReference3 value) {
        this.prvsRef = value;
    }

    /**
     * Gets the value of the rltdRef property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rltdRef property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRltdRef().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalReference3 }
     */
    public List<AdditionalReference3> getRltdRef() {
        if (rltdRef == null) {
            rltdRef = new ArrayList<AdditionalReference3>();
        }
        return this.rltdRef;
    }

}
