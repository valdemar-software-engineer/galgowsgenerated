package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de Tax17 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="Tax17"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Tp" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}TaxType12Code"/&gt;
 *           &lt;element name="XtndedTp" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Extended350Code"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}ActiveOrHistoricCurrencyAnd13DecimalAmount" maxOccurs="7" minOccurs="0"/&gt;
 *         &lt;element name="Rate" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}PercentageRate" minOccurs="0"/&gt;
 *         &lt;element name="Ctry" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}CountryCode"/&gt;
 *         &lt;element name="TaxClctnDtls" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}TaxCalculationInformation4" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tax17", propOrder = {"tp", "xtndedTp", "amt", "rate", "ctry", "taxClctnDtls"})
public class Tax17 {

    @XmlElement(name = "Tp") @XmlSchemaType(name = "string") protected TaxType12Code tp;
    @XmlElement(name = "XtndedTp") protected String xtndedTp;
    @XmlElement(name = "Amt") protected List<ActiveOrHistoricCurrencyAnd13DecimalAmount> amt;
    @XmlElement(name = "Rate") protected BigDecimal rate;
    @XmlElement(name = "Ctry", required = true) protected String ctry;
    @XmlElement(name = "TaxClctnDtls") protected TaxCalculationInformation4 taxClctnDtls;

    /**
     * Gets the value of the amt property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the amt property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAmt().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     */
    public List<ActiveOrHistoricCurrencyAnd13DecimalAmount> getAmt() {
        if (amt == null) {
            amt = new ArrayList<ActiveOrHistoricCurrencyAnd13DecimalAmount>();
        }
        return this.amt;
    }

    /**
     * Obtém o valor da propriedade ctry.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCtry() {
        return ctry;
    }

    /**
     * Define o valor da propriedade ctry.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCtry(String value) {
        this.ctry = value;
    }

    /**
     * Obtém o valor da propriedade rate.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Define o valor da propriedade rate.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setRate(BigDecimal value) {
        this.rate = value;
    }

    /**
     * Obtém o valor da propriedade taxClctnDtls.
     *
     * @return possible object is
     * {@link TaxCalculationInformation4 }
     */
    public TaxCalculationInformation4 getTaxClctnDtls() {
        return taxClctnDtls;
    }

    /**
     * Define o valor da propriedade taxClctnDtls.
     *
     * @param value allowed object is
     *              {@link TaxCalculationInformation4 }
     */
    public void setTaxClctnDtls(TaxCalculationInformation4 value) {
        this.taxClctnDtls = value;
    }

    /**
     * Obtém o valor da propriedade tp.
     *
     * @return possible object is
     * {@link TaxType12Code }
     */
    public TaxType12Code getTp() {
        return tp;
    }

    /**
     * Define o valor da propriedade tp.
     *
     * @param value allowed object is
     *              {@link TaxType12Code }
     */
    public void setTp(TaxType12Code value) {
        this.tp = value;
    }

    /**
     * Obtém o valor da propriedade xtndedTp.
     *
     * @return possible object is
     * {@link String }
     */
    public String getXtndedTp() {
        return xtndedTp;
    }

    /**
     * Define o valor da propriedade xtndedTp.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setXtndedTp(String value) {
        this.xtndedTp = value;
    }

}
