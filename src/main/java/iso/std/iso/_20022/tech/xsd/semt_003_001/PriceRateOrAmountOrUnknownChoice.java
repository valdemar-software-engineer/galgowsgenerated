package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Classe Java de PriceRateOrAmountOrUnknownChoice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="PriceRateOrAmountOrUnknownChoice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Rate" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PercentageRate"/&gt;
 *           &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyAnd13DecimalAmount"/&gt;
 *           &lt;element name="UknwnInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceRateOrAmountOrUnknownChoice", propOrder = {"rate", "amt", "uknwnInd"})
public class PriceRateOrAmountOrUnknownChoice {

    @XmlElement(name = "Rate") protected BigDecimal rate;
    @XmlElement(name = "Amt") protected ActiveOrHistoricCurrencyAnd13DecimalAmount amt;
    @XmlElement(name = "UknwnInd") protected Boolean uknwnInd;

    /**
     * Obtém o valor da propriedade amt.
     *
     * @return possible object is
     * {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     */
    public ActiveOrHistoricCurrencyAnd13DecimalAmount getAmt() {
        return amt;
    }

    /**
     * Define o valor da propriedade amt.
     *
     * @param value allowed object is
     *              {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     */
    public void setAmt(ActiveOrHistoricCurrencyAnd13DecimalAmount value) {
        this.amt = value;
    }

    /**
     * Obtém o valor da propriedade rate.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Define o valor da propriedade rate.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setRate(BigDecimal value) {
        this.rate = value;
    }

    /**
     * Obtém o valor da propriedade uknwnInd.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isUknwnInd() {
        return uknwnInd;
    }

    /**
     * Define o valor da propriedade uknwnInd.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setUknwnInd(Boolean value) {
        this.uknwnInd = value;
    }

}
