package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de BalanceQuantity4Choice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="BalanceQuantity4Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Qty" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Quantity6Choice"/&gt;
 *           &lt;element name="Prtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification22"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BalanceQuantity4Choice", propOrder = {"qty", "prtry"})
public class BalanceQuantity4Choice {

    @XmlElement(name = "Qty") protected Quantity6Choice qty;
    @XmlElement(name = "Prtry") protected GenericIdentification22 prtry;

    /**
     * Obtém o valor da propriedade prtry.
     *
     * @return possible object is
     * {@link GenericIdentification22 }
     */
    public GenericIdentification22 getPrtry() {
        return prtry;
    }

    /**
     * Define o valor da propriedade prtry.
     *
     * @param value allowed object is
     *              {@link GenericIdentification22 }
     */
    public void setPrtry(GenericIdentification22 value) {
        this.prtry = value;
    }

    /**
     * Obtém o valor da propriedade qty.
     *
     * @return possible object is
     * {@link Quantity6Choice }
     */
    public Quantity6Choice getQty() {
        return qty;
    }

    /**
     * Define o valor da propriedade qty.
     *
     * @param value allowed object is
     *              {@link Quantity6Choice }
     */
    public void setQty(Quantity6Choice value) {
        this.qty = value;
    }

}
