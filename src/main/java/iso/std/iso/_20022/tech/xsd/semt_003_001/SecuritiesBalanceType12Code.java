package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de SecuritiesBalanceType12Code.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;simpleType name="SecuritiesBalanceType12Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="BLOK"/&gt;
 *     &lt;enumeration value="AWAS"/&gt;
 *     &lt;enumeration value="BLCA"/&gt;
 *     &lt;enumeration value="BLOT"/&gt;
 *     &lt;enumeration value="BLOV"/&gt;
 *     &lt;enumeration value="BORR"/&gt;
 *     &lt;enumeration value="BODE"/&gt;
 *     &lt;enumeration value="BORE"/&gt;
 *     &lt;enumeration value="COLI"/&gt;
 *     &lt;enumeration value="COLO"/&gt;
 *     &lt;enumeration value="LOAN"/&gt;
 *     &lt;enumeration value="LODE"/&gt;
 *     &lt;enumeration value="LORE"/&gt;
 *     &lt;enumeration value="MARG"/&gt;
 *     &lt;enumeration value="PECA"/&gt;
 *     &lt;enumeration value="PEDA"/&gt;
 *     &lt;enumeration value="PEND"/&gt;
 *     &lt;enumeration value="PENR"/&gt;
 *     &lt;enumeration value="PLED"/&gt;
 *     &lt;enumeration value="REGO"/&gt;
 *     &lt;enumeration value="RSTR"/&gt;
 *     &lt;enumeration value="OTHR"/&gt;
 *     &lt;enumeration value="TRAN"/&gt;
 *     &lt;enumeration value="DRAW"/&gt;
 *     &lt;enumeration value="WDOC"/&gt;
 *     &lt;enumeration value="BTRA"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "SecuritiesBalanceType12Code")
@XmlEnum
public enum SecuritiesBalanceType12Code {

    BLOK,
    AWAS,
    BLCA,
    BLOT,
    BLOV,
    BORR,
    BODE,
    BORE,
    COLI,
    COLO,
    LOAN,
    LODE,
    LORE,
    MARG,
    PECA,
    PEDA,
    PEND,
    PENR,
    PLED,
    REGO,
    RSTR,
    OTHR,
    TRAN,
    DRAW,
    WDOC,
    BTRA;

    public static SecuritiesBalanceType12Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
