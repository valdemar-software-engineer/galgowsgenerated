package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Classe Java de PerformanceFactors1 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PerformanceFactors1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CorpActnFctr" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}DecimalNumber" minOccurs="0"/&gt;
 *         &lt;element name="CmltvCorpActnFctr" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}DecimalNumber" minOccurs="0"/&gt;
 *         &lt;element name="AcmltnPrd" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}DatePeriodDetails" minOccurs="0"/&gt;
 *         &lt;element name="NrmlPrfrmnc" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}DecimalNumber" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PerformanceFactors1", propOrder = {"corpActnFctr", "cmltvCorpActnFctr", "acmltnPrd", "nrmlPrfrmnc"})
public class PerformanceFactors1 {

    @XmlElement(name = "CorpActnFctr") protected BigDecimal corpActnFctr;
    @XmlElement(name = "CmltvCorpActnFctr") protected BigDecimal cmltvCorpActnFctr;
    @XmlElement(name = "AcmltnPrd") protected DatePeriodDetails acmltnPrd;
    @XmlElement(name = "NrmlPrfrmnc") protected BigDecimal nrmlPrfrmnc;

    /**
     * Obtém o valor da propriedade acmltnPrd.
     *
     * @return possible object is
     * {@link DatePeriodDetails }
     */
    public DatePeriodDetails getAcmltnPrd() {
        return acmltnPrd;
    }

    /**
     * Define o valor da propriedade acmltnPrd.
     *
     * @param value allowed object is
     *              {@link DatePeriodDetails }
     */
    public void setAcmltnPrd(DatePeriodDetails value) {
        this.acmltnPrd = value;
    }

    /**
     * Obtém o valor da propriedade cmltvCorpActnFctr.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getCmltvCorpActnFctr() {
        return cmltvCorpActnFctr;
    }

    /**
     * Define o valor da propriedade cmltvCorpActnFctr.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setCmltvCorpActnFctr(BigDecimal value) {
        this.cmltvCorpActnFctr = value;
    }

    /**
     * Obtém o valor da propriedade corpActnFctr.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getCorpActnFctr() {
        return corpActnFctr;
    }

    /**
     * Define o valor da propriedade corpActnFctr.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setCorpActnFctr(BigDecimal value) {
        this.corpActnFctr = value;
    }

    /**
     * Obtém o valor da propriedade nrmlPrfrmnc.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getNrmlPrfrmnc() {
        return nrmlPrfrmnc;
    }

    /**
     * Define o valor da propriedade nrmlPrfrmnc.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setNrmlPrfrmnc(BigDecimal value) {
        this.nrmlPrfrmnc = value;
    }

}
