package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de StatisticsByPredefinedTimePeriods2 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="StatisticsByPredefinedTimePeriods2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="HghstPricVal12Mnths" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}PriceValue5" minOccurs="0"/&gt;
 *         &lt;element name="LwstPricVal12Mnths" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}PriceValue5" minOccurs="0"/&gt;
 *         &lt;element name="OneYrPricChng" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}PriceValueChange1" minOccurs="0"/&gt;
 *         &lt;element name="ThreeYrPricChng" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}PriceValueChange1" minOccurs="0"/&gt;
 *         &lt;element name="FiveYrPricChng" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}PriceValueChange1" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatisticsByPredefinedTimePeriods2",
         propOrder = {"hghstPricVal12Mnths", "lwstPricVal12Mnths", "oneYrPricChng", "threeYrPricChng", "fiveYrPricChng"})
public class StatisticsByPredefinedTimePeriods2 {

    @XmlElement(name = "HghstPricVal12Mnths") protected PriceValue5 hghstPricVal12Mnths;
    @XmlElement(name = "LwstPricVal12Mnths") protected PriceValue5 lwstPricVal12Mnths;
    @XmlElement(name = "OneYrPricChng") protected PriceValueChange1 oneYrPricChng;
    @XmlElement(name = "ThreeYrPricChng") protected PriceValueChange1 threeYrPricChng;
    @XmlElement(name = "FiveYrPricChng") protected PriceValueChange1 fiveYrPricChng;

    /**
     * Obtém o valor da propriedade fiveYrPricChng.
     *
     * @return possible object is
     * {@link PriceValueChange1 }
     */
    public PriceValueChange1 getFiveYrPricChng() {
        return fiveYrPricChng;
    }

    /**
     * Define o valor da propriedade fiveYrPricChng.
     *
     * @param value allowed object is
     *              {@link PriceValueChange1 }
     */
    public void setFiveYrPricChng(PriceValueChange1 value) {
        this.fiveYrPricChng = value;
    }

    /**
     * Obtém o valor da propriedade hghstPricVal12Mnths.
     *
     * @return possible object is
     * {@link PriceValue5 }
     */
    public PriceValue5 getHghstPricVal12Mnths() {
        return hghstPricVal12Mnths;
    }

    /**
     * Define o valor da propriedade hghstPricVal12Mnths.
     *
     * @param value allowed object is
     *              {@link PriceValue5 }
     */
    public void setHghstPricVal12Mnths(PriceValue5 value) {
        this.hghstPricVal12Mnths = value;
    }

    /**
     * Obtém o valor da propriedade lwstPricVal12Mnths.
     *
     * @return possible object is
     * {@link PriceValue5 }
     */
    public PriceValue5 getLwstPricVal12Mnths() {
        return lwstPricVal12Mnths;
    }

    /**
     * Define o valor da propriedade lwstPricVal12Mnths.
     *
     * @param value allowed object is
     *              {@link PriceValue5 }
     */
    public void setLwstPricVal12Mnths(PriceValue5 value) {
        this.lwstPricVal12Mnths = value;
    }

    /**
     * Obtém o valor da propriedade oneYrPricChng.
     *
     * @return possible object is
     * {@link PriceValueChange1 }
     */
    public PriceValueChange1 getOneYrPricChng() {
        return oneYrPricChng;
    }

    /**
     * Define o valor da propriedade oneYrPricChng.
     *
     * @param value allowed object is
     *              {@link PriceValueChange1 }
     */
    public void setOneYrPricChng(PriceValueChange1 value) {
        this.oneYrPricChng = value;
    }

    /**
     * Obtém o valor da propriedade threeYrPricChng.
     *
     * @return possible object is
     * {@link PriceValueChange1 }
     */
    public PriceValueChange1 getThreeYrPricChng() {
        return threeYrPricChng;
    }

    /**
     * Define o valor da propriedade threeYrPricChng.
     *
     * @param value allowed object is
     *              {@link PriceValueChange1 }
     */
    public void setThreeYrPricChng(PriceValueChange1 value) {
        this.threeYrPricChng = value;
    }

}
