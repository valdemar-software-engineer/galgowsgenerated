package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de SecuritiesBalanceType7Code.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;simpleType name="SecuritiesBalanceType7Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="COLA"/&gt;
 *     &lt;enumeration value="OTHR"/&gt;
 *     &lt;enumeration value="CLEN"/&gt;
 *     &lt;enumeration value="DIRT"/&gt;
 *     &lt;enumeration value="NOMI"/&gt;
 *     &lt;enumeration value="SPOS"/&gt;
 *     &lt;enumeration value="UNRG"/&gt;
 *     &lt;enumeration value="ISSU"/&gt;
 *     &lt;enumeration value="QUAS"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "SecuritiesBalanceType7Code")
@XmlEnum
public enum SecuritiesBalanceType7Code {

    COLA,
    OTHR,
    CLEN,
    DIRT,
    NOMI,
    SPOS,
    UNRG,
    ISSU,
    QUAS;

    public static SecuritiesBalanceType7Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
