package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de YieldedOrValueType1Choice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="YieldedOrValueType1Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Yldd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator"/&gt;
 *           &lt;element name="ValTp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PriceValueType1Code"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "YieldedOrValueType1Choice", propOrder = {"yldd", "valTp"})
public class YieldedOrValueType1Choice {

    @XmlElement(name = "Yldd") protected Boolean yldd;
    @XmlElement(name = "ValTp") @XmlSchemaType(name = "string") protected PriceValueType1Code valTp;

    /**
     * Obtém o valor da propriedade valTp.
     *
     * @return possible object is
     * {@link PriceValueType1Code }
     */
    public PriceValueType1Code getValTp() {
        return valTp;
    }

    /**
     * Define o valor da propriedade valTp.
     *
     * @param value allowed object is
     *              {@link PriceValueType1Code }
     */
    public void setValTp(PriceValueType1Code value) {
        this.valTp = value;
    }

    /**
     * Obtém o valor da propriedade yldd.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isYldd() {
        return yldd;
    }

    /**
     * Define o valor da propriedade yldd.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setYldd(Boolean value) {
        this.yldd = value;
    }

}
