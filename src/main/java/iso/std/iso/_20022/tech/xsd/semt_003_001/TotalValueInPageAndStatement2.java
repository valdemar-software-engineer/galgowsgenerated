package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de TotalValueInPageAndStatement2 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="TotalValueInPageAndStatement2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TtlHldgsValOfPg" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}AmountAndDirection6" minOccurs="0"/&gt;
 *         &lt;element name="TtlHldgsValOfStmt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}AmountAndDirection6"/&gt;
 *         &lt;element name="TtlBookValOfStmt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}AmountAndDirection6" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TotalValueInPageAndStatement2",
         propOrder = {"ttlHldgsValOfPg", "ttlHldgsValOfStmt", "ttlBookValOfStmt"})
public class TotalValueInPageAndStatement2 {

    @XmlElement(name = "TtlHldgsValOfPg") protected AmountAndDirection6 ttlHldgsValOfPg;
    @XmlElement(name = "TtlHldgsValOfStmt", required = true) protected AmountAndDirection6 ttlHldgsValOfStmt;
    @XmlElement(name = "TtlBookValOfStmt") protected AmountAndDirection6 ttlBookValOfStmt;

    /**
     * Obtém o valor da propriedade ttlBookValOfStmt.
     *
     * @return possible object is
     * {@link AmountAndDirection6 }
     */
    public AmountAndDirection6 getTtlBookValOfStmt() {
        return ttlBookValOfStmt;
    }

    /**
     * Define o valor da propriedade ttlBookValOfStmt.
     *
     * @param value allowed object is
     *              {@link AmountAndDirection6 }
     */
    public void setTtlBookValOfStmt(AmountAndDirection6 value) {
        this.ttlBookValOfStmt = value;
    }

    /**
     * Obtém o valor da propriedade ttlHldgsValOfPg.
     *
     * @return possible object is
     * {@link AmountAndDirection6 }
     */
    public AmountAndDirection6 getTtlHldgsValOfPg() {
        return ttlHldgsValOfPg;
    }

    /**
     * Define o valor da propriedade ttlHldgsValOfPg.
     *
     * @param value allowed object is
     *              {@link AmountAndDirection6 }
     */
    public void setTtlHldgsValOfPg(AmountAndDirection6 value) {
        this.ttlHldgsValOfPg = value;
    }

    /**
     * Obtém o valor da propriedade ttlHldgsValOfStmt.
     *
     * @return possible object is
     * {@link AmountAndDirection6 }
     */
    public AmountAndDirection6 getTtlHldgsValOfStmt() {
        return ttlHldgsValOfStmt;
    }

    /**
     * Define o valor da propriedade ttlHldgsValOfStmt.
     *
     * @param value allowed object is
     *              {@link AmountAndDirection6 }
     */
    public void setTtlHldgsValOfStmt(AmountAndDirection6 value) {
        this.ttlHldgsValOfStmt = value;
    }

}
