package iso.std.iso._20022.tech.xsd.head_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ClearingSystemMemberIdentification2 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ClearingSystemMemberIdentification2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ClrSysId" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}ClearingSystemIdentification2Choice" minOccurs="0"/&gt;
 *         &lt;element name="MmbId" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}Max35Text"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClearingSystemMemberIdentification2", propOrder = {"clrSysId", "mmbId"})
public class ClearingSystemMemberIdentification2 {

    @XmlElement(name = "ClrSysId") protected ClearingSystemIdentification2Choice clrSysId;
    @XmlElement(name = "MmbId", required = true) protected String mmbId;

    /**
     * Obtém o valor da propriedade clrSysId.
     *
     * @return possible object is
     * {@link ClearingSystemIdentification2Choice }
     */
    public ClearingSystemIdentification2Choice getClrSysId() {
        return clrSysId;
    }

    /**
     * Define o valor da propriedade clrSysId.
     *
     * @param value allowed object is
     *              {@link ClearingSystemIdentification2Choice }
     */
    public void setClrSysId(ClearingSystemIdentification2Choice value) {
        this.clrSysId = value;
    }

    /**
     * Obtém o valor da propriedade mmbId.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMmbId() {
        return mmbId;
    }

    /**
     * Define o valor da propriedade mmbId.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMmbId(String value) {
        this.mmbId = value;
    }

}
