package iso.std.iso._20022.tech.xsd.camt_045_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de FlowDirectionType1Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="FlowDirectionType1Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INCG"/>
 *     &lt;enumeration value="OUTG"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "FlowDirectionType1Code")
@XmlEnum
public enum FlowDirectionType1Code {

    INCG,
    OUTG;

    public static FlowDirectionType1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
