package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de AggregateBalancePerSafekeepingPlace12 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="AggregateBalancePerSafekeepingPlace12"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SfkpgPlc" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SafekeepingPlaceFormat3Choice"/&gt;
 *         &lt;element name="PlcOfListg" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}MarketIdentification5" minOccurs="0"/&gt;
 *         &lt;element name="AggtBal" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Balance1"/&gt;
 *         &lt;element name="PricDtls" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PriceInformation5" maxOccurs="unbounded"/&gt;
 *         &lt;element name="FXDtls" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ForeignExchangeTerms14" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DaysAcrd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Number" minOccurs="0"/&gt;
 *         &lt;element name="AcctBaseCcyAmts" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}BalanceAmounts1"/&gt;
 *         &lt;element name="InstrmCcyAmts" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}BalanceAmounts1" minOccurs="0"/&gt;
 *         &lt;element name="AltrnRptgCcyAmts" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}BalanceAmounts1" minOccurs="0"/&gt;
 *         &lt;element name="QtyBrkdwn" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}QuantityBreakdown4" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="BalBrkdwn" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SubBalanceInformation6" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AddtlBalBrkdwn" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}AdditionalBalanceInformation6" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="HldgAddtlDtls" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max350Text" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AggregateBalancePerSafekeepingPlace12",
         propOrder = {"sfkpgPlc", "plcOfListg", "aggtBal", "pricDtls", "fxDtls", "daysAcrd", "acctBaseCcyAmts", "instrmCcyAmts", "altrnRptgCcyAmts", "qtyBrkdwn", "balBrkdwn", "addtlBalBrkdwn", "hldgAddtlDtls"})
public class AggregateBalancePerSafekeepingPlace12 {

    @XmlElement(name = "SfkpgPlc", required = true) protected SafekeepingPlaceFormat3Choice sfkpgPlc;
    @XmlElement(name = "PlcOfListg") protected MarketIdentification5 plcOfListg;
    @XmlElement(name = "AggtBal", required = true) protected Balance1 aggtBal;
    @XmlElement(name = "PricDtls", required = true) protected List<PriceInformation5> pricDtls;
    @XmlElement(name = "FXDtls") protected List<ForeignExchangeTerms14> fxDtls;
    @XmlElement(name = "DaysAcrd") protected BigDecimal daysAcrd;
    @XmlElement(name = "AcctBaseCcyAmts", required = true) protected BalanceAmounts1 acctBaseCcyAmts;
    @XmlElement(name = "InstrmCcyAmts") protected BalanceAmounts1 instrmCcyAmts;
    @XmlElement(name = "AltrnRptgCcyAmts") protected BalanceAmounts1 altrnRptgCcyAmts;
    @XmlElement(name = "QtyBrkdwn") protected List<QuantityBreakdown4> qtyBrkdwn;
    @XmlElement(name = "BalBrkdwn") protected List<SubBalanceInformation6> balBrkdwn;
    @XmlElement(name = "AddtlBalBrkdwn") protected List<AdditionalBalanceInformation6> addtlBalBrkdwn;
    @XmlElement(name = "HldgAddtlDtls") protected String hldgAddtlDtls;

    /**
     * Obtém o valor da propriedade acctBaseCcyAmts.
     *
     * @return possible object is
     * {@link BalanceAmounts1 }
     */
    public BalanceAmounts1 getAcctBaseCcyAmts() {
        return acctBaseCcyAmts;
    }

    /**
     * Define o valor da propriedade acctBaseCcyAmts.
     *
     * @param value allowed object is
     *              {@link BalanceAmounts1 }
     */
    public void setAcctBaseCcyAmts(BalanceAmounts1 value) {
        this.acctBaseCcyAmts = value;
    }

    /**
     * Gets the value of the addtlBalBrkdwn property.
     * <p/>
     * <p/>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addtlBalBrkdwn property.
     * <p/>
     * <p/>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddtlBalBrkdwn().add(newItem);
     * </pre>
     * <p/>
     * <p/>
     * <p/>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalBalanceInformation6 }
     */
    public List<AdditionalBalanceInformation6> getAddtlBalBrkdwn() {
        if (addtlBalBrkdwn == null) {
            addtlBalBrkdwn = new ArrayList<AdditionalBalanceInformation6>();
        }
        return this.addtlBalBrkdwn;
    }

    /**
     * Obtém o valor da propriedade aggtBal.
     *
     * @return possible object is
     * {@link Balance1 }
     */
    public Balance1 getAggtBal() {
        return aggtBal;
    }

    /**
     * Define o valor da propriedade aggtBal.
     *
     * @param value allowed object is
     *              {@link Balance1 }
     */
    public void setAggtBal(Balance1 value) {
        this.aggtBal = value;
    }

    /**
     * Obtém o valor da propriedade altrnRptgCcyAmts.
     *
     * @return possible object is
     * {@link BalanceAmounts1 }
     */
    public BalanceAmounts1 getAltrnRptgCcyAmts() {
        return altrnRptgCcyAmts;
    }

    /**
     * Define o valor da propriedade altrnRptgCcyAmts.
     *
     * @param value allowed object is
     *              {@link BalanceAmounts1 }
     */
    public void setAltrnRptgCcyAmts(BalanceAmounts1 value) {
        this.altrnRptgCcyAmts = value;
    }

    /**
     * Gets the value of the balBrkdwn property.
     * <p/>
     * <p/>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the balBrkdwn property.
     * <p/>
     * <p/>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBalBrkdwn().add(newItem);
     * </pre>
     * <p/>
     * <p/>
     * <p/>
     * Objects of the following type(s) are allowed in the list
     * {@link SubBalanceInformation6 }
     */
    public List<SubBalanceInformation6> getBalBrkdwn() {
        if (balBrkdwn == null) {
            balBrkdwn = new ArrayList<SubBalanceInformation6>();
        }
        return this.balBrkdwn;
    }

    /**
     * Obtém o valor da propriedade daysAcrd.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getDaysAcrd() {
        return daysAcrd;
    }

    /**
     * Define o valor da propriedade daysAcrd.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setDaysAcrd(BigDecimal value) {
        this.daysAcrd = value;
    }

    /**
     * Gets the value of the fxDtls property.
     * <p/>
     * <p/>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fxDtls property.
     * <p/>
     * <p/>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFXDtls().add(newItem);
     * </pre>
     * <p/>
     * <p/>
     * <p/>
     * Objects of the following type(s) are allowed in the list
     * {@link ForeignExchangeTerms14 }
     */
    public List<ForeignExchangeTerms14> getFXDtls() {
        if (fxDtls == null) {
            fxDtls = new ArrayList<ForeignExchangeTerms14>();
        }
        return this.fxDtls;
    }

    /**
     * Obtém o valor da propriedade hldgAddtlDtls.
     *
     * @return possible object is
     * {@link String }
     */
    public String getHldgAddtlDtls() {
        return hldgAddtlDtls;
    }

    /**
     * Define o valor da propriedade hldgAddtlDtls.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setHldgAddtlDtls(String value) {
        this.hldgAddtlDtls = value;
    }

    /**
     * Obtém o valor da propriedade instrmCcyAmts.
     *
     * @return possible object is
     * {@link BalanceAmounts1 }
     */
    public BalanceAmounts1 getInstrmCcyAmts() {
        return instrmCcyAmts;
    }

    /**
     * Define o valor da propriedade instrmCcyAmts.
     *
     * @param value allowed object is
     *              {@link BalanceAmounts1 }
     */
    public void setInstrmCcyAmts(BalanceAmounts1 value) {
        this.instrmCcyAmts = value;
    }

    /**
     * Obtém o valor da propriedade plcOfListg.
     *
     * @return possible object is
     * {@link MarketIdentification5 }
     */
    public MarketIdentification5 getPlcOfListg() {
        return plcOfListg;
    }

    /**
     * Define o valor da propriedade plcOfListg.
     *
     * @param value allowed object is
     *              {@link MarketIdentification5 }
     */
    public void setPlcOfListg(MarketIdentification5 value) {
        this.plcOfListg = value;
    }

    /**
     * Gets the value of the pricDtls property.
     * <p/>
     * <p/>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pricDtls property.
     * <p/>
     * <p/>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPricDtls().add(newItem);
     * </pre>
     * <p/>
     * <p/>
     * <p/>
     * Objects of the following type(s) are allowed in the list
     * {@link PriceInformation5 }
     */
    public List<PriceInformation5> getPricDtls() {
        if (pricDtls == null) {
            pricDtls = new ArrayList<PriceInformation5>();
        }
        return this.pricDtls;
    }

    /**
     * Gets the value of the qtyBrkdwn property.
     * <p/>
     * <p/>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the qtyBrkdwn property.
     * <p/>
     * <p/>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQtyBrkdwn().add(newItem);
     * </pre>
     * <p/>
     * <p/>
     * <p/>
     * Objects of the following type(s) are allowed in the list
     * {@link QuantityBreakdown4 }
     */
    public List<QuantityBreakdown4> getQtyBrkdwn() {
        if (qtyBrkdwn == null) {
            qtyBrkdwn = new ArrayList<QuantityBreakdown4>();
        }
        return this.qtyBrkdwn;
    }

    /**
     * Obtém o valor da propriedade sfkpgPlc.
     *
     * @return possible object is
     * {@link SafekeepingPlaceFormat3Choice }
     */
    public SafekeepingPlaceFormat3Choice getSfkpgPlc() {
        return sfkpgPlc;
    }

    /**
     * Define o valor da propriedade sfkpgPlc.
     *
     * @param value allowed object is
     *              {@link SafekeepingPlaceFormat3Choice }
     */
    public void setSfkpgPlc(SafekeepingPlaceFormat3Choice value) {
        this.sfkpgPlc = value;
    }

}
