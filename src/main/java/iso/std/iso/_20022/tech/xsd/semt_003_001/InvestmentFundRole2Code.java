package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de InvestmentFundRole2Code.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;simpleType name="InvestmentFundRole2Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="FMCO"/&gt;
 *     &lt;enumeration value="REGI"/&gt;
 *     &lt;enumeration value="TRAG"/&gt;
 *     &lt;enumeration value="INTR"/&gt;
 *     &lt;enumeration value="DIST"/&gt;
 *     &lt;enumeration value="CONC"/&gt;
 *     &lt;enumeration value="UCL1"/&gt;
 *     &lt;enumeration value="UCL2"/&gt;
 *     &lt;enumeration value="TRAN"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "InvestmentFundRole2Code")
@XmlEnum
public enum InvestmentFundRole2Code {

    FMCO("FMCO"),
    REGI("REGI"),
    TRAG("TRAG"),
    INTR("INTR"),
    DIST("DIST"),
    CONC("CONC"),
    @XmlEnumValue("UCL1")
    UCL_1("UCL1"),
    @XmlEnumValue("UCL2")
    UCL_2("UCL2"),
    TRAN("TRAN");
    private final String value;

    InvestmentFundRole2Code(String v) {
        value = v;
    }

    public static InvestmentFundRole2Code fromValue(String v) {
        for (InvestmentFundRole2Code c : InvestmentFundRole2Code.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public String value() {
        return value;
    }

}
