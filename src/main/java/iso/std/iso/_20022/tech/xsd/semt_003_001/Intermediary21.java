package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Intermediary21 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="Intermediary21"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PartyIdentification49Choice"/&gt;
 *         &lt;element name="Role" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Role2Choice" minOccurs="0"/&gt;
 *         &lt;element name="Acct" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Account11" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Intermediary21", propOrder = {"id", "role", "acct"})
public class Intermediary21 {

    @XmlElement(name = "Id", required = true) protected PartyIdentification49Choice id;
    @XmlElement(name = "Role") protected Role2Choice role;
    @XmlElement(name = "Acct") protected Account11 acct;

    /**
     * Obtém o valor da propriedade acct.
     *
     * @return possible object is
     * {@link Account11 }
     */
    public Account11 getAcct() {
        return acct;
    }

    /**
     * Define o valor da propriedade acct.
     *
     * @param value allowed object is
     *              {@link Account11 }
     */
    public void setAcct(Account11 value) {
        this.acct = value;
    }

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link PartyIdentification49Choice }
     */
    public PartyIdentification49Choice getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link PartyIdentification49Choice }
     */
    public void setId(PartyIdentification49Choice value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade role.
     *
     * @return possible object is
     * {@link Role2Choice }
     */
    public Role2Choice getRole() {
        return role;
    }

    /**
     * Define o valor da propriedade role.
     *
     * @param value allowed object is
     *              {@link Role2Choice }
     */
    public void setRole(Role2Choice value) {
        this.role = value;
    }

}
