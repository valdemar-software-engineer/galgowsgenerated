package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the iso.std.iso._20022.tech.xsd.semt_003_001 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Document_QNAME = new QName("urn:iso:std:iso:20022:tech:xsd:semt.003.001.04",
        "Document");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: iso.std.iso._20022.tech.xsd.semt_003_001
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Account11 }
     */
    public Account11 createAccount11() {
        return new Account11();
    }

    /**
     * Create an instance of {@link AccountIdentification1 }
     */
    public AccountIdentification1 createAccountIdentification1() {
        return new AccountIdentification1();
    }

    /**
     * Create an instance of {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     */
    public ActiveOrHistoricCurrencyAnd13DecimalAmount createActiveOrHistoricCurrencyAnd13DecimalAmount() {
        return new ActiveOrHistoricCurrencyAnd13DecimalAmount();
    }

    /**
     * Create an instance of {@link ActiveOrHistoricCurrencyAndAmount }
     */
    public ActiveOrHistoricCurrencyAndAmount createActiveOrHistoricCurrencyAndAmount() {
        return new ActiveOrHistoricCurrencyAndAmount();
    }

    /**
     * Create an instance of {@link AdditionalBalanceInformation6 }
     */
    public AdditionalBalanceInformation6 createAdditionalBalanceInformation6() {
        return new AdditionalBalanceInformation6();
    }

    /**
     * Create an instance of {@link AggregateBalanceInformation13 }
     */
    public AggregateBalanceInformation13 createAggregateBalanceInformation13() {
        return new AggregateBalanceInformation13();
    }

    /**
     * Create an instance of {@link AggregateBalancePerSafekeepingPlace12 }
     */
    public AggregateBalancePerSafekeepingPlace12 createAggregateBalancePerSafekeepingPlace12() {
        return new AggregateBalancePerSafekeepingPlace12();
    }

    /**
     * Create an instance of {@link AmountAndDirection6 }
     */
    public AmountAndDirection6 createAmountAndDirection6() {
        return new AmountAndDirection6();
    }

    /**
     * Create an instance of {@link Balance1 }
     */
    public Balance1 createBalance1() {
        return new Balance1();
    }

    /**
     * Create an instance of {@link BalanceAmounts1 }
     */
    public BalanceAmounts1 createBalanceAmounts1() {
        return new BalanceAmounts1();
    }

    /**
     * Create an instance of {@link BalanceAmounts2 }
     */
    public BalanceAmounts2 createBalanceAmounts2() {
        return new BalanceAmounts2();
    }

    /**
     * Create an instance of {@link BalanceQuantity4Choice }
     */
    public BalanceQuantity4Choice createBalanceQuantity4Choice() {
        return new BalanceQuantity4Choice();
    }

    /**
     * Create an instance of {@link ClassificationType2Choice }
     */
    public ClassificationType2Choice createClassificationType2Choice() {
        return new ClassificationType2Choice();
    }

    /**
     * Create an instance of {@link DateAndDateTimeChoice }
     */
    public DateAndDateTimeChoice createDateAndDateTimeChoice() {
        return new DateAndDateTimeChoice();
    }

    /**
     * Create an instance of {@link DerivativeBasicAttributes1 }
     */
    public DerivativeBasicAttributes1 createDerivativeBasicAttributes1() {
        return new DerivativeBasicAttributes1();
    }

    /**
     * Create an instance of {@link Document }
     */
    public Document createDocument() {
        return new Document();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Document }{@code >}}
     */
    @XmlElementDecl(namespace = "urn:iso:std:iso:20022:tech:xsd:semt.003.001.04", name = "Document")
    public JAXBElement<Document> createDocument(Document value) {
        return new JAXBElement<Document>(_Document_QNAME, Document.class, null, value);
    }

    /**
     * Create an instance of {@link FinancialInstrument21 }
     */
    public FinancialInstrument21 createFinancialInstrument21() {
        return new FinancialInstrument21();
    }

    /**
     * Create an instance of {@link FinancialInstrumentAttributes20 }
     */
    public FinancialInstrumentAttributes20 createFinancialInstrumentAttributes20() {
        return new FinancialInstrumentAttributes20();
    }

    /**
     * Create an instance of {@link FinancialInstrumentQuantity1Choice }
     */
    public FinancialInstrumentQuantity1Choice createFinancialInstrumentQuantity1Choice() {
        return new FinancialInstrumentQuantity1Choice();
    }

    /**
     * Create an instance of {@link ForeignExchangeTerms14 }
     */
    public ForeignExchangeTerms14 createForeignExchangeTerms14() {
        return new ForeignExchangeTerms14();
    }

    /**
     * Create an instance of {@link FormOfSecurity2Choice }
     */
    public FormOfSecurity2Choice createFormOfSecurity2Choice() {
        return new FormOfSecurity2Choice();
    }

    /**
     * Create an instance of {@link Frequency3Choice }
     */
    public Frequency3Choice createFrequency3Choice() {
        return new Frequency3Choice();
    }

    /**
     * Create an instance of {@link Frequency4Choice }
     */
    public Frequency4Choice createFrequency4Choice() {
        return new Frequency4Choice();
    }

    /**
     * Create an instance of {@link GenericIdentification1 }
     */
    public GenericIdentification1 createGenericIdentification1() {
        return new GenericIdentification1();
    }

    /**
     * Create an instance of {@link GenericIdentification13 }
     */
    public GenericIdentification13 createGenericIdentification13() {
        return new GenericIdentification13();
    }

    /**
     * Create an instance of {@link GenericIdentification19 }
     */
    public GenericIdentification19 createGenericIdentification19() {
        return new GenericIdentification19();
    }

    /**
     * Create an instance of {@link GenericIdentification20 }
     */
    public GenericIdentification20 createGenericIdentification20() {
        return new GenericIdentification20();
    }

    /**
     * Create an instance of {@link GenericIdentification21 }
     */
    public GenericIdentification21 createGenericIdentification21() {
        return new GenericIdentification21();
    }

    /**
     * Create an instance of {@link GenericIdentification22 }
     */
    public GenericIdentification22 createGenericIdentification22() {
        return new GenericIdentification22();
    }

    /**
     * Create an instance of {@link IdentificationSource3Choice }
     */
    public IdentificationSource3Choice createIdentificationSource3Choice() {
        return new IdentificationSource3Choice();
    }

    /**
     * Create an instance of {@link InterestComputationMethodFormat1Choice }
     */
    public InterestComputationMethodFormat1Choice createInterestComputationMethodFormat1Choice() {
        return new InterestComputationMethodFormat1Choice();
    }

    /**
     * Create an instance of {@link Intermediary21 }
     */
    public Intermediary21 createIntermediary21() {
        return new Intermediary21();
    }

    /**
     * Create an instance of {@link MarketIdentification1Choice }
     */
    public MarketIdentification1Choice createMarketIdentification1Choice() {
        return new MarketIdentification1Choice();
    }

    /**
     * Create an instance of {@link MarketIdentification5 }
     */
    public MarketIdentification5 createMarketIdentification5() {
        return new MarketIdentification5();
    }

    /**
     * Create an instance of {@link MarketIdentification6 }
     */
    public MarketIdentification6 createMarketIdentification6() {
        return new MarketIdentification6();
    }

    /**
     * Create an instance of {@link MarketType2Choice }
     */
    public MarketType2Choice createMarketType2Choice() {
        return new MarketType2Choice();
    }

    /**
     * Create an instance of {@link MarketType4Choice }
     */
    public MarketType4Choice createMarketType4Choice() {
        return new MarketType4Choice();
    }

    /**
     * Create an instance of {@link NameAndAddress5 }
     */
    public NameAndAddress5 createNameAndAddress5() {
        return new NameAndAddress5();
    }

    /**
     * Create an instance of {@link Number2Choice }
     */
    public Number2Choice createNumber2Choice() {
        return new Number2Choice();
    }

    /**
     * Create an instance of {@link Number3Choice }
     */
    public Number3Choice createNumber3Choice() {
        return new Number3Choice();
    }

    /**
     * Create an instance of {@link OptionStyle4Choice }
     */
    public OptionStyle4Choice createOptionStyle4Choice() {
        return new OptionStyle4Choice();
    }

    /**
     * Create an instance of {@link OptionType2Choice }
     */
    public OptionType2Choice createOptionType2Choice() {
        return new OptionType2Choice();
    }

    /**
     * Create an instance of {@link OriginalAndCurrentQuantities1 }
     */
    public OriginalAndCurrentQuantities1 createOriginalAndCurrentQuantities1() {
        return new OriginalAndCurrentQuantities1();
    }

    /**
     * Create an instance of {@link OtherIdentification1 }
     */
    public OtherIdentification1 createOtherIdentification1() {
        return new OtherIdentification1();
    }

    /**
     * Create an instance of {@link Pagination }
     */
    public Pagination createPagination() {
        return new Pagination();
    }

    /**
     * Create an instance of {@link PartyIdentification36Choice }
     */
    public PartyIdentification36Choice createPartyIdentification36Choice() {
        return new PartyIdentification36Choice();
    }

    /**
     * Create an instance of {@link PartyIdentification49Choice }
     */
    public PartyIdentification49Choice createPartyIdentification49Choice() {
        return new PartyIdentification49Choice();
    }

    /**
     * Create an instance of {@link PaymentDirection2Choice }
     */
    public PaymentDirection2Choice createPaymentDirection2Choice() {
        return new PaymentDirection2Choice();
    }

    /**
     * Create an instance of {@link PostalAddress1 }
     */
    public PostalAddress1 createPostalAddress1() {
        return new PostalAddress1();
    }

    /**
     * Create an instance of {@link PreferenceToIncome2Choice }
     */
    public PreferenceToIncome2Choice createPreferenceToIncome2Choice() {
        return new PreferenceToIncome2Choice();
    }

    /**
     * Create an instance of {@link Price2 }
     */
    public Price2 createPrice2() {
        return new Price2();
    }

    /**
     * Create an instance of {@link PriceInformation5 }
     */
    public PriceInformation5 createPriceInformation5() {
        return new PriceInformation5();
    }

    /**
     * Create an instance of {@link PriceRateOrAmountChoice }
     */
    public PriceRateOrAmountChoice createPriceRateOrAmountChoice() {
        return new PriceRateOrAmountChoice();
    }

    /**
     * Create an instance of {@link PriceRateOrAmountOrUnknownChoice }
     */
    public PriceRateOrAmountOrUnknownChoice createPriceRateOrAmountOrUnknownChoice() {
        return new PriceRateOrAmountOrUnknownChoice();
    }

    /**
     * Create an instance of {@link PriceType1Choice }
     */
    public PriceType1Choice createPriceType1Choice() {
        return new PriceType1Choice();
    }

    /**
     * Create an instance of {@link PurposeCode1Choice }
     */
    public PurposeCode1Choice createPurposeCode1Choice() {
        return new PurposeCode1Choice();
    }

    /**
     * Create an instance of {@link PurposeCode2Choice }
     */
    public PurposeCode2Choice createPurposeCode2Choice() {
        return new PurposeCode2Choice();
    }

    /**
     * Create an instance of {@link Quantity6Choice }
     */
    public Quantity6Choice createQuantity6Choice() {
        return new Quantity6Choice();
    }

    /**
     * Create an instance of {@link QuantityAndAvailability1 }
     */
    public QuantityAndAvailability1 createQuantityAndAvailability1() {
        return new QuantityAndAvailability1();
    }

    /**
     * Create an instance of {@link QuantityBreakdown4 }
     */
    public QuantityBreakdown4 createQuantityBreakdown4() {
        return new QuantityBreakdown4();
    }

    /**
     * Create an instance of {@link Role2Choice }
     */
    public Role2Choice createRole2Choice() {
        return new Role2Choice();
    }

    /**
     * Create an instance of {@link SafekeepingPlaceFormat3Choice }
     */
    public SafekeepingPlaceFormat3Choice createSafekeepingPlaceFormat3Choice() {
        return new SafekeepingPlaceFormat3Choice();
    }

    /**
     * Create an instance of {@link SafekeepingPlaceTypeAndAnyBICIdentifier1 }
     */
    public SafekeepingPlaceTypeAndAnyBICIdentifier1 createSafekeepingPlaceTypeAndAnyBICIdentifier1() {
        return new SafekeepingPlaceTypeAndAnyBICIdentifier1();
    }

    /**
     * Create an instance of {@link SafekeepingPlaceTypeAndText3 }
     */
    public SafekeepingPlaceTypeAndText3 createSafekeepingPlaceTypeAndText3() {
        return new SafekeepingPlaceTypeAndText3();
    }

    /**
     * Create an instance of {@link SecuritiesAccount11 }
     */
    public SecuritiesAccount11 createSecuritiesAccount11() {
        return new SecuritiesAccount11();
    }

    /**
     * Create an instance of {@link SecuritiesAccount14 }
     */
    public SecuritiesAccount14 createSecuritiesAccount14() {
        return new SecuritiesAccount14();
    }

    /**
     * Create an instance of {@link SecuritiesBalanceAccountingReportV04 }
     */
    public SecuritiesBalanceAccountingReportV04 createSecuritiesBalanceAccountingReportV04() {
        return new SecuritiesBalanceAccountingReportV04();
    }

    /**
     * Create an instance of {@link SecuritiesPaymentStatus2Choice }
     */
    public SecuritiesPaymentStatus2Choice createSecuritiesPaymentStatus2Choice() {
        return new SecuritiesPaymentStatus2Choice();
    }

    /**
     * Create an instance of {@link SecurityIdentification14 }
     */
    public SecurityIdentification14 createSecurityIdentification14() {
        return new SecurityIdentification14();
    }

    /**
     * Create an instance of {@link SimpleIdentificationInformation }
     */
    public SimpleIdentificationInformation createSimpleIdentificationInformation() {
        return new SimpleIdentificationInformation();
    }

    /**
     * Create an instance of {@link Statement20 }
     */
    public Statement20 createStatement20() {
        return new Statement20();
    }

    /**
     * Create an instance of {@link StatementBasis3Choice }
     */
    public StatementBasis3Choice createStatementBasis3Choice() {
        return new StatementBasis3Choice();
    }

    /**
     * Create an instance of {@link SubAccountIdentification16 }
     */
    public SubAccountIdentification16 createSubAccountIdentification16() {
        return new SubAccountIdentification16();
    }

    /**
     * Create an instance of {@link SubBalanceInformation6 }
     */
    public SubBalanceInformation6 createSubBalanceInformation6() {
        return new SubBalanceInformation6();
    }

    /**
     * Create an instance of {@link SubBalanceQuantity3Choice }
     */
    public SubBalanceQuantity3Choice createSubBalanceQuantity3Choice() {
        return new SubBalanceQuantity3Choice();
    }

    /**
     * Create an instance of {@link SubBalanceType5Choice }
     */
    public SubBalanceType5Choice createSubBalanceType5Choice() {
        return new SubBalanceType5Choice();
    }

    /**
     * Create an instance of {@link SubBalanceType6Choice }
     */
    public SubBalanceType6Choice createSubBalanceType6Choice() {
        return new SubBalanceType6Choice();
    }

    /**
     * Create an instance of {@link SupplementaryData1 }
     */
    public SupplementaryData1 createSupplementaryData1() {
        return new SupplementaryData1();
    }

    /**
     * Create an instance of {@link SupplementaryDataEnvelope1 }
     */
    public SupplementaryDataEnvelope1 createSupplementaryDataEnvelope1() {
        return new SupplementaryDataEnvelope1();
    }

    /**
     * Create an instance of {@link TotalValueInPageAndStatement2 }
     */
    public TotalValueInPageAndStatement2 createTotalValueInPageAndStatement2() {
        return new TotalValueInPageAndStatement2();
    }

    /**
     * Create an instance of {@link TypeOfPrice3Choice }
     */
    public TypeOfPrice3Choice createTypeOfPrice3Choice() {
        return new TypeOfPrice3Choice();
    }

    /**
     * Create an instance of {@link TypeOfPrice4Choice }
     */
    public TypeOfPrice4Choice createTypeOfPrice4Choice() {
        return new TypeOfPrice4Choice();
    }

    /**
     * Create an instance of {@link UpdateType2Choice }
     */
    public UpdateType2Choice createUpdateType2Choice() {
        return new UpdateType2Choice();
    }

    /**
     * Create an instance of {@link YieldedOrValueType1Choice }
     */
    public YieldedOrValueType1Choice createYieldedOrValueType1Choice() {
        return new YieldedOrValueType1Choice();
    }

}
