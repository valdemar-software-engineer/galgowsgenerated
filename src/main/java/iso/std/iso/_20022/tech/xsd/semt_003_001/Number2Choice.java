package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Number2Choice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="Number2Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Shrt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Exact3NumericText"/&gt;
 *           &lt;element name="Lng" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification1"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Number2Choice", propOrder = {"shrt", "lng"})
public class Number2Choice {

    @XmlElement(name = "Shrt") protected String shrt;
    @XmlElement(name = "Lng") protected GenericIdentification1 lng;

    /**
     * Obtém o valor da propriedade lng.
     *
     * @return possible object is
     * {@link GenericIdentification1 }
     */
    public GenericIdentification1 getLng() {
        return lng;
    }

    /**
     * Define o valor da propriedade lng.
     *
     * @param value allowed object is
     *              {@link GenericIdentification1 }
     */
    public void setLng(GenericIdentification1 value) {
        this.lng = value;
    }

    /**
     * Obtém o valor da propriedade shrt.
     *
     * @return possible object is
     * {@link String }
     */
    public String getShrt() {
        return shrt;
    }

    /**
     * Define o valor da propriedade shrt.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setShrt(String value) {
        this.shrt = value;
    }

}
