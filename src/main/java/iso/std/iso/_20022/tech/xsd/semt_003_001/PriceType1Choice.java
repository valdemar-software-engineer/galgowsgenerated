package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de PriceType1Choice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="PriceType1Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Mkt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Price2"/&gt;
 *           &lt;element name="Indctv" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Price2"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceType1Choice", propOrder = {"mkt", "indctv"})
public class PriceType1Choice {

    @XmlElement(name = "Mkt") protected Price2 mkt;
    @XmlElement(name = "Indctv") protected Price2 indctv;

    /**
     * Obtém o valor da propriedade indctv.
     *
     * @return possible object is
     * {@link Price2 }
     */
    public Price2 getIndctv() {
        return indctv;
    }

    /**
     * Define o valor da propriedade indctv.
     *
     * @param value allowed object is
     *              {@link Price2 }
     */
    public void setIndctv(Price2 value) {
        this.indctv = value;
    }

    /**
     * Obtém o valor da propriedade mkt.
     *
     * @return possible object is
     * {@link Price2 }
     */
    public Price2 getMkt() {
        return mkt;
    }

    /**
     * Define o valor da propriedade mkt.
     *
     * @param value allowed object is
     *              {@link Price2 }
     */
    public void setMkt(Price2 value) {
        this.mkt = value;
    }

}
