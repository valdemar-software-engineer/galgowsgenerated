package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ChargeType9Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="ChargeType9Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="MANF"/&gt;
 *     &lt;enumeration value="BEND"/&gt;
 *     &lt;enumeration value="FEND"/&gt;
 *     &lt;enumeration value="ADVI"/&gt;
 *     &lt;enumeration value="CUST"/&gt;
 *     &lt;enumeration value="PUBL"/&gt;
 *     &lt;enumeration value="ACCT"/&gt;
 *     &lt;enumeration value="EQUL"/&gt;
 *     &lt;enumeration value="PENA"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "ChargeType9Code")
@XmlEnum
public enum ChargeType9Code {

    MANF,
    BEND,
    FEND,
    ADVI,
    CUST,
    PUBL,
    ACCT,
    EQUL,
    PENA;

    public static ChargeType9Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
