package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de MarketIdentification1Choice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="MarketIdentification1Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="MktIdrCd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}MICIdentifier"/&gt;
 *           &lt;element name="Desc" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketIdentification1Choice", propOrder = {"mktIdrCd", "desc"})
public class MarketIdentification1Choice {

    @XmlElement(name = "MktIdrCd") protected String mktIdrCd;
    @XmlElement(name = "Desc") protected String desc;

    /**
     * Obtém o valor da propriedade desc.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Define o valor da propriedade desc.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDesc(String value) {
        this.desc = value;
    }

    /**
     * Obtém o valor da propriedade mktIdrCd.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMktIdrCd() {
        return mktIdrCd;
    }

    /**
     * Define o valor da propriedade mktIdrCd.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMktIdrCd(String value) {
        this.mktIdrCd = value;
    }

}
