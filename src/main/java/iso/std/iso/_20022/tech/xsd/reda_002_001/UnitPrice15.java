package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de UnitPrice15 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="UnitPrice15"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Tp" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}TypeOfPrice9Code"/&gt;
 *           &lt;element name="XtndedTp" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Extended350Code"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="PricMtd" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}PriceMethod1Code" minOccurs="0"/&gt;
 *         &lt;element name="ValInInvstmtCcy" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}PriceValue1" maxOccurs="unbounded"/&gt;
 *         &lt;element name="ValInAltrntvCcy" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}PriceValue1" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ForExctnInd" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}YesNoIndicator"/&gt;
 *         &lt;element name="CumDvddInd" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}YesNoIndicator"/&gt;
 *         &lt;element name="ClctnBsis" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}PercentageRate" minOccurs="0"/&gt;
 *         &lt;element name="EstmtdPricInd" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}YesNoIndicator"/&gt;
 *         &lt;element name="NbOfDaysAcrd" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Number" minOccurs="0"/&gt;
 *         &lt;element name="TaxblIncmPerShr" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}ActiveOrHistoricCurrencyAnd13DecimalAmount" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="TaxblIncmPerShrClctd" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}TaxableIncomePerShareCalculated2Code" minOccurs="0"/&gt;
 *           &lt;element name="XtndedTaxblIncmPerShrClctd" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Extended350Code" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="TaxblIncmPerDvdd" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}ActiveOrHistoricCurrencyAnd13DecimalAmount" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="EUDvddSts" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}EUDividendStatus1Code" minOccurs="0"/&gt;
 *           &lt;element name="XtndedEUDvddSts" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Extended350Code" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="ChrgDtls" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Charge15" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TaxLbltyDtls" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Tax17" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TaxRfndDtls" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Tax17" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnitPrice15",
         propOrder = {"tp", "xtndedTp", "pricMtd", "valInInvstmtCcy", "valInAltrntvCcy", "forExctnInd", "cumDvddInd", "clctnBsis", "estmtdPricInd", "nbOfDaysAcrd", "taxblIncmPerShr", "taxblIncmPerShrClctd", "xtndedTaxblIncmPerShrClctd", "taxblIncmPerDvdd", "euDvddSts", "xtndedEUDvddSts", "chrgDtls", "taxLbltyDtls", "taxRfndDtls"})
public class UnitPrice15 {

    @XmlElement(name = "Tp") @XmlSchemaType(name = "string") protected TypeOfPrice9Code tp;
    @XmlElement(name = "XtndedTp") protected String xtndedTp;
    @XmlElement(name = "PricMtd") @XmlSchemaType(name = "string") protected PriceMethod1Code pricMtd;
    @XmlElement(name = "ValInInvstmtCcy", required = true) protected List<PriceValue1> valInInvstmtCcy;
    @XmlElement(name = "ValInAltrntvCcy") protected List<PriceValue1> valInAltrntvCcy;
    @XmlElement(name = "ForExctnInd") protected boolean forExctnInd;
    @XmlElement(name = "CumDvddInd") protected boolean cumDvddInd;
    @XmlElement(name = "ClctnBsis") protected BigDecimal clctnBsis;
    @XmlElement(name = "EstmtdPricInd") protected boolean estmtdPricInd;
    @XmlElement(name = "NbOfDaysAcrd") protected BigDecimal nbOfDaysAcrd;
    @XmlElement(name = "TaxblIncmPerShr") protected ActiveOrHistoricCurrencyAnd13DecimalAmount taxblIncmPerShr;
    @XmlElement(name = "TaxblIncmPerShrClctd") @XmlSchemaType(name = "string") protected TaxableIncomePerShareCalculated2Code
        taxblIncmPerShrClctd;
    @XmlElement(name = "XtndedTaxblIncmPerShrClctd") protected String xtndedTaxblIncmPerShrClctd;
    @XmlElement(name = "TaxblIncmPerDvdd") protected ActiveOrHistoricCurrencyAnd13DecimalAmount taxblIncmPerDvdd;
    @XmlElement(name = "EUDvddSts") @XmlSchemaType(name = "string") protected EUDividendStatus1Code euDvddSts;
    @XmlElement(name = "XtndedEUDvddSts") protected String xtndedEUDvddSts;
    @XmlElement(name = "ChrgDtls") protected List<Charge15> chrgDtls;
    @XmlElement(name = "TaxLbltyDtls") protected List<Tax17> taxLbltyDtls;
    @XmlElement(name = "TaxRfndDtls") protected List<Tax17> taxRfndDtls;

    /**
     * Gets the value of the chrgDtls property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chrgDtls property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChrgDtls().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Charge15 }
     */
    public List<Charge15> getChrgDtls() {
        if (chrgDtls == null) {
            chrgDtls = new ArrayList<Charge15>();
        }
        return this.chrgDtls;
    }

    /**
     * Obtém o valor da propriedade clctnBsis.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getClctnBsis() {
        return clctnBsis;
    }

    /**
     * Define o valor da propriedade clctnBsis.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setClctnBsis(BigDecimal value) {
        this.clctnBsis = value;
    }

    /**
     * Obtém o valor da propriedade euDvddSts.
     *
     * @return possible object is
     * {@link EUDividendStatus1Code }
     */
    public EUDividendStatus1Code getEUDvddSts() {
        return euDvddSts;
    }

    /**
     * Define o valor da propriedade euDvddSts.
     *
     * @param value allowed object is
     *              {@link EUDividendStatus1Code }
     */
    public void setEUDvddSts(EUDividendStatus1Code value) {
        this.euDvddSts = value;
    }

    /**
     * Obtém o valor da propriedade nbOfDaysAcrd.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getNbOfDaysAcrd() {
        return nbOfDaysAcrd;
    }

    /**
     * Define o valor da propriedade nbOfDaysAcrd.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setNbOfDaysAcrd(BigDecimal value) {
        this.nbOfDaysAcrd = value;
    }

    /**
     * Obtém o valor da propriedade pricMtd.
     *
     * @return possible object is
     * {@link PriceMethod1Code }
     */
    public PriceMethod1Code getPricMtd() {
        return pricMtd;
    }

    /**
     * Define o valor da propriedade pricMtd.
     *
     * @param value allowed object is
     *              {@link PriceMethod1Code }
     */
    public void setPricMtd(PriceMethod1Code value) {
        this.pricMtd = value;
    }

    /**
     * Gets the value of the taxLbltyDtls property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxLbltyDtls property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaxLbltyDtls().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tax17 }
     */
    public List<Tax17> getTaxLbltyDtls() {
        if (taxLbltyDtls == null) {
            taxLbltyDtls = new ArrayList<Tax17>();
        }
        return this.taxLbltyDtls;
    }

    /**
     * Gets the value of the taxRfndDtls property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxRfndDtls property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaxRfndDtls().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tax17 }
     */
    public List<Tax17> getTaxRfndDtls() {
        if (taxRfndDtls == null) {
            taxRfndDtls = new ArrayList<Tax17>();
        }
        return this.taxRfndDtls;
    }

    /**
     * Obtém o valor da propriedade taxblIncmPerDvdd.
     *
     * @return possible object is
     * {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     */
    public ActiveOrHistoricCurrencyAnd13DecimalAmount getTaxblIncmPerDvdd() {
        return taxblIncmPerDvdd;
    }

    /**
     * Define o valor da propriedade taxblIncmPerDvdd.
     *
     * @param value allowed object is
     *              {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     */
    public void setTaxblIncmPerDvdd(ActiveOrHistoricCurrencyAnd13DecimalAmount value) {
        this.taxblIncmPerDvdd = value;
    }

    /**
     * Obtém o valor da propriedade taxblIncmPerShr.
     *
     * @return possible object is
     * {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     */
    public ActiveOrHistoricCurrencyAnd13DecimalAmount getTaxblIncmPerShr() {
        return taxblIncmPerShr;
    }

    /**
     * Define o valor da propriedade taxblIncmPerShr.
     *
     * @param value allowed object is
     *              {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     */
    public void setTaxblIncmPerShr(ActiveOrHistoricCurrencyAnd13DecimalAmount value) {
        this.taxblIncmPerShr = value;
    }

    /**
     * Obtém o valor da propriedade taxblIncmPerShrClctd.
     *
     * @return possible object is
     * {@link TaxableIncomePerShareCalculated2Code }
     */
    public TaxableIncomePerShareCalculated2Code getTaxblIncmPerShrClctd() {
        return taxblIncmPerShrClctd;
    }

    /**
     * Define o valor da propriedade taxblIncmPerShrClctd.
     *
     * @param value allowed object is
     *              {@link TaxableIncomePerShareCalculated2Code }
     */
    public void setTaxblIncmPerShrClctd(TaxableIncomePerShareCalculated2Code value) {
        this.taxblIncmPerShrClctd = value;
    }

    /**
     * Obtém o valor da propriedade tp.
     *
     * @return possible object is
     * {@link TypeOfPrice9Code }
     */
    public TypeOfPrice9Code getTp() {
        return tp;
    }

    /**
     * Define o valor da propriedade tp.
     *
     * @param value allowed object is
     *              {@link TypeOfPrice9Code }
     */
    public void setTp(TypeOfPrice9Code value) {
        this.tp = value;
    }

    /**
     * Gets the value of the valInAltrntvCcy property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valInAltrntvCcy property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValInAltrntvCcy().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PriceValue1 }
     */
    public List<PriceValue1> getValInAltrntvCcy() {
        if (valInAltrntvCcy == null) {
            valInAltrntvCcy = new ArrayList<PriceValue1>();
        }
        return this.valInAltrntvCcy;
    }

    /**
     * Gets the value of the valInInvstmtCcy property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valInInvstmtCcy property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValInInvstmtCcy().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PriceValue1 }
     */
    public List<PriceValue1> getValInInvstmtCcy() {
        if (valInInvstmtCcy == null) {
            valInInvstmtCcy = new ArrayList<PriceValue1>();
        }
        return this.valInInvstmtCcy;
    }

    /**
     * Obtém o valor da propriedade xtndedEUDvddSts.
     *
     * @return possible object is
     * {@link String }
     */
    public String getXtndedEUDvddSts() {
        return xtndedEUDvddSts;
    }

    /**
     * Define o valor da propriedade xtndedEUDvddSts.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setXtndedEUDvddSts(String value) {
        this.xtndedEUDvddSts = value;
    }

    /**
     * Obtém o valor da propriedade xtndedTaxblIncmPerShrClctd.
     *
     * @return possible object is
     * {@link String }
     */
    public String getXtndedTaxblIncmPerShrClctd() {
        return xtndedTaxblIncmPerShrClctd;
    }

    /**
     * Define o valor da propriedade xtndedTaxblIncmPerShrClctd.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setXtndedTaxblIncmPerShrClctd(String value) {
        this.xtndedTaxblIncmPerShrClctd = value;
    }

    /**
     * Obtém o valor da propriedade xtndedTp.
     *
     * @return possible object is
     * {@link String }
     */
    public String getXtndedTp() {
        return xtndedTp;
    }

    /**
     * Define o valor da propriedade xtndedTp.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setXtndedTp(String value) {
        this.xtndedTp = value;
    }

    /**
     * Obtém o valor da propriedade cumDvddInd.
     */
    public boolean isCumDvddInd() {
        return cumDvddInd;
    }

    /**
     * Define o valor da propriedade cumDvddInd.
     */
    public void setCumDvddInd(boolean value) {
        this.cumDvddInd = value;
    }

    /**
     * Obtém o valor da propriedade estmtdPricInd.
     */
    public boolean isEstmtdPricInd() {
        return estmtdPricInd;
    }

    /**
     * Define o valor da propriedade estmtdPricInd.
     */
    public void setEstmtdPricInd(boolean value) {
        this.estmtdPricInd = value;
    }

    /**
     * Obtém o valor da propriedade forExctnInd.
     */
    public boolean isForExctnInd() {
        return forExctnInd;
    }

    /**
     * Define o valor da propriedade forExctnInd.
     */
    public void setForExctnInd(boolean value) {
        this.forExctnInd = value;
    }

}
