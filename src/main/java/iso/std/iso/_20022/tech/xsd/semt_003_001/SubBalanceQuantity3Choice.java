package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de SubBalanceQuantity3Choice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="SubBalanceQuantity3Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Qty" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}FinancialInstrumentQuantity1Choice"/&gt;
 *           &lt;element name="Prtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification22"/&gt;
 *           &lt;element name="QtyAndAvlbty" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}QuantityAndAvailability1"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubBalanceQuantity3Choice", propOrder = {"qty", "prtry", "qtyAndAvlbty"})
public class SubBalanceQuantity3Choice {

    @XmlElement(name = "Qty") protected FinancialInstrumentQuantity1Choice qty;
    @XmlElement(name = "Prtry") protected GenericIdentification22 prtry;
    @XmlElement(name = "QtyAndAvlbty") protected QuantityAndAvailability1 qtyAndAvlbty;

    /**
     * Obtém o valor da propriedade prtry.
     *
     * @return possible object is
     * {@link GenericIdentification22 }
     */
    public GenericIdentification22 getPrtry() {
        return prtry;
    }

    /**
     * Define o valor da propriedade prtry.
     *
     * @param value allowed object is
     *              {@link GenericIdentification22 }
     */
    public void setPrtry(GenericIdentification22 value) {
        this.prtry = value;
    }

    /**
     * Obtém o valor da propriedade qty.
     *
     * @return possible object is
     * {@link FinancialInstrumentQuantity1Choice }
     */
    public FinancialInstrumentQuantity1Choice getQty() {
        return qty;
    }

    /**
     * Define o valor da propriedade qty.
     *
     * @param value allowed object is
     *              {@link FinancialInstrumentQuantity1Choice }
     */
    public void setQty(FinancialInstrumentQuantity1Choice value) {
        this.qty = value;
    }

    /**
     * Obtém o valor da propriedade qtyAndAvlbty.
     *
     * @return possible object is
     * {@link QuantityAndAvailability1 }
     */
    public QuantityAndAvailability1 getQtyAndAvlbty() {
        return qtyAndAvlbty;
    }

    /**
     * Define o valor da propriedade qtyAndAvlbty.
     *
     * @param value allowed object is
     *              {@link QuantityAndAvailability1 }
     */
    public void setQtyAndAvlbty(QuantityAndAvailability1 value) {
        this.qtyAndAvlbty = value;
    }

}
