package iso.std.iso._20022.tech.xsd.semt_003_001;

import br.anbimacom.balanceforsubaccountbrazil.BalForSubAcctBrData;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de SupplementaryDataEnvelope1 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="SupplementaryDataEnvelope1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.anbimacom.br/BalanceForSubAccountBrazil}BalForSubAcctBrData"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SupplementaryDataEnvelope1", propOrder = {"balForSubAcctBrData"})
public class SupplementaryDataEnvelope1 {

    @XmlElement(name = "BalForSubAcctBrData",
                namespace = "http://www.anbimacom.br/BalanceForSubAccountBrazil",
                required = true) protected BalForSubAcctBrData balForSubAcctBrData;

    /**
     * Obtém o valor da propriedade balForSubAcctBrData.
     *
     * @return possible object is
     * {@link BalForSubAcctBrData }
     */
    public BalForSubAcctBrData getBalForSubAcctBrData() {
        return balForSubAcctBrData;
    }

    /**
     * Define o valor da propriedade balForSubAcctBrData.
     *
     * @param value allowed object is
     *              {@link BalForSubAcctBrData }
     */
    public void setBalForSubAcctBrData(BalForSubAcctBrData value) {
        this.balForSubAcctBrData = value;
    }

}
