package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de PriceReportCancellationV04 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PriceReportCancellationV04"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MsgId" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}MessageIdentification1"/&gt;
 *         &lt;element name="PoolRef" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}AdditionalReference3" minOccurs="0"/&gt;
 *         &lt;element name="PrvsRef" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}AdditionalReference3"/&gt;
 *         &lt;element name="MsgPgntn" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Pagination"/&gt;
 *         &lt;element name="PricRptToBeCanc" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}PriceReport2" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceReportCancellationV04",
         propOrder = {"msgId", "poolRef", "prvsRef", "msgPgntn", "pricRptToBeCanc"})
public class PriceReportCancellationV04 {

    @XmlElement(name = "MsgId", required = true) protected MessageIdentification1 msgId;
    @XmlElement(name = "PoolRef") protected AdditionalReference3 poolRef;
    @XmlElement(name = "PrvsRef", required = true) protected AdditionalReference3 prvsRef;
    @XmlElement(name = "MsgPgntn", required = true) protected Pagination msgPgntn;
    @XmlElement(name = "PricRptToBeCanc") protected PriceReport2 pricRptToBeCanc;

    /**
     * Obtém o valor da propriedade msgId.
     *
     * @return possible object is
     * {@link MessageIdentification1 }
     */
    public MessageIdentification1 getMsgId() {
        return msgId;
    }

    /**
     * Define o valor da propriedade msgId.
     *
     * @param value allowed object is
     *              {@link MessageIdentification1 }
     */
    public void setMsgId(MessageIdentification1 value) {
        this.msgId = value;
    }

    /**
     * Obtém o valor da propriedade msgPgntn.
     *
     * @return possible object is
     * {@link Pagination }
     */
    public Pagination getMsgPgntn() {
        return msgPgntn;
    }

    /**
     * Define o valor da propriedade msgPgntn.
     *
     * @param value allowed object is
     *              {@link Pagination }
     */
    public void setMsgPgntn(Pagination value) {
        this.msgPgntn = value;
    }

    /**
     * Obtém o valor da propriedade poolRef.
     *
     * @return possible object is
     * {@link AdditionalReference3 }
     */
    public AdditionalReference3 getPoolRef() {
        return poolRef;
    }

    /**
     * Define o valor da propriedade poolRef.
     *
     * @param value allowed object is
     *              {@link AdditionalReference3 }
     */
    public void setPoolRef(AdditionalReference3 value) {
        this.poolRef = value;
    }

    /**
     * Obtém o valor da propriedade pricRptToBeCanc.
     *
     * @return possible object is
     * {@link PriceReport2 }
     */
    public PriceReport2 getPricRptToBeCanc() {
        return pricRptToBeCanc;
    }

    /**
     * Define o valor da propriedade pricRptToBeCanc.
     *
     * @param value allowed object is
     *              {@link PriceReport2 }
     */
    public void setPricRptToBeCanc(PriceReport2 value) {
        this.pricRptToBeCanc = value;
    }

    /**
     * Obtém o valor da propriedade prvsRef.
     *
     * @return possible object is
     * {@link AdditionalReference3 }
     */
    public AdditionalReference3 getPrvsRef() {
        return prvsRef;
    }

    /**
     * Define o valor da propriedade prvsRef.
     *
     * @param value allowed object is
     *              {@link AdditionalReference3 }
     */
    public void setPrvsRef(AdditionalReference3 value) {
        this.prvsRef = value;
    }

}
