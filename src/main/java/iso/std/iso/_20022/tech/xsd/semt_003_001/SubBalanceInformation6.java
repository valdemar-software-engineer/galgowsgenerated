package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de SubBalanceInformation6 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="SubBalanceInformation6"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SubBalTp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SubBalanceType5Choice"/&gt;
 *         &lt;element name="Qty" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SubBalanceQuantity3Choice"/&gt;
 *         &lt;element name="SubBalAddtlDtls" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max140Text" minOccurs="0"/&gt;
 *         &lt;element name="AddtlBalBrkdwnDtls" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}AdditionalBalanceInformation6" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubBalanceInformation6", propOrder = {"subBalTp", "qty", "subBalAddtlDtls", "addtlBalBrkdwnDtls"})
public class SubBalanceInformation6 {

    @XmlElement(name = "SubBalTp", required = true) protected SubBalanceType5Choice subBalTp;
    @XmlElement(name = "Qty", required = true) protected SubBalanceQuantity3Choice qty;
    @XmlElement(name = "SubBalAddtlDtls") protected String subBalAddtlDtls;
    @XmlElement(name = "AddtlBalBrkdwnDtls") protected List<AdditionalBalanceInformation6> addtlBalBrkdwnDtls;

    /**
     * Gets the value of the addtlBalBrkdwnDtls property.
     * <p/>
     * <p/>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addtlBalBrkdwnDtls property.
     * <p/>
     * <p/>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddtlBalBrkdwnDtls().add(newItem);
     * </pre>
     * <p/>
     * <p/>
     * <p/>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalBalanceInformation6 }
     */
    public List<AdditionalBalanceInformation6> getAddtlBalBrkdwnDtls() {
        if (addtlBalBrkdwnDtls == null) {
            addtlBalBrkdwnDtls = new ArrayList<AdditionalBalanceInformation6>();
        }
        return this.addtlBalBrkdwnDtls;
    }

    /**
     * Obtém o valor da propriedade qty.
     *
     * @return possible object is
     * {@link SubBalanceQuantity3Choice }
     */
    public SubBalanceQuantity3Choice getQty() {
        return qty;
    }

    /**
     * Define o valor da propriedade qty.
     *
     * @param value allowed object is
     *              {@link SubBalanceQuantity3Choice }
     */
    public void setQty(SubBalanceQuantity3Choice value) {
        this.qty = value;
    }

    /**
     * Obtém o valor da propriedade subBalAddtlDtls.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSubBalAddtlDtls() {
        return subBalAddtlDtls;
    }

    /**
     * Define o valor da propriedade subBalAddtlDtls.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSubBalAddtlDtls(String value) {
        this.subBalAddtlDtls = value;
    }

    /**
     * Obtém o valor da propriedade subBalTp.
     *
     * @return possible object is
     * {@link SubBalanceType5Choice }
     */
    public SubBalanceType5Choice getSubBalTp() {
        return subBalTp;
    }

    /**
     * Define o valor da propriedade subBalTp.
     *
     * @param value allowed object is
     *              {@link SubBalanceType5Choice }
     */
    public void setSubBalTp(SubBalanceType5Choice value) {
        this.subBalTp = value;
    }

}
