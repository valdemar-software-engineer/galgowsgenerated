package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de AddressType2Code.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;simpleType name="AddressType2Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ADDR"/&gt;
 *     &lt;enumeration value="PBOX"/&gt;
 *     &lt;enumeration value="HOME"/&gt;
 *     &lt;enumeration value="BIZZ"/&gt;
 *     &lt;enumeration value="MLTO"/&gt;
 *     &lt;enumeration value="DLVY"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "AddressType2Code")
@XmlEnum
public enum AddressType2Code {

    ADDR,
    PBOX,
    HOME,
    BIZZ,
    MLTO,
    DLVY;

    public static AddressType2Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
