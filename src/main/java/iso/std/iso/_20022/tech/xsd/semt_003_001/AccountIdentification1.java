package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de AccountIdentification1 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="AccountIdentification1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Prtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SimpleIdentificationInformation"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountIdentification1", propOrder = {"prtry"})
public class AccountIdentification1 {

    @XmlElement(name = "Prtry", required = true) protected SimpleIdentificationInformation prtry;

    /**
     * Obtém o valor da propriedade prtry.
     *
     * @return possible object is
     * {@link SimpleIdentificationInformation }
     */
    public SimpleIdentificationInformation getPrtry() {
        return prtry;
    }

    /**
     * Define o valor da propriedade prtry.
     *
     * @param value allowed object is
     *              {@link SimpleIdentificationInformation }
     */
    public void setPrtry(SimpleIdentificationInformation value) {
        this.prtry = value;
    }

}
