package iso.std.iso._20022.tech.xsd.reda_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Classe Java de Charge15 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="Charge15"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Tp" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}ChargeType9Code"/&gt;
 *           &lt;element name="XtndedTp" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}Extended350Code"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}ActiveCurrencyAnd13DecimalAmount"/&gt;
 *           &lt;element name="Rate" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PercentageRate"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice&gt;
 *           &lt;element name="ClctnBsis" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}CalculationBasis2Code" minOccurs="0"/&gt;
 *           &lt;element name="XtndedClctnBsis" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}Extended350Code" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Charge15", propOrder = {"tp", "xtndedTp", "amt", "rate", "clctnBsis", "xtndedClctnBsis"})
public class Charge15 {

    @XmlElement(name = "Tp") @XmlSchemaType(name = "string") protected ChargeType9Code tp;
    @XmlElement(name = "XtndedTp") protected String xtndedTp;
    @XmlElement(name = "Amt") protected ActiveCurrencyAnd13DecimalAmount amt;
    @XmlElement(name = "Rate") protected BigDecimal rate;
    @XmlElement(name = "ClctnBsis") @XmlSchemaType(name = "string") protected CalculationBasis2Code clctnBsis;
    @XmlElement(name = "XtndedClctnBsis") protected String xtndedClctnBsis;

    /**
     * Obtém o valor da propriedade amt.
     *
     * @return possible object is
     * {@link ActiveCurrencyAnd13DecimalAmount }
     */
    public ActiveCurrencyAnd13DecimalAmount getAmt() {
        return amt;
    }

    /**
     * Define o valor da propriedade amt.
     *
     * @param value allowed object is
     *              {@link ActiveCurrencyAnd13DecimalAmount }
     */
    public void setAmt(ActiveCurrencyAnd13DecimalAmount value) {
        this.amt = value;
    }

    /**
     * Obtém o valor da propriedade clctnBsis.
     *
     * @return possible object is
     * {@link CalculationBasis2Code }
     */
    public CalculationBasis2Code getClctnBsis() {
        return clctnBsis;
    }

    /**
     * Define o valor da propriedade clctnBsis.
     *
     * @param value allowed object is
     *              {@link CalculationBasis2Code }
     */
    public void setClctnBsis(CalculationBasis2Code value) {
        this.clctnBsis = value;
    }

    /**
     * Obtém o valor da propriedade rate.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Define o valor da propriedade rate.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setRate(BigDecimal value) {
        this.rate = value;
    }

    /**
     * Obtém o valor da propriedade tp.
     *
     * @return possible object is
     * {@link ChargeType9Code }
     */
    public ChargeType9Code getTp() {
        return tp;
    }

    /**
     * Define o valor da propriedade tp.
     *
     * @param value allowed object is
     *              {@link ChargeType9Code }
     */
    public void setTp(ChargeType9Code value) {
        this.tp = value;
    }

    /**
     * Obtém o valor da propriedade xtndedClctnBsis.
     *
     * @return possible object is
     * {@link String }
     */
    public String getXtndedClctnBsis() {
        return xtndedClctnBsis;
    }

    /**
     * Define o valor da propriedade xtndedClctnBsis.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setXtndedClctnBsis(String value) {
        this.xtndedClctnBsis = value;
    }

    /**
     * Obtém o valor da propriedade xtndedTp.
     *
     * @return possible object is
     * {@link String }
     */
    public String getXtndedTp() {
        return xtndedTp;
    }

    /**
     * Define o valor da propriedade xtndedTp.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setXtndedTp(String value) {
        this.xtndedTp = value;
    }

}
