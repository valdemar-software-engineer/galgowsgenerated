package iso.std.iso._20022.tech.xsd.camt_043_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de FundCashForecast4 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="FundCashForecast4">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Max35Text"/>
 *         &lt;element name="TradDtTm" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}DateAndDateTimeChoice"/>
 *         &lt;element name="PrvsTradDtTm" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}DateAndDateTimeChoice" minOccurs="0"/>
 *         &lt;element name="FinInstrmDtls" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}FinancialInstrument9"/>
 *         &lt;element name="TtlNAV" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}ActiveOrHistoricCurrencyAndAmount" minOccurs="0"/>
 *         &lt;element name="PrvsTtlNAV" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}ActiveOrHistoricCurrencyAndAmount" minOccurs="0"/>
 *         &lt;element name="TtlUnitsNb" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}FinancialInstrumentQuantity1" minOccurs="0"/>
 *         &lt;element name="PrvsTtlUnitsNb" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}FinancialInstrumentQuantity1" minOccurs="0"/>
 *         &lt;element name="TtlNAVChngRate" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}PercentageRate" minOccurs="0"/>
 *         &lt;element name="InvstmtCcy" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}ActiveOrHistoricCurrencyCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="NetCshFcstDtls" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}NetCashForecast2" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="XcptnlNetCshFlowInd" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}YesNoIndicator"/>
 *         &lt;element name="BrkdwnByCtry" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}BreakdownByCountry1" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="BrkdwnByCcy" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}BreakdownByCurrency1" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="BrkdwnByPty" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}BreakdownByParty1" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="BrkdwnByUsrDfndParam" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}BreakdownByUserDefinedParameter1" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FundCashForecast4",
         propOrder = {"id", "tradDtTm", "prvsTradDtTm", "finInstrmDtls", "ttlNAV", "prvsTtlNAV", "ttlUnitsNb", "prvsTtlUnitsNb", "ttlNAVChngRate", "invstmtCcy", "netCshFcstDtls", "xcptnlNetCshFlowInd", "brkdwnByCtry", "brkdwnByCcy", "brkdwnByPty", "brkdwnByUsrDfndParam"})
public class FundCashForecast4 {

    @XmlElement(name = "Id", required = true) protected String id;
    @XmlElement(name = "TradDtTm", required = true) protected DateAndDateTimeChoice tradDtTm;
    @XmlElement(name = "PrvsTradDtTm") protected DateAndDateTimeChoice prvsTradDtTm;
    @XmlElement(name = "FinInstrmDtls", required = true) protected FinancialInstrument9 finInstrmDtls;
    @XmlElement(name = "TtlNAV") protected ActiveOrHistoricCurrencyAndAmount ttlNAV;
    @XmlElement(name = "PrvsTtlNAV") protected ActiveOrHistoricCurrencyAndAmount prvsTtlNAV;
    @XmlElement(name = "TtlUnitsNb") protected FinancialInstrumentQuantity1 ttlUnitsNb;
    @XmlElement(name = "PrvsTtlUnitsNb") protected FinancialInstrumentQuantity1 prvsTtlUnitsNb;
    @XmlElement(name = "TtlNAVChngRate") protected BigDecimal ttlNAVChngRate;
    @XmlElement(name = "InvstmtCcy") protected List<String> invstmtCcy;
    @XmlElement(name = "NetCshFcstDtls") protected List<NetCashForecast2> netCshFcstDtls;
    @XmlElement(name = "XcptnlNetCshFlowInd") protected boolean xcptnlNetCshFlowInd;
    @XmlElement(name = "BrkdwnByCtry") protected List<BreakdownByCountry1> brkdwnByCtry;
    @XmlElement(name = "BrkdwnByCcy") protected List<BreakdownByCurrency1> brkdwnByCcy;
    @XmlElement(name = "BrkdwnByPty") protected List<BreakdownByParty1> brkdwnByPty;
    @XmlElement(name = "BrkdwnByUsrDfndParam") protected List<BreakdownByUserDefinedParameter1> brkdwnByUsrDfndParam;

    /**
     * Gets the value of the brkdwnByCcy property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the brkdwnByCcy property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBrkdwnByCcy().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BreakdownByCurrency1 }
     */
    public List<BreakdownByCurrency1> getBrkdwnByCcy() {
        if (brkdwnByCcy == null) {
            brkdwnByCcy = new ArrayList<BreakdownByCurrency1>();
        }
        return this.brkdwnByCcy;
    }

    /**
     * Gets the value of the brkdwnByCtry property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the brkdwnByCtry property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBrkdwnByCtry().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BreakdownByCountry1 }
     */
    public List<BreakdownByCountry1> getBrkdwnByCtry() {
        if (brkdwnByCtry == null) {
            brkdwnByCtry = new ArrayList<BreakdownByCountry1>();
        }
        return this.brkdwnByCtry;
    }

    /**
     * Gets the value of the brkdwnByPty property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the brkdwnByPty property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBrkdwnByPty().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BreakdownByParty1 }
     */
    public List<BreakdownByParty1> getBrkdwnByPty() {
        if (brkdwnByPty == null) {
            brkdwnByPty = new ArrayList<BreakdownByParty1>();
        }
        return this.brkdwnByPty;
    }

    /**
     * Gets the value of the brkdwnByUsrDfndParam property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the brkdwnByUsrDfndParam property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBrkdwnByUsrDfndParam().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BreakdownByUserDefinedParameter1 }
     */
    public List<BreakdownByUserDefinedParameter1> getBrkdwnByUsrDfndParam() {
        if (brkdwnByUsrDfndParam == null) {
            brkdwnByUsrDfndParam = new ArrayList<BreakdownByUserDefinedParameter1>();
        }
        return this.brkdwnByUsrDfndParam;
    }

    /**
     * Obtém o valor da propriedade finInstrmDtls.
     *
     * @return possible object is
     * {@link FinancialInstrument9 }
     */
    public FinancialInstrument9 getFinInstrmDtls() {
        return finInstrmDtls;
    }

    /**
     * Define o valor da propriedade finInstrmDtls.
     *
     * @param value allowed object is
     *              {@link FinancialInstrument9 }
     */
    public void setFinInstrmDtls(FinancialInstrument9 value) {
        this.finInstrmDtls = value;
    }

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link String }
     */
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the invstmtCcy property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the invstmtCcy property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvstmtCcy().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     */
    public List<String> getInvstmtCcy() {
        if (invstmtCcy == null) {
            invstmtCcy = new ArrayList<String>();
        }
        return this.invstmtCcy;
    }

    /**
     * Gets the value of the netCshFcstDtls property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the netCshFcstDtls property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNetCshFcstDtls().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NetCashForecast2 }
     */
    public List<NetCashForecast2> getNetCshFcstDtls() {
        if (netCshFcstDtls == null) {
            netCshFcstDtls = new ArrayList<NetCashForecast2>();
        }
        return this.netCshFcstDtls;
    }

    /**
     * Obtém o valor da propriedade prvsTradDtTm.
     *
     * @return possible object is
     * {@link DateAndDateTimeChoice }
     */
    public DateAndDateTimeChoice getPrvsTradDtTm() {
        return prvsTradDtTm;
    }

    /**
     * Define o valor da propriedade prvsTradDtTm.
     *
     * @param value allowed object is
     *              {@link DateAndDateTimeChoice }
     */
    public void setPrvsTradDtTm(DateAndDateTimeChoice value) {
        this.prvsTradDtTm = value;
    }

    /**
     * Obtém o valor da propriedade prvsTtlNAV.
     *
     * @return possible object is
     * {@link ActiveOrHistoricCurrencyAndAmount }
     */
    public ActiveOrHistoricCurrencyAndAmount getPrvsTtlNAV() {
        return prvsTtlNAV;
    }

    /**
     * Define o valor da propriedade prvsTtlNAV.
     *
     * @param value allowed object is
     *              {@link ActiveOrHistoricCurrencyAndAmount }
     */
    public void setPrvsTtlNAV(ActiveOrHistoricCurrencyAndAmount value) {
        this.prvsTtlNAV = value;
    }

    /**
     * Obtém o valor da propriedade prvsTtlUnitsNb.
     *
     * @return possible object is
     * {@link FinancialInstrumentQuantity1 }
     */
    public FinancialInstrumentQuantity1 getPrvsTtlUnitsNb() {
        return prvsTtlUnitsNb;
    }

    /**
     * Define o valor da propriedade prvsTtlUnitsNb.
     *
     * @param value allowed object is
     *              {@link FinancialInstrumentQuantity1 }
     */
    public void setPrvsTtlUnitsNb(FinancialInstrumentQuantity1 value) {
        this.prvsTtlUnitsNb = value;
    }

    /**
     * Obtém o valor da propriedade tradDtTm.
     *
     * @return possible object is
     * {@link DateAndDateTimeChoice }
     */
    public DateAndDateTimeChoice getTradDtTm() {
        return tradDtTm;
    }

    /**
     * Define o valor da propriedade tradDtTm.
     *
     * @param value allowed object is
     *              {@link DateAndDateTimeChoice }
     */
    public void setTradDtTm(DateAndDateTimeChoice value) {
        this.tradDtTm = value;
    }

    /**
     * Obtém o valor da propriedade ttlNAV.
     *
     * @return possible object is
     * {@link ActiveOrHistoricCurrencyAndAmount }
     */
    public ActiveOrHistoricCurrencyAndAmount getTtlNAV() {
        return ttlNAV;
    }

    /**
     * Define o valor da propriedade ttlNAV.
     *
     * @param value allowed object is
     *              {@link ActiveOrHistoricCurrencyAndAmount }
     */
    public void setTtlNAV(ActiveOrHistoricCurrencyAndAmount value) {
        this.ttlNAV = value;
    }

    /**
     * Obtém o valor da propriedade ttlNAVChngRate.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getTtlNAVChngRate() {
        return ttlNAVChngRate;
    }

    /**
     * Define o valor da propriedade ttlNAVChngRate.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setTtlNAVChngRate(BigDecimal value) {
        this.ttlNAVChngRate = value;
    }

    /**
     * Obtém o valor da propriedade ttlUnitsNb.
     *
     * @return possible object is
     * {@link FinancialInstrumentQuantity1 }
     */
    public FinancialInstrumentQuantity1 getTtlUnitsNb() {
        return ttlUnitsNb;
    }

    /**
     * Define o valor da propriedade ttlUnitsNb.
     *
     * @param value allowed object is
     *              {@link FinancialInstrumentQuantity1 }
     */
    public void setTtlUnitsNb(FinancialInstrumentQuantity1 value) {
        this.ttlUnitsNb = value;
    }

    /**
     * Obtém o valor da propriedade xcptnlNetCshFlowInd.
     */
    public boolean isXcptnlNetCshFlowInd() {
        return xcptnlNetCshFlowInd;
    }

    /**
     * Define o valor da propriedade xcptnlNetCshFlowInd.
     */
    public void setXcptnlNetCshFlowInd(boolean value) {
        this.xcptnlNetCshFlowInd = value;
    }

}
