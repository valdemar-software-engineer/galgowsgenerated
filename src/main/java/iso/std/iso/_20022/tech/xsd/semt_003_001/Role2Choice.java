package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Role2Choice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="Role2Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Cd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}InvestmentFundRole2Code"/&gt;
 *           &lt;element name="Prtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification20"/&gt;
 *           &lt;element name="Txt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max350Text"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Role2Choice", propOrder = {"cd", "prtry", "txt"})
public class Role2Choice {

    @XmlElement(name = "Cd") @XmlSchemaType(name = "string") protected InvestmentFundRole2Code cd;
    @XmlElement(name = "Prtry") protected GenericIdentification20 prtry;
    @XmlElement(name = "Txt") protected String txt;

    /**
     * Obtém o valor da propriedade cd.
     *
     * @return possible object is
     * {@link InvestmentFundRole2Code }
     */
    public InvestmentFundRole2Code getCd() {
        return cd;
    }

    /**
     * Define o valor da propriedade cd.
     *
     * @param value allowed object is
     *              {@link InvestmentFundRole2Code }
     */
    public void setCd(InvestmentFundRole2Code value) {
        this.cd = value;
    }

    /**
     * Obtém o valor da propriedade prtry.
     *
     * @return possible object is
     * {@link GenericIdentification20 }
     */
    public GenericIdentification20 getPrtry() {
        return prtry;
    }

    /**
     * Define o valor da propriedade prtry.
     *
     * @param value allowed object is
     *              {@link GenericIdentification20 }
     */
    public void setPrtry(GenericIdentification20 value) {
        this.prtry = value;
    }

    /**
     * Obtém o valor da propriedade txt.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTxt() {
        return txt;
    }

    /**
     * Define o valor da propriedade txt.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTxt(String value) {
        this.txt = value;
    }

}
