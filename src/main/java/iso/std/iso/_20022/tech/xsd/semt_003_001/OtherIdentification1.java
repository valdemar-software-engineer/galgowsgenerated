package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de OtherIdentification1 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="OtherIdentification1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text"/&gt;
 *         &lt;element name="Sfx" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max16Text" minOccurs="0"/&gt;
 *         &lt;element name="Tp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}IdentificationSource3Choice"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OtherIdentification1", propOrder = {"id", "sfx", "tp"})
public class OtherIdentification1 {

    @XmlElement(name = "Id", required = true) protected String id;
    @XmlElement(name = "Sfx") protected String sfx;
    @XmlElement(name = "Tp", required = true) protected IdentificationSource3Choice tp;

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link String }
     */
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade sfx.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSfx() {
        return sfx;
    }

    /**
     * Define o valor da propriedade sfx.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSfx(String value) {
        this.sfx = value;
    }

    /**
     * Obtém o valor da propriedade tp.
     *
     * @return possible object is
     * {@link IdentificationSource3Choice }
     */
    public IdentificationSource3Choice getTp() {
        return tp;
    }

    /**
     * Define o valor da propriedade tp.
     *
     * @param value allowed object is
     *              {@link IdentificationSource3Choice }
     */
    public void setTp(IdentificationSource3Choice value) {
        this.tp = value;
    }

}
