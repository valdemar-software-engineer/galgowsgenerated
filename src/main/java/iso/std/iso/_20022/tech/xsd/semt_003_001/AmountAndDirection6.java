package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de AmountAndDirection6 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="AmountAndDirection6"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyAndAmount"/&gt;
 *         &lt;element name="Sgn" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PlusOrMinusIndicator"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AmountAndDirection6", propOrder = {"amt", "sgn"})
public class AmountAndDirection6 {

    @XmlElement(name = "Amt", required = true) protected ActiveOrHistoricCurrencyAndAmount amt;
    @XmlElement(name = "Sgn") protected boolean sgn;

    /**
     * Obtém o valor da propriedade amt.
     *
     * @return possible object is
     * {@link ActiveOrHistoricCurrencyAndAmount }
     */
    public ActiveOrHistoricCurrencyAndAmount getAmt() {
        return amt;
    }

    /**
     * Define o valor da propriedade amt.
     *
     * @param value allowed object is
     *              {@link ActiveOrHistoricCurrencyAndAmount }
     */
    public void setAmt(ActiveOrHistoricCurrencyAndAmount value) {
        this.amt = value;
    }

    /**
     * Obtém o valor da propriedade sgn.
     */
    public boolean isSgn() {
        return sgn;
    }

    /**
     * Define o valor da propriedade sgn.
     */
    public void setSgn(boolean value) {
        this.sgn = value;
    }

}
