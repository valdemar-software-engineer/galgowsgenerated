package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de FinancialInstrument21 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="FinancialInstrument21"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ClssTp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
 *         &lt;element name="SctiesForm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}FormOfSecurity1Code" minOccurs="0"/&gt;
 *         &lt;element name="DstrbtnPlcy" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}DistributionPolicy1Code" minOccurs="0"/&gt;
 *         &lt;element name="PdctGrp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max140Text" minOccurs="0"/&gt;
 *         &lt;element name="UmbrllNm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
 *         &lt;element name="BaseCcy" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveCurrencyCode" minOccurs="0"/&gt;
 *         &lt;element name="DnmtnCcy" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveCurrencyCode" minOccurs="0"/&gt;
 *         &lt;element name="ReqdNAVCcy" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyCode" minOccurs="0"/&gt;
 *         &lt;element name="DualFndInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator" minOccurs="0"/&gt;
 *         &lt;element name="CtryOfDmcl" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}CountryCode" minOccurs="0"/&gt;
 *         &lt;element name="RegdDstrbtnCtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}CountryCode" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialInstrument21",
         propOrder = {"clssTp", "sctiesForm", "dstrbtnPlcy", "pdctGrp", "umbrllNm", "baseCcy", "dnmtnCcy", "reqdNAVCcy", "dualFndInd", "ctryOfDmcl", "regdDstrbtnCtry"})
public class FinancialInstrument21 {

    @XmlElement(name = "ClssTp") protected String clssTp;
    @XmlElement(name = "SctiesForm") @XmlSchemaType(name = "string") protected FormOfSecurity1Code sctiesForm;
    @XmlElement(name = "DstrbtnPlcy") @XmlSchemaType(name = "string") protected DistributionPolicy1Code dstrbtnPlcy;
    @XmlElement(name = "PdctGrp") protected String pdctGrp;
    @XmlElement(name = "UmbrllNm") protected String umbrllNm;
    @XmlElement(name = "BaseCcy") protected String baseCcy;
    @XmlElement(name = "DnmtnCcy") protected String dnmtnCcy;
    @XmlElement(name = "ReqdNAVCcy") protected String reqdNAVCcy;
    @XmlElement(name = "DualFndInd") protected Boolean dualFndInd;
    @XmlElement(name = "CtryOfDmcl") protected String ctryOfDmcl;
    @XmlElement(name = "RegdDstrbtnCtry") protected List<String> regdDstrbtnCtry;

    /**
     * Obtém o valor da propriedade baseCcy.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBaseCcy() {
        return baseCcy;
    }

    /**
     * Define o valor da propriedade baseCcy.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBaseCcy(String value) {
        this.baseCcy = value;
    }

    /**
     * Obtém o valor da propriedade clssTp.
     *
     * @return possible object is
     * {@link String }
     */
    public String getClssTp() {
        return clssTp;
    }

    /**
     * Define o valor da propriedade clssTp.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setClssTp(String value) {
        this.clssTp = value;
    }

    /**
     * Obtém o valor da propriedade ctryOfDmcl.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCtryOfDmcl() {
        return ctryOfDmcl;
    }

    /**
     * Define o valor da propriedade ctryOfDmcl.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCtryOfDmcl(String value) {
        this.ctryOfDmcl = value;
    }

    /**
     * Obtém o valor da propriedade dnmtnCcy.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDnmtnCcy() {
        return dnmtnCcy;
    }

    /**
     * Define o valor da propriedade dnmtnCcy.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDnmtnCcy(String value) {
        this.dnmtnCcy = value;
    }

    /**
     * Obtém o valor da propriedade dstrbtnPlcy.
     *
     * @return possible object is
     * {@link DistributionPolicy1Code }
     */
    public DistributionPolicy1Code getDstrbtnPlcy() {
        return dstrbtnPlcy;
    }

    /**
     * Define o valor da propriedade dstrbtnPlcy.
     *
     * @param value allowed object is
     *              {@link DistributionPolicy1Code }
     */
    public void setDstrbtnPlcy(DistributionPolicy1Code value) {
        this.dstrbtnPlcy = value;
    }

    /**
     * Obtém o valor da propriedade pdctGrp.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPdctGrp() {
        return pdctGrp;
    }

    /**
     * Define o valor da propriedade pdctGrp.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPdctGrp(String value) {
        this.pdctGrp = value;
    }

    /**
     * Gets the value of the regdDstrbtnCtry property.
     * <p/>
     * <p/>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regdDstrbtnCtry property.
     * <p/>
     * <p/>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegdDstrbtnCtry().add(newItem);
     * </pre>
     * <p/>
     * <p/>
     * <p/>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     */
    public List<String> getRegdDstrbtnCtry() {
        if (regdDstrbtnCtry == null) {
            regdDstrbtnCtry = new ArrayList<String>();
        }
        return this.regdDstrbtnCtry;
    }

    /**
     * Obtém o valor da propriedade reqdNAVCcy.
     *
     * @return possible object is
     * {@link String }
     */
    public String getReqdNAVCcy() {
        return reqdNAVCcy;
    }

    /**
     * Define o valor da propriedade reqdNAVCcy.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setReqdNAVCcy(String value) {
        this.reqdNAVCcy = value;
    }

    /**
     * Obtém o valor da propriedade sctiesForm.
     *
     * @return possible object is
     * {@link FormOfSecurity1Code }
     */
    public FormOfSecurity1Code getSctiesForm() {
        return sctiesForm;
    }

    /**
     * Define o valor da propriedade sctiesForm.
     *
     * @param value allowed object is
     *              {@link FormOfSecurity1Code }
     */
    public void setSctiesForm(FormOfSecurity1Code value) {
        this.sctiesForm = value;
    }

    /**
     * Obtém o valor da propriedade umbrllNm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getUmbrllNm() {
        return umbrllNm;
    }

    /**
     * Define o valor da propriedade umbrllNm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setUmbrllNm(String value) {
        this.umbrllNm = value;
    }

    /**
     * Obtém o valor da propriedade dualFndInd.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isDualFndInd() {
        return dualFndInd;
    }

    /**
     * Define o valor da propriedade dualFndInd.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setDualFndInd(Boolean value) {
        this.dualFndInd = value;
    }

}
