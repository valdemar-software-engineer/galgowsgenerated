package iso.std.iso._20022.tech.xsd.head_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de DateAndPlaceOfBirth complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="DateAndPlaceOfBirth"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BirthDt" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}ISODate"/&gt;
 *         &lt;element name="PrvcOfBirth" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}Max35Text" minOccurs="0"/&gt;
 *         &lt;element name="CityOfBirth" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}Max35Text"/&gt;
 *         &lt;element name="CtryOfBirth" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}CountryCode"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DateAndPlaceOfBirth", propOrder = {"birthDt", "prvcOfBirth", "cityOfBirth", "ctryOfBirth"})
public class DateAndPlaceOfBirth {

    @XmlElement(name = "BirthDt", required = true) @XmlSchemaType(name = "date") protected XMLGregorianCalendar birthDt;
    @XmlElement(name = "PrvcOfBirth") protected String prvcOfBirth;
    @XmlElement(name = "CityOfBirth", required = true) protected String cityOfBirth;
    @XmlElement(name = "CtryOfBirth", required = true) protected String ctryOfBirth;

    /**
     * Obtém o valor da propriedade birthDt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getBirthDt() {
        return birthDt;
    }

    /**
     * Define o valor da propriedade birthDt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setBirthDt(XMLGregorianCalendar value) {
        this.birthDt = value;
    }

    /**
     * Obtém o valor da propriedade cityOfBirth.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCityOfBirth() {
        return cityOfBirth;
    }

    /**
     * Define o valor da propriedade cityOfBirth.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCityOfBirth(String value) {
        this.cityOfBirth = value;
    }

    /**
     * Obtém o valor da propriedade ctryOfBirth.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCtryOfBirth() {
        return ctryOfBirth;
    }

    /**
     * Define o valor da propriedade ctryOfBirth.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCtryOfBirth(String value) {
        this.ctryOfBirth = value;
    }

    /**
     * Obtém o valor da propriedade prvcOfBirth.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPrvcOfBirth() {
        return prvcOfBirth;
    }

    /**
     * Define o valor da propriedade prvcOfBirth.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPrvcOfBirth(String value) {
        this.prvcOfBirth = value;
    }

}
