package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de MarketType4Code.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;simpleType name="MarketType4Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="FUND"/&gt;
 *     &lt;enumeration value="LMAR"/&gt;
 *     &lt;enumeration value="THEO"/&gt;
 *     &lt;enumeration value="VEND"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "MarketType4Code")
@XmlEnum
public enum MarketType4Code {

    FUND,
    LMAR,
    THEO,
    VEND;

    public static MarketType4Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
