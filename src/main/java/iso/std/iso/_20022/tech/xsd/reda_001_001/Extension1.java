package iso.std.iso._20022.tech.xsd.reda_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Extension1 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="Extension1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PlcAndNm" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}Max350Text"/&gt;
 *         &lt;element name="Txt" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}Max350Text"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Extension1", propOrder = {"plcAndNm", "txt"})
public class Extension1 {

    @XmlElement(name = "PlcAndNm", required = true) protected String plcAndNm;
    @XmlElement(name = "Txt", required = true) protected String txt;

    /**
     * Obtém o valor da propriedade plcAndNm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPlcAndNm() {
        return plcAndNm;
    }

    /**
     * Define o valor da propriedade plcAndNm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPlcAndNm(String value) {
        this.plcAndNm = value;
    }

    /**
     * Obtém o valor da propriedade txt.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTxt() {
        return txt;
    }

    /**
     * Define o valor da propriedade txt.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTxt(String value) {
        this.txt = value;
    }

}
