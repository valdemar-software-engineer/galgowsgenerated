package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de FinancialInstrumentAttributes20 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="FinancialInstrumentAttributes20"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PlcOfListg" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}MarketIdentification5" minOccurs="0"/&gt;
 *         &lt;element name="DayCntBsis" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}InterestComputationMethodFormat1Choice" minOccurs="0"/&gt;
 *         &lt;element name="RegnForm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}FormOfSecurity2Choice" minOccurs="0"/&gt;
 *         &lt;element name="PmtFrqcy" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Frequency3Choice" minOccurs="0"/&gt;
 *         &lt;element name="PmtSts" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SecuritiesPaymentStatus2Choice" minOccurs="0"/&gt;
 *         &lt;element name="PmtDrctn" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PaymentDirection2Choice" minOccurs="0"/&gt;
 *         &lt;element name="VarblRateChngFrqcy" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Frequency3Choice" minOccurs="0"/&gt;
 *         &lt;element name="PrefToIncm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PreferenceToIncome2Choice" minOccurs="0"/&gt;
 *         &lt;element name="ClssfctnTp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ClassificationType2Choice" minOccurs="0"/&gt;
 *         &lt;element name="OptnStyle" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}OptionStyle4Choice" minOccurs="0"/&gt;
 *         &lt;element name="OptnTp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}OptionType2Choice" minOccurs="0"/&gt;
 *         &lt;element name="DnmtnCcy" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyCode" minOccurs="0"/&gt;
 *         &lt;element name="CpnDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="XpryDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="FltgRateFxgDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="MtrtyDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="IsseDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="NxtCllblDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="PutblDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="DtdDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="FrstPmtDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODate" minOccurs="0"/&gt;
 *         &lt;element name="PrvsFctr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}BaseOneRate" minOccurs="0"/&gt;
 *         &lt;element name="CurFctr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}BaseOneRate" minOccurs="0"/&gt;
 *         &lt;element name="NxtFctr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}BaseOneRate" minOccurs="0"/&gt;
 *         &lt;element name="IntrstRate" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PercentageRate" minOccurs="0"/&gt;
 *         &lt;element name="NxtIntrstRate" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PercentageRate" minOccurs="0"/&gt;
 *         &lt;element name="IndxRateBsis" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PercentageRate" minOccurs="0"/&gt;
 *         &lt;element name="CpnAttchdNb" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Number2Choice" minOccurs="0"/&gt;
 *         &lt;element name="PoolNb" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Number2Choice" minOccurs="0"/&gt;
 *         &lt;element name="VarblRateInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator" minOccurs="0"/&gt;
 *         &lt;element name="CllblInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator" minOccurs="0"/&gt;
 *         &lt;element name="PutblInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator" minOccurs="0"/&gt;
 *         &lt;element name="MktOrIndctvPric" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PriceType1Choice" minOccurs="0"/&gt;
 *         &lt;element name="ExrcPric" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Price2" minOccurs="0"/&gt;
 *         &lt;element name="SbcptPric" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Price2" minOccurs="0"/&gt;
 *         &lt;element name="ConvsPric" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Price2" minOccurs="0"/&gt;
 *         &lt;element name="StrkPric" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Price2" minOccurs="0"/&gt;
 *         &lt;element name="MinNmnlQty" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}FinancialInstrumentQuantity1Choice" minOccurs="0"/&gt;
 *         &lt;element name="CtrctSz" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}FinancialInstrumentQuantity1Choice" minOccurs="0"/&gt;
 *         &lt;element name="UndrlygFinInstrmId" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SecurityIdentification14" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FinInstrmAttrAddtlDtls" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max350Text" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialInstrumentAttributes20",
         propOrder = {"plcOfListg", "dayCntBsis", "regnForm", "pmtFrqcy", "pmtSts", "pmtDrctn", "varblRateChngFrqcy", "prefToIncm", "clssfctnTp", "optnStyle", "optnTp", "dnmtnCcy", "cpnDt", "xpryDt", "fltgRateFxgDt", "mtrtyDt", "isseDt", "nxtCllblDt", "putblDt", "dtdDt", "frstPmtDt", "prvsFctr", "curFctr", "nxtFctr", "intrstRate", "nxtIntrstRate", "indxRateBsis", "cpnAttchdNb", "poolNb", "varblRateInd", "cllblInd", "putblInd", "mktOrIndctvPric", "exrcPric", "sbcptPric", "convsPric", "strkPric", "minNmnlQty", "ctrctSz", "undrlygFinInstrmId", "finInstrmAttrAddtlDtls"})
public class FinancialInstrumentAttributes20 {

    @XmlElement(name = "PlcOfListg") protected MarketIdentification5 plcOfListg;
    @XmlElement(name = "DayCntBsis") protected InterestComputationMethodFormat1Choice dayCntBsis;
    @XmlElement(name = "RegnForm") protected FormOfSecurity2Choice regnForm;
    @XmlElement(name = "PmtFrqcy") protected Frequency3Choice pmtFrqcy;
    @XmlElement(name = "PmtSts") protected SecuritiesPaymentStatus2Choice pmtSts;
    @XmlElement(name = "PmtDrctn") protected PaymentDirection2Choice pmtDrctn;
    @XmlElement(name = "VarblRateChngFrqcy") protected Frequency3Choice varblRateChngFrqcy;
    @XmlElement(name = "PrefToIncm") protected PreferenceToIncome2Choice prefToIncm;
    @XmlElement(name = "ClssfctnTp") protected ClassificationType2Choice clssfctnTp;
    @XmlElement(name = "OptnStyle") protected OptionStyle4Choice optnStyle;
    @XmlElement(name = "OptnTp") protected OptionType2Choice optnTp;
    @XmlElement(name = "DnmtnCcy") protected String dnmtnCcy;
    @XmlElement(name = "CpnDt") @XmlSchemaType(name = "date") protected XMLGregorianCalendar cpnDt;
    @XmlElement(name = "XpryDt") @XmlSchemaType(name = "date") protected XMLGregorianCalendar xpryDt;
    @XmlElement(name = "FltgRateFxgDt") @XmlSchemaType(name = "date") protected XMLGregorianCalendar fltgRateFxgDt;
    @XmlElement(name = "MtrtyDt") @XmlSchemaType(name = "date") protected XMLGregorianCalendar mtrtyDt;
    @XmlElement(name = "IsseDt") @XmlSchemaType(name = "date") protected XMLGregorianCalendar isseDt;
    @XmlElement(name = "NxtCllblDt") @XmlSchemaType(name = "date") protected XMLGregorianCalendar nxtCllblDt;
    @XmlElement(name = "PutblDt") @XmlSchemaType(name = "date") protected XMLGregorianCalendar putblDt;
    @XmlElement(name = "DtdDt") @XmlSchemaType(name = "date") protected XMLGregorianCalendar dtdDt;
    @XmlElement(name = "FrstPmtDt") @XmlSchemaType(name = "date") protected XMLGregorianCalendar frstPmtDt;
    @XmlElement(name = "PrvsFctr") protected BigDecimal prvsFctr;
    @XmlElement(name = "CurFctr") protected BigDecimal curFctr;
    @XmlElement(name = "NxtFctr") protected BigDecimal nxtFctr;
    @XmlElement(name = "IntrstRate") protected BigDecimal intrstRate;
    @XmlElement(name = "NxtIntrstRate") protected BigDecimal nxtIntrstRate;
    @XmlElement(name = "IndxRateBsis") protected BigDecimal indxRateBsis;
    @XmlElement(name = "CpnAttchdNb") protected Number2Choice cpnAttchdNb;
    @XmlElement(name = "PoolNb") protected Number2Choice poolNb;
    @XmlElement(name = "VarblRateInd") protected Boolean varblRateInd;
    @XmlElement(name = "CllblInd") protected Boolean cllblInd;
    @XmlElement(name = "PutblInd") protected Boolean putblInd;
    @XmlElement(name = "MktOrIndctvPric") protected PriceType1Choice mktOrIndctvPric;
    @XmlElement(name = "ExrcPric") protected Price2 exrcPric;
    @XmlElement(name = "SbcptPric") protected Price2 sbcptPric;
    @XmlElement(name = "ConvsPric") protected Price2 convsPric;
    @XmlElement(name = "StrkPric") protected Price2 strkPric;
    @XmlElement(name = "MinNmnlQty") protected FinancialInstrumentQuantity1Choice minNmnlQty;
    @XmlElement(name = "CtrctSz") protected FinancialInstrumentQuantity1Choice ctrctSz;
    @XmlElement(name = "UndrlygFinInstrmId") protected List<SecurityIdentification14> undrlygFinInstrmId;
    @XmlElement(name = "FinInstrmAttrAddtlDtls") protected String finInstrmAttrAddtlDtls;

    /**
     * Obtém o valor da propriedade clssfctnTp.
     *
     * @return possible object is
     * {@link ClassificationType2Choice }
     */
    public ClassificationType2Choice getClssfctnTp() {
        return clssfctnTp;
    }

    /**
     * Define o valor da propriedade clssfctnTp.
     *
     * @param value allowed object is
     *              {@link ClassificationType2Choice }
     */
    public void setClssfctnTp(ClassificationType2Choice value) {
        this.clssfctnTp = value;
    }

    /**
     * Obtém o valor da propriedade convsPric.
     *
     * @return possible object is
     * {@link Price2 }
     */
    public Price2 getConvsPric() {
        return convsPric;
    }

    /**
     * Define o valor da propriedade convsPric.
     *
     * @param value allowed object is
     *              {@link Price2 }
     */
    public void setConvsPric(Price2 value) {
        this.convsPric = value;
    }

    /**
     * Obtém o valor da propriedade cpnAttchdNb.
     *
     * @return possible object is
     * {@link Number2Choice }
     */
    public Number2Choice getCpnAttchdNb() {
        return cpnAttchdNb;
    }

    /**
     * Define o valor da propriedade cpnAttchdNb.
     *
     * @param value allowed object is
     *              {@link Number2Choice }
     */
    public void setCpnAttchdNb(Number2Choice value) {
        this.cpnAttchdNb = value;
    }

    /**
     * Obtém o valor da propriedade cpnDt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getCpnDt() {
        return cpnDt;
    }

    /**
     * Define o valor da propriedade cpnDt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setCpnDt(XMLGregorianCalendar value) {
        this.cpnDt = value;
    }

    /**
     * Obtém o valor da propriedade ctrctSz.
     *
     * @return possible object is
     * {@link FinancialInstrumentQuantity1Choice }
     */
    public FinancialInstrumentQuantity1Choice getCtrctSz() {
        return ctrctSz;
    }

    /**
     * Define o valor da propriedade ctrctSz.
     *
     * @param value allowed object is
     *              {@link FinancialInstrumentQuantity1Choice }
     */
    public void setCtrctSz(FinancialInstrumentQuantity1Choice value) {
        this.ctrctSz = value;
    }

    /**
     * Obtém o valor da propriedade curFctr.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getCurFctr() {
        return curFctr;
    }

    /**
     * Define o valor da propriedade curFctr.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setCurFctr(BigDecimal value) {
        this.curFctr = value;
    }

    /**
     * Obtém o valor da propriedade dayCntBsis.
     *
     * @return possible object is
     * {@link InterestComputationMethodFormat1Choice }
     */
    public InterestComputationMethodFormat1Choice getDayCntBsis() {
        return dayCntBsis;
    }

    /**
     * Define o valor da propriedade dayCntBsis.
     *
     * @param value allowed object is
     *              {@link InterestComputationMethodFormat1Choice }
     */
    public void setDayCntBsis(InterestComputationMethodFormat1Choice value) {
        this.dayCntBsis = value;
    }

    /**
     * Obtém o valor da propriedade dnmtnCcy.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDnmtnCcy() {
        return dnmtnCcy;
    }

    /**
     * Define o valor da propriedade dnmtnCcy.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDnmtnCcy(String value) {
        this.dnmtnCcy = value;
    }

    /**
     * Obtém o valor da propriedade dtdDt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDtdDt() {
        return dtdDt;
    }

    /**
     * Define o valor da propriedade dtdDt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDtdDt(XMLGregorianCalendar value) {
        this.dtdDt = value;
    }

    /**
     * Obtém o valor da propriedade exrcPric.
     *
     * @return possible object is
     * {@link Price2 }
     */
    public Price2 getExrcPric() {
        return exrcPric;
    }

    /**
     * Define o valor da propriedade exrcPric.
     *
     * @param value allowed object is
     *              {@link Price2 }
     */
    public void setExrcPric(Price2 value) {
        this.exrcPric = value;
    }

    /**
     * Obtém o valor da propriedade finInstrmAttrAddtlDtls.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFinInstrmAttrAddtlDtls() {
        return finInstrmAttrAddtlDtls;
    }

    /**
     * Define o valor da propriedade finInstrmAttrAddtlDtls.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFinInstrmAttrAddtlDtls(String value) {
        this.finInstrmAttrAddtlDtls = value;
    }

    /**
     * Obtém o valor da propriedade fltgRateFxgDt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getFltgRateFxgDt() {
        return fltgRateFxgDt;
    }

    /**
     * Define o valor da propriedade fltgRateFxgDt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setFltgRateFxgDt(XMLGregorianCalendar value) {
        this.fltgRateFxgDt = value;
    }

    /**
     * Obtém o valor da propriedade frstPmtDt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getFrstPmtDt() {
        return frstPmtDt;
    }

    /**
     * Define o valor da propriedade frstPmtDt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setFrstPmtDt(XMLGregorianCalendar value) {
        this.frstPmtDt = value;
    }

    /**
     * Obtém o valor da propriedade indxRateBsis.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getIndxRateBsis() {
        return indxRateBsis;
    }

    /**
     * Define o valor da propriedade indxRateBsis.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setIndxRateBsis(BigDecimal value) {
        this.indxRateBsis = value;
    }

    /**
     * Obtém o valor da propriedade intrstRate.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getIntrstRate() {
        return intrstRate;
    }

    /**
     * Define o valor da propriedade intrstRate.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setIntrstRate(BigDecimal value) {
        this.intrstRate = value;
    }

    /**
     * Obtém o valor da propriedade isseDt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getIsseDt() {
        return isseDt;
    }

    /**
     * Define o valor da propriedade isseDt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setIsseDt(XMLGregorianCalendar value) {
        this.isseDt = value;
    }

    /**
     * Obtém o valor da propriedade minNmnlQty.
     *
     * @return possible object is
     * {@link FinancialInstrumentQuantity1Choice }
     */
    public FinancialInstrumentQuantity1Choice getMinNmnlQty() {
        return minNmnlQty;
    }

    /**
     * Define o valor da propriedade minNmnlQty.
     *
     * @param value allowed object is
     *              {@link FinancialInstrumentQuantity1Choice }
     */
    public void setMinNmnlQty(FinancialInstrumentQuantity1Choice value) {
        this.minNmnlQty = value;
    }

    /**
     * Obtém o valor da propriedade mktOrIndctvPric.
     *
     * @return possible object is
     * {@link PriceType1Choice }
     */
    public PriceType1Choice getMktOrIndctvPric() {
        return mktOrIndctvPric;
    }

    /**
     * Define o valor da propriedade mktOrIndctvPric.
     *
     * @param value allowed object is
     *              {@link PriceType1Choice }
     */
    public void setMktOrIndctvPric(PriceType1Choice value) {
        this.mktOrIndctvPric = value;
    }

    /**
     * Obtém o valor da propriedade mtrtyDt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getMtrtyDt() {
        return mtrtyDt;
    }

    /**
     * Define o valor da propriedade mtrtyDt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setMtrtyDt(XMLGregorianCalendar value) {
        this.mtrtyDt = value;
    }

    /**
     * Obtém o valor da propriedade nxtCllblDt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getNxtCllblDt() {
        return nxtCllblDt;
    }

    /**
     * Define o valor da propriedade nxtCllblDt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setNxtCllblDt(XMLGregorianCalendar value) {
        this.nxtCllblDt = value;
    }

    /**
     * Obtém o valor da propriedade nxtFctr.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getNxtFctr() {
        return nxtFctr;
    }

    /**
     * Define o valor da propriedade nxtFctr.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setNxtFctr(BigDecimal value) {
        this.nxtFctr = value;
    }

    /**
     * Obtém o valor da propriedade nxtIntrstRate.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getNxtIntrstRate() {
        return nxtIntrstRate;
    }

    /**
     * Define o valor da propriedade nxtIntrstRate.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setNxtIntrstRate(BigDecimal value) {
        this.nxtIntrstRate = value;
    }

    /**
     * Obtém o valor da propriedade optnStyle.
     *
     * @return possible object is
     * {@link OptionStyle4Choice }
     */
    public OptionStyle4Choice getOptnStyle() {
        return optnStyle;
    }

    /**
     * Define o valor da propriedade optnStyle.
     *
     * @param value allowed object is
     *              {@link OptionStyle4Choice }
     */
    public void setOptnStyle(OptionStyle4Choice value) {
        this.optnStyle = value;
    }

    /**
     * Obtém o valor da propriedade optnTp.
     *
     * @return possible object is
     * {@link OptionType2Choice }
     */
    public OptionType2Choice getOptnTp() {
        return optnTp;
    }

    /**
     * Define o valor da propriedade optnTp.
     *
     * @param value allowed object is
     *              {@link OptionType2Choice }
     */
    public void setOptnTp(OptionType2Choice value) {
        this.optnTp = value;
    }

    /**
     * Obtém o valor da propriedade plcOfListg.
     *
     * @return possible object is
     * {@link MarketIdentification5 }
     */
    public MarketIdentification5 getPlcOfListg() {
        return plcOfListg;
    }

    /**
     * Define o valor da propriedade plcOfListg.
     *
     * @param value allowed object is
     *              {@link MarketIdentification5 }
     */
    public void setPlcOfListg(MarketIdentification5 value) {
        this.plcOfListg = value;
    }

    /**
     * Obtém o valor da propriedade pmtDrctn.
     *
     * @return possible object is
     * {@link PaymentDirection2Choice }
     */
    public PaymentDirection2Choice getPmtDrctn() {
        return pmtDrctn;
    }

    /**
     * Define o valor da propriedade pmtDrctn.
     *
     * @param value allowed object is
     *              {@link PaymentDirection2Choice }
     */
    public void setPmtDrctn(PaymentDirection2Choice value) {
        this.pmtDrctn = value;
    }

    /**
     * Obtém o valor da propriedade pmtFrqcy.
     *
     * @return possible object is
     * {@link Frequency3Choice }
     */
    public Frequency3Choice getPmtFrqcy() {
        return pmtFrqcy;
    }

    /**
     * Define o valor da propriedade pmtFrqcy.
     *
     * @param value allowed object is
     *              {@link Frequency3Choice }
     */
    public void setPmtFrqcy(Frequency3Choice value) {
        this.pmtFrqcy = value;
    }

    /**
     * Obtém o valor da propriedade pmtSts.
     *
     * @return possible object is
     * {@link SecuritiesPaymentStatus2Choice }
     */
    public SecuritiesPaymentStatus2Choice getPmtSts() {
        return pmtSts;
    }

    /**
     * Define o valor da propriedade pmtSts.
     *
     * @param value allowed object is
     *              {@link SecuritiesPaymentStatus2Choice }
     */
    public void setPmtSts(SecuritiesPaymentStatus2Choice value) {
        this.pmtSts = value;
    }

    /**
     * Obtém o valor da propriedade poolNb.
     *
     * @return possible object is
     * {@link Number2Choice }
     */
    public Number2Choice getPoolNb() {
        return poolNb;
    }

    /**
     * Define o valor da propriedade poolNb.
     *
     * @param value allowed object is
     *              {@link Number2Choice }
     */
    public void setPoolNb(Number2Choice value) {
        this.poolNb = value;
    }

    /**
     * Obtém o valor da propriedade prefToIncm.
     *
     * @return possible object is
     * {@link PreferenceToIncome2Choice }
     */
    public PreferenceToIncome2Choice getPrefToIncm() {
        return prefToIncm;
    }

    /**
     * Define o valor da propriedade prefToIncm.
     *
     * @param value allowed object is
     *              {@link PreferenceToIncome2Choice }
     */
    public void setPrefToIncm(PreferenceToIncome2Choice value) {
        this.prefToIncm = value;
    }

    /**
     * Obtém o valor da propriedade prvsFctr.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getPrvsFctr() {
        return prvsFctr;
    }

    /**
     * Define o valor da propriedade prvsFctr.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setPrvsFctr(BigDecimal value) {
        this.prvsFctr = value;
    }

    /**
     * Obtém o valor da propriedade putblDt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getPutblDt() {
        return putblDt;
    }

    /**
     * Define o valor da propriedade putblDt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setPutblDt(XMLGregorianCalendar value) {
        this.putblDt = value;
    }

    /**
     * Obtém o valor da propriedade regnForm.
     *
     * @return possible object is
     * {@link FormOfSecurity2Choice }
     */
    public FormOfSecurity2Choice getRegnForm() {
        return regnForm;
    }

    /**
     * Define o valor da propriedade regnForm.
     *
     * @param value allowed object is
     *              {@link FormOfSecurity2Choice }
     */
    public void setRegnForm(FormOfSecurity2Choice value) {
        this.regnForm = value;
    }

    /**
     * Obtém o valor da propriedade sbcptPric.
     *
     * @return possible object is
     * {@link Price2 }
     */
    public Price2 getSbcptPric() {
        return sbcptPric;
    }

    /**
     * Define o valor da propriedade sbcptPric.
     *
     * @param value allowed object is
     *              {@link Price2 }
     */
    public void setSbcptPric(Price2 value) {
        this.sbcptPric = value;
    }

    /**
     * Obtém o valor da propriedade strkPric.
     *
     * @return possible object is
     * {@link Price2 }
     */
    public Price2 getStrkPric() {
        return strkPric;
    }

    /**
     * Define o valor da propriedade strkPric.
     *
     * @param value allowed object is
     *              {@link Price2 }
     */
    public void setStrkPric(Price2 value) {
        this.strkPric = value;
    }

    /**
     * Gets the value of the undrlygFinInstrmId property.
     * <p/>
     * <p/>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the undrlygFinInstrmId property.
     * <p/>
     * <p/>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUndrlygFinInstrmId().add(newItem);
     * </pre>
     * <p/>
     * <p/>
     * <p/>
     * Objects of the following type(s) are allowed in the list
     * {@link SecurityIdentification14 }
     */
    public List<SecurityIdentification14> getUndrlygFinInstrmId() {
        if (undrlygFinInstrmId == null) {
            undrlygFinInstrmId = new ArrayList<SecurityIdentification14>();
        }
        return this.undrlygFinInstrmId;
    }

    /**
     * Obtém o valor da propriedade varblRateChngFrqcy.
     *
     * @return possible object is
     * {@link Frequency3Choice }
     */
    public Frequency3Choice getVarblRateChngFrqcy() {
        return varblRateChngFrqcy;
    }

    /**
     * Define o valor da propriedade varblRateChngFrqcy.
     *
     * @param value allowed object is
     *              {@link Frequency3Choice }
     */
    public void setVarblRateChngFrqcy(Frequency3Choice value) {
        this.varblRateChngFrqcy = value;
    }

    /**
     * Obtém o valor da propriedade xpryDt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getXpryDt() {
        return xpryDt;
    }

    /**
     * Define o valor da propriedade xpryDt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setXpryDt(XMLGregorianCalendar value) {
        this.xpryDt = value;
    }

    /**
     * Obtém o valor da propriedade cllblInd.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isCllblInd() {
        return cllblInd;
    }

    /**
     * Obtém o valor da propriedade putblInd.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isPutblInd() {
        return putblInd;
    }

    /**
     * Obtém o valor da propriedade varblRateInd.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isVarblRateInd() {
        return varblRateInd;
    }

    /**
     * Define o valor da propriedade cllblInd.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setCllblInd(Boolean value) {
        this.cllblInd = value;
    }

    /**
     * Define o valor da propriedade putblInd.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setPutblInd(Boolean value) {
        this.putblInd = value;
    }

    /**
     * Define o valor da propriedade varblRateInd.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setVarblRateInd(Boolean value) {
        this.varblRateInd = value;
    }

}
