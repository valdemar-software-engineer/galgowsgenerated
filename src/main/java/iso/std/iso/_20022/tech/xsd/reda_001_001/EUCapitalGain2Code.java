package iso.std.iso._20022.tech.xsd.reda_001_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de EUCapitalGain2Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="EUCapitalGain2Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="EUSI"/&gt;
 *     &lt;enumeration value="EUSO"/&gt;
 *     &lt;enumeration value="UKWN"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "EUCapitalGain2Code")
@XmlEnum
public enum EUCapitalGain2Code {

    EUSI,
    EUSO,
    UKWN;

    public static EUCapitalGain2Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
