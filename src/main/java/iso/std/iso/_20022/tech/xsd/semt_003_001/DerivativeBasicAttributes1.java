package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de DerivativeBasicAttributes1 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="DerivativeBasicAttributes1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="NtnlCcyAndAmt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyAndAmount"/&gt;
 *         &lt;element name="IntrstInclInPric" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DerivativeBasicAttributes1", propOrder = {"ntnlCcyAndAmt", "intrstInclInPric"})
public class DerivativeBasicAttributes1 {

    @XmlElement(name = "NtnlCcyAndAmt", required = true) protected ActiveOrHistoricCurrencyAndAmount ntnlCcyAndAmt;
    @XmlElement(name = "IntrstInclInPric") protected Boolean intrstInclInPric;

    /**
     * Obtém o valor da propriedade ntnlCcyAndAmt.
     *
     * @return possible object is
     * {@link ActiveOrHistoricCurrencyAndAmount }
     */
    public ActiveOrHistoricCurrencyAndAmount getNtnlCcyAndAmt() {
        return ntnlCcyAndAmt;
    }

    /**
     * Define o valor da propriedade ntnlCcyAndAmt.
     *
     * @param value allowed object is
     *              {@link ActiveOrHistoricCurrencyAndAmount }
     */
    public void setNtnlCcyAndAmt(ActiveOrHistoricCurrencyAndAmount value) {
        this.ntnlCcyAndAmt = value;
    }

    /**
     * Obtém o valor da propriedade intrstInclInPric.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isIntrstInclInPric() {
        return intrstInclInPric;
    }

    /**
     * Define o valor da propriedade intrstInclInPric.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setIntrstInclInPric(Boolean value) {
        this.intrstInclInPric = value;
    }

}
