package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * <p>Classe Java de ForeignExchangeTerms14 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="ForeignExchangeTerms14"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UnitCcy" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyCode"/&gt;
 *         &lt;element name="QtdCcy" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ActiveOrHistoricCurrencyCode"/&gt;
 *         &lt;element name="XchgRate" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}BaseOneRate"/&gt;
 *         &lt;element name="QtnDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ISODateTime" minOccurs="0"/&gt;
 *         &lt;element name="QtgInstn" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PartyIdentification49Choice" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ForeignExchangeTerms14", propOrder = {"unitCcy", "qtdCcy", "xchgRate", "qtnDt", "qtgInstn"})
public class ForeignExchangeTerms14 {

    @XmlElement(name = "UnitCcy", required = true) protected String unitCcy;
    @XmlElement(name = "QtdCcy", required = true) protected String qtdCcy;
    @XmlElement(name = "XchgRate", required = true) protected BigDecimal xchgRate;
    @XmlElement(name = "QtnDt") @XmlSchemaType(name = "dateTime") protected XMLGregorianCalendar qtnDt;
    @XmlElement(name = "QtgInstn") protected PartyIdentification49Choice qtgInstn;

    /**
     * Obtém o valor da propriedade qtdCcy.
     *
     * @return possible object is
     * {@link String }
     */
    public String getQtdCcy() {
        return qtdCcy;
    }

    /**
     * Define o valor da propriedade qtdCcy.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setQtdCcy(String value) {
        this.qtdCcy = value;
    }

    /**
     * Obtém o valor da propriedade qtgInstn.
     *
     * @return possible object is
     * {@link PartyIdentification49Choice }
     */
    public PartyIdentification49Choice getQtgInstn() {
        return qtgInstn;
    }

    /**
     * Define o valor da propriedade qtgInstn.
     *
     * @param value allowed object is
     *              {@link PartyIdentification49Choice }
     */
    public void setQtgInstn(PartyIdentification49Choice value) {
        this.qtgInstn = value;
    }

    /**
     * Obtém o valor da propriedade qtnDt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getQtnDt() {
        return qtnDt;
    }

    /**
     * Define o valor da propriedade qtnDt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setQtnDt(XMLGregorianCalendar value) {
        this.qtnDt = value;
    }

    /**
     * Obtém o valor da propriedade unitCcy.
     *
     * @return possible object is
     * {@link String }
     */
    public String getUnitCcy() {
        return unitCcy;
    }

    /**
     * Define o valor da propriedade unitCcy.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setUnitCcy(String value) {
        this.unitCcy = value;
    }

    /**
     * Obtém o valor da propriedade xchgRate.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getXchgRate() {
        return xchgRate;
    }

    /**
     * Define o valor da propriedade xchgRate.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setXchgRate(BigDecimal value) {
        this.xchgRate = value;
    }

}
