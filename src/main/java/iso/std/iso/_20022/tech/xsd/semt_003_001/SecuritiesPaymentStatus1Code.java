package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de SecuritiesPaymentStatus1Code.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;simpleType name="SecuritiesPaymentStatus1Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="FULL"/&gt;
 *     &lt;enumeration value="NILL"/&gt;
 *     &lt;enumeration value="PART"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "SecuritiesPaymentStatus1Code")
@XmlEnum
public enum SecuritiesPaymentStatus1Code {

    FULL,
    NILL,
    PART;

    public static SecuritiesPaymentStatus1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
