package iso.std.iso._20022.tech.xsd.camt_045_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de NetCashForecast3 complex type.
 *
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 *
 * <pre>
 * &lt;complexType name="NetCashForecast3"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="NetAmt" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}ActiveOrHistoricCurrencyAndAmount" minOccurs="0"/&gt;
 *         &lt;element name="NetUnitsNb" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}FinancialInstrumentQuantity1" minOccurs="0"/&gt;
 *         &lt;element name="FlowDrctn" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}FlowDirectionType1Code"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NetCashForecast3", propOrder = {
    "netAmt",
    "netUnitsNb",
    "flowDrctn"
})
public class NetCashForecast3 {

    @XmlElement(name = "NetAmt")
    protected ActiveOrHistoricCurrencyAndAmount netAmt;
    @XmlElement(name = "NetUnitsNb")
    protected FinancialInstrumentQuantity1 netUnitsNb;
    @XmlElement(name = "FlowDrctn", required = true)
    @XmlSchemaType(name = "string")
    protected FlowDirectionType1Code flowDrctn;

    /**
     * Obtém o valor da propriedade flowDrctn.
     *
     * @return
     *     possible object is
     *     {@link FlowDirectionType1Code }
     *
     */
    public FlowDirectionType1Code getFlowDrctn() {
        return flowDrctn;
    }

    /**
     * Define o valor da propriedade flowDrctn.
     *
     * @param value
     *     allowed object is
     *     {@link FlowDirectionType1Code }
     *
     */
    public void setFlowDrctn(FlowDirectionType1Code value) {
        this.flowDrctn = value;
    }

    /**
     * Obtém o valor da propriedade netAmt.
     *
     * @return
     *     possible object is
     *     {@link ActiveOrHistoricCurrencyAndAmount }
     *
     */
    public ActiveOrHistoricCurrencyAndAmount getNetAmt() {
        return netAmt;
    }

    /**
     * Define o valor da propriedade netAmt.
     *
     * @param value
     *     allowed object is
     *     {@link ActiveOrHistoricCurrencyAndAmount }
     *
     */
    public void setNetAmt(ActiveOrHistoricCurrencyAndAmount value) {
        this.netAmt = value;
    }

    /**
     * Obtém o valor da propriedade netUnitsNb.
     *
     * @return
     *     possible object is
     *     {@link FinancialInstrumentQuantity1 }
     *
     */
    public FinancialInstrumentQuantity1 getNetUnitsNb() {
        return netUnitsNb;
    }

    /**
     * Define o valor da propriedade netUnitsNb.
     *
     * @param value
     *     allowed object is
     *     {@link FinancialInstrumentQuantity1 }
     *
     */
    public void setNetUnitsNb(FinancialInstrumentQuantity1 value) {
        this.netUnitsNb = value;
    }

}
