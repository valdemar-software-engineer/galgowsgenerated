package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de InterestComputationMethod2Code.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;simpleType name="InterestComputationMethod2Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="A001"/&gt;
 *     &lt;enumeration value="A002"/&gt;
 *     &lt;enumeration value="A003"/&gt;
 *     &lt;enumeration value="A004"/&gt;
 *     &lt;enumeration value="A005"/&gt;
 *     &lt;enumeration value="A006"/&gt;
 *     &lt;enumeration value="A007"/&gt;
 *     &lt;enumeration value="A008"/&gt;
 *     &lt;enumeration value="A009"/&gt;
 *     &lt;enumeration value="A010"/&gt;
 *     &lt;enumeration value="A011"/&gt;
 *     &lt;enumeration value="A012"/&gt;
 *     &lt;enumeration value="A013"/&gt;
 *     &lt;enumeration value="A014"/&gt;
 *     &lt;enumeration value="NARR"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "InterestComputationMethod2Code")
@XmlEnum
public enum InterestComputationMethod2Code {

    @XmlEnumValue("A001")
    A_001("A001"),
    @XmlEnumValue("A002")
    A_002("A002"),
    @XmlEnumValue("A003")
    A_003("A003"),
    @XmlEnumValue("A004")
    A_004("A004"),
    @XmlEnumValue("A005")
    A_005("A005"),
    @XmlEnumValue("A006")
    A_006("A006"),
    @XmlEnumValue("A007")
    A_007("A007"),
    @XmlEnumValue("A008")
    A_008("A008"),
    @XmlEnumValue("A009")
    A_009("A009"),
    @XmlEnumValue("A010")
    A_010("A010"),
    @XmlEnumValue("A011")
    A_011("A011"),
    @XmlEnumValue("A012")
    A_012("A012"),
    @XmlEnumValue("A013")
    A_013("A013"),
    @XmlEnumValue("A014")
    A_014("A014"),
    NARR("NARR");
    private final String value;

    InterestComputationMethod2Code(String v) {
        value = v;
    }

    public static InterestComputationMethod2Code fromValue(String v) {
        for (InterestComputationMethod2Code c : InterestComputationMethod2Code.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public String value() {
        return value;
    }

}
