package iso.std.iso._20022.tech.xsd.reda_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de ValuationStatistics3 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ValuationStatistics3"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Ccy" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}ActiveOrHistoricCurrencyCode"/&gt;
 *         &lt;element name="PricTpChngBsis" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PriceType2"/&gt;
 *         &lt;element name="PricChng" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PriceValueChange1"/&gt;
 *         &lt;element name="Yld" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PercentageRate" minOccurs="0"/&gt;
 *         &lt;element name="ByPrdfndTmPrds" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}StatisticsByPredefinedTimePeriods2" minOccurs="0"/&gt;
 *         &lt;element name="ByUsrDfndTmPrd" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}StatisticsByUserDefinedTimePeriod2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValuationStatistics3",
         propOrder = {"ccy", "pricTpChngBsis", "pricChng", "yld", "byPrdfndTmPrds", "byUsrDfndTmPrd"})
public class ValuationStatistics3 {

    @XmlElement(name = "Ccy", required = true) protected String ccy;
    @XmlElement(name = "PricTpChngBsis", required = true) protected PriceType2 pricTpChngBsis;
    @XmlElement(name = "PricChng", required = true) protected PriceValueChange1 pricChng;
    @XmlElement(name = "Yld") protected BigDecimal yld;
    @XmlElement(name = "ByPrdfndTmPrds") protected StatisticsByPredefinedTimePeriods2 byPrdfndTmPrds;
    @XmlElement(name = "ByUsrDfndTmPrd") protected List<StatisticsByUserDefinedTimePeriod2> byUsrDfndTmPrd;

    /**
     * Obtém o valor da propriedade byPrdfndTmPrds.
     *
     * @return possible object is
     * {@link StatisticsByPredefinedTimePeriods2 }
     */
    public StatisticsByPredefinedTimePeriods2 getByPrdfndTmPrds() {
        return byPrdfndTmPrds;
    }

    /**
     * Define o valor da propriedade byPrdfndTmPrds.
     *
     * @param value allowed object is
     *              {@link StatisticsByPredefinedTimePeriods2 }
     */
    public void setByPrdfndTmPrds(StatisticsByPredefinedTimePeriods2 value) {
        this.byPrdfndTmPrds = value;
    }

    /**
     * Gets the value of the byUsrDfndTmPrd property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the byUsrDfndTmPrd property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getByUsrDfndTmPrd().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatisticsByUserDefinedTimePeriod2 }
     */
    public List<StatisticsByUserDefinedTimePeriod2> getByUsrDfndTmPrd() {
        if (byUsrDfndTmPrd == null) {
            byUsrDfndTmPrd = new ArrayList<StatisticsByUserDefinedTimePeriod2>();
        }
        return this.byUsrDfndTmPrd;
    }

    /**
     * Obtém o valor da propriedade ccy.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * Define o valor da propriedade ccy.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCcy(String value) {
        this.ccy = value;
    }

    /**
     * Obtém o valor da propriedade pricChng.
     *
     * @return possible object is
     * {@link PriceValueChange1 }
     */
    public PriceValueChange1 getPricChng() {
        return pricChng;
    }

    /**
     * Define o valor da propriedade pricChng.
     *
     * @param value allowed object is
     *              {@link PriceValueChange1 }
     */
    public void setPricChng(PriceValueChange1 value) {
        this.pricChng = value;
    }

    /**
     * Obtém o valor da propriedade pricTpChngBsis.
     *
     * @return possible object is
     * {@link PriceType2 }
     */
    public PriceType2 getPricTpChngBsis() {
        return pricTpChngBsis;
    }

    /**
     * Define o valor da propriedade pricTpChngBsis.
     *
     * @param value allowed object is
     *              {@link PriceType2 }
     */
    public void setPricTpChngBsis(PriceType2 value) {
        this.pricTpChngBsis = value;
    }

    /**
     * Obtém o valor da propriedade yld.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getYld() {
        return yld;
    }

    /**
     * Define o valor da propriedade yld.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setYld(BigDecimal value) {
        this.yld = value;
    }

}
