package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de AdditionalReference3 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="AdditionalReference3"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Ref" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Max35Text"/&gt;
 *         &lt;element name="RefIssr" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}PartyIdentification2Choice" minOccurs="0"/&gt;
 *         &lt;element name="MsgNm" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Max35Text" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalReference3", propOrder = {"ref", "refIssr", "msgNm"})
public class AdditionalReference3 {

    @XmlElement(name = "Ref", required = true) protected String ref;
    @XmlElement(name = "RefIssr") protected PartyIdentification2Choice refIssr;
    @XmlElement(name = "MsgNm") protected String msgNm;

    /**
     * Obtém o valor da propriedade msgNm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMsgNm() {
        return msgNm;
    }

    /**
     * Define o valor da propriedade msgNm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMsgNm(String value) {
        this.msgNm = value;
    }

    /**
     * Obtém o valor da propriedade ref.
     *
     * @return possible object is
     * {@link String }
     */
    public String getRef() {
        return ref;
    }

    /**
     * Define o valor da propriedade ref.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setRef(String value) {
        this.ref = value;
    }

    /**
     * Obtém o valor da propriedade refIssr.
     *
     * @return possible object is
     * {@link PartyIdentification2Choice }
     */
    public PartyIdentification2Choice getRefIssr() {
        return refIssr;
    }

    /**
     * Define o valor da propriedade refIssr.
     *
     * @param value allowed object is
     *              {@link PartyIdentification2Choice }
     */
    public void setRefIssr(PartyIdentification2Choice value) {
        this.refIssr = value;
    }

}
