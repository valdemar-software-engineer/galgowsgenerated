package iso.std.iso._20022.tech.xsd.camt_045_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ChargeType10Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="ChargeType10Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BEND"/>
 *     &lt;enumeration value="FEND"/>
 *     &lt;enumeration value="PENA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "ChargeType10Code")
@XmlEnum
public enum ChargeType10Code {

    BEND,
    FEND,
    PENA;

    public static ChargeType10Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
