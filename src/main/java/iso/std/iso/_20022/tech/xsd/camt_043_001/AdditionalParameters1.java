package iso.std.iso._20022.tech.xsd.camt_043_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de AdditionalParameters1 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="AdditionalParameters1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Ctry" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}CountryCode" minOccurs="0"/>
 *         &lt;element name="Ccy" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}ActiveOrHistoricCurrencyCode" minOccurs="0"/>
 *         &lt;element name="GeoArea" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Max35Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalParameters1", propOrder = {"ctry", "ccy", "geoArea"})
public class AdditionalParameters1 {

    @XmlElement(name = "Ctry") protected String ctry;
    @XmlElement(name = "Ccy") protected String ccy;
    @XmlElement(name = "GeoArea") protected String geoArea;

    /**
     * Obtém o valor da propriedade ccy.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * Define o valor da propriedade ccy.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCcy(String value) {
        this.ccy = value;
    }

    /**
     * Obtém o valor da propriedade ctry.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCtry() {
        return ctry;
    }

    /**
     * Define o valor da propriedade ctry.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCtry(String value) {
        this.ctry = value;
    }

    /**
     * Obtém o valor da propriedade geoArea.
     *
     * @return possible object is
     * {@link String }
     */
    public String getGeoArea() {
        return geoArea;
    }

    /**
     * Define o valor da propriedade geoArea.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setGeoArea(String value) {
        this.geoArea = value;
    }

}
