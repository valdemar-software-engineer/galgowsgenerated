package iso.std.iso._20022.tech.xsd.reda_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de PriceType2 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PriceType2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Strd" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}TypeOfPrice6Code"/&gt;
 *         &lt;element name="AddtlInf" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}Max350Text" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceType2", propOrder = {"strd", "addtlInf"})
public class PriceType2 {

    @XmlElement(name = "Strd", required = true) @XmlSchemaType(name = "string") protected TypeOfPrice6Code strd;
    @XmlElement(name = "AddtlInf") protected String addtlInf;

    /**
     * Obtém o valor da propriedade addtlInf.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAddtlInf() {
        return addtlInf;
    }

    /**
     * Define o valor da propriedade addtlInf.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAddtlInf(String value) {
        this.addtlInf = value;
    }

    /**
     * Obtém o valor da propriedade strd.
     *
     * @return possible object is
     * {@link TypeOfPrice6Code }
     */
    public TypeOfPrice6Code getStrd() {
        return strd;
    }

    /**
     * Define o valor da propriedade strd.
     *
     * @param value allowed object is
     *              {@link TypeOfPrice6Code }
     */
    public void setStrd(TypeOfPrice6Code value) {
        this.strd = value;
    }

}
