package iso.std.iso._20022.tech.xsd.reda_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de PriceValuation3 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PriceValuation3"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}Max35Text"/&gt;
 *         &lt;element name="ValtnDtTm" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}DateAndDateTimeChoice" minOccurs="0"/&gt;
 *         &lt;element name="NAVDtTm" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}DateAndDateTimeChoice"/&gt;
 *         &lt;element name="FinInstrmDtls" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}FinancialInstrument8"/&gt;
 *         &lt;element name="FndMgmtCpny" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PartyIdentification2Choice" minOccurs="0"/&gt;
 *         &lt;element name="TtlNAV" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}ActiveOrHistoricCurrencyAndAmount" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="NAV" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}FinancialInstrumentQuantity1" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TtlUnitsNb" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}FinancialInstrumentQuantity1" minOccurs="0"/&gt;
 *         &lt;element name="NxtValtnDtTm" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}DateAndDateTimeChoice" minOccurs="0"/&gt;
 *         &lt;element name="PrvsValtnDtTm" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}DateAndDateTimeChoice" minOccurs="0"/&gt;
 *         &lt;element name="ValtnTp" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}ValuationTiming1Code"/&gt;
 *         &lt;element name="ValtnFrqcy" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}EventFrequency1Code" minOccurs="0"/&gt;
 *         &lt;element name="InvstNetAmt" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}ActiveCurrencyAndAmount" minOccurs="0"/&gt;
 *         &lt;element name="RedNetAmt" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}ActiveCurrencyAndAmount" minOccurs="0"/&gt;
 *         &lt;element name="LclTaxAmt" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}ActiveCurrencyAndAmount" minOccurs="0"/&gt;
 *         &lt;element name="OffclValtnInd" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}YesNoIndicator"/&gt;
 *         &lt;element name="SspdInd" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}YesNoIndicator"/&gt;
 *         &lt;element name="PricDtls" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}UnitPrice15" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ValtnSttstcs" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}ValuationStatistics3" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PrfrmncDtls" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PerformanceFactors1" minOccurs="0"/&gt;
 *         &lt;element name="AddtlInf" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}Max350Text" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceValuation3",
         propOrder = {"id", "valtnDtTm", "navDtTm", "finInstrmDtls", "fndMgmtCpny", "ttlNAV", "nav", "ttlUnitsNb", "nxtValtnDtTm", "prvsValtnDtTm", "valtnTp", "valtnFrqcy", "invstNetAmt", "redNetAmt", "lclTaxAmt", "offclValtnInd", "sspdInd", "pricDtls", "valtnSttstcs", "prfrmncDtls", "addtlInf"})
public class PriceValuation3 {

    @XmlElement(name = "Id", required = true) protected String id;
    @XmlElement(name = "ValtnDtTm") protected DateAndDateTimeChoice valtnDtTm;
    @XmlElement(name = "NAVDtTm", required = true) protected DateAndDateTimeChoice navDtTm;
    @XmlElement(name = "FinInstrmDtls", required = true) protected FinancialInstrument8 finInstrmDtls;
    @XmlElement(name = "FndMgmtCpny") protected PartyIdentification2Choice fndMgmtCpny;
    @XmlElement(name = "TtlNAV") protected List<ActiveOrHistoricCurrencyAndAmount> ttlNAV;
    @XmlElement(name = "NAV") protected List<FinancialInstrumentQuantity1> nav;
    @XmlElement(name = "TtlUnitsNb") protected FinancialInstrumentQuantity1 ttlUnitsNb;
    @XmlElement(name = "NxtValtnDtTm") protected DateAndDateTimeChoice nxtValtnDtTm;
    @XmlElement(name = "PrvsValtnDtTm") protected DateAndDateTimeChoice prvsValtnDtTm;
    @XmlElement(name = "ValtnTp", required = true) @XmlSchemaType(name = "string") protected ValuationTiming1Code
        valtnTp;
    @XmlElement(name = "ValtnFrqcy") @XmlSchemaType(name = "string") protected EventFrequency1Code valtnFrqcy;
    @XmlElement(name = "InvstNetAmt") protected ActiveCurrencyAndAmount invstNetAmt;
    @XmlElement(name = "RedNetAmt") protected ActiveCurrencyAndAmount redNetAmt;
    @XmlElement(name = "LclTaxAmt") protected ActiveCurrencyAndAmount lclTaxAmt;
    @XmlElement(name = "OffclValtnInd") protected boolean offclValtnInd;
    @XmlElement(name = "SspdInd") protected boolean sspdInd;
    @XmlElement(name = "PricDtls") protected List<UnitPrice15> pricDtls;
    @XmlElement(name = "ValtnSttstcs") protected List<ValuationStatistics3> valtnSttstcs;
    @XmlElement(name = "PrfrmncDtls") protected PerformanceFactors1 prfrmncDtls;
    @XmlElement(name = "AddtlInf") protected List<String> addtlInf;

    /**
     * Gets the value of the addtlInf property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addtlInf property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddtlInf().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     */
    public List<String> getAddtlInf() {
        if (addtlInf == null) {
            addtlInf = new ArrayList<String>();
        }
        return this.addtlInf;
    }

    /**
     * Obtém o valor da propriedade finInstrmDtls.
     *
     * @return possible object is
     * {@link FinancialInstrument8 }
     */
    public FinancialInstrument8 getFinInstrmDtls() {
        return finInstrmDtls;
    }

    /**
     * Define o valor da propriedade finInstrmDtls.
     *
     * @param value allowed object is
     *              {@link FinancialInstrument8 }
     */
    public void setFinInstrmDtls(FinancialInstrument8 value) {
        this.finInstrmDtls = value;
    }

    /**
     * Obtém o valor da propriedade fndMgmtCpny.
     *
     * @return possible object is
     * {@link PartyIdentification2Choice }
     */
    public PartyIdentification2Choice getFndMgmtCpny() {
        return fndMgmtCpny;
    }

    /**
     * Define o valor da propriedade fndMgmtCpny.
     *
     * @param value allowed object is
     *              {@link PartyIdentification2Choice }
     */
    public void setFndMgmtCpny(PartyIdentification2Choice value) {
        this.fndMgmtCpny = value;
    }

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link String }
     */
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade invstNetAmt.
     *
     * @return possible object is
     * {@link ActiveCurrencyAndAmount }
     */
    public ActiveCurrencyAndAmount getInvstNetAmt() {
        return invstNetAmt;
    }

    /**
     * Define o valor da propriedade invstNetAmt.
     *
     * @param value allowed object is
     *              {@link ActiveCurrencyAndAmount }
     */
    public void setInvstNetAmt(ActiveCurrencyAndAmount value) {
        this.invstNetAmt = value;
    }

    /**
     * Obtém o valor da propriedade lclTaxAmt.
     *
     * @return possible object is
     * {@link ActiveCurrencyAndAmount }
     */
    public ActiveCurrencyAndAmount getLclTaxAmt() {
        return lclTaxAmt;
    }

    /**
     * Define o valor da propriedade lclTaxAmt.
     *
     * @param value allowed object is
     *              {@link ActiveCurrencyAndAmount }
     */
    public void setLclTaxAmt(ActiveCurrencyAndAmount value) {
        this.lclTaxAmt = value;
    }

    /**
     * Gets the value of the nav property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nav property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNAV().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FinancialInstrumentQuantity1 }
     */
    public List<FinancialInstrumentQuantity1> getNAV() {
        if (nav == null) {
            nav = new ArrayList<FinancialInstrumentQuantity1>();
        }
        return this.nav;
    }

    /**
     * Obtém o valor da propriedade navDtTm.
     *
     * @return possible object is
     * {@link DateAndDateTimeChoice }
     */
    public DateAndDateTimeChoice getNAVDtTm() {
        return navDtTm;
    }

    /**
     * Define o valor da propriedade navDtTm.
     *
     * @param value allowed object is
     *              {@link DateAndDateTimeChoice }
     */
    public void setNAVDtTm(DateAndDateTimeChoice value) {
        this.navDtTm = value;
    }

    /**
     * Obtém o valor da propriedade nxtValtnDtTm.
     *
     * @return possible object is
     * {@link DateAndDateTimeChoice }
     */
    public DateAndDateTimeChoice getNxtValtnDtTm() {
        return nxtValtnDtTm;
    }

    /**
     * Define o valor da propriedade nxtValtnDtTm.
     *
     * @param value allowed object is
     *              {@link DateAndDateTimeChoice }
     */
    public void setNxtValtnDtTm(DateAndDateTimeChoice value) {
        this.nxtValtnDtTm = value;
    }

    /**
     * Obtém o valor da propriedade prfrmncDtls.
     *
     * @return possible object is
     * {@link PerformanceFactors1 }
     */
    public PerformanceFactors1 getPrfrmncDtls() {
        return prfrmncDtls;
    }

    /**
     * Define o valor da propriedade prfrmncDtls.
     *
     * @param value allowed object is
     *              {@link PerformanceFactors1 }
     */
    public void setPrfrmncDtls(PerformanceFactors1 value) {
        this.prfrmncDtls = value;
    }

    /**
     * Gets the value of the pricDtls property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pricDtls property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPricDtls().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UnitPrice15 }
     */
    public List<UnitPrice15> getPricDtls() {
        if (pricDtls == null) {
            pricDtls = new ArrayList<UnitPrice15>();
        }
        return this.pricDtls;
    }

    /**
     * Obtém o valor da propriedade prvsValtnDtTm.
     *
     * @return possible object is
     * {@link DateAndDateTimeChoice }
     */
    public DateAndDateTimeChoice getPrvsValtnDtTm() {
        return prvsValtnDtTm;
    }

    /**
     * Define o valor da propriedade prvsValtnDtTm.
     *
     * @param value allowed object is
     *              {@link DateAndDateTimeChoice }
     */
    public void setPrvsValtnDtTm(DateAndDateTimeChoice value) {
        this.prvsValtnDtTm = value;
    }

    /**
     * Obtém o valor da propriedade redNetAmt.
     *
     * @return possible object is
     * {@link ActiveCurrencyAndAmount }
     */
    public ActiveCurrencyAndAmount getRedNetAmt() {
        return redNetAmt;
    }

    /**
     * Define o valor da propriedade redNetAmt.
     *
     * @param value allowed object is
     *              {@link ActiveCurrencyAndAmount }
     */
    public void setRedNetAmt(ActiveCurrencyAndAmount value) {
        this.redNetAmt = value;
    }

    /**
     * Gets the value of the ttlNAV property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ttlNAV property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTtlNAV().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActiveOrHistoricCurrencyAndAmount }
     */
    public List<ActiveOrHistoricCurrencyAndAmount> getTtlNAV() {
        if (ttlNAV == null) {
            ttlNAV = new ArrayList<ActiveOrHistoricCurrencyAndAmount>();
        }
        return this.ttlNAV;
    }

    /**
     * Obtém o valor da propriedade ttlUnitsNb.
     *
     * @return possible object is
     * {@link FinancialInstrumentQuantity1 }
     */
    public FinancialInstrumentQuantity1 getTtlUnitsNb() {
        return ttlUnitsNb;
    }

    /**
     * Define o valor da propriedade ttlUnitsNb.
     *
     * @param value allowed object is
     *              {@link FinancialInstrumentQuantity1 }
     */
    public void setTtlUnitsNb(FinancialInstrumentQuantity1 value) {
        this.ttlUnitsNb = value;
    }

    /**
     * Obtém o valor da propriedade valtnDtTm.
     *
     * @return possible object is
     * {@link DateAndDateTimeChoice }
     */
    public DateAndDateTimeChoice getValtnDtTm() {
        return valtnDtTm;
    }

    /**
     * Define o valor da propriedade valtnDtTm.
     *
     * @param value allowed object is
     *              {@link DateAndDateTimeChoice }
     */
    public void setValtnDtTm(DateAndDateTimeChoice value) {
        this.valtnDtTm = value;
    }

    /**
     * Obtém o valor da propriedade valtnFrqcy.
     *
     * @return possible object is
     * {@link EventFrequency1Code }
     */
    public EventFrequency1Code getValtnFrqcy() {
        return valtnFrqcy;
    }

    /**
     * Define o valor da propriedade valtnFrqcy.
     *
     * @param value allowed object is
     *              {@link EventFrequency1Code }
     */
    public void setValtnFrqcy(EventFrequency1Code value) {
        this.valtnFrqcy = value;
    }

    /**
     * Gets the value of the valtnSttstcs property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valtnSttstcs property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValtnSttstcs().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValuationStatistics3 }
     */
    public List<ValuationStatistics3> getValtnSttstcs() {
        if (valtnSttstcs == null) {
            valtnSttstcs = new ArrayList<ValuationStatistics3>();
        }
        return this.valtnSttstcs;
    }

    /**
     * Obtém o valor da propriedade valtnTp.
     *
     * @return possible object is
     * {@link ValuationTiming1Code }
     */
    public ValuationTiming1Code getValtnTp() {
        return valtnTp;
    }

    /**
     * Define o valor da propriedade valtnTp.
     *
     * @param value allowed object is
     *              {@link ValuationTiming1Code }
     */
    public void setValtnTp(ValuationTiming1Code value) {
        this.valtnTp = value;
    }

    /**
     * Obtém o valor da propriedade offclValtnInd.
     */
    public boolean isOffclValtnInd() {
        return offclValtnInd;
    }

    /**
     * Define o valor da propriedade offclValtnInd.
     */
    public void setOffclValtnInd(boolean value) {
        this.offclValtnInd = value;
    }

    /**
     * Obtém o valor da propriedade sspdInd.
     */
    public boolean isSspdInd() {
        return sspdInd;
    }

    /**
     * Define o valor da propriedade sspdInd.
     */
    public void setSspdInd(boolean value) {
        this.sspdInd = value;
    }

}
