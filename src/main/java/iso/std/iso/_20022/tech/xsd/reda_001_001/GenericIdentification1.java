package iso.std.iso._20022.tech.xsd.reda_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de GenericIdentification1 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="GenericIdentification1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}Max35Text"/&gt;
 *         &lt;element name="SchmeNm" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}Max35Text" minOccurs="0"/&gt;
 *         &lt;element name="Issr" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}Max35Text" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericIdentification1", propOrder = {"id", "schmeNm", "issr"})
public class GenericIdentification1 {

    @XmlElement(name = "Id", required = true) protected String id;
    @XmlElement(name = "SchmeNm") protected String schmeNm;
    @XmlElement(name = "Issr") protected String issr;

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link String }
     */
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade issr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIssr() {
        return issr;
    }

    /**
     * Define o valor da propriedade issr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIssr(String value) {
        this.issr = value;
    }

    /**
     * Obtém o valor da propriedade schmeNm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSchmeNm() {
        return schmeNm;
    }

    /**
     * Define o valor da propriedade schmeNm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSchmeNm(String value) {
        this.schmeNm = value;
    }

}
