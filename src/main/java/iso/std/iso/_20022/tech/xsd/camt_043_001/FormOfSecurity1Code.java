package iso.std.iso._20022.tech.xsd.camt_043_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de FormOfSecurity1Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="FormOfSecurity1Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BEAR"/>
 *     &lt;enumeration value="REGD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "FormOfSecurity1Code")
@XmlEnum
public enum FormOfSecurity1Code {

    BEAR,
    REGD;

    public static FormOfSecurity1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
