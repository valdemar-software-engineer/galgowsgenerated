package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de SafekeepingPlaceTypeAndAnyBICIdentifier1 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="SafekeepingPlaceTypeAndAnyBICIdentifier1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SfkpgPlcTp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SafekeepingPlace1Code"/&gt;
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}AnyBICIdentifier"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SafekeepingPlaceTypeAndAnyBICIdentifier1", propOrder = {"sfkpgPlcTp", "id"})
public class SafekeepingPlaceTypeAndAnyBICIdentifier1 {

    @XmlElement(name = "SfkpgPlcTp", required = true) @XmlSchemaType(name = "string") protected SafekeepingPlace1Code
        sfkpgPlcTp;
    @XmlElement(name = "Id", required = true) protected String id;

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link String }
     */
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade sfkpgPlcTp.
     *
     * @return possible object is
     * {@link SafekeepingPlace1Code }
     */
    public SafekeepingPlace1Code getSfkpgPlcTp() {
        return sfkpgPlcTp;
    }

    /**
     * Define o valor da propriedade sfkpgPlcTp.
     *
     * @param value allowed object is
     *              {@link SafekeepingPlace1Code }
     */
    public void setSfkpgPlcTp(SafekeepingPlace1Code value) {
        this.sfkpgPlcTp = value;
    }

}
