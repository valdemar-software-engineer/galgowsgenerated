package iso.std.iso._20022.tech.xsd.camt_043_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de CommissionType6Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="CommissionType6Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FEND"/>
 *     &lt;enumeration value="BEND"/>
 *     &lt;enumeration value="CDPL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "CommissionType6Code")
@XmlEnum
public enum CommissionType6Code {

    FEND,
    BEND,
    CDPL;

    public static CommissionType6Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
