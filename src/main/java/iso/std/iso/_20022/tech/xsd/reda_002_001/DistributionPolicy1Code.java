package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de DistributionPolicy1Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="DistributionPolicy1Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="DIST"/&gt;
 *     &lt;enumeration value="ACCU"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "DistributionPolicy1Code")
@XmlEnum
public enum DistributionPolicy1Code {

    DIST,
    ACCU;

    public static DistributionPolicy1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
