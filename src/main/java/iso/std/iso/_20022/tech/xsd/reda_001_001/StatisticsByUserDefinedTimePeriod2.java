package iso.std.iso._20022.tech.xsd.reda_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Classe Java de StatisticsByUserDefinedTimePeriod2 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="StatisticsByUserDefinedTimePeriod2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Prd" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}DateOrDateTimePeriodChoice"/&gt;
 *         &lt;element name="HghstPricVal" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PriceValue5" minOccurs="0"/&gt;
 *         &lt;element name="LwstPricVal" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PriceValue5" minOccurs="0"/&gt;
 *         &lt;element name="PricChng" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PriceValueChange1" minOccurs="0"/&gt;
 *         &lt;element name="Yld" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PercentageRate" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatisticsByUserDefinedTimePeriod2",
         propOrder = {"prd", "hghstPricVal", "lwstPricVal", "pricChng", "yld"})
public class StatisticsByUserDefinedTimePeriod2 {

    @XmlElement(name = "Prd", required = true) protected DateOrDateTimePeriodChoice prd;
    @XmlElement(name = "HghstPricVal") protected PriceValue5 hghstPricVal;
    @XmlElement(name = "LwstPricVal") protected PriceValue5 lwstPricVal;
    @XmlElement(name = "PricChng") protected PriceValueChange1 pricChng;
    @XmlElement(name = "Yld") protected BigDecimal yld;

    /**
     * Obtém o valor da propriedade hghstPricVal.
     *
     * @return possible object is
     * {@link PriceValue5 }
     */
    public PriceValue5 getHghstPricVal() {
        return hghstPricVal;
    }

    /**
     * Define o valor da propriedade hghstPricVal.
     *
     * @param value allowed object is
     *              {@link PriceValue5 }
     */
    public void setHghstPricVal(PriceValue5 value) {
        this.hghstPricVal = value;
    }

    /**
     * Obtém o valor da propriedade lwstPricVal.
     *
     * @return possible object is
     * {@link PriceValue5 }
     */
    public PriceValue5 getLwstPricVal() {
        return lwstPricVal;
    }

    /**
     * Define o valor da propriedade lwstPricVal.
     *
     * @param value allowed object is
     *              {@link PriceValue5 }
     */
    public void setLwstPricVal(PriceValue5 value) {
        this.lwstPricVal = value;
    }

    /**
     * Obtém o valor da propriedade prd.
     *
     * @return possible object is
     * {@link DateOrDateTimePeriodChoice }
     */
    public DateOrDateTimePeriodChoice getPrd() {
        return prd;
    }

    /**
     * Define o valor da propriedade prd.
     *
     * @param value allowed object is
     *              {@link DateOrDateTimePeriodChoice }
     */
    public void setPrd(DateOrDateTimePeriodChoice value) {
        this.prd = value;
    }

    /**
     * Obtém o valor da propriedade pricChng.
     *
     * @return possible object is
     * {@link PriceValueChange1 }
     */
    public PriceValueChange1 getPricChng() {
        return pricChng;
    }

    /**
     * Define o valor da propriedade pricChng.
     *
     * @param value allowed object is
     *              {@link PriceValueChange1 }
     */
    public void setPricChng(PriceValueChange1 value) {
        this.pricChng = value;
    }

    /**
     * Obtém o valor da propriedade yld.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getYld() {
        return yld;
    }

    /**
     * Define o valor da propriedade yld.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setYld(BigDecimal value) {
        this.yld = value;
    }

}
