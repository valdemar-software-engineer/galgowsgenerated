package iso.std.iso._20022.tech.xsd.camt_045_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de InvestmentFundTransactionOutType1Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="InvestmentFundTransactionOutType1Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="REDM"/>
 *     &lt;enumeration value="SWIO"/>
 *     &lt;enumeration value="INSP"/>
 *     &lt;enumeration value="CROO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "InvestmentFundTransactionOutType1Code")
@XmlEnum
public enum InvestmentFundTransactionOutType1Code {

    REDM,
    SWIO,
    INSP,
    CROO;

    public static InvestmentFundTransactionOutType1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
