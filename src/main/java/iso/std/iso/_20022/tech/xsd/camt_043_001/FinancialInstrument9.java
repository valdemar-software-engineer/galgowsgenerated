package iso.std.iso._20022.tech.xsd.camt_043_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de FinancialInstrument9 complex type.
 *
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 *
 * <pre>
 * &lt;complexType name="FinancialInstrument9"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}SecurityIdentification3Choice"/&gt;
 *         &lt;element name="Nm" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Max350Text" minOccurs="0"/&gt;
 *         &lt;element name="SplmtryId" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Max35Text" minOccurs="0"/&gt;
 *         &lt;element name="ReqdNAVCcy" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}ActiveOrHistoricCurrencyCode" minOccurs="0"/&gt;
 *         &lt;element name="ClssTp" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Max35Text" minOccurs="0"/&gt;
 *         &lt;element name="SctiesForm" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}FormOfSecurity1Code" minOccurs="0"/&gt;
 *         &lt;element name="DstrbtnPlcy" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}DistributionPolicy1Code" minOccurs="0"/&gt;
 *         &lt;element name="DualFndInd" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}YesNoIndicator"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialInstrument9", propOrder = {
    "id",
    "nm",
    "splmtryId",
    "reqdNAVCcy",
    "clssTp",
    "sctiesForm",
    "dstrbtnPlcy",
    "dualFndInd"
})
public class FinancialInstrument9 {

    @XmlElement(name = "Id", required = true)
    protected SecurityIdentification3Choice id;
    @XmlElement(name = "Nm")
    protected String nm;
    @XmlElement(name = "SplmtryId")
    protected String splmtryId;
    @XmlElement(name = "ReqdNAVCcy")
    protected String reqdNAVCcy;
    @XmlElement(name = "ClssTp")
    protected String clssTp;
    @XmlElement(name = "SctiesForm")
    @XmlSchemaType(name = "string")
    protected FormOfSecurity1Code sctiesForm;
    @XmlElement(name = "DstrbtnPlcy")
    @XmlSchemaType(name = "string")
    protected DistributionPolicy1Code dstrbtnPlcy;
    @XmlElement(name = "DualFndInd")
    protected boolean dualFndInd;

    /**
     * Obtém o valor da propriedade clssTp.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getClssTp() {
        return clssTp;
    }

    /**
     * Define o valor da propriedade clssTp.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setClssTp(String value) {
        this.clssTp = value;
    }

    /**
     * Obtém o valor da propriedade dstrbtnPlcy.
     *
     * @return
     *     possible object is
     *     {@link DistributionPolicy1Code }
     *
     */
    public DistributionPolicy1Code getDstrbtnPlcy() {
        return dstrbtnPlcy;
    }

    /**
     * Define o valor da propriedade dstrbtnPlcy.
     *
     * @param value
     *     allowed object is
     *     {@link DistributionPolicy1Code }
     *
     */
    public void setDstrbtnPlcy(DistributionPolicy1Code value) {
        this.dstrbtnPlcy = value;
    }

    /**
     * Obtém o valor da propriedade id.
     *
     * @return
     *     possible object is
     *     {@link SecurityIdentification3Choice }
     *
     */
    public SecurityIdentification3Choice getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value
     *     allowed object is
     *     {@link SecurityIdentification3Choice }
     *
     */
    public void setId(SecurityIdentification3Choice value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade nm.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNm() {
        return nm;
    }

    /**
     * Define o valor da propriedade nm.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNm(String value) {
        this.nm = value;
    }

    /**
     * Obtém o valor da propriedade reqdNAVCcy.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getReqdNAVCcy() {
        return reqdNAVCcy;
    }

    /**
     * Define o valor da propriedade reqdNAVCcy.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setReqdNAVCcy(String value) {
        this.reqdNAVCcy = value;
    }

    /**
     * Obtém o valor da propriedade sctiesForm.
     *
     * @return
     *     possible object is
     *     {@link FormOfSecurity1Code }
     *
     */
    public FormOfSecurity1Code getSctiesForm() {
        return sctiesForm;
    }

    /**
     * Define o valor da propriedade sctiesForm.
     *
     * @param value
     *     allowed object is
     *     {@link FormOfSecurity1Code }
     *
     */
    public void setSctiesForm(FormOfSecurity1Code value) {
        this.sctiesForm = value;
    }

    /**
     * Obtém o valor da propriedade splmtryId.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSplmtryId() {
        return splmtryId;
    }

    /**
     * Define o valor da propriedade splmtryId.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSplmtryId(String value) {
        this.splmtryId = value;
    }

    /**
     * Obtém o valor da propriedade dualFndInd.
     *
     */
    public boolean isDualFndInd() {
        return dualFndInd;
    }

    /**
     * Define o valor da propriedade dualFndInd.
     *
     */
    public void setDualFndInd(boolean value) {
        this.dualFndInd = value;
    }

}
