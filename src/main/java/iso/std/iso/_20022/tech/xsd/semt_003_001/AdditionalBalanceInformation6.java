package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de AdditionalBalanceInformation6 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="AdditionalBalanceInformation6"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SubBalTp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SubBalanceType6Choice"/&gt;
 *         &lt;element name="Qty" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SubBalanceQuantity3Choice"/&gt;
 *         &lt;element name="SubBalAddtlDtls" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max140Text" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalBalanceInformation6", propOrder = {"subBalTp", "qty", "subBalAddtlDtls"})
public class AdditionalBalanceInformation6 {

    @XmlElement(name = "SubBalTp", required = true) protected SubBalanceType6Choice subBalTp;
    @XmlElement(name = "Qty", required = true) protected SubBalanceQuantity3Choice qty;
    @XmlElement(name = "SubBalAddtlDtls") protected String subBalAddtlDtls;

    /**
     * Obtém o valor da propriedade qty.
     *
     * @return possible object is
     * {@link SubBalanceQuantity3Choice }
     */
    public SubBalanceQuantity3Choice getQty() {
        return qty;
    }

    /**
     * Define o valor da propriedade qty.
     *
     * @param value allowed object is
     *              {@link SubBalanceQuantity3Choice }
     */
    public void setQty(SubBalanceQuantity3Choice value) {
        this.qty = value;
    }

    /**
     * Obtém o valor da propriedade subBalAddtlDtls.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSubBalAddtlDtls() {
        return subBalAddtlDtls;
    }

    /**
     * Define o valor da propriedade subBalAddtlDtls.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSubBalAddtlDtls(String value) {
        this.subBalAddtlDtls = value;
    }

    /**
     * Obtém o valor da propriedade subBalTp.
     *
     * @return possible object is
     * {@link SubBalanceType6Choice }
     */
    public SubBalanceType6Choice getSubBalTp() {
        return subBalTp;
    }

    /**
     * Define o valor da propriedade subBalTp.
     *
     * @param value allowed object is
     *              {@link SubBalanceType6Choice }
     */
    public void setSubBalTp(SubBalanceType6Choice value) {
        this.subBalTp = value;
    }

}
