package iso.std.iso._20022.tech.xsd.head_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de PartyIdentification42 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PartyIdentification42"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Nm" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}Max140Text" minOccurs="0"/&gt;
 *         &lt;element name="PstlAdr" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}PostalAddress6" minOccurs="0"/&gt;
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}Party10Choice" minOccurs="0"/&gt;
 *         &lt;element name="CtryOfRes" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}CountryCode" minOccurs="0"/&gt;
 *         &lt;element name="CtctDtls" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}ContactDetails2" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyIdentification42", propOrder = {"nm", "pstlAdr", "id", "ctryOfRes", "ctctDtls"})
public class PartyIdentification42 {

    @XmlElement(name = "Nm") protected String nm;
    @XmlElement(name = "PstlAdr") protected PostalAddress6 pstlAdr;
    @XmlElement(name = "Id") protected Party10Choice id;
    @XmlElement(name = "CtryOfRes") protected String ctryOfRes;
    @XmlElement(name = "CtctDtls") protected ContactDetails2 ctctDtls;

    /**
     * Obtém o valor da propriedade ctctDtls.
     *
     * @return possible object is
     * {@link ContactDetails2 }
     */
    public ContactDetails2 getCtctDtls() {
        return ctctDtls;
    }

    /**
     * Define o valor da propriedade ctctDtls.
     *
     * @param value allowed object is
     *              {@link ContactDetails2 }
     */
    public void setCtctDtls(ContactDetails2 value) {
        this.ctctDtls = value;
    }

    /**
     * Obtém o valor da propriedade ctryOfRes.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCtryOfRes() {
        return ctryOfRes;
    }

    /**
     * Define o valor da propriedade ctryOfRes.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCtryOfRes(String value) {
        this.ctryOfRes = value;
    }

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link Party10Choice }
     */
    public Party10Choice getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link Party10Choice }
     */
    public void setId(Party10Choice value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade nm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNm() {
        return nm;
    }

    /**
     * Define o valor da propriedade nm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNm(String value) {
        this.nm = value;
    }

    /**
     * Obtém o valor da propriedade pstlAdr.
     *
     * @return possible object is
     * {@link PostalAddress6 }
     */
    public PostalAddress6 getPstlAdr() {
        return pstlAdr;
    }

    /**
     * Define o valor da propriedade pstlAdr.
     *
     * @param value allowed object is
     *              {@link PostalAddress6 }
     */
    public void setPstlAdr(PostalAddress6 value) {
        this.pstlAdr = value;
    }

}
