package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de TaxType12Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="TaxType12Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="INPO"/&gt;
 *     &lt;enumeration value="EUTR"/&gt;
 *     &lt;enumeration value="AKT1"/&gt;
 *     &lt;enumeration value="AKT2"/&gt;
 *     &lt;enumeration value="ZWIS"/&gt;
 *     &lt;enumeration value="MIET"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "TaxType12Code")
@XmlEnum
public enum TaxType12Code {

    INPO("INPO"),
    EUTR("EUTR"),
    @XmlEnumValue("AKT1")
    AKT_1("AKT1"),
    @XmlEnumValue("AKT2")
    AKT_2("AKT2"),
    ZWIS("ZWIS"),
    MIET("MIET");
    private final String value;

    TaxType12Code(String v) {
        value = v;
    }

    public static TaxType12Code fromValue(String v) {
        for (TaxType12Code c : TaxType12Code.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public String value() {
        return value;
    }

}
