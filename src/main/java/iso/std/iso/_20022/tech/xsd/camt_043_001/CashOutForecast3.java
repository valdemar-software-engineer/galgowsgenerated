package iso.std.iso._20022.tech.xsd.camt_043_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de CashOutForecast3 complex type.
 *
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 *
 * <pre>
 * &lt;complexType name="CashOutForecast3"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CshSttlmDt" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}ISODate"/&gt;
 *         &lt;element name="SubTtlAmt" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}ActiveOrHistoricCurrencyAndAmount" minOccurs="0"/&gt;
 *         &lt;element name="SubTtlUnitsNb" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}FinancialInstrumentQuantity1" minOccurs="0"/&gt;
 *         &lt;element name="XcptnlCshFlowInd" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}YesNoIndicator" minOccurs="0"/&gt;
 *         &lt;element name="CshOutBrkdwnDtls" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}FundCashOutBreakdown2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CashOutForecast3", propOrder = {
    "cshSttlmDt",
    "subTtlAmt",
    "subTtlUnitsNb",
    "xcptnlCshFlowInd",
    "cshOutBrkdwnDtls"
})
public class CashOutForecast3 {

    @XmlElement(name = "CshSttlmDt", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar cshSttlmDt;
    @XmlElement(name = "SubTtlAmt")
    protected ActiveOrHistoricCurrencyAndAmount subTtlAmt;
    @XmlElement(name = "SubTtlUnitsNb")
    protected FinancialInstrumentQuantity1 subTtlUnitsNb;
    @XmlElement(name = "XcptnlCshFlowInd")
    protected Boolean xcptnlCshFlowInd;
    @XmlElement(name = "CshOutBrkdwnDtls")
    protected List<FundCashOutBreakdown2> cshOutBrkdwnDtls;

    /**
     * Gets the value of the cshOutBrkdwnDtls property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cshOutBrkdwnDtls property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCshOutBrkdwnDtls().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FundCashOutBreakdown2 }
     *
     *
     */
    public List<FundCashOutBreakdown2> getCshOutBrkdwnDtls() {
        if (cshOutBrkdwnDtls == null) {
            cshOutBrkdwnDtls = new ArrayList<FundCashOutBreakdown2>();
        }
        return this.cshOutBrkdwnDtls;
    }

    /**
     * Obtém o valor da propriedade cshSttlmDt.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getCshSttlmDt() {
        return cshSttlmDt;
    }

    /**
     * Define o valor da propriedade cshSttlmDt.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setCshSttlmDt(XMLGregorianCalendar value) {
        this.cshSttlmDt = value;
    }

    /**
     * Obtém o valor da propriedade subTtlAmt.
     *
     * @return
     *     possible object is
     *     {@link ActiveOrHistoricCurrencyAndAmount }
     *
     */
    public ActiveOrHistoricCurrencyAndAmount getSubTtlAmt() {
        return subTtlAmt;
    }

    /**
     * Define o valor da propriedade subTtlAmt.
     *
     * @param value
     *     allowed object is
     *     {@link ActiveOrHistoricCurrencyAndAmount }
     *
     */
    public void setSubTtlAmt(ActiveOrHistoricCurrencyAndAmount value) {
        this.subTtlAmt = value;
    }

    /**
     * Obtém o valor da propriedade subTtlUnitsNb.
     *
     * @return
     *     possible object is
     *     {@link FinancialInstrumentQuantity1 }
     *
     */
    public FinancialInstrumentQuantity1 getSubTtlUnitsNb() {
        return subTtlUnitsNb;
    }

    /**
     * Define o valor da propriedade subTtlUnitsNb.
     *
     * @param value
     *     allowed object is
     *     {@link FinancialInstrumentQuantity1 }
     *
     */
    public void setSubTtlUnitsNb(FinancialInstrumentQuantity1 value) {
        this.subTtlUnitsNb = value;
    }

    /**
     * Obtém o valor da propriedade xcptnlCshFlowInd.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public Boolean isXcptnlCshFlowInd() {
        return xcptnlCshFlowInd;
    }

    /**
     * Define o valor da propriedade xcptnlCshFlowInd.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setXcptnlCshFlowInd(Boolean value) {
        this.xcptnlCshFlowInd = value;
    }

}
