package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de QuantityAndAvailability1 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="QuantityAndAvailability1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Qty" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}FinancialInstrumentQuantity1Choice"/&gt;
 *         &lt;element name="AvlbtyInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuantityAndAvailability1", propOrder = {"qty", "avlbtyInd"})
public class QuantityAndAvailability1 {

    @XmlElement(name = "Qty", required = true) protected FinancialInstrumentQuantity1Choice qty;
    @XmlElement(name = "AvlbtyInd") protected boolean avlbtyInd;

    /**
     * Obtém o valor da propriedade qty.
     *
     * @return possible object is
     * {@link FinancialInstrumentQuantity1Choice }
     */
    public FinancialInstrumentQuantity1Choice getQty() {
        return qty;
    }

    /**
     * Define o valor da propriedade qty.
     *
     * @param value allowed object is
     *              {@link FinancialInstrumentQuantity1Choice }
     */
    public void setQty(FinancialInstrumentQuantity1Choice value) {
        this.qty = value;
    }

    /**
     * Obtém o valor da propriedade avlbtyInd.
     */
    public boolean isAvlbtyInd() {
        return avlbtyInd;
    }

    /**
     * Define o valor da propriedade avlbtyInd.
     */
    public void setAvlbtyInd(boolean value) {
        this.avlbtyInd = value;
    }

}
