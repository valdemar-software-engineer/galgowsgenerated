package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de BalanceAmounts2 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="BalanceAmounts2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="HldgVal" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}AmountAndDirection6"/&gt;
 *         &lt;element name="BookVal" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}AmountAndDirection6" minOccurs="0"/&gt;
 *         &lt;element name="UrlsdGnLoss" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}AmountAndDirection6" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BalanceAmounts2", propOrder = {"hldgVal", "bookVal", "urlsdGnLoss"})
public class BalanceAmounts2 {

    @XmlElement(name = "HldgVal", required = true) protected AmountAndDirection6 hldgVal;
    @XmlElement(name = "BookVal") protected AmountAndDirection6 bookVal;
    @XmlElement(name = "UrlsdGnLoss") protected AmountAndDirection6 urlsdGnLoss;

    /**
     * Obtém o valor da propriedade bookVal.
     *
     * @return possible object is
     * {@link AmountAndDirection6 }
     */
    public AmountAndDirection6 getBookVal() {
        return bookVal;
    }

    /**
     * Define o valor da propriedade bookVal.
     *
     * @param value allowed object is
     *              {@link AmountAndDirection6 }
     */
    public void setBookVal(AmountAndDirection6 value) {
        this.bookVal = value;
    }

    /**
     * Obtém o valor da propriedade hldgVal.
     *
     * @return possible object is
     * {@link AmountAndDirection6 }
     */
    public AmountAndDirection6 getHldgVal() {
        return hldgVal;
    }

    /**
     * Define o valor da propriedade hldgVal.
     *
     * @param value allowed object is
     *              {@link AmountAndDirection6 }
     */
    public void setHldgVal(AmountAndDirection6 value) {
        this.hldgVal = value;
    }

    /**
     * Obtém o valor da propriedade urlsdGnLoss.
     *
     * @return possible object is
     * {@link AmountAndDirection6 }
     */
    public AmountAndDirection6 getUrlsdGnLoss() {
        return urlsdGnLoss;
    }

    /**
     * Define o valor da propriedade urlsdGnLoss.
     *
     * @param value allowed object is
     *              {@link AmountAndDirection6 }
     */
    public void setUrlsdGnLoss(AmountAndDirection6 value) {
        this.urlsdGnLoss = value;
    }

}
