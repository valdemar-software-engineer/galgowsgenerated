package iso.std.iso._20022.tech.xsd.camt_045_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de CashInForecast3 complex type.
 *
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 *
 * <pre>
 * &lt;complexType name="CashInForecast3"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CshSttlmDt" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}ISODate"/&gt;
 *         &lt;element name="SubTtlAmt" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}ActiveOrHistoricCurrencyAndAmount" minOccurs="0"/&gt;
 *         &lt;element name="SubTtlUnitsNb" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}FinancialInstrumentQuantity1" minOccurs="0"/&gt;
 *         &lt;element name="XcptnlCshFlowInd" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}YesNoIndicator" minOccurs="0"/&gt;
 *         &lt;element name="CshInBrkdwnDtls" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}FundCashInBreakdown2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CashInForecast3", propOrder = {
    "cshSttlmDt",
    "subTtlAmt",
    "subTtlUnitsNb",
    "xcptnlCshFlowInd",
    "cshInBrkdwnDtls"
})
public class CashInForecast3 {

    @XmlElement(name = "CshSttlmDt", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar cshSttlmDt;
    @XmlElement(name = "SubTtlAmt")
    protected ActiveOrHistoricCurrencyAndAmount subTtlAmt;
    @XmlElement(name = "SubTtlUnitsNb")
    protected FinancialInstrumentQuantity1 subTtlUnitsNb;
    @XmlElement(name = "XcptnlCshFlowInd")
    protected Boolean xcptnlCshFlowInd;
    @XmlElement(name = "CshInBrkdwnDtls")
    protected List<FundCashInBreakdown2> cshInBrkdwnDtls;

    /**
     * Gets the value of the cshInBrkdwnDtls property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cshInBrkdwnDtls property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCshInBrkdwnDtls().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FundCashInBreakdown2 }
     *
     *
     */
    public List<FundCashInBreakdown2> getCshInBrkdwnDtls() {
        if (cshInBrkdwnDtls == null) {
            cshInBrkdwnDtls = new ArrayList<FundCashInBreakdown2>();
        }
        return this.cshInBrkdwnDtls;
    }

    /**
     * Obtém o valor da propriedade cshSttlmDt.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getCshSttlmDt() {
        return cshSttlmDt;
    }

    /**
     * Define o valor da propriedade cshSttlmDt.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setCshSttlmDt(XMLGregorianCalendar value) {
        this.cshSttlmDt = value;
    }

    /**
     * Obtém o valor da propriedade subTtlAmt.
     *
     * @return
     *     possible object is
     *     {@link ActiveOrHistoricCurrencyAndAmount }
     *
     */
    public ActiveOrHistoricCurrencyAndAmount getSubTtlAmt() {
        return subTtlAmt;
    }

    /**
     * Define o valor da propriedade subTtlAmt.
     *
     * @param value
     *     allowed object is
     *     {@link ActiveOrHistoricCurrencyAndAmount }
     *
     */
    public void setSubTtlAmt(ActiveOrHistoricCurrencyAndAmount value) {
        this.subTtlAmt = value;
    }

    /**
     * Obtém o valor da propriedade subTtlUnitsNb.
     *
     * @return
     *     possible object is
     *     {@link FinancialInstrumentQuantity1 }
     *
     */
    public FinancialInstrumentQuantity1 getSubTtlUnitsNb() {
        return subTtlUnitsNb;
    }

    /**
     * Define o valor da propriedade subTtlUnitsNb.
     *
     * @param value
     *     allowed object is
     *     {@link FinancialInstrumentQuantity1 }
     *
     */
    public void setSubTtlUnitsNb(FinancialInstrumentQuantity1 value) {
        this.subTtlUnitsNb = value;
    }

    /**
     * Obtém o valor da propriedade xcptnlCshFlowInd.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    public Boolean isXcptnlCshFlowInd() {
        return xcptnlCshFlowInd;
    }

    /**
     * Define o valor da propriedade xcptnlCshFlowInd.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    public void setXcptnlCshFlowInd(Boolean value) {
        this.xcptnlCshFlowInd = value;
    }

}
