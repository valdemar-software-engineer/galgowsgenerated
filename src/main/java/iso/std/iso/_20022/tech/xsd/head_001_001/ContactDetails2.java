package iso.std.iso._20022.tech.xsd.head_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ContactDetails2 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="ContactDetails2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="NmPrfx" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}NamePrefix1Code" minOccurs="0"/&gt;
 *         &lt;element name="Nm" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}Max140Text" minOccurs="0"/&gt;
 *         &lt;element name="PhneNb" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}PhoneNumber" minOccurs="0"/&gt;
 *         &lt;element name="MobNb" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}PhoneNumber" minOccurs="0"/&gt;
 *         &lt;element name="FaxNb" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}PhoneNumber" minOccurs="0"/&gt;
 *         &lt;element name="EmailAdr" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}Max2048Text" minOccurs="0"/&gt;
 *         &lt;element name="Othr" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}Max35Text" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactDetails2", propOrder = {"nmPrfx", "nm", "phneNb", "mobNb", "faxNb", "emailAdr", "othr"})
public class ContactDetails2 {

    @XmlElement(name = "NmPrfx") @XmlSchemaType(name = "string") protected NamePrefix1Code nmPrfx;
    @XmlElement(name = "Nm") protected String nm;
    @XmlElement(name = "PhneNb") protected String phneNb;
    @XmlElement(name = "MobNb") protected String mobNb;
    @XmlElement(name = "FaxNb") protected String faxNb;
    @XmlElement(name = "EmailAdr") protected String emailAdr;
    @XmlElement(name = "Othr") protected String othr;

    /**
     * Obtém o valor da propriedade emailAdr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getEmailAdr() {
        return emailAdr;
    }

    /**
     * Define o valor da propriedade emailAdr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setEmailAdr(String value) {
        this.emailAdr = value;
    }

    /**
     * Obtém o valor da propriedade faxNb.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFaxNb() {
        return faxNb;
    }

    /**
     * Define o valor da propriedade faxNb.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFaxNb(String value) {
        this.faxNb = value;
    }

    /**
     * Obtém o valor da propriedade mobNb.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMobNb() {
        return mobNb;
    }

    /**
     * Define o valor da propriedade mobNb.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMobNb(String value) {
        this.mobNb = value;
    }

    /**
     * Obtém o valor da propriedade nm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNm() {
        return nm;
    }

    /**
     * Define o valor da propriedade nm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNm(String value) {
        this.nm = value;
    }

    /**
     * Obtém o valor da propriedade nmPrfx.
     *
     * @return possible object is
     * {@link NamePrefix1Code }
     */
    public NamePrefix1Code getNmPrfx() {
        return nmPrfx;
    }

    /**
     * Define o valor da propriedade nmPrfx.
     *
     * @param value allowed object is
     *              {@link NamePrefix1Code }
     */
    public void setNmPrfx(NamePrefix1Code value) {
        this.nmPrfx = value;
    }

    /**
     * Obtém o valor da propriedade othr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOthr() {
        return othr;
    }

    /**
     * Define o valor da propriedade othr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setOthr(String value) {
        this.othr = value;
    }

    /**
     * Obtém o valor da propriedade phneNb.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPhneNb() {
        return phneNb;
    }

    /**
     * Define o valor da propriedade phneNb.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPhneNb(String value) {
        this.phneNb = value;
    }

}
