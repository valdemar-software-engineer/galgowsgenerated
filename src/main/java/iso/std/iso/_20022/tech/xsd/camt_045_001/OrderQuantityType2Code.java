package iso.std.iso._20022.tech.xsd.camt_045_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de OrderQuantityType2Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="OrderQuantityType2Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UNIT"/>
 *     &lt;enumeration value="CASH"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "OrderQuantityType2Code")
@XmlEnum
public enum OrderQuantityType2Code {

    UNIT,
    CASH;

    public static OrderQuantityType2Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
