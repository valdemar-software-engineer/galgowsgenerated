package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Pagination complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="Pagination"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PgNb" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Max5NumericText"/&gt;
 *         &lt;element name="LastPgInd" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}YesNoIndicator"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Pagination", propOrder = {"pgNb", "lastPgInd"})
public class Pagination {

    @XmlElement(name = "PgNb", required = true) protected String pgNb;
    @XmlElement(name = "LastPgInd") protected boolean lastPgInd;

    /**
     * Obtém o valor da propriedade pgNb.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPgNb() {
        return pgNb;
    }

    /**
     * Define o valor da propriedade pgNb.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPgNb(String value) {
        this.pgNb = value;
    }

    /**
     * Obtém o valor da propriedade lastPgInd.
     */
    public boolean isLastPgInd() {
        return lastPgInd;
    }

    /**
     * Define o valor da propriedade lastPgInd.
     */
    public void setLastPgInd(boolean value) {
        this.lastPgInd = value;
    }

}
