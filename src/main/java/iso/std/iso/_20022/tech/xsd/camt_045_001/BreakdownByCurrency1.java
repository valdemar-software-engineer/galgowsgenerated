package iso.std.iso._20022.tech.xsd.camt_045_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de BreakdownByCurrency1 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="BreakdownByCurrency1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Ccy" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}ActiveOrHistoricCurrencyCode"/>
 *         &lt;element name="CshOutFcst" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}CashOutForecast3" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CshInFcst" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}CashInForecast3" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="NetCshFcst" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}NetCashForecast2" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BreakdownByCurrency1", propOrder = {"ccy", "cshOutFcst", "cshInFcst", "netCshFcst"})
public class BreakdownByCurrency1 {

    @XmlElement(name = "Ccy", required = true) protected String ccy;
    @XmlElement(name = "CshOutFcst") protected List<CashOutForecast3> cshOutFcst;
    @XmlElement(name = "CshInFcst") protected List<CashInForecast3> cshInFcst;
    @XmlElement(name = "NetCshFcst") protected List<NetCashForecast2> netCshFcst;

    /**
     * Obtém o valor da propriedade ccy.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * Define o valor da propriedade ccy.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCcy(String value) {
        this.ccy = value;
    }

    /**
     * Gets the value of the cshInFcst property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cshInFcst property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCshInFcst().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CashInForecast3 }
     */
    public List<CashInForecast3> getCshInFcst() {
        if (cshInFcst == null) {
            cshInFcst = new ArrayList<CashInForecast3>();
        }
        return this.cshInFcst;
    }

    /**
     * Gets the value of the cshOutFcst property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cshOutFcst property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCshOutFcst().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CashOutForecast3 }
     */
    public List<CashOutForecast3> getCshOutFcst() {
        if (cshOutFcst == null) {
            cshOutFcst = new ArrayList<CashOutForecast3>();
        }
        return this.cshOutFcst;
    }

    /**
     * Gets the value of the netCshFcst property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the netCshFcst property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNetCshFcst().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NetCashForecast2 }
     */
    public List<NetCashForecast2> getNetCshFcst() {
        if (netCshFcst == null) {
            netCshFcst = new ArrayList<NetCashForecast2>();
        }
        return this.netCshFcst;
    }

}
