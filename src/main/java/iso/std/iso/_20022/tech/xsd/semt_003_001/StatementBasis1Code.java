package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de StatementBasis1Code.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;simpleType name="StatementBasis1Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CONT"/&gt;
 *     &lt;enumeration value="SETT"/&gt;
 *     &lt;enumeration value="TRAD"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "StatementBasis1Code")
@XmlEnum
public enum StatementBasis1Code {

    CONT,
    SETT,
    TRAD;

    public static StatementBasis1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
