package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de InterestComputationMethodFormat1Choice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="InterestComputationMethodFormat1Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Cd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}InterestComputationMethod2Code"/&gt;
 *           &lt;element name="Prtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification20"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InterestComputationMethodFormat1Choice", propOrder = {"cd", "prtry"})
public class InterestComputationMethodFormat1Choice {

    @XmlElement(name = "Cd") @XmlSchemaType(name = "string") protected InterestComputationMethod2Code cd;
    @XmlElement(name = "Prtry") protected GenericIdentification20 prtry;

    /**
     * Obtém o valor da propriedade cd.
     *
     * @return possible object is
     * {@link InterestComputationMethod2Code }
     */
    public InterestComputationMethod2Code getCd() {
        return cd;
    }

    /**
     * Define o valor da propriedade cd.
     *
     * @param value allowed object is
     *              {@link InterestComputationMethod2Code }
     */
    public void setCd(InterestComputationMethod2Code value) {
        this.cd = value;
    }

    /**
     * Obtém o valor da propriedade prtry.
     *
     * @return possible object is
     * {@link GenericIdentification20 }
     */
    public GenericIdentification20 getPrtry() {
        return prtry;
    }

    /**
     * Define o valor da propriedade prtry.
     *
     * @param value allowed object is
     *              {@link GenericIdentification20 }
     */
    public void setPrtry(GenericIdentification20 value) {
        this.prtry = value;
    }

}
