package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Document complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="Document"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SctiesBalAcctgRpt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SecuritiesBalanceAccountingReportV04"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", propOrder = {"sctiesBalAcctgRpt"})
public class Document {

    @XmlElement(name = "SctiesBalAcctgRpt", required = true) protected SecuritiesBalanceAccountingReportV04
        sctiesBalAcctgRpt;

    /**
     * Obtém o valor da propriedade sctiesBalAcctgRpt.
     *
     * @return possible object is
     * {@link SecuritiesBalanceAccountingReportV04 }
     */
    public SecuritiesBalanceAccountingReportV04 getSctiesBalAcctgRpt() {
        return sctiesBalAcctgRpt;
    }

    /**
     * Define o valor da propriedade sctiesBalAcctgRpt.
     *
     * @param value allowed object is
     *              {@link SecuritiesBalanceAccountingReportV04 }
     */
    public void setSctiesBalAcctgRpt(SecuritiesBalanceAccountingReportV04 value) {
        this.sctiesBalAcctgRpt = value;
    }

}
