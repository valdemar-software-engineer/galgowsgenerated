package iso.std.iso._20022.tech.xsd.reda_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Classe Java de TaxCalculationInformation4 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="TaxCalculationInformation4"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="EUCptlGn" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}EUCapitalGain2Code" minOccurs="0"/&gt;
 *           &lt;element name="XtndedEUCptlGn" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}Extended350Code" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="PctgOfDebtClm" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PercentageRate" minOccurs="0"/&gt;
 *         &lt;element name="PctgGrdfthdDebt" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PercentageRate" minOccurs="0"/&gt;
 *         &lt;element name="TaxblIncmPerDvdd" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}ActiveOrHistoricCurrencyAnd13DecimalAmount" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="EUDvddSts" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}EUDividendStatus1Code" minOccurs="0"/&gt;
 *           &lt;element name="XtndedEUDvddSts" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}Extended350Code" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxCalculationInformation4",
         propOrder = {"euCptlGn", "xtndedEUCptlGn", "pctgOfDebtClm", "pctgGrdfthdDebt", "taxblIncmPerDvdd", "euDvddSts", "xtndedEUDvddSts"})
public class TaxCalculationInformation4 {

    @XmlElement(name = "EUCptlGn") @XmlSchemaType(name = "string") protected EUCapitalGain2Code euCptlGn;
    @XmlElement(name = "XtndedEUCptlGn") protected String xtndedEUCptlGn;
    @XmlElement(name = "PctgOfDebtClm") protected BigDecimal pctgOfDebtClm;
    @XmlElement(name = "PctgGrdfthdDebt") protected BigDecimal pctgGrdfthdDebt;
    @XmlElement(name = "TaxblIncmPerDvdd") protected ActiveOrHistoricCurrencyAnd13DecimalAmount taxblIncmPerDvdd;
    @XmlElement(name = "EUDvddSts") @XmlSchemaType(name = "string") protected EUDividendStatus1Code euDvddSts;
    @XmlElement(name = "XtndedEUDvddSts") protected String xtndedEUDvddSts;

    /**
     * Obtém o valor da propriedade euCptlGn.
     *
     * @return possible object is
     * {@link EUCapitalGain2Code }
     */
    public EUCapitalGain2Code getEUCptlGn() {
        return euCptlGn;
    }

    /**
     * Define o valor da propriedade euCptlGn.
     *
     * @param value allowed object is
     *              {@link EUCapitalGain2Code }
     */
    public void setEUCptlGn(EUCapitalGain2Code value) {
        this.euCptlGn = value;
    }

    /**
     * Obtém o valor da propriedade euDvddSts.
     *
     * @return possible object is
     * {@link EUDividendStatus1Code }
     */
    public EUDividendStatus1Code getEUDvddSts() {
        return euDvddSts;
    }

    /**
     * Define o valor da propriedade euDvddSts.
     *
     * @param value allowed object is
     *              {@link EUDividendStatus1Code }
     */
    public void setEUDvddSts(EUDividendStatus1Code value) {
        this.euDvddSts = value;
    }

    /**
     * Obtém o valor da propriedade pctgGrdfthdDebt.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getPctgGrdfthdDebt() {
        return pctgGrdfthdDebt;
    }

    /**
     * Define o valor da propriedade pctgGrdfthdDebt.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setPctgGrdfthdDebt(BigDecimal value) {
        this.pctgGrdfthdDebt = value;
    }

    /**
     * Obtém o valor da propriedade pctgOfDebtClm.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getPctgOfDebtClm() {
        return pctgOfDebtClm;
    }

    /**
     * Define o valor da propriedade pctgOfDebtClm.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setPctgOfDebtClm(BigDecimal value) {
        this.pctgOfDebtClm = value;
    }

    /**
     * Obtém o valor da propriedade taxblIncmPerDvdd.
     *
     * @return possible object is
     * {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     */
    public ActiveOrHistoricCurrencyAnd13DecimalAmount getTaxblIncmPerDvdd() {
        return taxblIncmPerDvdd;
    }

    /**
     * Define o valor da propriedade taxblIncmPerDvdd.
     *
     * @param value allowed object is
     *              {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     */
    public void setTaxblIncmPerDvdd(ActiveOrHistoricCurrencyAnd13DecimalAmount value) {
        this.taxblIncmPerDvdd = value;
    }

    /**
     * Obtém o valor da propriedade xtndedEUCptlGn.
     *
     * @return possible object is
     * {@link String }
     */
    public String getXtndedEUCptlGn() {
        return xtndedEUCptlGn;
    }

    /**
     * Define o valor da propriedade xtndedEUCptlGn.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setXtndedEUCptlGn(String value) {
        this.xtndedEUCptlGn = value;
    }

    /**
     * Obtém o valor da propriedade xtndedEUDvddSts.
     *
     * @return possible object is
     * {@link String }
     */
    public String getXtndedEUDvddSts() {
        return xtndedEUDvddSts;
    }

    /**
     * Define o valor da propriedade xtndedEUDvddSts.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setXtndedEUDvddSts(String value) {
        this.xtndedEUDvddSts = value;
    }

}
