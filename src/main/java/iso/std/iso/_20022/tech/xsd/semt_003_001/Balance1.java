package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Balance1 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="Balance1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShrtLngInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}ShortLong1Code"/&gt;
 *         &lt;element name="Qty" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}BalanceQuantity4Choice"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Balance1", propOrder = {"shrtLngInd", "qty"})
public class Balance1 {

    @XmlElement(name = "ShrtLngInd", required = true) @XmlSchemaType(name = "string") protected ShortLong1Code
        shrtLngInd;
    @XmlElement(name = "Qty", required = true) protected BalanceQuantity4Choice qty;

    /**
     * Obtém o valor da propriedade qty.
     *
     * @return possible object is
     * {@link BalanceQuantity4Choice }
     */
    public BalanceQuantity4Choice getQty() {
        return qty;
    }

    /**
     * Define o valor da propriedade qty.
     *
     * @param value allowed object is
     *              {@link BalanceQuantity4Choice }
     */
    public void setQty(BalanceQuantity4Choice value) {
        this.qty = value;
    }

    /**
     * Obtém o valor da propriedade shrtLngInd.
     *
     * @return possible object is
     * {@link ShortLong1Code }
     */
    public ShortLong1Code getShrtLngInd() {
        return shrtLngInd;
    }

    /**
     * Define o valor da propriedade shrtLngInd.
     *
     * @param value allowed object is
     *              {@link ShortLong1Code }
     */
    public void setShrtLngInd(ShortLong1Code value) {
        this.shrtLngInd = value;
    }

}
