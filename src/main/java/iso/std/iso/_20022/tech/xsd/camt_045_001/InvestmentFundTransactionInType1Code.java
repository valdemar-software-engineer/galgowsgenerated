package iso.std.iso._20022.tech.xsd.camt_045_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de InvestmentFundTransactionInType1Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="InvestmentFundTransactionInType1Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SUBS"/>
 *     &lt;enumeration value="SWII"/>
 *     &lt;enumeration value="INSP"/>
 *     &lt;enumeration value="CROI"/>
 *     &lt;enumeration value="RDIV"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "InvestmentFundTransactionInType1Code")
@XmlEnum
public enum InvestmentFundTransactionInType1Code {

    SUBS,
    SWII,
    INSP,
    CROI,
    RDIV;

    public static InvestmentFundTransactionInType1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
