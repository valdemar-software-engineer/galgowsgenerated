package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ClassificationType2Choice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="ClassificationType2Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="ClssfctnFinInstrm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}CFIIdentifier"/&gt;
 *           &lt;element name="AltrnClssfctn" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification19"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClassificationType2Choice", propOrder = {"clssfctnFinInstrm", "altrnClssfctn"})
public class ClassificationType2Choice {

    @XmlElement(name = "ClssfctnFinInstrm") protected String clssfctnFinInstrm;
    @XmlElement(name = "AltrnClssfctn") protected GenericIdentification19 altrnClssfctn;

    /**
     * Obtém o valor da propriedade altrnClssfctn.
     *
     * @return possible object is
     * {@link GenericIdentification19 }
     */
    public GenericIdentification19 getAltrnClssfctn() {
        return altrnClssfctn;
    }

    /**
     * Define o valor da propriedade altrnClssfctn.
     *
     * @param value allowed object is
     *              {@link GenericIdentification19 }
     */
    public void setAltrnClssfctn(GenericIdentification19 value) {
        this.altrnClssfctn = value;
    }

    /**
     * Obtém o valor da propriedade clssfctnFinInstrm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getClssfctnFinInstrm() {
        return clssfctnFinInstrm;
    }

    /**
     * Define o valor da propriedade clssfctnFinInstrm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setClssfctnFinInstrm(String value) {
        this.clssfctnFinInstrm = value;
    }

}
