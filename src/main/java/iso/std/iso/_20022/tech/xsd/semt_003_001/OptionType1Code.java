package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de OptionType1Code.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;simpleType name="OptionType1Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CALL"/&gt;
 *     &lt;enumeration value="PUTO"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "OptionType1Code")
@XmlEnum
public enum OptionType1Code {

    CALL,
    PUTO;

    public static OptionType1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
