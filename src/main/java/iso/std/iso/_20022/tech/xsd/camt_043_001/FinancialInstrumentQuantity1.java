package iso.std.iso._20022.tech.xsd.camt_043_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Classe Java de FinancialInstrumentQuantity1 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="FinancialInstrumentQuantity1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Unit" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}DecimalNumber"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialInstrumentQuantity1", propOrder = {"unit"})
public class FinancialInstrumentQuantity1 {

    @XmlElement(name = "Unit", required = true) protected BigDecimal unit;

    /**
     * Obtém o valor da propriedade unit.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getUnit() {
        return unit;
    }

    /**
     * Define o valor da propriedade unit.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setUnit(BigDecimal value) {
        this.unit = value;
    }

}
