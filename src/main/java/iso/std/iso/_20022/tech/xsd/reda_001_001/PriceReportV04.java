package iso.std.iso._20022.tech.xsd.reda_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de PriceReportV04 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PriceReportV04"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MsgId" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}MessageIdentification1"/&gt;
 *         &lt;element name="PoolRef" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}AdditionalReference3" minOccurs="0"/&gt;
 *         &lt;element name="PrvsRef" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}AdditionalReference3" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="RltdRef" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}AdditionalReference3" minOccurs="0"/&gt;
 *         &lt;element name="MsgPgntn" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}Pagination"/&gt;
 *         &lt;element name="PricValtnDtls" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}PriceValuation3" maxOccurs="unbounded"/&gt;
 *         &lt;element name="Xtnsn" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}Extension1" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceReportV04",
         propOrder = {"msgId", "poolRef", "prvsRef", "rltdRef", "msgPgntn", "pricValtnDtls", "xtnsn"})
public class PriceReportV04 {

    @XmlElement(name = "MsgId", required = true) protected MessageIdentification1 msgId;
    @XmlElement(name = "PoolRef") protected AdditionalReference3 poolRef;
    @XmlElement(name = "PrvsRef") protected List<AdditionalReference3> prvsRef;
    @XmlElement(name = "RltdRef") protected AdditionalReference3 rltdRef;
    @XmlElement(name = "MsgPgntn", required = true) protected Pagination msgPgntn;
    @XmlElement(name = "PricValtnDtls", required = true) protected List<PriceValuation3> pricValtnDtls;
    @XmlElement(name = "Xtnsn") protected List<Extension1> xtnsn;

    /**
     * Obtém o valor da propriedade msgId.
     *
     * @return possible object is
     * {@link MessageIdentification1 }
     */
    public MessageIdentification1 getMsgId() {
        return msgId;
    }

    /**
     * Define o valor da propriedade msgId.
     *
     * @param value allowed object is
     *              {@link MessageIdentification1 }
     */
    public void setMsgId(MessageIdentification1 value) {
        this.msgId = value;
    }

    /**
     * Obtém o valor da propriedade msgPgntn.
     *
     * @return possible object is
     * {@link Pagination }
     */
    public Pagination getMsgPgntn() {
        return msgPgntn;
    }

    /**
     * Define o valor da propriedade msgPgntn.
     *
     * @param value allowed object is
     *              {@link Pagination }
     */
    public void setMsgPgntn(Pagination value) {
        this.msgPgntn = value;
    }

    /**
     * Obtém o valor da propriedade poolRef.
     *
     * @return possible object is
     * {@link AdditionalReference3 }
     */
    public AdditionalReference3 getPoolRef() {
        return poolRef;
    }

    /**
     * Define o valor da propriedade poolRef.
     *
     * @param value allowed object is
     *              {@link AdditionalReference3 }
     */
    public void setPoolRef(AdditionalReference3 value) {
        this.poolRef = value;
    }

    /**
     * Gets the value of the pricValtnDtls property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pricValtnDtls property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPricValtnDtls().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PriceValuation3 }
     */
    public List<PriceValuation3> getPricValtnDtls() {
        if (pricValtnDtls == null) {
            pricValtnDtls = new ArrayList<PriceValuation3>();
        }
        return this.pricValtnDtls;
    }

    /**
     * Gets the value of the prvsRef property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prvsRef property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrvsRef().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalReference3 }
     */
    public List<AdditionalReference3> getPrvsRef() {
        if (prvsRef == null) {
            prvsRef = new ArrayList<AdditionalReference3>();
        }
        return this.prvsRef;
    }

    /**
     * Obtém o valor da propriedade rltdRef.
     *
     * @return possible object is
     * {@link AdditionalReference3 }
     */
    public AdditionalReference3 getRltdRef() {
        return rltdRef;
    }

    /**
     * Define o valor da propriedade rltdRef.
     *
     * @param value allowed object is
     *              {@link AdditionalReference3 }
     */
    public void setRltdRef(AdditionalReference3 value) {
        this.rltdRef = value;
    }

    /**
     * Gets the value of the xtnsn property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the xtnsn property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getXtnsn().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Extension1 }
     */
    public List<Extension1> getXtnsn() {
        if (xtnsn == null) {
            xtnsn = new ArrayList<Extension1>();
        }
        return this.xtnsn;
    }

}
