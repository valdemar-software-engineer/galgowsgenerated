package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de SecuritiesAccountPurposeType1Code.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;simpleType name="SecuritiesAccountPurposeType1Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="MARG"/&gt;
 *     &lt;enumeration value="SHOR"/&gt;
 *     &lt;enumeration value="ABRD"/&gt;
 *     &lt;enumeration value="CEND"/&gt;
 *     &lt;enumeration value="DVPA"/&gt;
 *     &lt;enumeration value="PHYS"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "SecuritiesAccountPurposeType1Code")
@XmlEnum
public enum SecuritiesAccountPurposeType1Code {

    MARG,
    SHOR,
    ABRD,
    CEND,
    DVPA,
    PHYS;

    public static SecuritiesAccountPurposeType1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
