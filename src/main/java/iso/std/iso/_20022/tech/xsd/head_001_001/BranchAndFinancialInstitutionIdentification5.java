package iso.std.iso._20022.tech.xsd.head_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de BranchAndFinancialInstitutionIdentification5 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="BranchAndFinancialInstitutionIdentification5"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FinInstnId" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}FinancialInstitutionIdentification8"/&gt;
 *         &lt;element name="BrnchId" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}BranchData2" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BranchAndFinancialInstitutionIdentification5", propOrder = {"finInstnId", "brnchId"})
public class BranchAndFinancialInstitutionIdentification5 {

    @XmlElement(name = "FinInstnId", required = true) protected FinancialInstitutionIdentification8 finInstnId;
    @XmlElement(name = "BrnchId") protected BranchData2 brnchId;

    /**
     * Obtém o valor da propriedade brnchId.
     *
     * @return possible object is
     * {@link BranchData2 }
     */
    public BranchData2 getBrnchId() {
        return brnchId;
    }

    /**
     * Define o valor da propriedade brnchId.
     *
     * @param value allowed object is
     *              {@link BranchData2 }
     */
    public void setBrnchId(BranchData2 value) {
        this.brnchId = value;
    }

    /**
     * Obtém o valor da propriedade finInstnId.
     *
     * @return possible object is
     * {@link FinancialInstitutionIdentification8 }
     */
    public FinancialInstitutionIdentification8 getFinInstnId() {
        return finInstnId;
    }

    /**
     * Define o valor da propriedade finInstnId.
     *
     * @param value allowed object is
     *              {@link FinancialInstitutionIdentification8 }
     */
    public void setFinInstnId(FinancialInstitutionIdentification8 value) {
        this.finInstnId = value;
    }

}
