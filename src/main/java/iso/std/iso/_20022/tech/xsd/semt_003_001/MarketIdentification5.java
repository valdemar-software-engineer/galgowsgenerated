package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de MarketIdentification5 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="MarketIdentification5"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}MarketIdentification1Choice" minOccurs="0"/&gt;
 *         &lt;element name="Tp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}MarketType2Choice"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketIdentification5", propOrder = {"id", "tp"})
public class MarketIdentification5 {

    @XmlElement(name = "Id") protected MarketIdentification1Choice id;
    @XmlElement(name = "Tp", required = true) protected MarketType2Choice tp;

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link MarketIdentification1Choice }
     */
    public MarketIdentification1Choice getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link MarketIdentification1Choice }
     */
    public void setId(MarketIdentification1Choice value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade tp.
     *
     * @return possible object is
     * {@link MarketType2Choice }
     */
    public MarketType2Choice getTp() {
        return tp;
    }

    /**
     * Define o valor da propriedade tp.
     *
     * @param value allowed object is
     *              {@link MarketType2Choice }
     */
    public void setTp(MarketType2Choice value) {
        this.tp = value;
    }

}
