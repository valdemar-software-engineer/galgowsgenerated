package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de PartyIdentification49Choice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="PartyIdentification49Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="AnyBIC" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}AnyBICIdentifier"/&gt;
 *           &lt;element name="PrtryId" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification19"/&gt;
 *           &lt;element name="NmAndAdr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}NameAndAddress5"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyIdentification49Choice", propOrder = {"anyBIC", "prtryId", "nmAndAdr"})
public class PartyIdentification49Choice {

    @XmlElement(name = "AnyBIC") protected String anyBIC;
    @XmlElement(name = "PrtryId") protected GenericIdentification19 prtryId;
    @XmlElement(name = "NmAndAdr") protected NameAndAddress5 nmAndAdr;

    /**
     * Obtém o valor da propriedade anyBIC.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAnyBIC() {
        return anyBIC;
    }

    /**
     * Define o valor da propriedade anyBIC.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAnyBIC(String value) {
        this.anyBIC = value;
    }

    /**
     * Obtém o valor da propriedade nmAndAdr.
     *
     * @return possible object is
     * {@link NameAndAddress5 }
     */
    public NameAndAddress5 getNmAndAdr() {
        return nmAndAdr;
    }

    /**
     * Define o valor da propriedade nmAndAdr.
     *
     * @param value allowed object is
     *              {@link NameAndAddress5 }
     */
    public void setNmAndAdr(NameAndAddress5 value) {
        this.nmAndAdr = value;
    }

    /**
     * Obtém o valor da propriedade prtryId.
     *
     * @return possible object is
     * {@link GenericIdentification19 }
     */
    public GenericIdentification19 getPrtryId() {
        return prtryId;
    }

    /**
     * Define o valor da propriedade prtryId.
     *
     * @param value allowed object is
     *              {@link GenericIdentification19 }
     */
    public void setPrtryId(GenericIdentification19 value) {
        this.prtryId = value;
    }

}
