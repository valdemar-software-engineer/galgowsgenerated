package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de SubAccountIdentification16 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="SubAccountIdentification16"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AcctOwnr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PartyIdentification36Choice" minOccurs="0"/&gt;
 *         &lt;element name="SfkpgAcct" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SecuritiesAccount14"/&gt;
 *         &lt;element name="ActvtyInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator"/&gt;
 *         &lt;element name="BalForSubAcct" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}AggregateBalanceInformation13" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubAccountIdentification16", propOrder = {"acctOwnr", "sfkpgAcct", "actvtyInd", "balForSubAcct"})
public class SubAccountIdentification16 {

    @XmlElement(name = "AcctOwnr") protected PartyIdentification36Choice acctOwnr;
    @XmlElement(name = "SfkpgAcct", required = true) protected SecuritiesAccount14 sfkpgAcct;
    @XmlElement(name = "ActvtyInd") protected boolean actvtyInd;
    @XmlElement(name = "BalForSubAcct") protected List<AggregateBalanceInformation13> balForSubAcct;

    /**
     * Obtém o valor da propriedade acctOwnr.
     *
     * @return possible object is
     * {@link PartyIdentification36Choice }
     */
    public PartyIdentification36Choice getAcctOwnr() {
        return acctOwnr;
    }

    /**
     * Define o valor da propriedade acctOwnr.
     *
     * @param value allowed object is
     *              {@link PartyIdentification36Choice }
     */
    public void setAcctOwnr(PartyIdentification36Choice value) {
        this.acctOwnr = value;
    }

    /**
     * Gets the value of the balForSubAcct property.
     * <p/>
     * <p/>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the balForSubAcct property.
     * <p/>
     * <p/>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBalForSubAcct().add(newItem);
     * </pre>
     * <p/>
     * <p/>
     * <p/>
     * Objects of the following type(s) are allowed in the list
     * {@link AggregateBalanceInformation13 }
     */
    public List<AggregateBalanceInformation13> getBalForSubAcct() {
        if (balForSubAcct == null) {
            balForSubAcct = new ArrayList<AggregateBalanceInformation13>();
        }
        return this.balForSubAcct;
    }

    /**
     * Obtém o valor da propriedade sfkpgAcct.
     *
     * @return possible object is
     * {@link SecuritiesAccount14 }
     */
    public SecuritiesAccount14 getSfkpgAcct() {
        return sfkpgAcct;
    }

    /**
     * Define o valor da propriedade sfkpgAcct.
     *
     * @param value allowed object is
     *              {@link SecuritiesAccount14 }
     */
    public void setSfkpgAcct(SecuritiesAccount14 value) {
        this.sfkpgAcct = value;
    }

    /**
     * Obtém o valor da propriedade actvtyInd.
     */
    public boolean isActvtyInd() {
        return actvtyInd;
    }

    /**
     * Define o valor da propriedade actvtyInd.
     */
    public void setActvtyInd(boolean value) {
        this.actvtyInd = value;
    }

}
