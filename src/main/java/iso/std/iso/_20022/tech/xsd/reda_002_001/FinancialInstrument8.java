package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de FinancialInstrument8 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="FinancialInstrument8"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}SecurityIdentification3Choice" maxOccurs="10"/&gt;
 *         &lt;element name="Nm" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Max350Text" minOccurs="0"/&gt;
 *         &lt;element name="SplmtryId" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Max35Text" minOccurs="0"/&gt;
 *         &lt;element name="DnmtnCcy" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}ActiveOrHistoricCurrencyCode" minOccurs="0"/&gt;
 *         &lt;element name="ClssTp" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Max35Text" minOccurs="0"/&gt;
 *         &lt;element name="SctiesForm" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}FormOfSecurity1Code" minOccurs="0"/&gt;
 *         &lt;element name="DstrbtnPlcy" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}DistributionPolicy1Code" minOccurs="0"/&gt;
 *         &lt;element name="DualFndInd" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}YesNoIndicator"/&gt;
 *         &lt;element name="Xtnsn" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}Extension1" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialInstrument8",
         propOrder = {"id", "nm", "splmtryId", "dnmtnCcy", "clssTp", "sctiesForm", "dstrbtnPlcy", "dualFndInd", "xtnsn"})
public class FinancialInstrument8 {

    @XmlElement(name = "Id", required = true) protected List<SecurityIdentification3Choice> id;
    @XmlElement(name = "Nm") protected String nm;
    @XmlElement(name = "SplmtryId") protected String splmtryId;
    @XmlElement(name = "DnmtnCcy") protected String dnmtnCcy;
    @XmlElement(name = "ClssTp") protected String clssTp;
    @XmlElement(name = "SctiesForm") @XmlSchemaType(name = "string") protected FormOfSecurity1Code sctiesForm;
    @XmlElement(name = "DstrbtnPlcy") @XmlSchemaType(name = "string") protected DistributionPolicy1Code dstrbtnPlcy;
    @XmlElement(name = "DualFndInd") protected boolean dualFndInd;
    @XmlElement(name = "Xtnsn") protected List<Extension1> xtnsn;

    /**
     * Obtém o valor da propriedade clssTp.
     *
     * @return possible object is
     * {@link String }
     */
    public String getClssTp() {
        return clssTp;
    }

    /**
     * Define o valor da propriedade clssTp.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setClssTp(String value) {
        this.clssTp = value;
    }

    /**
     * Obtém o valor da propriedade dnmtnCcy.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDnmtnCcy() {
        return dnmtnCcy;
    }

    /**
     * Define o valor da propriedade dnmtnCcy.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDnmtnCcy(String value) {
        this.dnmtnCcy = value;
    }

    /**
     * Obtém o valor da propriedade dstrbtnPlcy.
     *
     * @return possible object is
     * {@link DistributionPolicy1Code }
     */
    public DistributionPolicy1Code getDstrbtnPlcy() {
        return dstrbtnPlcy;
    }

    /**
     * Define o valor da propriedade dstrbtnPlcy.
     *
     * @param value allowed object is
     *              {@link DistributionPolicy1Code }
     */
    public void setDstrbtnPlcy(DistributionPolicy1Code value) {
        this.dstrbtnPlcy = value;
    }

    /**
     * Gets the value of the id property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the id property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getId().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SecurityIdentification3Choice }
     */
    public List<SecurityIdentification3Choice> getId() {
        if (id == null) {
            id = new ArrayList<SecurityIdentification3Choice>();
        }
        return this.id;
    }

    /**
     * Obtém o valor da propriedade nm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNm() {
        return nm;
    }

    /**
     * Define o valor da propriedade nm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNm(String value) {
        this.nm = value;
    }

    /**
     * Obtém o valor da propriedade sctiesForm.
     *
     * @return possible object is
     * {@link FormOfSecurity1Code }
     */
    public FormOfSecurity1Code getSctiesForm() {
        return sctiesForm;
    }

    /**
     * Define o valor da propriedade sctiesForm.
     *
     * @param value allowed object is
     *              {@link FormOfSecurity1Code }
     */
    public void setSctiesForm(FormOfSecurity1Code value) {
        this.sctiesForm = value;
    }

    /**
     * Obtém o valor da propriedade splmtryId.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSplmtryId() {
        return splmtryId;
    }

    /**
     * Define o valor da propriedade splmtryId.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSplmtryId(String value) {
        this.splmtryId = value;
    }

    /**
     * Gets the value of the xtnsn property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the xtnsn property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getXtnsn().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Extension1 }
     */
    public List<Extension1> getXtnsn() {
        if (xtnsn == null) {
            xtnsn = new ArrayList<Extension1>();
        }
        return this.xtnsn;
    }

    /**
     * Obtém o valor da propriedade dualFndInd.
     */
    public boolean isDualFndInd() {
        return dualFndInd;
    }

    /**
     * Define o valor da propriedade dualFndInd.
     */
    public void setDualFndInd(boolean value) {
        this.dualFndInd = value;
    }

}
