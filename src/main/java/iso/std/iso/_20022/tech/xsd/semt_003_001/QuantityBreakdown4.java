package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de QuantityBreakdown4 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="QuantityBreakdown4"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LotNb" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Number2Choice" minOccurs="0"/&gt;
 *         &lt;element name="LotQty" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}FinancialInstrumentQuantity1Choice" minOccurs="0"/&gt;
 *         &lt;element name="LotDtTm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}DateAndDateTimeChoice" minOccurs="0"/&gt;
 *         &lt;element name="LotPric" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Price2" minOccurs="0"/&gt;
 *         &lt;element name="TpOfPric" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}TypeOfPrice3Choice" minOccurs="0"/&gt;
 *         &lt;element name="AcctBaseCcyAmts" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}BalanceAmounts2" minOccurs="0"/&gt;
 *         &lt;element name="InstrmCcyAmts" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}BalanceAmounts2" minOccurs="0"/&gt;
 *         &lt;element name="AltrnRptgCcyAmts" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}BalanceAmounts2" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuantityBreakdown4",
         propOrder = {"lotNb", "lotQty", "lotDtTm", "lotPric", "tpOfPric", "acctBaseCcyAmts", "instrmCcyAmts", "altrnRptgCcyAmts"})
public class QuantityBreakdown4 {

    @XmlElement(name = "LotNb") protected Number2Choice lotNb;
    @XmlElement(name = "LotQty") protected FinancialInstrumentQuantity1Choice lotQty;
    @XmlElement(name = "LotDtTm") protected DateAndDateTimeChoice lotDtTm;
    @XmlElement(name = "LotPric") protected Price2 lotPric;
    @XmlElement(name = "TpOfPric") protected TypeOfPrice3Choice tpOfPric;
    @XmlElement(name = "AcctBaseCcyAmts") protected BalanceAmounts2 acctBaseCcyAmts;
    @XmlElement(name = "InstrmCcyAmts") protected BalanceAmounts2 instrmCcyAmts;
    @XmlElement(name = "AltrnRptgCcyAmts") protected BalanceAmounts2 altrnRptgCcyAmts;

    /**
     * Obtém o valor da propriedade acctBaseCcyAmts.
     *
     * @return possible object is
     * {@link BalanceAmounts2 }
     */
    public BalanceAmounts2 getAcctBaseCcyAmts() {
        return acctBaseCcyAmts;
    }

    /**
     * Define o valor da propriedade acctBaseCcyAmts.
     *
     * @param value allowed object is
     *              {@link BalanceAmounts2 }
     */
    public void setAcctBaseCcyAmts(BalanceAmounts2 value) {
        this.acctBaseCcyAmts = value;
    }

    /**
     * Obtém o valor da propriedade altrnRptgCcyAmts.
     *
     * @return possible object is
     * {@link BalanceAmounts2 }
     */
    public BalanceAmounts2 getAltrnRptgCcyAmts() {
        return altrnRptgCcyAmts;
    }

    /**
     * Define o valor da propriedade altrnRptgCcyAmts.
     *
     * @param value allowed object is
     *              {@link BalanceAmounts2 }
     */
    public void setAltrnRptgCcyAmts(BalanceAmounts2 value) {
        this.altrnRptgCcyAmts = value;
    }

    /**
     * Obtém o valor da propriedade instrmCcyAmts.
     *
     * @return possible object is
     * {@link BalanceAmounts2 }
     */
    public BalanceAmounts2 getInstrmCcyAmts() {
        return instrmCcyAmts;
    }

    /**
     * Define o valor da propriedade instrmCcyAmts.
     *
     * @param value allowed object is
     *              {@link BalanceAmounts2 }
     */
    public void setInstrmCcyAmts(BalanceAmounts2 value) {
        this.instrmCcyAmts = value;
    }

    /**
     * Obtém o valor da propriedade lotDtTm.
     *
     * @return possible object is
     * {@link DateAndDateTimeChoice }
     */
    public DateAndDateTimeChoice getLotDtTm() {
        return lotDtTm;
    }

    /**
     * Define o valor da propriedade lotDtTm.
     *
     * @param value allowed object is
     *              {@link DateAndDateTimeChoice }
     */
    public void setLotDtTm(DateAndDateTimeChoice value) {
        this.lotDtTm = value;
    }

    /**
     * Obtém o valor da propriedade lotNb.
     *
     * @return possible object is
     * {@link Number2Choice }
     */
    public Number2Choice getLotNb() {
        return lotNb;
    }

    /**
     * Define o valor da propriedade lotNb.
     *
     * @param value allowed object is
     *              {@link Number2Choice }
     */
    public void setLotNb(Number2Choice value) {
        this.lotNb = value;
    }

    /**
     * Obtém o valor da propriedade lotPric.
     *
     * @return possible object is
     * {@link Price2 }
     */
    public Price2 getLotPric() {
        return lotPric;
    }

    /**
     * Define o valor da propriedade lotPric.
     *
     * @param value allowed object is
     *              {@link Price2 }
     */
    public void setLotPric(Price2 value) {
        this.lotPric = value;
    }

    /**
     * Obtém o valor da propriedade lotQty.
     *
     * @return possible object is
     * {@link FinancialInstrumentQuantity1Choice }
     */
    public FinancialInstrumentQuantity1Choice getLotQty() {
        return lotQty;
    }

    /**
     * Define o valor da propriedade lotQty.
     *
     * @param value allowed object is
     *              {@link FinancialInstrumentQuantity1Choice }
     */
    public void setLotQty(FinancialInstrumentQuantity1Choice value) {
        this.lotQty = value;
    }

    /**
     * Obtém o valor da propriedade tpOfPric.
     *
     * @return possible object is
     * {@link TypeOfPrice3Choice }
     */
    public TypeOfPrice3Choice getTpOfPric() {
        return tpOfPric;
    }

    /**
     * Define o valor da propriedade tpOfPric.
     *
     * @param value allowed object is
     *              {@link TypeOfPrice3Choice }
     */
    public void setTpOfPric(TypeOfPrice3Choice value) {
        this.tpOfPric = value;
    }

}
