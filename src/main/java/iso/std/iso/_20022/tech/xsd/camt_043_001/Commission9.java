package iso.std.iso._20022.tech.xsd.camt_043_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Classe Java de Commission9 complex type.
 *
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 *
 * <pre>
 * &lt;complexType name="Commission9"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Tp" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}CommissionType6Code"/&gt;
 *           &lt;element name="XtndedTp" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Extended350Code"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}ActiveOrHistoricCurrencyAnd13DecimalAmount"/&gt;
 *           &lt;element name="Rate" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}PercentageRate"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Commission9", propOrder = {
    "tp",
    "xtndedTp",
    "amt",
    "rate"
})
public class Commission9 {

    @XmlElement(name = "Tp")
    @XmlSchemaType(name = "string")
    protected CommissionType6Code tp;
    @XmlElement(name = "XtndedTp")
    protected String xtndedTp;
    @XmlElement(name = "Amt")
    protected ActiveOrHistoricCurrencyAnd13DecimalAmount amt;
    @XmlElement(name = "Rate")
    protected BigDecimal rate;

    /**
     * Obtém o valor da propriedade amt.
     *
     * @return
     *     possible object is
     *     {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     *
     */
    public ActiveOrHistoricCurrencyAnd13DecimalAmount getAmt() {
        return amt;
    }

    /**
     * Define o valor da propriedade amt.
     *
     * @param value
     *     allowed object is
     *     {@link ActiveOrHistoricCurrencyAnd13DecimalAmount }
     *
     */
    public void setAmt(ActiveOrHistoricCurrencyAnd13DecimalAmount value) {
        this.amt = value;
    }

    /**
     * Obtém o valor da propriedade rate.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Define o valor da propriedade rate.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *
     */
    public void setRate(BigDecimal value) {
        this.rate = value;
    }

    /**
     * Obtém o valor da propriedade tp.
     *
     * @return
     *     possible object is
     *     {@link CommissionType6Code }
     *
     */
    public CommissionType6Code getTp() {
        return tp;
    }

    /**
     * Define o valor da propriedade tp.
     *
     * @param value
     *     allowed object is
     *     {@link CommissionType6Code }
     *
     */
    public void setTp(CommissionType6Code value) {
        this.tp = value;
    }

    /**
     * Obtém o valor da propriedade xtndedTp.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getXtndedTp() {
        return xtndedTp;
    }

    /**
     * Define o valor da propriedade xtndedTp.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setXtndedTp(String value) {
        this.xtndedTp = value;
    }

}
