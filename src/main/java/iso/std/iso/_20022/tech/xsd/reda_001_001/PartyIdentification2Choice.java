package iso.std.iso._20022.tech.xsd.reda_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de PartyIdentification2Choice complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="PartyIdentification2Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="BICOrBEI" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}AnyBICIdentifier"/&gt;
 *           &lt;element name="PrtryId" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}GenericIdentification1"/&gt;
 *           &lt;element name="NmAndAdr" type="{urn:iso:std:iso:20022:tech:xsd:reda.001.001.03}NameAndAddress5"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyIdentification2Choice", propOrder = {"bicOrBEI", "prtryId", "nmAndAdr"})
public class PartyIdentification2Choice {

    @XmlElement(name = "BICOrBEI") protected String bicOrBEI;
    @XmlElement(name = "PrtryId") protected GenericIdentification1 prtryId;
    @XmlElement(name = "NmAndAdr") protected NameAndAddress5 nmAndAdr;

    /**
     * Obtém o valor da propriedade bicOrBEI.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBICOrBEI() {
        return bicOrBEI;
    }

    /**
     * Define o valor da propriedade bicOrBEI.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBICOrBEI(String value) {
        this.bicOrBEI = value;
    }

    /**
     * Obtém o valor da propriedade nmAndAdr.
     *
     * @return possible object is
     * {@link NameAndAddress5 }
     */
    public NameAndAddress5 getNmAndAdr() {
        return nmAndAdr;
    }

    /**
     * Define o valor da propriedade nmAndAdr.
     *
     * @param value allowed object is
     *              {@link NameAndAddress5 }
     */
    public void setNmAndAdr(NameAndAddress5 value) {
        this.nmAndAdr = value;
    }

    /**
     * Obtém o valor da propriedade prtryId.
     *
     * @return possible object is
     * {@link GenericIdentification1 }
     */
    public GenericIdentification1 getPrtryId() {
        return prtryId;
    }

    /**
     * Define o valor da propriedade prtryId.
     *
     * @param value allowed object is
     *              {@link GenericIdentification1 }
     */
    public void setPrtryId(GenericIdentification1 value) {
        this.prtryId = value;
    }

}
