package iso.std.iso._20022.tech.xsd.reda_001_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ValuationTiming1Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="ValuationTiming1Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="EXCP"/&gt;
 *     &lt;enumeration value="USUA"/&gt;
 *     &lt;enumeration value="PATC"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "ValuationTiming1Code")
@XmlEnum
public enum ValuationTiming1Code {

    EXCP,
    USUA,
    PATC;

    public static ValuationTiming1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
