package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de EventFrequency4Code.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;simpleType name="EventFrequency4Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="YEAR"/&gt;
 *     &lt;enumeration value="ADHO"/&gt;
 *     &lt;enumeration value="MNTH"/&gt;
 *     &lt;enumeration value="DAIL"/&gt;
 *     &lt;enumeration value="INDA"/&gt;
 *     &lt;enumeration value="WEEK"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "EventFrequency4Code")
@XmlEnum
public enum EventFrequency4Code {

    YEAR,
    ADHO,
    MNTH,
    DAIL,
    INDA,
    WEEK;

    public static EventFrequency4Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
