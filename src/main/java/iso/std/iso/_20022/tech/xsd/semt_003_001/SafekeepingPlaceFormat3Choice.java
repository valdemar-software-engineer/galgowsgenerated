package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de SafekeepingPlaceFormat3Choice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="SafekeepingPlaceFormat3Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SafekeepingPlaceTypeAndText3"/&gt;
 *           &lt;element name="Ctry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}CountryCode"/&gt;
 *           &lt;element name="TpAndId" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SafekeepingPlaceTypeAndAnyBICIdentifier1"/&gt;
 *           &lt;element name="Prtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification21"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SafekeepingPlaceFormat3Choice", propOrder = {"id", "ctry", "tpAndId", "prtry"})
public class SafekeepingPlaceFormat3Choice {

    @XmlElement(name = "Id") protected SafekeepingPlaceTypeAndText3 id;
    @XmlElement(name = "Ctry") protected String ctry;
    @XmlElement(name = "TpAndId") protected SafekeepingPlaceTypeAndAnyBICIdentifier1 tpAndId;
    @XmlElement(name = "Prtry") protected GenericIdentification21 prtry;

    /**
     * Obtém o valor da propriedade ctry.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCtry() {
        return ctry;
    }

    /**
     * Define o valor da propriedade ctry.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCtry(String value) {
        this.ctry = value;
    }

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link SafekeepingPlaceTypeAndText3 }
     */
    public SafekeepingPlaceTypeAndText3 getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link SafekeepingPlaceTypeAndText3 }
     */
    public void setId(SafekeepingPlaceTypeAndText3 value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade prtry.
     *
     * @return possible object is
     * {@link GenericIdentification21 }
     */
    public GenericIdentification21 getPrtry() {
        return prtry;
    }

    /**
     * Define o valor da propriedade prtry.
     *
     * @param value allowed object is
     *              {@link GenericIdentification21 }
     */
    public void setPrtry(GenericIdentification21 value) {
        this.prtry = value;
    }

    /**
     * Obtém o valor da propriedade tpAndId.
     *
     * @return possible object is
     * {@link SafekeepingPlaceTypeAndAnyBICIdentifier1 }
     */
    public SafekeepingPlaceTypeAndAnyBICIdentifier1 getTpAndId() {
        return tpAndId;
    }

    /**
     * Define o valor da propriedade tpAndId.
     *
     * @param value allowed object is
     *              {@link SafekeepingPlaceTypeAndAnyBICIdentifier1 }
     */
    public void setTpAndId(SafekeepingPlaceTypeAndAnyBICIdentifier1 value) {
        this.tpAndId = value;
    }

}
