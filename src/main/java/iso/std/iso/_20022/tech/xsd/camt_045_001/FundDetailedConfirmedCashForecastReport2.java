package iso.std.iso._20022.tech.xsd.camt_045_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de FundDetailedConfirmedCashForecastReport2 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="FundDetailedConfirmedCashForecastReport2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FndCshFcstDtls" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}FundCashForecast4" maxOccurs="unbounded"/>
 *         &lt;element name="CnsltdNetCshFcst" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}NetCashForecast3" minOccurs="0"/>
 *         &lt;element name="Xtnsn" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}Extension1" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FundDetailedConfirmedCashForecastReport2", propOrder = {"fndCshFcstDtls", "cnsltdNetCshFcst", "xtnsn"})
public class FundDetailedConfirmedCashForecastReport2 {

    @XmlElement(name = "FndCshFcstDtls", required = true) protected List<FundCashForecast4> fndCshFcstDtls;
    @XmlElement(name = "CnsltdNetCshFcst") protected NetCashForecast3 cnsltdNetCshFcst;
    @XmlElement(name = "Xtnsn") protected List<Extension1> xtnsn;

    /**
     * Obtém o valor da propriedade cnsltdNetCshFcst.
     *
     * @return possible object is
     * {@link NetCashForecast3 }
     */
    public NetCashForecast3 getCnsltdNetCshFcst() {
        return cnsltdNetCshFcst;
    }

    /**
     * Define o valor da propriedade cnsltdNetCshFcst.
     *
     * @param value allowed object is
     *              {@link NetCashForecast3 }
     */
    public void setCnsltdNetCshFcst(NetCashForecast3 value) {
        this.cnsltdNetCshFcst = value;
    }

    /**
     * Gets the value of the fndCshFcstDtls property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fndCshFcstDtls property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFndCshFcstDtls().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FundCashForecast4 }
     */
    public List<FundCashForecast4> getFndCshFcstDtls() {
        if (fndCshFcstDtls == null) {
            fndCshFcstDtls = new ArrayList<FundCashForecast4>();
        }
        return this.fndCshFcstDtls;
    }

    /**
     * Gets the value of the xtnsn property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the xtnsn property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getXtnsn().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Extension1 }
     */
    public List<Extension1> getXtnsn() {
        if (xtnsn == null) {
            xtnsn = new ArrayList<Extension1>();
        }
        return this.xtnsn;
    }

}
