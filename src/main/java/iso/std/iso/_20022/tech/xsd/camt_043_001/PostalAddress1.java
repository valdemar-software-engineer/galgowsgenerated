package iso.std.iso._20022.tech.xsd.camt_043_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de PostalAddress1 complex type.
 *
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 *
 * <pre>
 * &lt;complexType name="PostalAddress1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AdrTp" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}AddressType2Code" minOccurs="0"/&gt;
 *         &lt;element name="AdrLine" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Max70Text" maxOccurs="5" minOccurs="0"/&gt;
 *         &lt;element name="StrtNm" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Max70Text" minOccurs="0"/&gt;
 *         &lt;element name="BldgNb" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Max16Text" minOccurs="0"/&gt;
 *         &lt;element name="PstCd" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Max16Text" minOccurs="0"/&gt;
 *         &lt;element name="TwnNm" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Max35Text" minOccurs="0"/&gt;
 *         &lt;element name="CtrySubDvsn" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}Max35Text" minOccurs="0"/&gt;
 *         &lt;element name="Ctry" type="{urn:iso:std:iso:20022:tech:xsd:camt.043.001.03}CountryCode"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PostalAddress1", propOrder = {
    "adrTp",
    "adrLine",
    "strtNm",
    "bldgNb",
    "pstCd",
    "twnNm",
    "ctrySubDvsn",
    "ctry"
})
public class PostalAddress1 {

    @XmlElement(name = "AdrTp")
    @XmlSchemaType(name = "string")
    protected AddressType2Code adrTp;
    @XmlElement(name = "AdrLine")
    protected List<String> adrLine;
    @XmlElement(name = "StrtNm")
    protected String strtNm;
    @XmlElement(name = "BldgNb")
    protected String bldgNb;
    @XmlElement(name = "PstCd")
    protected String pstCd;
    @XmlElement(name = "TwnNm")
    protected String twnNm;
    @XmlElement(name = "CtrySubDvsn")
    protected String ctrySubDvsn;
    @XmlElement(name = "Ctry", required = true)
    protected String ctry;

    /**
     * Gets the value of the adrLine property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adrLine property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdrLine().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     *
     *
     */
    public List<String> getAdrLine() {
        if (adrLine == null) {
            adrLine = new ArrayList<String>();
        }
        return this.adrLine;
    }

    /**
     * Obtém o valor da propriedade adrTp.
     *
     * @return
     *     possible object is
     *     {@link AddressType2Code }
     *
     */
    public AddressType2Code getAdrTp() {
        return adrTp;
    }

    /**
     * Define o valor da propriedade adrTp.
     *
     * @param value
     *     allowed object is
     *     {@link AddressType2Code }
     *
     */
    public void setAdrTp(AddressType2Code value) {
        this.adrTp = value;
    }

    /**
     * Obtém o valor da propriedade bldgNb.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBldgNb() {
        return bldgNb;
    }

    /**
     * Define o valor da propriedade bldgNb.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBldgNb(String value) {
        this.bldgNb = value;
    }

    /**
     * Obtém o valor da propriedade ctry.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCtry() {
        return ctry;
    }

    /**
     * Define o valor da propriedade ctry.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCtry(String value) {
        this.ctry = value;
    }

    /**
     * Obtém o valor da propriedade ctrySubDvsn.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCtrySubDvsn() {
        return ctrySubDvsn;
    }

    /**
     * Define o valor da propriedade ctrySubDvsn.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCtrySubDvsn(String value) {
        this.ctrySubDvsn = value;
    }

    /**
     * Obtém o valor da propriedade pstCd.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPstCd() {
        return pstCd;
    }

    /**
     * Define o valor da propriedade pstCd.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPstCd(String value) {
        this.pstCd = value;
    }

    /**
     * Obtém o valor da propriedade strtNm.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStrtNm() {
        return strtNm;
    }

    /**
     * Define o valor da propriedade strtNm.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStrtNm(String value) {
        this.strtNm = value;
    }

    /**
     * Obtém o valor da propriedade twnNm.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTwnNm() {
        return twnNm;
    }

    /**
     * Define o valor da propriedade twnNm.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTwnNm(String value) {
        this.twnNm = value;
    }

}
