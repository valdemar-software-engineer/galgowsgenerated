package iso.std.iso._20022.tech.xsd.camt_045_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de DistributionPolicy1Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="DistributionPolicy1Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DIST"/>
 *     &lt;enumeration value="ACCU"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "DistributionPolicy1Code")
@XmlEnum
public enum DistributionPolicy1Code {

    DIST,
    ACCU;

    public static DistributionPolicy1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
