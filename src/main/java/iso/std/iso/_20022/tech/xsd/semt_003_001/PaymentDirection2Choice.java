package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de PaymentDirection2Choice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="PaymentDirection2Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Ind" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PaymentDirectionIndicator"/&gt;
 *           &lt;element name="Prtry" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}GenericIdentification20"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentDirection2Choice", propOrder = {"ind", "prtry"})
public class PaymentDirection2Choice {

    @XmlElement(name = "Ind") protected Boolean ind;
    @XmlElement(name = "Prtry") protected GenericIdentification20 prtry;

    /**
     * Obtém o valor da propriedade prtry.
     *
     * @return possible object is
     * {@link GenericIdentification20 }
     */
    public GenericIdentification20 getPrtry() {
        return prtry;
    }

    /**
     * Define o valor da propriedade prtry.
     *
     * @param value allowed object is
     *              {@link GenericIdentification20 }
     */
    public void setPrtry(GenericIdentification20 value) {
        this.prtry = value;
    }

    /**
     * Obtém o valor da propriedade ind.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isInd() {
        return ind;
    }

    /**
     * Define o valor da propriedade ind.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setInd(Boolean value) {
        this.ind = value;
    }

}
