package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Statement20 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="Statement20"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RptNb" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Number3Choice" minOccurs="0"/&gt;
 *         &lt;element name="QryRef" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
 *         &lt;element name="StmtId" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
 *         &lt;element name="StmtDtTm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}DateAndDateTimeChoice"/&gt;
 *         &lt;element name="Frqcy" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Frequency4Choice"/&gt;
 *         &lt;element name="UpdTp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}UpdateType2Choice"/&gt;
 *         &lt;element name="StmtBsis" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}StatementBasis3Choice"/&gt;
 *         &lt;element name="ActvtyInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator"/&gt;
 *         &lt;element name="AudtdInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator"/&gt;
 *         &lt;element name="SubAcctInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator"/&gt;
 *         &lt;element name="TaxLotInd" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YesNoIndicator" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Statement20",
         propOrder = {"rptNb", "qryRef", "stmtId", "stmtDtTm", "frqcy", "updTp", "stmtBsis", "actvtyInd", "audtdInd", "subAcctInd", "taxLotInd"})
public class Statement20 {

    @XmlElement(name = "RptNb") protected Number3Choice rptNb;
    @XmlElement(name = "QryRef") protected String qryRef;
    @XmlElement(name = "StmtId") protected String stmtId;
    @XmlElement(name = "StmtDtTm", required = true) protected DateAndDateTimeChoice stmtDtTm;
    @XmlElement(name = "Frqcy", required = true) protected Frequency4Choice frqcy;
    @XmlElement(name = "UpdTp", required = true) protected UpdateType2Choice updTp;
    @XmlElement(name = "StmtBsis", required = true) protected StatementBasis3Choice stmtBsis;
    @XmlElement(name = "ActvtyInd") protected boolean actvtyInd;
    @XmlElement(name = "AudtdInd") protected boolean audtdInd;
    @XmlElement(name = "SubAcctInd") protected boolean subAcctInd;
    @XmlElement(name = "TaxLotInd") protected Boolean taxLotInd;

    /**
     * Obtém o valor da propriedade frqcy.
     *
     * @return possible object is
     * {@link Frequency4Choice }
     */
    public Frequency4Choice getFrqcy() {
        return frqcy;
    }

    /**
     * Define o valor da propriedade frqcy.
     *
     * @param value allowed object is
     *              {@link Frequency4Choice }
     */
    public void setFrqcy(Frequency4Choice value) {
        this.frqcy = value;
    }

    /**
     * Obtém o valor da propriedade qryRef.
     *
     * @return possible object is
     * {@link String }
     */
    public String getQryRef() {
        return qryRef;
    }

    /**
     * Define o valor da propriedade qryRef.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setQryRef(String value) {
        this.qryRef = value;
    }

    /**
     * Obtém o valor da propriedade rptNb.
     *
     * @return possible object is
     * {@link Number3Choice }
     */
    public Number3Choice getRptNb() {
        return rptNb;
    }

    /**
     * Define o valor da propriedade rptNb.
     *
     * @param value allowed object is
     *              {@link Number3Choice }
     */
    public void setRptNb(Number3Choice value) {
        this.rptNb = value;
    }

    /**
     * Obtém o valor da propriedade stmtBsis.
     *
     * @return possible object is
     * {@link StatementBasis3Choice }
     */
    public StatementBasis3Choice getStmtBsis() {
        return stmtBsis;
    }

    /**
     * Define o valor da propriedade stmtBsis.
     *
     * @param value allowed object is
     *              {@link StatementBasis3Choice }
     */
    public void setStmtBsis(StatementBasis3Choice value) {
        this.stmtBsis = value;
    }

    /**
     * Obtém o valor da propriedade stmtDtTm.
     *
     * @return possible object is
     * {@link DateAndDateTimeChoice }
     */
    public DateAndDateTimeChoice getStmtDtTm() {
        return stmtDtTm;
    }

    /**
     * Define o valor da propriedade stmtDtTm.
     *
     * @param value allowed object is
     *              {@link DateAndDateTimeChoice }
     */
    public void setStmtDtTm(DateAndDateTimeChoice value) {
        this.stmtDtTm = value;
    }

    /**
     * Obtém o valor da propriedade stmtId.
     *
     * @return possible object is
     * {@link String }
     */
    public String getStmtId() {
        return stmtId;
    }

    /**
     * Define o valor da propriedade stmtId.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setStmtId(String value) {
        this.stmtId = value;
    }

    /**
     * Obtém o valor da propriedade updTp.
     *
     * @return possible object is
     * {@link UpdateType2Choice }
     */
    public UpdateType2Choice getUpdTp() {
        return updTp;
    }

    /**
     * Define o valor da propriedade updTp.
     *
     * @param value allowed object is
     *              {@link UpdateType2Choice }
     */
    public void setUpdTp(UpdateType2Choice value) {
        this.updTp = value;
    }

    /**
     * Obtém o valor da propriedade actvtyInd.
     */
    public boolean isActvtyInd() {
        return actvtyInd;
    }

    /**
     * Define o valor da propriedade actvtyInd.
     */
    public void setActvtyInd(boolean value) {
        this.actvtyInd = value;
    }

    /**
     * Obtém o valor da propriedade audtdInd.
     */
    public boolean isAudtdInd() {
        return audtdInd;
    }

    /**
     * Define o valor da propriedade audtdInd.
     */
    public void setAudtdInd(boolean value) {
        this.audtdInd = value;
    }

    /**
     * Obtém o valor da propriedade subAcctInd.
     */
    public boolean isSubAcctInd() {
        return subAcctInd;
    }

    /**
     * Define o valor da propriedade subAcctInd.
     */
    public void setSubAcctInd(boolean value) {
        this.subAcctInd = value;
    }

    /**
     * Obtém o valor da propriedade taxLotInd.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isTaxLotInd() {
        return taxLotInd;
    }

    /**
     * Define o valor da propriedade taxLotInd.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setTaxLotInd(Boolean value) {
        this.taxLotInd = value;
    }

}
