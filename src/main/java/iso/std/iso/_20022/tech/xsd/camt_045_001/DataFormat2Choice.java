package iso.std.iso._20022.tech.xsd.camt_045_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de DataFormat2Choice complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="DataFormat2Choice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="Strd" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}GenericIdentification1"/>
 *           &lt;element name="Ustrd" type="{urn:iso:std:iso:20022:tech:xsd:camt.045.001.02}Max140Text"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataFormat2Choice", propOrder = {"strd", "ustrd"})
public class DataFormat2Choice {

    @XmlElement(name = "Strd") protected GenericIdentification1 strd;
    @XmlElement(name = "Ustrd") protected String ustrd;

    /**
     * Obtém o valor da propriedade strd.
     *
     * @return possible object is
     * {@link GenericIdentification1 }
     */
    public GenericIdentification1 getStrd() {
        return strd;
    }

    /**
     * Define o valor da propriedade strd.
     *
     * @param value allowed object is
     *              {@link GenericIdentification1 }
     */
    public void setStrd(GenericIdentification1 value) {
        this.strd = value;
    }

    /**
     * Obtém o valor da propriedade ustrd.
     *
     * @return possible object is
     * {@link String }
     */
    public String getUstrd() {
        return ustrd;
    }

    /**
     * Define o valor da propriedade ustrd.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setUstrd(String value) {
        this.ustrd = value;
    }

}
