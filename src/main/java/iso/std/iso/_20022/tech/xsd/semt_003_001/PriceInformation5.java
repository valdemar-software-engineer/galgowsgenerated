package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de PriceInformation5 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="PriceInformation5"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Tp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}TypeOfPrice4Choice"/&gt;
 *         &lt;element name="Val" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PriceRateOrAmountOrUnknownChoice"/&gt;
 *         &lt;element name="ValTp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YieldedOrValueType1Choice"/&gt;
 *         &lt;element name="SrcOfPric" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}MarketIdentification6" minOccurs="0"/&gt;
 *         &lt;element name="QtnDt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}DateAndDateTimeChoice" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceInformation5", propOrder = {"tp", "val", "valTp", "srcOfPric", "qtnDt"})
public class PriceInformation5 {

    @XmlElement(name = "Tp", required = true) protected TypeOfPrice4Choice tp;
    @XmlElement(name = "Val", required = true) protected PriceRateOrAmountOrUnknownChoice val;
    @XmlElement(name = "ValTp", required = true) protected YieldedOrValueType1Choice valTp;
    @XmlElement(name = "SrcOfPric") protected MarketIdentification6 srcOfPric;
    @XmlElement(name = "QtnDt") protected DateAndDateTimeChoice qtnDt;

    /**
     * Obtém o valor da propriedade qtnDt.
     *
     * @return possible object is
     * {@link DateAndDateTimeChoice }
     */
    public DateAndDateTimeChoice getQtnDt() {
        return qtnDt;
    }

    /**
     * Define o valor da propriedade qtnDt.
     *
     * @param value allowed object is
     *              {@link DateAndDateTimeChoice }
     */
    public void setQtnDt(DateAndDateTimeChoice value) {
        this.qtnDt = value;
    }

    /**
     * Obtém o valor da propriedade srcOfPric.
     *
     * @return possible object is
     * {@link MarketIdentification6 }
     */
    public MarketIdentification6 getSrcOfPric() {
        return srcOfPric;
    }

    /**
     * Define o valor da propriedade srcOfPric.
     *
     * @param value allowed object is
     *              {@link MarketIdentification6 }
     */
    public void setSrcOfPric(MarketIdentification6 value) {
        this.srcOfPric = value;
    }

    /**
     * Obtém o valor da propriedade tp.
     *
     * @return possible object is
     * {@link TypeOfPrice4Choice }
     */
    public TypeOfPrice4Choice getTp() {
        return tp;
    }

    /**
     * Define o valor da propriedade tp.
     *
     * @param value allowed object is
     *              {@link TypeOfPrice4Choice }
     */
    public void setTp(TypeOfPrice4Choice value) {
        this.tp = value;
    }

    /**
     * Obtém o valor da propriedade val.
     *
     * @return possible object is
     * {@link PriceRateOrAmountOrUnknownChoice }
     */
    public PriceRateOrAmountOrUnknownChoice getVal() {
        return val;
    }

    /**
     * Define o valor da propriedade val.
     *
     * @param value allowed object is
     *              {@link PriceRateOrAmountOrUnknownChoice }
     */
    public void setVal(PriceRateOrAmountOrUnknownChoice value) {
        this.val = value;
    }

    /**
     * Obtém o valor da propriedade valTp.
     *
     * @return possible object is
     * {@link YieldedOrValueType1Choice }
     */
    public YieldedOrValueType1Choice getValTp() {
        return valTp;
    }

    /**
     * Define o valor da propriedade valTp.
     *
     * @param value allowed object is
     *              {@link YieldedOrValueType1Choice }
     */
    public void setValTp(YieldedOrValueType1Choice value) {
        this.valTp = value;
    }

}
