package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de SecuritiesBalanceAccountingReportV04 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="SecuritiesBalanceAccountingReportV04"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Pgntn" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Pagination"/&gt;
 *         &lt;element name="StmtGnlDtls" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Statement20"/&gt;
 *         &lt;element name="AcctOwnr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PartyIdentification36Choice" minOccurs="0"/&gt;
 *         &lt;element name="AcctSvcr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PartyIdentification49Choice" minOccurs="0"/&gt;
 *         &lt;element name="SfkpgAcct" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SecuritiesAccount11"/&gt;
 *         &lt;element name="IntrmyInf" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Intermediary21" maxOccurs="10" minOccurs="0"/&gt;
 *         &lt;element name="BalForAcct" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}AggregateBalanceInformation13" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SubAcctDtls" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}SubAccountIdentification16" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AcctBaseCcyTtlAmts" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}TotalValueInPageAndStatement2" minOccurs="0"/&gt;
 *         &lt;element name="AltrnRptgCcyTtlAmts" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}TotalValueInPageAndStatement2" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SecuritiesBalanceAccountingReportV04",
         propOrder = {"pgntn", "stmtGnlDtls", "acctOwnr", "acctSvcr", "sfkpgAcct", "intrmyInf", "balForAcct", "subAcctDtls", "acctBaseCcyTtlAmts", "altrnRptgCcyTtlAmts"})
public class SecuritiesBalanceAccountingReportV04 {

    @XmlElement(name = "Pgntn", required = true) protected Pagination pgntn;
    @XmlElement(name = "StmtGnlDtls", required = true) protected Statement20 stmtGnlDtls;
    @XmlElement(name = "AcctOwnr") protected PartyIdentification36Choice acctOwnr;
    @XmlElement(name = "AcctSvcr") protected PartyIdentification49Choice acctSvcr;
    @XmlElement(name = "SfkpgAcct", required = true) protected SecuritiesAccount11 sfkpgAcct;
    @XmlElement(name = "IntrmyInf") protected List<Intermediary21> intrmyInf;
    @XmlElement(name = "BalForAcct") protected List<AggregateBalanceInformation13> balForAcct;
    @XmlElement(name = "SubAcctDtls") protected List<SubAccountIdentification16> subAcctDtls;
    @XmlElement(name = "AcctBaseCcyTtlAmts") protected TotalValueInPageAndStatement2 acctBaseCcyTtlAmts;
    @XmlElement(name = "AltrnRptgCcyTtlAmts") protected TotalValueInPageAndStatement2 altrnRptgCcyTtlAmts;

    /**
     * Obtém o valor da propriedade acctBaseCcyTtlAmts.
     *
     * @return possible object is
     * {@link TotalValueInPageAndStatement2 }
     */
    public TotalValueInPageAndStatement2 getAcctBaseCcyTtlAmts() {
        return acctBaseCcyTtlAmts;
    }

    /**
     * Define o valor da propriedade acctBaseCcyTtlAmts.
     *
     * @param value allowed object is
     *              {@link TotalValueInPageAndStatement2 }
     */
    public void setAcctBaseCcyTtlAmts(TotalValueInPageAndStatement2 value) {
        this.acctBaseCcyTtlAmts = value;
    }

    /**
     * Obtém o valor da propriedade acctOwnr.
     *
     * @return possible object is
     * {@link PartyIdentification36Choice }
     */
    public PartyIdentification36Choice getAcctOwnr() {
        return acctOwnr;
    }

    /**
     * Define o valor da propriedade acctOwnr.
     *
     * @param value allowed object is
     *              {@link PartyIdentification36Choice }
     */
    public void setAcctOwnr(PartyIdentification36Choice value) {
        this.acctOwnr = value;
    }

    /**
     * Obtém o valor da propriedade acctSvcr.
     *
     * @return possible object is
     * {@link PartyIdentification49Choice }
     */
    public PartyIdentification49Choice getAcctSvcr() {
        return acctSvcr;
    }

    /**
     * Define o valor da propriedade acctSvcr.
     *
     * @param value allowed object is
     *              {@link PartyIdentification49Choice }
     */
    public void setAcctSvcr(PartyIdentification49Choice value) {
        this.acctSvcr = value;
    }

    /**
     * Obtém o valor da propriedade altrnRptgCcyTtlAmts.
     *
     * @return possible object is
     * {@link TotalValueInPageAndStatement2 }
     */
    public TotalValueInPageAndStatement2 getAltrnRptgCcyTtlAmts() {
        return altrnRptgCcyTtlAmts;
    }

    /**
     * Define o valor da propriedade altrnRptgCcyTtlAmts.
     *
     * @param value allowed object is
     *              {@link TotalValueInPageAndStatement2 }
     */
    public void setAltrnRptgCcyTtlAmts(TotalValueInPageAndStatement2 value) {
        this.altrnRptgCcyTtlAmts = value;
    }

    /**
     * Gets the value of the balForAcct property.
     * <p/>
     * <p/>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the balForAcct property.
     * <p/>
     * <p/>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBalForAcct().add(newItem);
     * </pre>
     * <p/>
     * <p/>
     * <p/>
     * Objects of the following type(s) are allowed in the list
     * {@link AggregateBalanceInformation13 }
     */
    public List<AggregateBalanceInformation13> getBalForAcct() {
        if (balForAcct == null) {
            balForAcct = new ArrayList<AggregateBalanceInformation13>();
        }
        return this.balForAcct;
    }

    /**
     * Gets the value of the intrmyInf property.
     * <p/>
     * <p/>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the intrmyInf property.
     * <p/>
     * <p/>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntrmyInf().add(newItem);
     * </pre>
     * <p/>
     * <p/>
     * <p/>
     * Objects of the following type(s) are allowed in the list
     * {@link Intermediary21 }
     */
    public List<Intermediary21> getIntrmyInf() {
        if (intrmyInf == null) {
            intrmyInf = new ArrayList<Intermediary21>();
        }
        return this.intrmyInf;
    }

    /**
     * Obtém o valor da propriedade pgntn.
     *
     * @return possible object is
     * {@link Pagination }
     */
    public Pagination getPgntn() {
        return pgntn;
    }

    /**
     * Define o valor da propriedade pgntn.
     *
     * @param value allowed object is
     *              {@link Pagination }
     */
    public void setPgntn(Pagination value) {
        this.pgntn = value;
    }

    /**
     * Obtém o valor da propriedade sfkpgAcct.
     *
     * @return possible object is
     * {@link SecuritiesAccount11 }
     */
    public SecuritiesAccount11 getSfkpgAcct() {
        return sfkpgAcct;
    }

    /**
     * Define o valor da propriedade sfkpgAcct.
     *
     * @param value allowed object is
     *              {@link SecuritiesAccount11 }
     */
    public void setSfkpgAcct(SecuritiesAccount11 value) {
        this.sfkpgAcct = value;
    }

    /**
     * Obtém o valor da propriedade stmtGnlDtls.
     *
     * @return possible object is
     * {@link Statement20 }
     */
    public Statement20 getStmtGnlDtls() {
        return stmtGnlDtls;
    }

    /**
     * Define o valor da propriedade stmtGnlDtls.
     *
     * @param value allowed object is
     *              {@link Statement20 }
     */
    public void setStmtGnlDtls(Statement20 value) {
        this.stmtGnlDtls = value;
    }

    /**
     * Gets the value of the subAcctDtls property.
     * <p/>
     * <p/>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subAcctDtls property.
     * <p/>
     * <p/>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubAcctDtls().add(newItem);
     * </pre>
     * <p/>
     * <p/>
     * <p/>
     * Objects of the following type(s) are allowed in the list
     * {@link SubAccountIdentification16 }
     */
    public List<SubAccountIdentification16> getSubAcctDtls() {
        if (subAcctDtls == null) {
            subAcctDtls = new ArrayList<SubAccountIdentification16>();
        }
        return this.subAcctDtls;
    }

}
