package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Price2 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="Price2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Tp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}YieldedOrValueType1Choice"/&gt;
 *         &lt;element name="Val" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PriceRateOrAmountChoice"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Price2", propOrder = {"tp", "val"})
public class Price2 {

    @XmlElement(name = "Tp", required = true) protected YieldedOrValueType1Choice tp;
    @XmlElement(name = "Val", required = true) protected PriceRateOrAmountChoice val;

    /**
     * Obtém o valor da propriedade tp.
     *
     * @return possible object is
     * {@link YieldedOrValueType1Choice }
     */
    public YieldedOrValueType1Choice getTp() {
        return tp;
    }

    /**
     * Define o valor da propriedade tp.
     *
     * @param value allowed object is
     *              {@link YieldedOrValueType1Choice }
     */
    public void setTp(YieldedOrValueType1Choice value) {
        this.tp = value;
    }

    /**
     * Obtém o valor da propriedade val.
     *
     * @return possible object is
     * {@link PriceRateOrAmountChoice }
     */
    public PriceRateOrAmountChoice getVal() {
        return val;
    }

    /**
     * Define o valor da propriedade val.
     *
     * @param value allowed object is
     *              {@link PriceRateOrAmountChoice }
     */
    public void setVal(PriceRateOrAmountChoice value) {
        this.val = value;
    }

}
