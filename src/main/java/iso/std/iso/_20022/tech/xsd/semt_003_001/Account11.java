package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Account11 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="Account11"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}AccountIdentification1"/&gt;
 *         &lt;element name="AcctSvcr" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PartyIdentification49Choice" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Account11", propOrder = {"id", "acctSvcr"})
public class Account11 {

    @XmlElement(name = "Id", required = true) protected AccountIdentification1 id;
    @XmlElement(name = "AcctSvcr") protected PartyIdentification49Choice acctSvcr;

    /**
     * Obtém o valor da propriedade acctSvcr.
     *
     * @return possible object is
     * {@link PartyIdentification49Choice }
     */
    public PartyIdentification49Choice getAcctSvcr() {
        return acctSvcr;
    }

    /**
     * Define o valor da propriedade acctSvcr.
     *
     * @param value allowed object is
     *              {@link PartyIdentification49Choice }
     */
    public void setAcctSvcr(PartyIdentification49Choice value) {
        this.acctSvcr = value;
    }

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link AccountIdentification1 }
     */
    public AccountIdentification1 getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link AccountIdentification1 }
     */
    public void setId(AccountIdentification1 value) {
        this.id = value;
    }

}
