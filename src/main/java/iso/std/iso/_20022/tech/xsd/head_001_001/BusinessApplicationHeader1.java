package iso.std.iso._20022.tech.xsd.head_001_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de BusinessApplicationHeader1 complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="BusinessApplicationHeader1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CharSet" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}UnicodeChartsCode" minOccurs="0"/&gt;
 *         &lt;element name="Fr" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}Party9Choice"/&gt;
 *         &lt;element name="To" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}Party9Choice"/&gt;
 *         &lt;element name="BizMsgIdr" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}Max35Text"/&gt;
 *         &lt;element name="MsgDefIdr" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}Max35Text"/&gt;
 *         &lt;element name="BizSvc" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}Max35Text" minOccurs="0"/&gt;
 *         &lt;element name="CreDt" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}ISONormalisedDateTime"/&gt;
 *         &lt;element name="CpyDplct" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}CopyDuplicate1Code" minOccurs="0"/&gt;
 *         &lt;element name="PssblDplct" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}YesNoIndicator" minOccurs="0"/&gt;
 *         &lt;element name="Prty" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}BusinessMessagePriorityCode" minOccurs="0"/&gt;
 *         &lt;element name="Sgntr" type="{urn:iso:std:iso:20022:tech:xsd:head.001.001.01}SignatureEnvelope" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessApplicationHeader1",
         propOrder = {"charSet", "fr", "to", "bizMsgIdr", "msgDefIdr", "bizSvc", "creDt", "cpyDplct", "pssblDplct", "prty", "sgntr"})
public class BusinessApplicationHeader1 {

    @XmlElement(name = "CharSet") protected String charSet;
    @XmlElement(name = "Fr", required = true) protected Party9Choice fr;
    @XmlElement(name = "To", required = true) protected Party9Choice to;
    @XmlElement(name = "BizMsgIdr", required = true) protected String bizMsgIdr;
    @XmlElement(name = "MsgDefIdr", required = true) protected String msgDefIdr;
    @XmlElement(name = "BizSvc") protected String bizSvc;
    @XmlElement(name = "CreDt", required = true) @XmlSchemaType(name = "dateTime") protected XMLGregorianCalendar creDt;
    @XmlElement(name = "CpyDplct") @XmlSchemaType(name = "string") protected CopyDuplicate1Code cpyDplct;
    @XmlElement(name = "PssblDplct") protected Boolean pssblDplct;
    @XmlElement(name = "Prty") protected String prty;
    @XmlElement(name = "Sgntr") protected SignatureEnvelope sgntr;

    /**
     * Obtém o valor da propriedade bizMsgIdr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBizMsgIdr() {
        return bizMsgIdr;
    }

    /**
     * Define o valor da propriedade bizMsgIdr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBizMsgIdr(String value) {
        this.bizMsgIdr = value;
    }

    /**
     * Obtém o valor da propriedade bizSvc.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBizSvc() {
        return bizSvc;
    }

    /**
     * Define o valor da propriedade bizSvc.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBizSvc(String value) {
        this.bizSvc = value;
    }

    /**
     * Obtém o valor da propriedade charSet.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCharSet() {
        return charSet;
    }

    /**
     * Define o valor da propriedade charSet.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCharSet(String value) {
        this.charSet = value;
    }

    /**
     * Obtém o valor da propriedade cpyDplct.
     *
     * @return possible object is
     * {@link CopyDuplicate1Code }
     */
    public CopyDuplicate1Code getCpyDplct() {
        return cpyDplct;
    }

    /**
     * Define o valor da propriedade cpyDplct.
     *
     * @param value allowed object is
     *              {@link CopyDuplicate1Code }
     */
    public void setCpyDplct(CopyDuplicate1Code value) {
        this.cpyDplct = value;
    }

    /**
     * Obtém o valor da propriedade creDt.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getCreDt() {
        return creDt;
    }

    /**
     * Define o valor da propriedade creDt.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setCreDt(XMLGregorianCalendar value) {
        this.creDt = value;
    }

    /**
     * Obtém o valor da propriedade fr.
     *
     * @return possible object is
     * {@link Party9Choice }
     */
    public Party9Choice getFr() {
        return fr;
    }

    /**
     * Define o valor da propriedade fr.
     *
     * @param value allowed object is
     *              {@link Party9Choice }
     */
    public void setFr(Party9Choice value) {
        this.fr = value;
    }

    /**
     * Obtém o valor da propriedade msgDefIdr.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMsgDefIdr() {
        return msgDefIdr;
    }

    /**
     * Define o valor da propriedade msgDefIdr.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMsgDefIdr(String value) {
        this.msgDefIdr = value;
    }

    /**
     * Obtém o valor da propriedade prty.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPrty() {
        return prty;
    }

    /**
     * Define o valor da propriedade prty.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPrty(String value) {
        this.prty = value;
    }

    /**
     * Obtém o valor da propriedade sgntr.
     *
     * @return possible object is
     * {@link SignatureEnvelope }
     */
    public SignatureEnvelope getSgntr() {
        return sgntr;
    }

    /**
     * Define o valor da propriedade sgntr.
     *
     * @param value allowed object is
     *              {@link SignatureEnvelope }
     */
    public void setSgntr(SignatureEnvelope value) {
        this.sgntr = value;
    }

    /**
     * Obtém o valor da propriedade to.
     *
     * @return possible object is
     * {@link Party9Choice }
     */
    public Party9Choice getTo() {
        return to;
    }

    /**
     * Define o valor da propriedade to.
     *
     * @param value allowed object is
     *              {@link Party9Choice }
     */
    public void setTo(Party9Choice value) {
        this.to = value;
    }

    /**
     * Obtém o valor da propriedade pssblDplct.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isPssblDplct() {
        return pssblDplct;
    }

    /**
     * Define o valor da propriedade pssblDplct.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setPssblDplct(Boolean value) {
        this.pssblDplct = value;
    }

}
