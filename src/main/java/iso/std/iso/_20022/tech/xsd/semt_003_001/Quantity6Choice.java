package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Quantity6Choice complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="Quantity6Choice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Qty" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}FinancialInstrumentQuantity1Choice"/&gt;
 *           &lt;element name="OrgnlAndCurFace" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}OriginalAndCurrentQuantities1"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Quantity6Choice", propOrder = {"qty", "orgnlAndCurFace"})
public class Quantity6Choice {

    @XmlElement(name = "Qty") protected FinancialInstrumentQuantity1Choice qty;
    @XmlElement(name = "OrgnlAndCurFace") protected OriginalAndCurrentQuantities1 orgnlAndCurFace;

    /**
     * Obtém o valor da propriedade orgnlAndCurFace.
     *
     * @return possible object is
     * {@link OriginalAndCurrentQuantities1 }
     */
    public OriginalAndCurrentQuantities1 getOrgnlAndCurFace() {
        return orgnlAndCurFace;
    }

    /**
     * Define o valor da propriedade orgnlAndCurFace.
     *
     * @param value allowed object is
     *              {@link OriginalAndCurrentQuantities1 }
     */
    public void setOrgnlAndCurFace(OriginalAndCurrentQuantities1 value) {
        this.orgnlAndCurFace = value;
    }

    /**
     * Obtém o valor da propriedade qty.
     *
     * @return possible object is
     * {@link FinancialInstrumentQuantity1Choice }
     */
    public FinancialInstrumentQuantity1Choice getQty() {
        return qty;
    }

    /**
     * Define o valor da propriedade qty.
     *
     * @param value allowed object is
     *              {@link FinancialInstrumentQuantity1Choice }
     */
    public void setQty(FinancialInstrumentQuantity1Choice value) {
        this.qty = value;
    }

}
