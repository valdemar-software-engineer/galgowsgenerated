package iso.std.iso._20022.tech.xsd.reda_002_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java de DateTimePeriodDetails complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="DateTimePeriodDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FrDtTm" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}ISODateTime"/&gt;
 *         &lt;element name="ToDtTm" type="{urn:iso:std:iso:20022:tech:xsd:reda.002.001.03}ISODateTime"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DateTimePeriodDetails", propOrder = {"frDtTm", "toDtTm"})
public class DateTimePeriodDetails {

    @XmlElement(name = "FrDtTm", required = true) @XmlSchemaType(name = "dateTime") protected XMLGregorianCalendar
        frDtTm;
    @XmlElement(name = "ToDtTm", required = true) @XmlSchemaType(name = "dateTime") protected XMLGregorianCalendar
        toDtTm;

    /**
     * Obtém o valor da propriedade frDtTm.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getFrDtTm() {
        return frDtTm;
    }

    /**
     * Define o valor da propriedade frDtTm.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setFrDtTm(XMLGregorianCalendar value) {
        this.frDtTm = value;
    }

    /**
     * Obtém o valor da propriedade toDtTm.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getToDtTm() {
        return toDtTm;
    }

    /**
     * Define o valor da propriedade toDtTm.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setToDtTm(XMLGregorianCalendar value) {
        this.toDtTm = value;
    }

}
