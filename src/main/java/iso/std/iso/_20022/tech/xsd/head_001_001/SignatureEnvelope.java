package iso.std.iso._20022.tech.xsd.head_001_001;

import org.w3c.dom.Element;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de SignatureEnvelope complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="SignatureEnvelope"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;any processContents='lax' namespace='http://www.w3.org/2000/09/xmldsig#'/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SignatureEnvelope", propOrder = {"any"})
public class SignatureEnvelope {

    @XmlAnyElement(lax = true) protected Object any;

    /**
     * Obtém o valor da propriedade any.
     *
     * @return possible object is
     * {@link Element }
     * {@link Object }
     */
    public Object getAny() {
        return any;
    }

    /**
     * Define o valor da propriedade any.
     *
     * @param value allowed object is
     *              {@link Element }
     *              {@link Object }
     */
    public void setAny(Object value) {
        this.any = value;
    }

}
