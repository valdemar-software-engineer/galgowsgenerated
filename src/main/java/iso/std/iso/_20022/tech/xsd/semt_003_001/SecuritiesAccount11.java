package iso.std.iso._20022.tech.xsd.semt_003_001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de SecuritiesAccount11 complex type.
 * <p/>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p/>
 * <pre>
 * &lt;complexType name="SecuritiesAccount11"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text"/&gt;
 *         &lt;element name="Tp" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}PurposeCode1Choice" minOccurs="0"/&gt;
 *         &lt;element name="Nm" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max70Text" minOccurs="0"/&gt;
 *         &lt;element name="Dsgnt" type="{urn:iso:std:iso:20022:tech:xsd:semt.003.001.04}Max35Text" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SecuritiesAccount11", propOrder = {"id", "tp", "nm", "dsgnt"})
public class SecuritiesAccount11 {

    @XmlElement(name = "Id", required = true) protected String id;
    @XmlElement(name = "Tp") protected PurposeCode1Choice tp;
    @XmlElement(name = "Nm") protected String nm;
    @XmlElement(name = "Dsgnt") protected String dsgnt;

    /**
     * Obtém o valor da propriedade dsgnt.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDsgnt() {
        return dsgnt;
    }

    /**
     * Define o valor da propriedade dsgnt.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDsgnt(String value) {
        this.dsgnt = value;
    }

    /**
     * Obtém o valor da propriedade id.
     *
     * @return possible object is
     * {@link String }
     */
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade nm.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNm() {
        return nm;
    }

    /**
     * Define o valor da propriedade nm.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNm(String value) {
        this.nm = value;
    }

    /**
     * Obtém o valor da propriedade tp.
     *
     * @return possible object is
     * {@link PurposeCode1Choice }
     */
    public PurposeCode1Choice getTp() {
        return tp;
    }

    /**
     * Define o valor da propriedade tp.
     *
     * @param value allowed object is
     *              {@link PurposeCode1Choice }
     */
    public void setTp(PurposeCode1Choice value) {
        this.tp = value;
    }

}
