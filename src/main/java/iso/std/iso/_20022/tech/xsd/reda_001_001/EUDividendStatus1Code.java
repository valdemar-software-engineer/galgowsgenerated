package iso.std.iso._20022.tech.xsd.reda_001_001;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de EUDividendStatus1Code.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="EUDividendStatus1Code"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="DIVI"/&gt;
 *     &lt;enumeration value="DIVO"/&gt;
 *     &lt;enumeration value="UKWN"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "EUDividendStatus1Code")
@XmlEnum
public enum EUDividendStatus1Code {

    DIVI,
    DIVO,
    UKWN;

    public static EUDividendStatus1Code fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
