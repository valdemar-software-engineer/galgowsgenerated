package org.w3._2005._05.xmlmime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java de hexBinary complex type.
 * <p>
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;complexType name="hexBinary">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>hexBinary">
 *       &lt;attribute ref="{http://www.w3.org/2005/05/xmlmime}contentType"/>
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hexBinary", propOrder = {"value"})
public class HexBinary {

    @XmlValue @XmlJavaTypeAdapter(HexBinaryAdapter.class) @XmlSchemaType(name = "hexBinary") protected byte[] value;
    @XmlAttribute(name = "contentType", namespace = "http://www.w3.org/2005/05/xmlmime") protected String contentType;

    /**
     * Obtém o valor da propriedade contentType.
     *
     * @return possible object is
     * {@link String }
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Define o valor da propriedade contentType.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setContentType(String value) {
        this.contentType = value;
    }

    /**
     * Obtém o valor da propriedade value.
     *
     * @return possible object is
     * {@link String }
     */
    public byte[] getValue() {
        return value;
    }

    /**
     * Define o valor da propriedade value.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setValue(byte[] value) {
        this.value = value;
    }

}
